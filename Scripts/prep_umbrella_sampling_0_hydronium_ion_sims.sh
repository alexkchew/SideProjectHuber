#!/bin/bash

# prep_umbrella_sampling_0_hydronium_ion_sims.sh
# The purpose of this script is to prepare umbrella sampling simulations for the solvent effects project. This script was originally designed to study the binding energy of hydronium ion to the reactant. Although, it is written to be generalized for reactant-reactant binding, reactant-product binding, etc. (We will see!)

## NOTES:
#   - We define solute 1 as the center of our system
#   - We define solute 2 as the one that is being pulled towards solute 1

# Written by: Alex K. Chew (alexkchew@gmail.com, 08/02/2018)

## ALGORITHM:
#   - Use previous mixed-solvent environment that is well-mixed to generate reactant-hydronium ion system configuration
#   - Pull the solute to the hydronium ion to a specific separation distance
#   - Equilibrate the solute-hydronium ion complex with the distance fixed
#   - Push this script to another script, which will set up procedures for umbrella sampling simulations

## USAGE: bash prep_umbrella_sampling.sh

# *** UPDATES *** #
# 20180802 - Created the script
# 20180815 - Generated nvt and npt equilibration
# 20181008 - Inclusion of chlorine ion into the system

### RELOADING GLOBAL VARS AND FUNCTIONS
source "bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING LOGICALS
want_chlorine_ion="True" # True if you want chlorine ion within the system

## DEFINING NAME OF CHLORINE
counter_ion_name="chlorine"
counter_ion_res_name="CLL"
vdwradii_file="vdwradii.dat" # VDW radii file

## NAME OF SOLUTE 2
solute_2_name="hydronium_LINCS_3"  #2 "hydronium" # 
solute_2_res_name="HYD"

## DEFINE DESIRED BOX LENGTH
box_length="6" # "6" # 

## DEFINING DESIRED SOLVENT NAMES
# solvent_water_mass_fraction="1.00" # Mass fraction of water
solvent_water_mass_fraction="0.00" # Mass fraction of water
cosolvent_name="dmso" #"dioxane" # Name of the cosolvent
cosolvent_res_name="dmso" # "DIO" # Residue name of the cosolvent
# cosolvent_name="dmso" # Name of the cosolvent
# cosolvent_res_name="dmso" # Residue name of the cosolvent

## TEMPERATURE TO RUN THIS EQUILIBRIUM STEP
temperature="300.00" # Kelvins

## WATER MODEL
water_model="spce"

## DEFINING FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## DEFINING INSERTION USED FOR EXPANSION
# expand_insert_dist="0.00" # "0.3" # nms, used for expansion, insertion, then allow NPT to take over
expand_insert_dist="0.01" # "0.3" # nms, used for expansion, insertion, then allow NPT to take over

########################
### OUTPUT VARIABLES ###
########################
## DEFINING OUTPUT DIRECTORY
output_dir=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${solute_2_res_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 

## OUTPUT FILES
output_gro="mixed_solv.gro"
output_top="mixed_solv.top"

##########################
### DEFINING MDP FILES ###
##########################

## DEFINING MDP PATH
mdp_file_path="${MDP_DIR}/umbrella_sampling_hyd"

## DEFINING MDP FILES
em_mdp="minim_LIQ.mdp" # "em.mdp"                         # Energy minimization mdp file
em_mdp="em.mdp"
equil_npt_mdp="initial_equil_npt.mdp"   # Initial npt equilibration

## INPUTS FOR MDP FILE
equil_npt_n_steps="1000000"     # 1 ns NPT equilibration (Note! This uses a 0.001 time step)

################################
### DEFINING SUBMISSION FILE ###
################################

## DEFINING SUBMISSION FILE
submit_input_file_name="submit_prep_umbrella_sampling_0_hydronium_ion_sims.sh"
submit_output_file_name="submit.sh"
submit_file_path="${SUBMISSION_DIR}/${submit_input_file_name}"

#####################################
### FINDING PREPARATION DIRECTORY ###
#####################################

## FINDING PREPARATION DIRECTORY
prep_dir_name="$(solvent_effects_find_prep_solvent_folder "${solvent_water_mass_fraction}" "${cosolvent_res_name}")"

## DEFINING FULL PATH TO DIRECTORY
path2prepdir="${PREP_MIXED_COSOLVENTS}/${water_model}/${box_length}nm/${prep_dir_name}"

## CHECKING IF PREPARATION DIRECTORY EXISTS
if [ ! -e "${path2prepdir}" ]; then
    echo "Error!!! $path2prepdir does not exist"
    echo "Make sure to run preparation of co_solvents first!!! (prep_cosolvent.sh)"
    echo "Stopping here....."
    exit
fi

## DEFINING PREPARATION FILES
# GRO Files
prep_gro_file_name="mixed_solv_equil.gro"
prep_gro_file_path="${path2prepdir}/${prep_gro_file_name}"
# TOP Files
prep_top_file_name="mixed_solv.top" # Testing purposes
prep_top_file_path="${path2prepdir}/${prep_top_file_name}"

## CHECKING IF GRO AND TOP FILE EXISTS
if [[ ! -e "$prep_gro_file_path" || ! -e "$prep_top_file_path" ]]; then
    echo "Error, no GRO file or top preparation file!"
    echo "Check gro: ${prep_gro_file_path}"
    echo "Check top: ${prep_top_file_path}"
    echo "Exiting now...."
    exit
fi

###############################################
### DEFINING SIMULATION WORKING DIRECTORIES ###
###############################################
## DEFINING WORKING DIRECTORY
if [ "${want_chlorine_ion}" == "True" ]; then
    WorkSpace="${PREP_HYD_SIMS_HCL}"
else
    WorkSpace="${PREP_HYD_SIMS}"
fi

# Checking if working directory exists
if [ ! -e ${WorkSpace} ]; then # Checking if solute exists
    mkdir -p ${WorkSpace}
fi

## DEFINING SIMULATION DIRECTORY
Simulation_dir="${output_dir}" # Directory where all the runs will take place

## DEFINING FULL PATH
path2Sim_dir=${WorkSpace}/${Simulation_dir} # Defining path structure

## CREATING SIM DIRECTORY
create_dir "${path2Sim_dir}" -f

## GOING INTO THE DIRECTORY
cd "${path2Sim_dir}"

##########################################
### COPYING MIXED-SOLVENT ENVIRONMENTS ###
##########################################
echo -e "\n***COPYING OVER GRO AND TOP FILE FROM: ${prep_gro_file_path}***"
# Gro file
cp -r ${prep_gro_file_path} "${path2Sim_dir}/${output_gro}"
# Top file
cp -r ${prep_top_file_path} "${path2Sim_dir}/${output_top}"
# ITP FILE
cp -r ${path2prepdir}/{*.itp,*.prm}  "${path2Sim_dir}"

if [ "${want_chlorine_ion}" == "True" ]; then
    ## COPYING OVER VDW RADII
    cp -r "${mdp_file_path}/${vdwradii_file}" "${path2Sim_dir}/${vdwradii_file}"
fi
#####################
### ADDING SOLUTE ###
#####################

## DEFINING GRO FILE OF INSERTED
output_insert_gro=${output_gro%.gro}_inserted.gro

## DEFINING NUMBER OF SOLUTES TO ADD (DEFAULTS)
solute_2_num_to_add="1"

## ADDING SOLUTE 2 (No expansion)
solvent_effects_add_solute "${solute_2_name}" "${solute_2_res_name}" "${solute_2_num_to_add}" "${output_gro}" "${output_insert_gro}" "0" "${output_top}"

if [ "${want_chlorine_ion}" == "True" ]; then
    solvent_effects_add_solute "${counter_ion_name}" "${counter_ion_res_name}" "${solute_2_num_to_add}" "${output_insert_gro}" "${output_insert_gro}" "0" "${output_top}"
fi


## CLEANING UP DIRECTORY FROM COPIES
rm \#*
## CREATING SETUP FILES DIRECTORY
setup_files_dir="setup_files"
mkdir ${setup_files_dir}
## MOVING GRO FILES TO SETUP FILES
mv *.gro ${setup_files_dir}
cp -r ${setup_files_dir}/${output_insert_gro} ./${output_gro}

###############################
### COPYING ALL INPUT FILES ###
###############################
## FORCE FIELD FILES
echo -e "\n*** COPYING OVER FORCE FIELD (${forcefield_dir}) ***"
cp -r "${INPUT_DIR}/${forcefield_dir}" ./

## ITP AND PRM
echo -e "\n*** COPYING OVER INPUT FILES (GRO, ITP, ETC.) ***"
cp -rv "${PREP_SOLUTE_DIR}/${solute_2_name}/"{*.itp,*.prm} ./

## COPYING OVER COUNTER ION (if desired)
if [ "${want_chlorine_ion}" == "True" ]; then
    cp -rv "${PREP_SOLUTE_DIR}/${counter_ion_name}/"{*.itp,*.prm} ./
fi

## MDP FILE
echo -e "\n*** COPYING OVER MDP FILES ***"
cp -rv "${mdp_file_path}/"{${em_mdp},${equil_npt_mdp}} ./

## SUBMISSION FILE
echo -e "\n*** COPYING OVER SUBMISSION FILES ***"
cp -rv "${submit_file_path}" ./"${submit_output_file_name}"

###########################
### EDITING INPUT FILES ###
########################### ## FIX PARAMS FILES

## ADJUSTING EQUILIBRATION MDP FOR NPT EQUILIBRATION
sed -i "s/NSTEPS/${equil_npt_n_steps}/g" ${equil_npt_mdp}
sed -i "s/TEMPERATURE/${temperature}/g" ${equil_npt_mdp}

## ADJUSTING SUBMISSION FILE
sed -i "s/DIRECTORY/${Simulation_dir}/g" ${submit_output_file_name}
sed -i "s/NVT_EQUIL_MDP/${equil_nvt_mdp}/g" ${submit_output_file_name}
sed -i "s/NPT_EQUIL_MDP/${equil_npt_mdp}/g" ${submit_output_file_name}

#############################################
### ADDING PARAMETERS AND ITP TO TOPOLOGY ###
#############################################
echo -e "\n*** EDITING TOPOLOGY FILE TO INCLUDE ITP AND PRMS ***"

## NOTE: We may run into errors in the future if the cosolvent parameter and itp files are not available. We may want to consider just generalized itp/prm files, somehow!

## DEFINING STRING TO LOOK FOR
if [[ -z "$(grep -nE "${cosolvent_name}.itp" "${output_top}")"  ]]; then
    topology_string_to_add_itp="forcefield.itp"
    topology_string_to_add_prm="forcefield.itp"
else
    topology_string_to_add_itp="${cosolvent_name}.itp"
    topology_string_to_add_prm="${cosolvent_name}.prm"
fi

## ADDING COUNTERION IF NECESSARY TO TOPOLOGY
if [ "${want_chlorine_ion}" == "True" ]; then
    topology_add_include_specific "${output_top}" "${topology_string_to_add_itp}" "#include \"${counter_ion_name}.itp\""
fi

## ITP FILES
topology_add_include_specific "${output_top}" "${topology_string_to_add_itp}" "#include \"${solute_2_name}.itp\""

## ADDING COUNTERION IF NECESSARY TO TOPOLOGY
if [ "${want_chlorine_ion}" == "True" ]; then
    topology_add_include_specific "${output_top}" "${topology_string_to_add_prm}" "#include \"${counter_ion_name}.prm\""
fi

## PARAMETER FILES
topology_add_include_specific "${output_top}" "${topology_string_to_add_prm}" "#include \"${solute_2_name}.prm\""


###########################
### ENERGY MINIMIZATION ###
###########################
echo -e "\n*** ENERGY MINIMIZING ***"
## CREATING GROMPP FILE
gmx grompp -f ${em_mdp} -c ${output_gro} -p ${output_top} -o "${output_gro%.gro}"_em.tpr 
## RUNNING ENERGY MINIMIZATION
gmx mdrun -nt 1 -v -deffnm "${output_gro%.gro}"_em

#########################
### UPDATING JOB LIST ###
#########################
## UPDATING DIRECTORY FOR JOB LIST
echo "${path2Sim_dir}" >> ${JOB_FILE}

############################ PRINTING SUMMARY ############################
echo -e "\n************* SUMMARY *************"
echo "Generated initial hydronium ion in mixed-solvent systems simulations for the following:"
echo "- HYDRONIUM_ION: ${solute_2_name} (${solute_2_res_name})" 
echo "- BOX LENGTH: ${box_length}"
echo "- MASS FRACTION OF WATER: ${solvent_water_mass_fraction}"
echo "- COSOLVENT: ${cosolvent_name}"
echo "- WORKING DIR: ${path2Sim_dir}"
