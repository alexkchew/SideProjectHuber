#!/bin/bash

# One_System_Prep_cosolvents.sh
# Written by Alex Chew 05/11/2017
# The purpose of this script is to take your cosolvent mixture, add a molecule, and run equilibrium + production run. 
# Usage: bash One_System_Prep_cosolvents.sh tBuOH tBuOH 363.15 0.5 GVL_L GVLL 6 spce
# $1 is the name of the solute
# $2 is the system temperature
# $3 is the weight fraction of water
# $4 is the name of the co-solvent
# $5 is the desired box length
# $6 is Water model (tip3p, spce, spc, etc.)
# $7 is Simulation directory

# NOTES:
#   - To get no solute, simply put "None" as the solute name (You can also put for residue name, should not change results)

##* -- Updates -- *##
# 20170517: Updated to include 100% Water weight fraction
# 20170601: Added hydronium ion addition and availability to change the desired water model (originally tip3p)
# 20170613: Added ability to get 0% water
# 20171017: Temporarily added 100 ns vs 200 ns
# 20180509: Added variable for simulation directory
# 20180604: Double-checking preparation script for "NoSolute"
# 20191004: Added residue names and removing residue name variable

### Functions ###

## RELOADING BIN FILE
source "bin/bash_rc.sh"

#######################
### INPUT VARIABLES ###
#######################
solute_name="$1" # "sorbitol"
# solute_residue_name="$2" #"SOR"
system_temp="$2" #363.15 # System temperature, edited in mdp file labelled "TEMPERATURE"
solvent_one_wt_frac="$3" #1.0 # fraction of first solvent (water)

# Defining co-solvent
solvent_two_name="$4" #dioxane # Co-solvent molecule name   # GVL_L, dioxane, tetrahydrofuran
# solvent_two_residue_name="$5" #DIO # Cosolvent residue name # GVLL, DIO, THF

# Defining desired box length
desired_box_length="$5"  #4 nms

# Defining water model
water_model="$6" # tip3p, spce, spc, etc. (original tip3p) "spce"

# DEFINING MAIN SIMULATION DIRECTORY
Main_Sim_Dir="$7"

## GETTING RESIDUE NAME OF THE SOLUTE
if [  "${solute_name}" != "NoSolute" ]; then
    ## DEFINING SOLUTE ITP
    solute_itp_file="${PREP_SOLUTE_DIR}/${solute_name}/${solute_name}.itp"
    ## GETTING RESIDUE NAME
    solute_residue_name=$(itp_get_resname ${solute_itp_file})

fi

## GETTING RESIDUE NAME OF THE COSOLVENT
## DEFINING SOLVENT ITP FILE
solvent_itp_file="${PREP_SOLVENT_DIR}/${solvent_two_name}/${solvent_two_name}.itp"

## GETTING RESIDUE NAME
solvent_two_residue_name=$(itp_get_resname ${solvent_itp_file})

### DEFINING OUTPUT DIRECTORY
percent_solvent_one_wt_frac=$(awk -v num=$solvent_one_wt_frac 'BEGIN{ printf "%d",num*100}') 
# CHECK IF YOU WANT A SOLUTE
if [  "${solute_name}" != "NoSolute" ]; then 
    output_dir="${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_${solvent_two_name}"
else
   No_solute_name="NoSolute" 
   output_dir="${desired_box_length}_nm_${No_solute_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_${solvent_two_name}"
fi

######################################
### DEFINING IMPORTANT DIRECTORIES ###
######################################

# Input directories/files
forcefield_dir="charmm36-nov2016.ff"

# Output directories
output_gro="mixed_solv.gro"
output_top="mixed_solv.top"

# MDP Files
em_mdp="minim_LIQ.mdp" # Energy minimization mdp file
equil_mdp="equil_LIQ.mdp" # Equilibration mdp file
prod_mdp="production_LIQ.mdp" # MD Production mdp file

# In MDP file, defining times -- Changes NSTEPS in mdp file -- Assumes dt = 0.002 ps
EquilTime="250000" # ps, equivalent to 500 ps
#ProdTime="100000000" # equivalent to 200 ns #"50000000" # ps, -- 100 ns equivalent to 100 ns ("50000000")
## DEFINING PRODUCTION TIME
if [  "${solute_name}" == "NoSolute" ]; then
    ProdTime="10000000" # 20 ns (Limiting for Alex S.)
    # "25000000" # 50 ns (Limiting the amount of time for production)
else
    # ProdTime="25000000" # 200 ns
    # ProdTime="1000" # 4 ns
    ProdTime="2000000" # 2 ns
    # 25000000 -- 50 ns
    # 100000000 -- 200 ns
fi

# Defining expansion for insertion of molecule
expand_insert_dist="0.5" # "0.3" # nms, used for expansion, insertion, then allow NPT to take over

# Submission script
submitScriptName="submit.sh"
submitScriptInput="${SUBMISSION_DIR}/${submitScriptName}"

## CHECKING PREPARATION -- Definitions for cosolvent mixture ##

# Defining directory path
mixed_solvent_dir="${PREP_MIXED_COSOLVENTS}/${water_model}"

## FOR NONE CASE IN PURE WATER
# if [ $(awk -v wt_frac=${solvent_one_wt_frac} 'BEGIN{ if (wt_frac != 1.00 && wt_frac != 0.00) print 1; else print 0}') -ne 1 ]; then
if [ $(awk -v wt_frac=${solvent_one_wt_frac} 'BEGIN{ if (wt_frac == 1.00) print 1; else print 0}') -ne 0 ]; then
       if [  "${solute_name}" != "NoSolute" ]; then 
        output_dir=${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_Pure
       else 
        output_dir=${desired_box_length}_nm_${No_solute_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_Pure
       fi
fi

## FINDING PREPARATION DIRECTORY
prep_dir_name="$(solvent_effects_find_prep_solvent_folder "${solvent_one_wt_frac}" "${solvent_two_residue_name}")"

## DEFINING FULL PATH TO THE DIRECTORY
path2prepdir="${mixed_solvent_dir}/${desired_box_length}nm/${prep_dir_name}"

# Checking if preparation exists
if [ ! -e "$path2prepdir" ]; then
    echo "Error!!! $path2prepdir does not exist"
    echo "Make sure to run preparation of co_solvents first!!! (prep_cosolvent.sh)"
    echo "Stopping here....."
    exit
fi

##################################
### DEFINING PREPARATION FILES ###
##################################
# GRO Files
prep_gro_file_name="mixed_solv_equil.gro"
prep_gro_file_path="$path2prepdir/$prep_gro_file_name"
# Topology Files
prep_top_file_name="mixed_solv.top" # Testing purposes
prep_top_file_path="$path2prepdir/$prep_top_file_name"

# Checking if gro / top file exists
if [[ ! -e "$prep_gro_file_path" || ! -e "$prep_top_file_path" ]]; then
    echo "Error, no GRO file or top file!"
    echo "Check gro: $prep_gro_file_path"
    echo "Check top: $prep_top_file_path"
    echo "Exiting now...."
    exit
fi

###############################################
### DEFINING SIMULATION WORKING DIRECTORIES ###
###############################################
## DEFINING WORKING DIRECTORY
WorkSpace="${PATH2SIM}"
#if [  "${solute_name}" != "None" ]; then
#    WorkSpace="${WorkSpace}/${solute_name}_new_solvents" # _100
#else # SOLUTE NAME DOES NOT EXIST, THUS PURE COSOLVENTS / MIXED SOLVENTS ONLY
#    WorkSpace="${WorkSpace}/${No_solute_name}"
#fi

# Checking if working directory exists
if [ ! -e ${WorkSpace} ]; then # Checking if solute exists
    mkdir -p ${WorkSpace}
fi

# Defining simulation directory
Simulation_dir=mdRun_${system_temp}_${output_dir} # Directory where all the runs will take place
path2Sim_dir=$WorkSpace/${Main_Sim_Dir}/$Simulation_dir # Defining path structure

# Checking if a duplicate is already available.
if [ -e ${path2Sim_dir} ]; then
    echo "Duplicated Workspace -- No Good!!!"
    echo "Deleting $path2Sim_dir and remaking it"
    echo "Pausing for 5 seconds (in case you want to cancel now)"
    sleep 5
    rm -rv "${path2Sim_dir}"
fi
## Creating directories and copying preparation files ##

# Making directory for your specific job
mkdir -p "${path2Sim_dir}"

# Going into working directory
cd "${path2Sim_dir}"

# Copying cosolvent mixture files
# Gro file
cp -r ${prep_gro_file_path} "$path2Sim_dir/${output_gro}"
# Top file
cp -r ${prep_top_file_path} "$path2Sim_dir/${output_top}"

#####################
### ADDING SOLUTE ###
#####################

## DEFINING GRO FILE OF INSERTED
output_insert_gro=${output_gro%.gro}_inserted.gro
## DEFINING NUMBER OF SOLUTES TO ADD
numSolute=1

## CHECKING IF YOU WANT A SOLUTE
if [  "${solute_name}" != "NoSolute" ]; then
    solvent_effects_add_solute "${solute_name}" "${solute_residue_name}" "${numSolute}" "${output_gro}" "${output_insert_gro}" "${expand_insert_dist}" "${output_top}"
    else
    ## NO SOLUTE
    cp ${output_gro} ${output_insert_gro}
    
fi

## CLEANING DIRECTORY
mkdir setup_files
mv *.gro setup_files
cp -r setup_files/${output_insert_gro} ./${output_gro}

#############################################
### ADDING PARAMETERS AND ITP TO TOPOLOGY ###
#############################################

## Finding line to add to
lineNum2system=$(grep -nr "\[ system \]" $output_top | sed 's/\([0-9]*\).*/\1/') #
## Now, adding lines to topology
line2add=$(( $lineNum2system-1 ))
sed -i "${line2add}a \ " $output_top # Blank line
## CHECKING IF YOU WANT A SOLUTE
if [  "${solute_name}" != "NoSolute" ]; then
    sed -i "${line2add}a #include \"$solute_name.itp\"" ${output_top} # itp file
    sed -i "4a #include \"${solute_name}.prm\"" ${output_top} # Parameter
    sed -i "${line2add}a ; Include topology for solute, ${solute_name}" ${output_top} # adding comment
fi

###########################
### EDITING SUBMIT FILE ###
###########################

## CHANGING SUBMIT NAME
sed "s/sideProject/$output_dir/g" ${submitScriptInput} > $submitScriptName

## EDITING MDP FILES
sed -i "s/EM_MDP/${em_mdp}/g" $submitScriptName
sed -i "s/EQUIL_MDP/${equil_mdp}/g" $submitScriptName
sed -i "s/PROD_MDP/${prod_mdp}/g" $submitScriptName

###############################
### COPYING ALL INPUT FILES ###
###############################
## CHECKING IF YOU WANT A SOLUTE
if [  "${solute_name}" != "NoSolute" ]; then
    # itp + parameters for solute
    cp -rv "${PREP_SOLUTE_DIR}/${solute_name}/"{*.itp,*.prm} ./
fi
# itp + parameters for solvent
cp -rv "${PREP_SOLVENT_DIR}/${solvent_two_name}/"{*.itp,*.prm} ./
# mdp files
cp -rv "${MDP_DIR}/${em_mdp}" ./
cp -rv "${MDP_DIR}/${equil_mdp}" ./
cp -rv "${MDP_DIR}/${prod_mdp}" ./
# force field
cp -r "${INPUT_DIR}/${forcefield_dir}" ./

#########################
### EDITING MDP FILES ###
#########################
# Editing mdp times
sed -i "s/NSTEPS/${EquilTime}/g" ${equil_mdp}
sed -i "s/NSTEPS/${ProdTime}/g" ${prod_mdp}

# Editing mdp file for temperature
sed -i "s/TEMPERATURE/${system_temp}/g" ${equil_mdp}
sed -i "s/TEMPERATURE/${system_temp}/g" ${prod_mdp}
#
# Submitting the job -- by putting it into a list, then allow user to edit
if [ ! -e ${PATH2SCRIPTS} ]; then
    mkdir -p "${PATH2SCRIPTS}"
fi
echo "${path2Sim_dir}" >> "${JOB_FILE}"

# Printing results
echo "------ Summary ------"
if [  "${solute_name}" != "NoSolute" ]; then
    echo "Solute Molecule: ${solute_name} ( ${solute_residue_name} )"
else
    echo "NO SOLUTE SELECTED! PURE SOLVENT MIXTURES!"
fi
    
echo "Solvent Molecule: $solvent_two_name ( ${solvent_two_residue_name} )"
echo "Weight Fraction of Water: $solvent_one_wt_frac"
echo "Full Simulation Working Directory: $WorkSpace/$NPT_dir"
echo "Temperature (K): $system_temp"
echo "Submit job by using list in: ${JOB_FILE}"
