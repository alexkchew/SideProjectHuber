#!/bin/bash

# prep_umbrella_sampling_3_umbrella_sampling_sims.sh
# The purpose of this script is to find the configurations from pulling simulations, then run umbrella sampling simulations

# Written by: Alex K. Chew (alexkchew@gmail.com, 08/02/2018)

## ALGORITHM:
#   - Use previous mixed-solvent environment with water and cosolvent
#   - Generate configurations based on the umbrella sampling inputs in this script
#   - Then, generalize the configurations into simple conf_1_*.gro with a summary file

## USAGE: bash prep_umbrella_sampling_3_pulling.sh

# *** UPDATES *** #
#   20181012 - Updating umbrella sampling script to update intial distance every time

### FUNCTION TO EXTRACT THE DESIRED DISTANCE FROM THE NAME
# The purpose of this function is to extract the desired configuration increment
# INPUTS:
#   $1: [str]
#       name of the conf, e.g. conf_desired_0.650nm_actual_0.643nm.gro
# OUTPUTS:
#   echos desired increment, e.g. 0.650
# USAGE EXAMPLE:
#   convert_conf_name_to_distance conf_desired_0.650nm_actual_0.643nm.gro
#   -> 0.650
function convert_conf_name_to_distance () {
    ## DEFINING NAME
    input_name_="$1"
    ## USING COMBINATION OF ECHO, SED, AWK TO EXTRACT INFORMATION
    extracted_conf="$(echo "${input_name_}" | awk -F_ '{print $3}' | sed "s/[^[:digit:].-]//g")"
    ## PRINTING
    echo "${extracted_conf}"
}

### RELOADING GLOBAL VARS AND FUNCTIONS
source "bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING LOGICALS
want_chlorine_ion="True" # True if you want chlorine ion within the system

## NAME OF SOLUTE 1
# solute_1_name="xylitol"
# solute_1_res_name="XYL"
solute_1_name="12-propanediol"
solute_1_res_name="PDO"

## NAME OF SOLUTE 2
solute_2_name="hydronium_LINCS_3" # "hydronium" # 
solute_2_res_name="HYD"

## DEFINE DESIRED BOX LENGTH
box_length="6" # "6" # "6"

## DEFINING UPPER AND LOWER BOUNDS, WITH INCREMENTS OF CONFIGURATIONS FOR UMBRELLA SAMPLING
umb_upper_limit="1.90" # nm
umb_inc="0.05" # increments in nm
umb_lower_limit="0.3" # nm

## DEFINING DESIRED DISTANCES
umb_desired_distances=0.30,0.35,0.40,0.45,0.50,0.55,0.60,0.65,0.70,0.75,0.80,0.85,0.90,0.95,1.00,1.05,1.10,1.15,1.20,1.25,1.30,1.35,1.40,1.45,1.50,1.55,1.60,1.65,1.70,1.75,1.80,1.85,1.90

## ADJUSTING FOR LARGE BOX LENGTHS
if [ "${box_length}" == "9" ]; then
umb_desired_distances=0.30,0.35,0.40,0.45,0.50,0.55,0.60,0.65,0.70,0.75,0.80,0.85,0.90,0.95,1.00,1.05,1.10,1.15,1.20,1.25,1.30,1.35,1.40,1.45,1.50,1.55,1.60,1.65,1.70,1.75,1.80,1.85,1.90,1.95,2.00,2.05,2.10,2.15,2.20,2.25,2.30,2.35,2.40,2.45,2.50,2.55,2.60,2.65,2.70,2.75,2.80,2.85,2.90
fi
## DEFINING DESIRED SPRING CONSTANTS
#declare -a umb_desired_spring_constants=("20000" "20000" "20000" "20000" "20000" "20000" "20000" "20000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000" "10000")

## DEFINING MAIN DIRECTORY TO RUN SIMULATIONS IN
Main_Sim_Dir="181214-Umbrella_sampling_PDO_HCL_6nm_15000_spring_constant_300K" # 15000"
# "181110-Umbrella_sampling_PDO_6nm_WITH_CL"

## DEFINING UMBRELLA SAMPLING PULLING CONSTANT
umb_k_constant="15000" # "10000" "15000" # 75000
# default: 15000
# rerunning 0 cosolvent with 10000
# note: pure cosolvent runs did not successfully run via umbrella sampling -- possibly due to instabilities of the hydronium ion in that solvent environment (probably unphysical....)

## PYTHON SCRIPT
python_find_configurations="${MDBUILDER}/umbrella_sampling/umbrella_sampling_select_configurations.py"

## DEFINING NUMBER OF TIMES TO SPLIT THE WINDOWS
num_split="11" # "1" "2" # 3 Denotes the number of splits you want for N number of lambdas (i.e. N/num_split === number of independent nodes you want to run the simulation on)

## DEFINING NUMBER OF CORES
num_cores="28"

############################
### PREDEFINED VARIABLES ###
############################

## DEFINING DESIRED SOLVENT NAMES
# solvent_water_mass_fraction="1.00" # Mass fraction of water
solvent_water_mass_fraction="0.10" # Mass fraction of water
cosolvent_name="dmso" # "dioxane" # Name of the cosolvent
cosolvent_res_name="dmso" # "DIO" # Residue name of the cosolvent

## TEMPERATURE TO RUN THIS EQUILIBRIUM STEP
temperature="300.00" # Kelvins
if [ "${solute_1_name}" == "12-propanediol" ]; then
    temperature="433.15" # Kelvins
fi
temperature="300.00" # Kelvins


## WATER MODEL
water_model="spce"

## DEFINING FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## PULLING SIMULATIONS NAMES
pull_config_files="us_config"
pull_config_prefix="conf"
pull_summary_file="pull_summary.dat"
pull_topology_file="mixed_solv.top" # Topology file

##############################
### FINDING PULL DIRECTORY ###
##############################
## DEFINING WORKING DIRECTORY
if [ "${want_chlorine_ion}" == "True" ]; then
    WorkSpace="${PREP_HYD_US_HCL}"
else
    WorkSpace="${PREP_HYD_US}"
fi

## FINDING DIRECTORY
pull_dir=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${solute_2_res_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 

## DEFINING DIRECTORY TO FIND HYD ION DIRECTORY
full_path_hyd_solute_dir="${WorkSpace}/PULL_${solute_1_name}_${pull_dir}"

## CHECKING IF DIRECTORY EXISTS
check_dir_exist "${full_path_hyd_solute_dir}"

############################
### DEFINING INPUT FILES ###
############################

## DEFINING MDP PATH
mdp_file_path="${MDP_DIR}/umbrella_sampling_hyd"

## DEFINING MDP FILES
mdp_equil="umb_npt_equil.mdp"
mdp_prod="umb_npt_prod.mdp"

## DEFINING MDP TIME
mdp_equil_time="500000" # 1 ns equilibration
mdp_prod_time="7500000" # 15 ns production runs
# "5000000" # 10 ns production
## RUNNING TEST MDP
# mdp_equil_time="125000" # 250 ps
# mdp_prod_time="250000" # 500 ps

## DEFINING SUBMISSION PATH
submit_file_path="${SUBMISSION_DIR}"

## DEFINING SUBMISSION FILE
submit_per_window_file_name="submit_prep_umbrella_sampling_3_us_per_window.sh"
submit_full_split_script="submit_prep_umbrella_sampling_3_us_full_script.sh"

## DEFINING FULL SUBMISSION SCRIPT
submit_per_window_file_path="${submit_file_path}/${submit_per_window_file_name}"
submit_full_split_script_path="${submit_file_path}/${submit_full_split_script}"

#############################
### DEFINING OUTPUT FILES ###
#############################

## FINDING OUTPUT DIRECTORY FOR SOLUTE
solute_dir=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${solute_1_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 

## OUTPUT DIRECTORY
output_dir="UMB_${solute_dir}"
output_path="${PATH2SIM}/${Main_Sim_Dir}/${output_dir}" # FEP 

## OUTPUT FILES
output_summary="conf_summary.csv"

## OUTPUT SUBMIT
output_submit_file="submit.sh"

## DEFINING OUTPUT TO WINDOWS AND SUMMARY
output_summary_file="summary.dat"

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## CONTAINS ALL INPUT INFORMATION
output_inputs="${output_path}/1_input_files" 
## CONTAINS ALL GRO CONFIGURATIONS
output_configs="${output_path}/2_configurations"
## CONTAINS ALL SUBMISSION FILES
output_submit="${output_path}/3_submit_files"
## CONTAINS ALL SIMULATIONS
output_sims="${output_path}/4_simulations"
## CONTAINS ALL ANALYSIS
output_analysis="${output_path}/5_analysis"

## SUB DIRECTORYS WITHIN SIM
output_sims_0_npt_equil="0_NPT"
output_sims_1_npt_prod="1_MD"

## DEFINING FULL PATHS
path_summary_file="${output_path}/${output_summary_file}"
## DEFINING SUB DIRECTORY
output_inputs_mdp_folder="${output_inputs}/mdp_files"
output_inputs_mdp_folder_0_npt_equil="${output_inputs_mdp_folder}/${output_sims_0_npt_equil}"
output_inputs_mdp_folder_1_npt_prod="${output_inputs_mdp_folder}/${output_sims_1_npt_prod}"

###################
### MAIN SCRIPT ###
###################

## PRINTING
echo "-------------------------------------------------------------"
echo "CREATING DIRECTORY FOR: ${output_path}"
echo "GENERATING CONFIGURATIONS BASED ON ${full_path_hyd_solute_dir}"
echo "-------------------------------------------------------------"

## CREATING OUTPUT DIRECTORY
create_dir "${output_path}" -f

## GOING INTO OUTPUT DIRECTORY
cd "${output_path}"

## CREATING SUB FOLDERS
mkdir -p "${output_inputs}" "${output_configs}" "${output_submit}" "${output_sims}" "${output_analysis}"
mkdir -p "${output_inputs_mdp_folder_0_npt_equil}" "${output_inputs_mdp_folder_1_npt_prod}"

## RUNNING MAIN PYTHON SCRIPT TO COPY CONFIGURATIONS 
python3 "${python_find_configurations}" \
    --imain "${full_path_hyd_solute_dir}" \
    --isum "${pull_summary_file}" \
    --icon "${pull_config_files}" \
    --ipref "${pull_config_prefix}" \
    --omain "${output_configs}" \
    --osum "${output_summary}" \
    --opref "${pull_config_prefix}" \
    --configs "${umb_desired_distances}"
# --upper "${umb_upper_limit}" --lower "${umb_lower_limit}" --inc "${umb_inc}"

##########################
### COPYING OVER FILES ###
##########################
echo "*** COPYING OVER FILES ***"
## MDP FILES
cp -r "${mdp_file_path}"/{${mdp_equil},${mdp_prod}} "${output_inputs}"
## TOPOLOGY
cp -r "${full_path_hyd_solute_dir}/${pull_topology_file}" "${output_inputs}/${pull_topology_file}"
# ITP FILE
cp -r ${full_path_hyd_solute_dir}/{*.itp,*.prm} "${output_inputs}"
## FORCE FIELD
cp -r "${INPUT_DIR}/${forcefield_dir}" "${output_inputs}"
## SUBMISSION FILE (SINGLE WINDOW BASIS)
cp -r "${submit_per_window_file_path}" "${output_submit}/${submit_per_window_file_name}"

#####################
### EDITING FILES ###
#####################
echo "*** EDITING FILES USING SED ***"
## EQUILIBRATION MDP FILE
sed -i "s/_TEMPERATURE_/${temperature}/g" ${output_inputs}/{${mdp_equil},${mdp_prod}}
sed -i "s/_GROUP_1_NAME_/${solute_1_res_name}/g" ${output_inputs}/{${mdp_equil},${mdp_prod}}
sed -i "s/_GROUP_2_NAME_/${solute_2_res_name}/g" ${output_inputs}/{${mdp_equil},${mdp_prod}}
sed -i "s/_NSTEPS_/${mdp_equil_time}/g" ${output_inputs}/${mdp_equil}
sed -i "s/_NSTEPS_/${mdp_prod_time}/g" ${output_inputs}/${mdp_prod}

######################################
### LOOPING AND CREATING TPR FILES ###
######################################

## MOVING TO DIRECTORY WITH TOPOLOGY
cd "${output_inputs}"

## FINDING TOTAL WINDOWS
total_windows=$(ls ${output_configs}/*.gro | wc -l)

## PRINTING
echo "*** CREATING TPR FILES ***"
echo "Total windows: ${total_windows}"

## CREATE A COUNTER TO KEEP TRACK
counter=0

## LOOPING THROUGH EACH CONFIGURATION
for each_configuration in $(ls ${output_configs}/*.gro); do
    ## DEFINING BASE NAME
    current_config_basename="$(basename ${each_configuration})" # GETTING BASE NAME ONLY
    ## DEFINING CURRENT SPRING CONSTANT
    current_desired_spring_constant=${umb_k_constant} # "${umb_desired_spring_constants[${counter}]}"
    ## DEFINING CURRENT DESIRED DISTANCE
    current_desired_distance=$(convert_conf_name_to_distance ${current_config_basename})
    
    ## PRINTING
    echo "Generating tpr file for ${counter} out of ${total_windows} -- ${current_desired_distance} nm, k= ${current_desired_spring_constant}"
    
    ## DEFINING NAME OF THE CONFIGURATION
    config_name="${current_desired_distance}_nm_${current_desired_spring_constant}_k"
    
    ## DEFINING NAMES FOR MDP FILE
    current_mdp_equil=${mdp_equil%.mdp}_${config_name}.mdp
    current_mdp_prod=${mdp_prod%.mdp}_${config_name}.mdp
    
    ## CREATING PATH TO EACH
    current_path_mdp_equil="${output_inputs_mdp_folder_0_npt_equil}/${current_mdp_equil}"
    current_path_mdp_prod="${output_inputs_mdp_folder_1_npt_prod}/${current_mdp_prod}"
    
    ## CREATING MDP FILES
    sed "s/_PULLKCONSTANT_/${umb_k_constant}/g" ${output_inputs}/${mdp_equil} > "${current_path_mdp_equil}"
    sed "s/_PULLKCONSTANT_/${umb_k_constant}/g" ${output_inputs}/${mdp_prod} > "${current_path_mdp_prod}"
    
    ## EDITING MDP FILES
    sed -i "s/_INITIALDIST_/${current_desired_distance}/g" {${current_path_mdp_equil},${current_path_mdp_prod}}
    
    ## CREATE A DIRECTORY BASED ON CONFIGURATION
    mkdir -p "${output_sims}/${config_name}/"{${output_sims_0_npt_equil},${output_sims_1_npt_prod}}
    ## DEFINING GRO FILE
    current_gro_file="${each_configuration}"
    ## USING GMX TO CREATE A TPR FILE
    gmx grompp -f "${current_path_mdp_equil}" -c "${current_gro_file}" -p "${pull_topology_file}" -o "${output_sims}/${config_name}/${output_sims_0_npt_equil}/npt${config_name}.tpr" > /dev/null 2>&1
    ## CHECKING FOR EXISTANCE
    if [ ! -e "${output_sims}/${config_name}/${output_sims_0_npt_equil}/npt${config_name}.tpr" ]; then
        echo "Error! TPR file does not exist!"
        echo "Check for: ${output_sims}/${config_name}/${output_sims_0_npt_equil}/npt${config_name}.tpr"
        echo "Pausing here so you can see the error!"
        sleep 5
    fi
    
    ## OUTPUTTING SPRING CONSTANT
    if [ "${counter}" == "0" ]; then
        echo "${counter} ${config_name}" > "${path_summary_file}"
    else
        echo "${counter} ${config_name}" >> "${path_summary_file}"
    fi
    
    ## ADDING ONE TO COUNTER
    counter=$((counter+1 ))
done

## CLEANING MOLECULE FOLDER
rm \#mdout.mdp.*

###################################
### WORKING ON SUBMISSION FILES ###
###################################
## PRINTING
echo "** CREATING SUBMISSION SCRIPTS **"

## FINDING LAST WINDOW BASED ON -1
final_window_based_on_zero=$((${total_windows}-1))

## FINDING DIVIDED VALUES INTO MULTIPLE JOBS (ROUNDING DOWN)
divided_value="$((${total_windows} / ${num_split}))"

## LOOPING THROUGH EACH SPLIT
for each_split_index in $(seq 0 $((${num_split}-1)) ); do
        ## DEFINING EACH LAMBDA
        ### INITIAL SPLIT
        if [ "${each_split_index}" == 0 ]; then
            initial_lambda="$((${each_split_index}*${divided_value}))"
            final_lambda="$(( ${each_split_index}+${divided_value}-1 ))"
        ### FINAL SPLIT
        elif [ "${each_split_index}" == "$((${num_split}-1))" ]; then
            initial_lambda="$((${final_lambda}+1))"
            final_lambda="${final_window_based_on_zero}"
        ### MIDDLE SPLITS
        else
            initial_lambda="$((${final_lambda}+1))"
            final_lambda="$(( ${initial_lambda}+${divided_value}-1 ))"
            # final_lambda="$((${initial_lambda}+${divided_value}+1))"
        fi
        
        ## DEFINING SUBMISSION SCRIPT
        each_split_submission_script="${output_path}/${output_submit_file%.sh}_${each_split_index}.sh"

        ## EDITING SUBMISSION SCRIPT
        sed "s/_FIRSTWINDOW_/${initial_lambda}/g" "${submit_full_split_script_path}" > "${each_split_submission_script}"
        sed -i "s/_LASTWINDOW_/${final_lambda}/g" "${each_split_submission_script}"
        sed -i "s/_DIRECTORYNAME_/${output_dir}_${each_split_index}/g" "${each_split_submission_script}"
        sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${each_split_submission_script}"
        sed -i "s/_SUBMISSIONDIR_/$(basename ${output_submit})/g" "${each_split_submission_script}"
        sed -i "s/_SUBMISSIONSCRIPT_/${submit_per_window_file_name}/g" "${each_split_submission_script}"
        
        ## ADDING TO JOB LIST
        # if [ "${each_split_index}" == 0 ]; then
        echo "${each_split_submission_script}" >> "${JOB_FILE}"
        # fi

done

