#!/bin/bash

# analysis_extraction.sh 
# This bash script will take the output files and copy to the current working directory. This is absolutely necessary because we need the .xtc, .gro, and .tpr files for calculating important interactions like hydrogen bonding, etc.
# Written by Alex Chew (03/18/2017)
# Variables
# $1: wantHCl = True or False if you want hydronium ion

# Usage: bash analysisTools_Extraction.sh True

##*-- Updates --*##
# 17-05-12 - Cleaning up useless code
# 17-05-18 - Adding *.edr files to things to copy; added array functions
# 17-06-05 - Added hydronium chloride option
# 17-06-12 - Added number of HCl option
# 17-06-23 - Added multiple alternative options
# 17-07-06 - Fixed up script for prod_extend
# 17-08-01 - Fixing up script to include slashes and desired solvent
# 18-05-13 - Enabled variables

### VARIABLES
#   $1: input extraction directory
#   $2: specific directory to output
#   $3: alternative simulations like prod extension, etc.
#   $4: number of HCls

## LOADING GLOBAL VARIABLES
source "bin/bash_rc.sh"

# -- USER-SPECIFIC PARAMETERS (BEGIN) -- #

# Declaring directories from simulation folder that you would like your data from
declare -a extractDirList=("$1")  # "levoglucosan" "tBuOH" "sorbitol" "xylitol"
#specific_analysis_dir="levoglucosan" # Directory where you want specifically the analysis to occur
specific_analysis_dir="$2" # Directory where you want specifically the analysis to occur ${HCl_Dir_Name}${prod_extend_dir_name}

# Desired hydronium chloride
wantAlt="$3" # "HCl" "prod_extend"
numHCl="$4" # Number of HCl's desired

# Slash
mySlash=""
# Making HCl directory if wantHCl is true
if [[ "$wantAlt" == "HCl" ]]; then
    HCl_Dir="hydronium_chloride_${numHCl}"
    HCl_Dir_Name="_${HCl_Dir}"
    echo "Looking into $HCl_Dir directories"
    mySlash="/"
    else
    HCl_Dir=""
    
fi

if [[ "$wantAlt" == "prod_extend" ]]; then
    ## LOADING GROMACS 5_0_1
    load_gromacs_5_0_1_thread
    ## DEFININE DIRECTORY
    prod_extend_dir="prod_extend"
    prod_extend_dir_name="_${prod_extend_dir}"
    ## DEFINING SPECIFIC EXTEND
    specific_extend="prod_5_ns_xtcout_50_nstlist_5" # Name of your extension
    ## DEFINING PATH TO MDP FILE
    prod_extend_mdp_file="production_LIQ_extended.mdp"
    ## DEFINING FULL PATH
    full_prod_extend_path="${prod_extend_dir}/${specific_extend}"
    echo "Looking into $prod_extend_dir directories"
    mySlash="/"
    ## DEFINING FILE MPATH INFORMATION
    top_file="mixed_solv.top"
    prod_gro="mixed_solv_prod.gro"
    tpr_file="mixed_prod_extended.tpr"
    else
    prod_extend_dir=""
fi

#DesiredSolvent="dioxane" # Solvent to extract things from


# Defining parameters for extraction
#production_file_start="mdRun_"
production_file_start="" # "mdRun_"

## Creating output directory ##

# Defining working directory for output
fullAnalysisWorkingDir="${PATH2ANALYSIS}/$specific_analysis_dir"

# Making sure there is no duplicate output directory
if [ -e $fullAnalysisWorkingDir ]; then
    echo "$fullAnalysisWorkingDir already exists, removing duplicates!"
    echo "Waiting 5 seconds until deletion..."
    sleep 5
    rm -r $fullAnalysisWorkingDir
fi

# Creating a working directory for analysis
mkdir -p $fullAnalysisWorkingDir

# -- USER-SPECIFIC PARAMETERS (END) -- #

# First, we need to get all the data files we need - Done by straight copying

for extractDir in ${extractDirList[@]}; do

    # Finding all files that you will need
    FilesRequired=$(echo ${PATH2SIM}/$extractDir/$production_file_start*)

    for currentSeq in $FilesRequired; do

        echo "Working on: $currentSeq"

        SequenceName=$(basename $currentSeq) # Finds sequence name
        currentSolvent=$(echo $SequenceName | cut -d '_' -f 9-)

        # Making directory
        mkdir -p $fullAnalysisWorkingDir/$SequenceName

        # Creating full input and output paths
        
        ## DEFINING CLEAN INPUT PATH
        input_path_clean="${PATH2SIM}/$extractDir/${SequenceName}" # No additional pathing information
        inputPath="${PATH2SIM}/$extractDir/${SequenceName}${mySlash}${HCl_Dir}${full_prod_extend_path}" # /
        outputPath="$fullAnalysisWorkingDir/$SequenceName"

        # Simple copy and paste
        if [ "$wantAlt" == "prod_extend" ]; then
            cp -r $inputPath/{*_prod.xtc,*_prod.tpr,*_prod.gro,*_prod.edr} $outputPath # *_prod.trr
            ## COPYING MDP FILE
            # cp -r "${inputPath}/${prod_extend_mdp_file}" "${outputPath}" # *_prod.trr
            ## GO TO WORKING DIRECTORY
            cd "${input_path_clean}"
            ## USING TPR TO GENERATE PROD EXTEND
            gmx grompp -f "${inputPath}/${prod_extend_mdp_file}" -p ${top_file} -c ${prod_gro} -o ${outputPath}/${tpr_file} -maxwarn 1
            
        else
            cp -r $inputPath/{*.top,*_prod.xtc,*_prod.tpr,*_prod.gro,*_prod.edr,*.itp} $outputPath
        fi


        echo -e "Copying xtc, tpr, and gro file from $inputPath to $outputPath \n"
        
    done

done
# Printing what we have done
echo "------------------"
echo "Copied over .xtc, .gro, and .tpr files from: ${CURRENTWD}/$extractDir"
echo "Analysis directory at: $fullAnalysisWorkingDir"


