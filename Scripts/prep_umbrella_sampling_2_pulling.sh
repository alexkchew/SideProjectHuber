#!/bin/bash

# prep_umbrella_sampling_3_pulling.sh
# The purpose of this script is to start pulling the hydronium ion from a far distance towards the substrate. Given the configurations, we can start generating pull configurations. 

# Written by: Alex K. Chew (alexkchew@gmail.com, 08/02/2018)

## ALGORITHM:
#   - Use previous mixed-solvent environment with water and cosolvent
#   - Generate configurations based on the umbrella sampling inputs in this script
#   - Then, generalize the configurations into simple conf_1_*.gro with a summary file

## USAGE: bash prep_umbrella_sampling_3_pulling.sh

# *** UPDATES *** #

### RELOADING GLOBAL VARS AND FUNCTIONS
source "bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING LOGICALS
want_chlorine_ion="True" # True if you want chlorine ion within the system

## NAME OF SOLUTE 1
solute_1_name="xylitol"
solute_1_res_name="XYL"
solute_1_name="12-propanediol"
solute_1_res_name="PDO"

## NAME OF SOLUTE 2
solute_2_name="hydronium_LINCS_3" # "hydronium" # 
solute_2_res_name="HYD"

## DEFINE DESIRED BOX LENGTH
box_length="6" # "6"

############################
### PREDEFINED VARIABLES ###
############################ 

## DEFINING DESIRED SOLVENT NAMES
# solvent_water_mass_fraction="1.00" # Mass fraction of water
solvent_water_mass_fraction="0.10" # Mass fraction of water
cosolvent_name="dioxane" # "dioxane" # Name of the cosolvent
cosolvent_res_name="DIO" # "DIO" # Residue name of the cosolvent

## TEMPERATURE TO RUN THIS EQUILIBRIUM STEP
temperature="300.00" # Kelvins

if [ "${solute_1_name}" == "12-propanediol" ]; then
    temperature="433.15" # Kelvins
fi

temperature="300.00" # Kelvins


## WATER MODEL
water_model="spce"

## DEFINING FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## FINDING DIRECTORY
hyd_solute_dir=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${solute_2_res_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 


## DEFINING DIRECTORY TO FIND HYDRONION DIR
## DEFINING WORKING DIRECTORY
if [ "${want_chlorine_ion}" == "True" ]; then
    prep_hyd_solute="${PREP_HYD_SOLUTE_SIMS_HCL}"
else
    prep_hyd_solute="${PREP_HYD_SOLUTE_SIMS}"
fi

## DEFINING DIRECTORY TO FIND HYD ION DIRECTORY
full_path_hyd_solute_dir="${prep_hyd_solute}/PREPHYD_${solute_1_name}_${hyd_solute_dir}"

## CHECKING IF DIRECTORY EXISTS
check_dir_exist "${full_path_hyd_solute_dir}"

############################
### DEFINING INPUT FILES ###
############################
# This section defines the input files from the hydronium ion simulations
## INPUT BASHRC FOR UMBRELLA SAMPLING
input_umb_sampling_bashrc="${BASHRC_UMB_SAMPLING}"

## INPUT FILE
input_gro_file="mixed_solv_equil_1_npt.gro"
input_top_file="mixed_solv.top"
## INPUT PATH
input_gro_path="${full_path_hyd_solute_dir}/${input_gro_file}"
input_top_path="${full_path_hyd_solute_dir}/${input_top_file}"


## CHECKING IF GRO AND TOP FILE EXISTS
if [[ ! -e "${input_gro_path}" || ! -e "${input_top_path}" ]]; then
    echo "Error, no GRO file or top preparation file!"
    echo "Check gro: ${input_gro_path}"
    echo "Check top: ${input_top_path}"
    echo "Exiting now...."
    exit
fi

#################################
### DEFINING OUTPUT VARIABLES ###
#################################
### DEFINING OUTPUT DIRECTORY
output_dir="PULL_${solute_1_name}_${hyd_solute_dir}"

## OUTPUT FILES
output_prefix="mixed_solv"
output_gro="${output_prefix}.gro"
output_top="${output_prefix}.top"

## OUTPUT SUMMARY
output_summary_dat="pull_summary.dat" # summary data file containing center of masses per frame
output_config_prefix="conf" # configuration prefix to output as gro files
output_config_dir="us_config" # output configuration directorys

## DEFINING SPRING CONSTANT
mdp_initial_pull_k="2000" # initial pulling to get to the right spot
# 3000
# 5000

mdp_pull_k="3000" # <-- pulling force # strength of spring constant in kJ mol^-1 nm^-2    
# 5000
# 10000

######################################
### DEFINING PULL CODE INFORMATION ###
######################################
### DEFINING PULLING INFORMATION
mdp_group_1_name="${solute_1_res_name}"
mdp_group_2_name="${solute_2_res_name}"
mdp_initial_distance="1.90" # Distance in nm
mdp_pull_rate="-0.005" # nm / ps <-- can be positive or negative, positive pull away, negative pull towards


## DEFINING MINIMUM DISTANCE
min_distance="0.05" # nm, minimum distance between groups 1 and 2 <-- DEPRECIATED

## DEFINING TIME STEP
mdp_time_step="0.001" # "0.002" # ps, timesteps

## FINDING ABSOLUTE  VALUE OF PULL RATE
mdp_pull_rate_abs=${mdp_pull_rate#-} # DEPRECIATED

## DEFINING MDP EQUILIBRATION
mdp_2_equil="25000000" # 50 ns

## FINDING TOTAL AMOUNT OF TIME REQUIRED
mdp_n_steps="380000" # "240000" # 220 ps --> 0.1 closest value, cannot be above 240000
## CAN FIGURE OUT BY DIVIDING INITIAL DISTANCE BY PULL RATE -- gets you in ps -- then multiple by 1000
# ALTERNATIVE COMMAND: This calculates the number of steps based on the inputs.
## NOTE: This does not take into account extra time, which may be useful for endpoints, therefore it is ideal to have distance further away from your desired point
# $(awk -v min_dist="${min_distance}" -v dt="${mdp_time_step}" -v pull_rate="${mdp_pull_rate_abs}" -v initial_dist="${mdp_initial_distance}" 'BEGIN {printf "%d",(initial_dist-min_dist)/(pull_rate*dt); exit}' )

### DEFINING OUTPUT XTC PRECISION
mdp_output_xtc_outputs="1000" # output every 1 ps

## CORRECTING FOR LARGER BOX LENGTHS
if [ "${box_length}" == "9" ]; then
    ## INCREASING INITIAL DISTANCE
    mdp_initial_distance="2.90"
    ## REDEFININE NUMBER OF STEPS
    mdp_n_steps="580000" # $(awk -v dt="${mdp_time_step}" -v pull_rate="${mdp_pull_rate_abs}" -v initial_dist="${mdp_initial_distance}" 'BEGIN {printf "%d",initial_dist/(pull_rate*dt); exit}' )
    ## PRINTING
    echo "Box length set to 9, setting initial distance to: ${mdp_initial_distance}"
    echo "Setting number of time steps to: ${mdp_n_steps} ps"
    sleep 2
fi

##########################
### DEFINING MDP FILES ###
##########################

## DEFINING MDP PATH
mdp_file_path="${MDP_DIR}/umbrella_sampling_hyd"

## DEFINING MDP FILES
pull_mdp_1="initial_pull_1_frozen_solute.mdp"
pull_mdp_2="initial_pull_2_extended.mdp"
pull_mdp_3="initial_pull_3_unrestrain_pull.mdp"

################################
### DEFINING SUBMISSION FILE ###
################################

## DEFINING SUBMISSION FILE
submit_input_file_name="submit_prep_umbrella_sampling_2_us_pull.sh"
submit_output_file_name="submit.sh"
submit_file_path="${SUBMISSION_DIR}/${submit_input_file_name}"

###############################################
### DEFINING SIMULATION WORKING DIRECTORIES ###
###############################################
## DEFINING WORKING DIRECTORY
if [ "${want_chlorine_ion}" == "True" ]; then
    WorkSpace="${PREP_HYD_US_HCL}"
else
    WorkSpace="${PREP_HYD_US}"
fi

#if [  "${solute_name}" != "None" ]; then
#    WorkSpace="${WorkSpace}/${solute_name}_new_solvents" # _100
#else # SOLUTE NAME DOES NOT EXIST, THUS PURE COSOLVENTS / MIXED SOLVENTS ONLY
#    WorkSpace="${WorkSpace}/${No_solute_name}"
#fi

# Checking if working directory exists
if [ ! -e ${WorkSpace} ]; then # Checking if solute exists
    mkdir -p ${WorkSpace}
fi

## DEFINING FULL PATH
path2Sim_dir="${WorkSpace}/${output_dir}" # Defining path structure

## CREATING SIM DIRECTORY
create_dir "${path2Sim_dir}" -f

## GOING INTO THE DIRECTORY
cd "${path2Sim_dir}"

##########################################
### COPYING MIXED-SOLVENT ENVIRONMENTS ###
##########################################
echo -e "\n***COPYING OVER GRO AND TOP FILE FROM: ${prep_gro_file_path}***"
# Gro file
cp -r ${input_gro_path} "${path2Sim_dir}/${output_gro}"
# Top file
cp -r ${input_top_path} "${path2Sim_dir}/${output_top}"
# ITP FILE
cp -r ${full_path_hyd_solute_dir}/{*.itp,*.prm}  "${path2Sim_dir}"

###############################
### COPYING ALL INPUT FILES ###
###############################
## FORCE FIELD FILES
echo -e "\n*** COPYING OVER FORCE FIELD (${forcefield_dir}) ***"
cp -r "${INPUT_DIR}/${forcefield_dir}" ./
#
### ITP AND PRM
#echo -e "\n*** COPYING OVER INPUT FILES (GRO, ITP, ETC.) ***"
#cp -rv "${PREP_SOLUTE_DIR}/${solute_1_name}/"{*.itp,*.prm} ./
#cp -rv "${PREP_SOLUTE_DIR}/${solute_2_name}/"{*.itp,*.prm} ./
#cp -rv "${PREP_SOLUTE_DIR}/${counter_ion_name}/"{*.itp,*.prm} ./

## MDP FILE
echo -e "\n*** COPYING OVER MDP FILES ***"
cp -rv "${mdp_file_path}/"{${pull_mdp_1},${pull_mdp_2},${pull_mdp_3}} ./

### SUBMISSION FILE
#echo -e "\n*** COPYING OVER SUBMISSION FILES ***"
cp -rv "${submit_file_path}" ./"${submit_output_file_name}"

#################
### CENTERING ###
#################
# CENTERING THE SOLUTE
gmx trjconv -f "${output_gro}" -s "${input_gro_path%.gro}".tpr -o "${output_gro%.gro}"_center.gro -center -pbc mol << INPUTS
${solute_1_res_name}
System
INPUTS

##########################
### UPDATING MDP FILES ###
##########################

## UPDATING SPRING CONSTANT FOR FIRST
sed -i "s/_SPRINGCONSTANT_/${mdp_initial_pull_k}/g" ${pull_mdp_1}

## UPDATING MDP FILE FOR RESTRAIN SIMS
sed -i "s/_FREEZEGRPS_/${mdp_group_1_name}/g" {${pull_mdp_1},${pull_mdp_2}}
sed -i "s/_TEMPERATURE_/${temperature}/g" {${pull_mdp_1},${pull_mdp_2}}
sed -i "s/_GROUP_1_NAME_/${mdp_group_1_name}/g" {${pull_mdp_1},${pull_mdp_2}}
sed -i "s/_GROUP_2_NAME_/${mdp_group_2_name}/g" {${pull_mdp_1},${pull_mdp_2}}
sed -i "s/_INITIAL_DIST_/${mdp_initial_distance}/g" {${pull_mdp_1},${pull_mdp_2}}

## UPDATING PULL MDP FILE FOR SECOND TYPE
sed -i "s/_NSTEPS_/${mdp_2_equil}/g" ${pull_mdp_2}

## UPDATING PULL MDP FILE FOR PULLING SIM
sed -i "s/_TIMESTEP_/${mdp_time_step}/g" ${pull_mdp_3}
sed -i "s/_NSTEPS_/${mdp_n_steps}/g" ${pull_mdp_3}
sed -i "s/_TEMPERATURE_/${temperature}/g" ${pull_mdp_3}
sed -i "s/_GROUP_1_NAME_/${mdp_group_1_name}/g" ${pull_mdp_3}
sed -i "s/_GROUP_2_NAME_/${mdp_group_2_name}/g" ${pull_mdp_3}
sed -i "s/_INITIALDIST_/${mdp_initial_distance}/g" ${pull_mdp_3}
sed -i "s/_PULLRATE_/${mdp_pull_rate}/g" ${pull_mdp_3}
sed -i "s/_PULLKCONSTANT_/${mdp_pull_k}/g" ${pull_mdp_3}
sed -i "s/_NSTXTCOUT_/${mdp_output_xtc_outputs}/g" ${pull_mdp_3}

## ADJUSTING SUBMISSION FILE
sed -i "s/_DIRECTORY_/${output_dir}/g" ${submit_output_file_name}
sed -i "s/_CENTERGROUP_/${solute_1_res_name}/g" ${submit_output_file_name}
sed -i "s/_MDP_PULL_1_FILE_/${pull_mdp_1}/g" ${submit_output_file_name}
sed -i "s/_MDP_PULL_2_FILE_/${pull_mdp_2}/g" ${submit_output_file_name}
sed -i "s/_MDP_PULL_3_FILE_/${pull_mdp_3}/g" ${submit_output_file_name}
sed -i "s#_UMBBASHRC_#${input_umb_sampling_bashrc}#g" ${submit_output_file_name}
sed -i "s/_PULL_GROUP_1_/${mdp_group_1_name}/g" ${submit_output_file_name}
sed -i "s/_PULL_GROUP_2_/${mdp_group_2_name}/g" ${submit_output_file_name}
sed -i "s/_OUTPUT_SUMMARY_DAT_/${output_summary_dat}/g" ${submit_output_file_name}
sed -i "s/_OUTPUT_CONFIG_PREFIX_/${output_config_prefix}/g" ${submit_output_file_name}
sed -i "s/_OUTPUT_CONFIG_DIR_/${output_config_dir}/g" ${submit_output_file_name}
sed -i "s/_OUTPUT_PREFIX_/${output_prefix}/g" ${submit_output_file_name}

######################################
### RUNNING GROMPP TO CREATE A JOB ###
######################################
gmx grompp -f "${pull_mdp_1}" -c "${output_gro%.gro}"_center.gro -p "${output_top}" -o "${output_prefix}_pull_1" -maxwarn 1

#########################
### UPDATING JOB LIST ###
#########################
## UPDATING DIRECTORY FOR JOB LIST
echo "${path2Sim_dir}" >> ${JOB_FILE}

############################ PRINTING SUMMARY ############################
echo -e "\n************* SUMMARY *************"
echo "Generated solute-hydronium pulling in mixed-solvent systems simulations for the following:"
echo "- SOLUTE: ${solute_1_name} (${solute_1_res_name})" 
echo "- HYDRONIUM_ION: ${solute_2_name} (${solute_2_res_name})" 
echo "- BOX LENGTH: ${box_length}"
echo "- MASS FRACTION OF WATER: ${solvent_water_mass_fraction}"
echo "- COSOLVENT: ${cosolvent_name}"
echo "- WORKING DIR: ${path2Sim_dir}"

### GENERATING CONFIGURATIONS
# gmx trjconv -s mixed_solv_pull.tpr -f mixed_solv_pull.xtc -o conf.gro -sep
# 0 <-- system
# gmx distance -s mixed_solv_pull.tpr -f conf0.gro -select 'com of group "XYL" plus com of group "HYD"' -oall dist0.xvg
# outputs a dist0.xvg, which includes the time in (ps) and the distance in (nm)

## GENERATING POSITION RESTRAINTS
# gmx genrestr -f mixed_solv_center.gro -o posre.itp -fc 10000 10000 10000

