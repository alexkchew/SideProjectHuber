#!/bin/bash

# switchsubmit.sh
# The purpose of this script is to switch the submit.sh file depending on which server you are on, e.g. SWARM or CHTC-HPC, etc.

# Usage: bash switchsubmit.sh name_of_dir

# NOTES:
# - Simply switch the "Files_Within_Sim" variable to the directory you care about

## VARIABLES:
# $1: name of your directory
# $2: type of translation (FEP, US, etc.)

# Originally written by Alex Chew (04/04/2017)
# 2017-04-09: Updating to work in SWARM too
# 2017-07-13: Using this for side project Huber
# 2017-07-17: Updated directories for this script to be within scripts folder
# 2017-07-26: Updated to include COMET
# 2018-06-26: Updating script to include custom free energy perturbation switching

# --- USER-SPECIFIC PARAMETERS (BEGIN) ---

### RELOADING GLOBAL VARS AND FUNCTIONS
source "bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

# Defining important directories
currentWD="${PATH2SCRIPTS}" # Current working directory

# Defining directories within your simulation folder that you want to work on
declare -a Files_Within_Sim=("$1") # "ETBE" "xylitol" "levoglucosan" declare -a Files_Within_Sim=() # "ETBE" "xylitol" "levoglucosan" 

# Checking current hostname -- DEPRECIATED
#if [[ $(echo $HOSTNAME) == *"smic"* ]]; then # Check if we are in the smic server
#    currentWD="/work/achew/scratch/SideProjectHuber"
#elif [[ $(echo $HOSTNAME) == *"stampede2"* ]]; then 
#    currentWD="/work/04920/tg842191/stampede2/scratch/SideProjectHuber"
#    
#fi

# Defining paths within this directory -- REDEFINED FROM MAIN VARIABLES
path2Scripts="${PATH2SCRIPTS}" # Where all script files are stored
path2Sim="${PATH2SIM}" # Where all simulations are stored
path2Submit="${PATH2SWITCHSUBMIT}" # Where all submit.sh scripts are stored

# Defining general submission script name
output_submit_name="submit.sh" # Usually does not change, because of Monitoring jobs script!
# general_submit_name="submit_expanded_box.sh" # Usually does not change, because of Monitoring jobs script!
copy_submit_name="submit_copy.sh" # Copies submission script to this if there is already a submission script

#########################
### FINDING HOST NAME ###
#########################
find_hostname_info ServerKeyWord

if [ -z "${ServerKeyWord}" ]; then
    echo "Error! Sever keyword not found. Please check find_hostname_info in the server_general_research_function.sh within the bin/bashfiles!"

else
    ## PRINTING
    echo "SERVER HOST NAME FOUND: ${ServerKeyWord}"
    echo "STOP HERE IF AN ERROR IS FOUND!"
fi

##################################
### FINDING SUBMIT SCRIPT TYPE ###
##################################

## DEFINING DESIRED SWITCH TYPE
switch_type="${2:-GENERAL}" 
    ## GENERAL: Simply copy and replace submission based on your submit file
    ## FEP: Free energy perturbation switching requires switching of multiple submit files
    ## US: Same as FEP (Different submission script)
    ## EXPAND_BOX: Expand box submission code
    
## DEFINING LOGICAL
want_batch="${3:-False}" # False if you do not want the jobs to batched together
# True if you want batched jobs
    
echo -e "\n--------------------------------"
echo "switch type selection: ${switch_type}"
echo "Pausing here... make sure switch type is correct!"
if [[ "${want_batch}" == "True" ]]; then
    echo "** BATCHING process is turned ON **"
else
    echo "** BATCHING process is turned OFF **"
fi
echo "Possible switch types: FEP, US, GENERAL, EXPAND_BOX, MOST_LIKELY"
sleep 5

## DEFINING SUBMISSION FILE
if [ "${switch_type}" == "GENERAL" ]; then
    submitFile="submit.sh"
elif [ "${switch_type}" == "EXPAND_BOX" ]; then
    submitFile="submit_expanded_box.sh"
elif [ "${switch_type}" == "MOST_LIKELY" ]; then
    submitFile="submit_KL_most_likely_config.sh" # Currently only on stampede
## FREE ENERGY PERTURBATION
elif [ "${switch_type}" == "FEP" ]; then
    submitFile="free_energy_perturbation_submit.sh"
    
    ## EDITING FOR STAMPEDE
    if [[ "${ServerKeyWord}" == "STAMPEDE" ]]; then
        submitFile="free_energy_perturbation_submit_knights_landing.sh"
    fi
    
    if [[ "${ServerKeyWord}" == "CHTC_HPC" ]]; then
        submitFile="free_energy_perturbation_submit_16_cores.sh"
    fi
    
## UMBRELLA SAMPLING
elif [ "${switch_type}" == "US" ]; then
    submitFile="umbrella_sampling_submit.sh"
fi

## FULL PATH TO SUBMISSION FILE
fullpath2submitFile="$path2Submit/$ServerKeyWord/$submitFile"

## PRINTING
echo -e "\n--------------------------------"
echo "Based on your server key word (${ServerKeyWord}) and type (${switch_type}), "
echo "We have selected the following submission script: ${submitFile}"
echo "Submission script path: ${fullpath2submitFile}"
echo "Stop the script here if this is wrong! Pausing for 3 seconds ..."; sleep 3

# Defining job lists
job_list_file="${path2Scripts}/job_list.txt"

# Looping through each main simulation directory
for currentSimdir in "${Files_Within_Sim[@]}"; do
    fullpath2Sim=${path2Sim}/${currentSimdir}
    echo "Full path to sim is: ${fullpath2Sim}"
    
    # Looping through each file within the simulation directory
    for currentdir in $(ls -d ${fullpath2Sim}/*/ -d); do
    
        echo "Current Directory is: ${currentdir}" # Full path
        
        ###########################
        ### GENERAL SWITCH TYPE ###
        ###########################
        
        if [[ "${switch_type}" == "GENERAL" ]] || [[ "${switch_type}" == "EXPAND_BOX" ]] || [[ "${switch_type}" == "MOST_LIKELY" ]]; then
            # Checking if there is already a submission script. If so, copy it and leave it as submit_copy.sh
            if [ -e ${currentdir}/${output_submit_name} ]; then
                echo "${output_submit_name} is already available! Copying to prevent overwrite!" 
                echo "Backing up ${output_submit_name} to ${copy_submit_name}"
                cp ${currentdir}/${output_submit_name} ${currentdir}/${copy_submit_name}
            fi

            # Correcting for the job name
            job_name=$(basename ${currentdir} | sed "s/mdRun\_//g")
            
            ## UPDATING JOBN AME
            sed "s/JOB_NAME/${job_name}/g" ${fullpath2submitFile} > "${currentdir}/${output_submit_name}"

            # Printing what we did
            echo -e "Adding ${submitFile} from ${fullpath2submitFile} to ${currentdir} \n"

            ## ADDING TO JOB LIST
            echo "${currentdir}" >> ${job_list_file}
        #######################
        ### FEP SWITCH TYPE ###
        #######################
        elif [[ "${switch_type}" == "FEP" ]] || [[ "${switch_type}" == "US" ]]; then
                
            ## DEFINING SUBMIT FILE TO LOOK FOR
            # fep_input_submit="submit_*.sh"
            fep_input_submit="extend_submit_*.sh"
            fep_input_submit_backup_folder="${currentdir}/backup_submit_folder"
            ## CREATING BACKUP FOLDER
            mkdir -p "${fep_input_submit_backup_folder}"
            
            ## CREATING COUNTER
            counter="0"
            
            ## BATCHING TURNED ON
            if [[ ${want_batch} == "True" ]]; then
                batch_submit="batch_submit.sh"
                
                ## CHECKING IF YOU ARE IN CHTC AND HAVE A PURE CASE
                currentdir_basename="$(basename ${currentdir})"
                
                # [[ "${currentdir_basename}" == *"_Pure" ]] && 
                if [[ "${ServerKeyWord}" == "CHTC_HPC" ]]; then
                    
                    ## USE DIFFERENT BATCH SUBMIT
                    batch_submit="batch_submit_Pure.sh"
                    # echo "Using different submission for pure cases: ${batch_submit}"
                fi
                
                # Defining full path to the current submission file
                fullpath2batch_file="${path2Submit}/${ServerKeyWord}/${batch_submit}"
                ## COPYING SUBMISSION FILE
                cp -r "${fullpath2batch_file}" "${currentdir}"
                ## DEFINING PATH TO BATCH FILE
                path_batch_file="${currentdir}${batch_submit}"
                ## DEFINING FOLDER TO BATCH SUBMISSION
                batch_folder_name="batch_submit"
                batch_folder="${currentdir}${batch_folder_name}"
                ## CREATING FOLDER
                mkdir -p "${batch_folder}"
            fi
            
            ## UPDATING FOR STAMPEDE
#             if [[ "${currentdir_basename}" == *"_Pure" ]] && [[ "${ServerKeyWord}" == "STAMPEDE" ]]; then
#                 submitFile="free_energy_perturbation_submit_knights_landing_Pure.sh"
#                 ## REDEFINING FULL PATH
#                 fullpath2submitFile="$path2Submit/$ServerKeyWord/$submitFile"
# #            else
# #                submitFile="free_energy_perturbation_submit_knights_landing_Pure.sh"
# #                ## REDEFINING FULL PATH
# #                fullpath2submitFile="$path2Submit/$ServerKeyWord/$submitFile"
#             fi
                
            ## LOOPING THROUGH EACH SUBMISSION FILE
            for each_submit_file in $(ls ${currentdir}${fep_input_submit}); do
                ## DEFINING NAME OF SUBMIT FILE
                each_submit_file_name="$(basename "${each_submit_file}")"
                ## PRINTING
                echo "CORRECTING SUBMIT FILE: ${each_submit_file_name}"
                ## DEFINING TEMPORARY SUBMIT FILE
                fep_temp_submit_file="${currentdir}/${each_submit_file_name%.sh}_temp.sh"
                ## BACKING UP FILE
                cp "${each_submit_file}" ${fep_input_submit_backup_folder}
                ## SEARCHING LINES FOR EDITABLE REGIONS
                fep_header_line_num="$(grep -n "###SERVER_SPECIFIC_COMMANDS_START" ${each_submit_file} | sed 's/\([0-9]*\).*/\1/')"
                fep_ending_line_num="$(grep -n "###SERVER_SPECIFIC_COMMANDS_END" ${each_submit_file} | sed 's/\([0-9]*\).*/\1/')"
                ## CREATING TEMP FILE AND RECOMPILING THE SUBMIT FILE
                ### HEADER
                head -n"${fep_header_line_num}" "${each_submit_file}" > "${fep_temp_submit_file}"
                ### MIDDLE
                cat "${fullpath2submitFile}" >> "${fep_temp_submit_file}"
                ### END
                tail -n +"${fep_ending_line_num}" "${each_submit_file}" >>"${fep_temp_submit_file}"
                ## CHECK IF BATCHING IS DESIRED
                if [[ ${want_batch} != "True" ]]; then
                    ## ADDING TO JOB LIST
                    echo "${each_submit_file}" >> ${job_list_file}
                else

                    ## ADDING COMMANDS FOR EACH SUBMISSION SCRIPT USING TEMPORARY SUBMISSION FILE
                    echo "#!/bin/bash" > "${batch_folder}/batch_${each_submit_file_name%.sh}.sh"
                    tail -n +"${fep_ending_line_num}" "${each_submit_file}" >> "${batch_folder}/batch_${each_submit_file_name%.sh}.sh"
                    ## ADDING COMMAND TO BATCH SUBMIT
                    echo "# LAUNCH ON NODE ${counter}" >> ${currentdir}${batch_submit}
                    echo "srun -lN1 -n1 -r ${counter} bash "${batch_folder_name}/batch_${each_submit_file_name%.sh}.sh" > slurm-\${SLURM_JOB_ID}_${counter}.out &" >> "${path_batch_file}"
                    echo ""  >> "${path_batch_file}"
                    
                    ## ADDING TO COUNTER
                    counter=$(( ${counter} + 1))
                    
                fi
                ## COPYING SUBMISSION FILE OVER
                cp -r "${fep_temp_submit_file}" "${each_submit_file}"
                ## CLEANING UP TEMPORARY SUBMIT FILE
                rm "${fep_temp_submit_file}"
                ## UPDATING THE NAME OF THE JOB
                job_name="$(basename ${currentdir})_${each_submit_file_name%.sh}"
                sed -i "s/JOB_NAME/${job_name}/g" "${each_submit_file}"

            done
            
            ## CORRECTING FOR NUMBER OF NODES FOR EACH SCRIPT
            if [[ ${want_batch} == "True" ]]; then
                ## COMPUTING TOTAL NODES
                total_nodes="$(( ${counter} ))"
                ## DEFINING JOB NAME
                job_name="$(basename ${currentdir})_batchnode_${total_nodes}"
                ## USING SED TO CORRECT BATCH JOB
                sed -i "s/_JOBNAME_/${job_name}/g" "${path_batch_file}"
                sed -i "s/_NUMNODES_/${total_nodes}/g" "${path_batch_file}"
                ## ADDING WAIT COMMAND TO BASH FILE
                echo "## WAIT FOR ALL PROCESSES TO FINISH" >> "${path_batch_file}"
                echo "wait" >> "${path_batch_file}"
                ## ADDING FULL PATH TO JOB LIST
                echo "${path_batch_file}" >> ${job_list_file}
            fi
            
        else
            echo "Error! No switch type found for: ${switch_type}"
            echo "Stopping here!"
            echo "Please check the switchsubmit.sh script to ensure that you have a switch type of that kind"
            exit
        fi
        
    done
    
done

### PRINTING SUMMARY ###
echo "----- SUMMARY----- "
echo "Replaced ${output_submit_name} with ${ServerKeyWord} server scripts"
echo "Simulation paths replaced: ${Files_Within_Sim[@]}"
echo "Re-added the jobs to ${job_list_file}"
echo "Good luck with the job submission!"