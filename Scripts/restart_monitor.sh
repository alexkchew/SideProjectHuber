#!/bin/bash

# restart_monitor.sh
# (Break off from Monitor_Jobs.sh)
# Written by Alex Chew (Updated 04/07/2017)
# The purpose of this script is to look into the job_list and see if we still have jobs to submit. The idea is that this script will monitor the job processing, so I can do other things like analysis tools.

# -- Updates -- #
# 170425 - Added option to change max threshold

# This script will use two text files: one for job list, another to keep track the submitted jobs. The job script assumes you are using slurm. Hopefully, we can have the job submission automated. For the SWARM server, we should submit no more than three jobs at one time for this.

# Now used for resetting jobs

# Usage: bash restart_monitor.sh maxThreshold
# $1: maximum number of jobs at once

# Defining important directories/files
currentWD="/home/akchew/scratch/SideProjectHuber" # Current working directory
scriptDir="$currentWD/Scripts" # Script directory
job_list_file="$scriptDir/reset_job.txt"
submit_script="reset_job.sh"
NetID="akchew" # Used to specify user for squeue

# Defining output
job_complete_file="reset_job_completed.txt" # Stores which job has been submitted

# Count total number of jobs at this time
maxThresholdJob="$1" # only 6 jobs at a time <-- temporary for CHTC
currentTaken=$(squeue -u $NetID | tail -n+2 | wc -l)
sleepTime=60 # Seconds for sleep in until loop

# Using an until loop to keep running the script
until [ -z $(head -1 "$job_list_file") ]; do # Until the job_list is empty

currentTaken=$(squeue -u $NetID | tail -n+2 | wc -l)
# Checking if we can submit a job
while [ "$currentTaken" -lt "$maxThresholdJob" ]; do

    # First, we need to look into job_list
    # Getting first line
    firstLine=$(head -1 "$job_list_file")

    # Check if the first line is there
    if [ -z "$firstLine" ]; then
        echo "No more jobs in $job_list_file"
        echo "Stopping Monitor_jobs.sh"
        exit
    fi

    # Going to file and submitting
    cd "$firstLine"
    echo "Submitting $firstLine"
    sbatch "$submit_script"

    # Copying the first line to completed job
    echo "$firstLine" >> "$scriptDir/$job_complete_file"

    # Deleting first line to prevent overwriting
    sed -i "1d" "$job_list_file"

currentTaken=$(squeue -u $NetID | tail -n+2 | wc -l)
done

echo "Sleeping for $sleepTime at $(date)"
sleep $sleepTime

done

echo "Jobs are completed. Check out $job_complete_file"