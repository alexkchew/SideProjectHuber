#!/bin/bash

# One_System_Prep_free_energy.sh
# Written by Alex Chew (06/16/2017)
# The purpoes of this script is to take your water box, add an ion, and prepare for a free energy calculation

# HCl is from the following reference:
# Bonthuis, D. J., Mamatkulov, S. I. & Netz, R. R. Optimization of classical nonpolarizable force fields for OH- and H3O+. J. Chem. Phys. 144, (2016).


# Usage: bash One_System_Prep_free_energy.sh

##* -- Updates -- *##
# 20170617: Added variable ion to add (hydronium)
# 20170618: Enable turning off EnerPres -- Correction for energy and pressure
# 20180311: Working on getting this script to enable solute insertion
# 20180315: Working to get script to enable copying of gro files from other directories -- remade this script in "One_System_Prep_continuation_free_energy_sh

## RELOADING BIN FILE
source "bin/bash_rc.sh"

### Functions ###

# -- USER DEFINED PARAMETERS -- #
## SETTING PREFIX FOR ALL FILES
output_prefix="mixed_solv" # Prefix for gro and top files

## DEFINING DETAILS OF THE INPUT
solute_name="propanal" # "acetone"
solute_residue_name="PRO" # "ACE"
system_temp="433.15" # Temperature in K

# Defining number of water molecules
num_solv_one="506" # Number of molecules used in   #4 nms

# Defining water model
water_model="spce" # tip3p, spce, spc, etc. (original tip3p) "spce"

## DEFINING OUTPUT DIRECOTRY
output_dir="FEP_${system_temp}_${solute_name}_${num_solv_one}_${water_model}"

### DEFINING IMPORTANT DIRECTORIES (BEGIN) ###
## SPECIFYING INPUT FILES
currentPATH2PREP="${PATH2PREP}/prep_free_energy" # Preparation directory for this script
hydrogen_chloride_dir="${INPUT_DIR}/hydronium_chloride"

## DEFINING GRO + ITP FILES
input_solute_path="${PREP_SOLUTE_DIR}/${solute_name}" # path to solute folder
input_gro_name="${solute_name}.pdb"
input_gro_path="${input_solute_path}/${input_gro_name}"
input_prm_name="${solute_name}.prm"
input_prm_path="${input_solute_path}/${input_prm_name}"
input_itp_name="${solute_name}.itp"
input_itp_path="${input_solute_path}/${input_itp_name}"

# Defining input gro and input topology file
prep_output_dir="$currentPATH2PREP/${num_solv_one}_${water_model}"
input_gro="$prep_output_dir/${output_prefix}_equil.gro"
input_top="$prep_output_dir/${output_prefix}.top"

# Solute / solvent / forcefield /mdp files
solute_dir="${INPUT_DIR}/solutes" # Solute directory
solvent_dir="${INPUT_DIR}/solvents" # Solvent directory
forcefield_dir="${INPUT_DIR}/charmm36-nov2016.ff"
specific_MDP_DIR="${MDP_DIR}/free_energy" # mdp file for free energy calculation

# Submission files
submit_per_lambda="${SUBMISSION_DIR}/submit_free_energy_per_lambda.sh"
output_submit="submit_lambda.sh" # name of output script
full_submit_script="${SUBMISSION_DIR}/submit_free_energy_full.sh"
output_submit_full="submit.sh" # Matching Monitor job

## GMX Insert Parameters
numOfAddition="1" # Number you would like to add
numTries="100000000" # A lot of tries for gmx-insert
expand_insert_dist="0.1" # nms, used for expansion, insertion, then allow NPT to take over

# Defining MDP files
mdp_em_1="${specific_MDP_DIR}/em_steep.mdp"
mdp_em_2="${specific_MDP_DIR}/em_l-bfgs.mdp"
mdp_nvt="${specific_MDP_DIR}/nvt.mdp"
mdp_npt="${specific_MDP_DIR}/npt.mdp"
mdp_md="${specific_MDP_DIR}/md.mdp"

# Creating array
declare -a mdp_array=(${mdp_em_1} ${mdp_em_2} ${mdp_nvt} ${mdp_npt} ${mdp_md})

# Finding number of lamdba values
numLambda=$(grep "vdw_lambdas" ${mdp_md} | cut -d "=" -f 2 | wc -w) # 
numLambda_accountzero=$(( $numLambda-1 )) # Accounting that we start from 0

# Check if lambda is empty
if [ -z "$numLambda" ]; then
    echo "Error!!! Incorrect number of lambda values. Check ${mdp_md}"
    echo "Exiting now..."
    exit
else
    echo "Total number of lambdas: ${numLambda}"
fi

# Re-defining output file to go into Simulations folder
output_dir="$PATH2SIM/${output_dir}"

### DEFINING IMPORTANT DIRECTORIES (END) ###

# Checking if the output directory exists
if [ -e ${output_dir} ]; then
    echo "$output_dir already exists, removing duplicates!"
    echo "Waiting 5 seconds until deletion..."
    sleep 5
    rm -r $output_dir
fi

# Creating directory for us to work in
mkdir -p ${output_dir}; echo "Creating directory: ${output_dir}"; sleep 2

# Going into directory
cd "$output_dir"

# Creating directory structure
output_molecule="${output_dir}/1_Molecule" # Location for the molecule GRO, etc.
output_mdp="${output_dir}/2_MDP" # Location of MDP files
output_ff="${output_dir}/3_Force_Field" # Location of force field files
output_lambda="${output_dir}/4_lambda" # Location of lambda files (each step)
output_submission="${output_dir}/5_Submission_Files" # Location of Submission Files
output_analysis="${output_dir}/6_Analysis" # Location of Analysis (empty)

# Creating directory structure
mkdir -p ${output_molecule} ${output_mdp} ${output_ff} ${output_lambda} ${output_submission} ${output_analysis}

### Molecule ###
# Defining output
output_top="${output_molecule}/${output_prefix}.top"
# Copy gro and topology file
cp -r ${input_gro} ${output_molecule}/${output_prefix}_clean.gro
cp -r ${input_prm_path} ${output_molecule}
cp -r ${input_top} ${output_top}

# Expanding gro box and adding molecules
expandBox ${output_molecule}/${output_prefix}_clean.gro ${expand_insert_dist}

# Now, adding ion into gro file
gmx insert-molecules -f ${output_molecule}/${output_prefix}_clean.gro -ci $input_gro_path -nmol $numOfAddition -try $numTries -o ${output_molecule}/${output_prefix}.gro

# Adding to topology
echo "$solute_residue_name    $numOfAddition" >> ${output_top}

## Adding ITP file to TOP File ##
# Copying itp file
cp $input_itp_path ${output_molecule}

## EDITING TOPOLOGY TO INCLUDE SOLUTE
# sed -i "s/SOLUTE/${solute_name}/g" $output_top

## Finding line to add to topology file to include parameters for hydrogen chloride after parameters
lineNum2forcefield=$(grep -nr ".prm" "$output_top"  | tail -n1 | grep -Eo '^[^:]+' )
line2add=$(( $lineNum2forcefield+1 ))

sed -i "${line2add}a \ " $output_top
sed -i "${line2add}a #include \"$input_itp_name\"" $output_top # Parameter
sed -i "${line2add}a ; Including itp for solute" ${output_top}

sed -i "${line2add}a \ " $output_top
sed -i "${line2add}a #include \"$input_prm_name\"" $output_top # Parameter
sed -i "${line2add}a ; Including params for solute" ${output_top}

# Cleaning up any additional files
rm ${output_molecule}/\#*

### Force field ###
cp -r ${forcefield_dir} ${output_molecule}

### MDP Files ###
# Now, adding MDP files to respective MDP directories
output_mdp_em1="${output_mdp}/1_EM"
output_mdp_em2="${output_mdp}/2_EM"
output_mdp_nvt="${output_mdp}/3_NVT"
output_mdp_npt="${output_mdp}/4_NPT"
output_mdp_md="${output_mdp}/5_MD"

# Declaing array for output_mdp
declare -a output_mdp_array=($output_mdp_em1 $output_mdp_em2 $output_mdp_nvt $output_mdp_npt $output_mdp_md)

# Making the directories
mkdir -p ${output_mdp_em1} ${output_mdp_em2} ${output_mdp_nvt} ${output_mdp_npt} ${output_mdp_md}

# Sequencing through each MDP file for each alpha value
for currentLambda in $(seq 0 $numLambda_accountzero); do
    # Echoing
    echo "Working on $currentLambda MDP Files"
    
    # MDP Files #
    for currentMDP in $(seq 0 $(( ${#output_mdp_array[@]}-1 ))); do
        # Getting basename of your script
        currentMDPName=$(basename ${mdp_array[currentMDP]})
        currentMDPName_nomdp=${currentMDPName%.*}

        # Copying mdp file
        sed "s/MOLECULETYPE/${solute_residue_name}/g" ${mdp_array[currentMDP]} | sed "s/INITIAL_LAMBDA_STATE/${currentLambda}/g" | sed "s/TEMPERATURE/${system_temp}/g" > ${output_mdp_array[currentMDP]}/${currentMDPName_nomdp}_${currentLambda}.mdp
        
    done
    
    # Submission Files #
    newSubmissionFileName=${output_submit%.*}_${currentLambda}.sh
    
    # Editing file
    sed "s/LAMBDA_VALUE/${currentLambda}/g" ${submit_per_lambda} > ${output_submission}/${newSubmissionFileName}
    sed -i "s#FREE_ENERGY_DIR#${output_dir}#g" ${output_submission}/${newSubmissionFileName}
    
    # Converting to unix
    dos2unix ${output_submission}/${newSubmissionFileName}
    
done

# Finally, copying full submission script
output_dir_name=$(basename $output_dir)
sed "s/LASTALPHA/${numLambda_accountzero}/g" ${full_submit_script} > ${output_dir}/${output_submit_full}
sed -i "s/DIRECTORYNAME/$output_dir_name/g" ${output_dir}/${output_submit_full}


