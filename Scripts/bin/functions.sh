#!/bin/bash

# functions.sh
# This contains all global functions for this project
# Created by: Alex K. Chew (alexkchew@gmail.com, 02/27/2018)

### FUNCTIONS AVAILABLE TO YOU
#   read_gro_box_length: reads gro file's box length

### NAMING CONVENTIONS
#   solvent_effects_find_input_dir_name: finds input directory name for simulations
#   solvent_effects_find_prep_solvent_folder: finds preparation solvent folder
#   solvent_effects_get_output_dir_name: finds the output directory for solvent effects

### EDITING GRO FILE COMMANDS
#   solvent_effects_add_solute: adds solutes for solvent effects project

### USEFUL COMMANDS
# REMOVES EVERYTHING FOR GREP EXCEPT LINE NUMBER: sed 's/\([0-9]*\).*/\1/'

### ANALYSIS COMMANDS
#   truncate_and_center: truncates and center the molecule

## PRINTING
echo "*** LOADING FUNCTIONS (functions.sh) ***"

#### FUNCTIONS ####


#######################################
### FUNCTIONS TO PREPARE A MOLECULE
#######################################


### FUNCTION TO PREPARE MOLECULE FOR CHARMM36
# The purpose of this function is to prepare a molecule for CHARMM36 force field. 
# We are assuming you are at the building directory with a mol2 file and a str file available!
# INPUTS:
#   $1: molecule names
#   $2: residue name
# OUTPUTS:
#   FOLDER WITH MOLECULE NAME AND EVERYTHING IS COMPLETED! (Simply copy itp, pdb, etc.)
## USAGE: prep_charmm36_molecule URE urea
function prep_charmm36_molecule () {
  ## DEFINING VARIABLES
  input_molecule_name="$1"
  input_residue_name="$2"

  ## DEFINING MOLECULE FILES
  input_molecule_str="${input_molecule_name}.str"
  input_molecule_mol2="${input_molecule_name}.mol2"

  ## DEFINING FORCE FIELD
  force_field_folder="charmm36-nov2016.ff"
  python_script="cgenff_charmm2gmx.py"

  ## DEFINING PYTHON ACTIVATION ENVIRONMENT TYPE
  # python_env_type="py27"

  ## CHECKING FILES
  check_input_str="$(check_file_exist ${input_molecule_str})"
  check_input_mol2="$(check_file_exist ${input_molecule_mol2})"
  check_ff_folder="$(check_file_exist ${force_field_folder})"
  check_python_script="$(check_file_exist ${python_script})"

  if [ "${check_input_str}" == "True" ] && [ "${check_input_mol2}" == "True" ] && [ "${check_ff_folder}" == "True" ] && [ "${check_python_script}" == "True" ]; then
    ## PRINTING
    echo "*** RUNNING PREPARATION CHARMM36 ***"
    echo "MOLECULE FULL NAME: ${input_molecule_name}"
    echo "MOLECULE RESIDUE NAME: ${input_residue_name}"

    ## ACTIVATE PYTHON
    # echo "ACTIVATING PYTHON: ${python_env_type}"

    ## FIXING RESIDUE NAMES FOR STR FILE
    echo "FIXING RESIDUE NAMES OF MOL2 AND STR TO: ${input_residue_name}"
    ## FINDING RESIDUE NAME IN STR FILE
    current_residue_name="$(grep "\bRESI\b" ${input_molecule_str} |awk '{print $2}' | sed -e 's/[]\/$*.^[]/\\&/g')"
    if [ "${current_residue_name}" != "${input_residue_name}" ]; then
        echo "STR FILE: CORRECTING RESIDUE NAME ${current_residue_name} to ${input_residue_name}" ## TODO: Check out what is going on here with sed -- str may not be sufficient
        sed -i "s/${current_residue_name}/${input_residue_name}/g" ${input_molecule_str}
    fi

    ## FIXING RESIDUE NAMES FOR MOL2 IFILE
    # current_residue_name=$(head -2 ${input_molecule_mol2} |tail -1 |awk '{print $1}')
    current_residue_name="$(mol2_find_residue_name ${input_molecule_mol2} | sed -e 's/[]\/$*.^[]/\\&/g')"
    if [ "${current_residue_name}" != "${input_residue_name}" ]; then
        echo "MOL2 FILE: CORRECTING RESIDUE NAME ${current_residue_name} to ${input_residue_name}"
        sed -i "s/${current_residue_name}/${input_residue_name}/g" ${input_molecule_mol2}
    fi
    ## ACTIVATING PYTHON ENVIRONMENT
    # source activate "${python_env_type}"

    ## RUNNING CGENFF
    echo "RUNNING COMMAND: ./${python_script} "${input_residue_name}" "${input_molecule_mol2}" "${input_molecule_str}" "${force_field_folder}""
    python ${python_script} "${input_residue_name}" "${input_molecule_mol2}" "${input_molecule_str}" "${force_field_folder}"

    ## CONVERTING THE MOLECULE NAME TO LOWERCASE (since python script by default does this!)
    input_residue_name_lowercase="$(echo ${input_residue_name} |tr '[:upper:]' '[:lower:]')"

    ## CHECKING IF FILE EXISTS
    if [ ! -e "${input_residue_name_lowercase}.itp" ] || [ ! -e "${input_residue_name_lowercase}.prm" ] || [ ! -e "${input_residue_name_lowercase}_ini.pdb" ];
    then
        echo "Error! One or more of the following files do not exist:"
        echo "ITP file: ${input_residue_name_lowercase}.itp"
        echo "PDB file: ${input_residue_name_lowercase}_ini.pdb"
        echo "PRM file: ${input_residue_name_lowercase}.prm"
        echo "There may be something wrong with running the python script! Please check for error output, stopping here!"
    else
        ## CREATING DIRECTORY AND COPYING ALL THE FILES
        echo "CREATING DIRECTORY: ${input_molecule_name}"
        mkdir -p ${input_molecule_name}
        mkdir -p ${input_molecule_name}/prep_files

        ## MOVING FILES
        mv {${input_molecule_str},${input_molecule_mol2}} ${input_molecule_name}/prep_files
        ## RENAMING THE OUTPUTS ACCORDINGLY
        mv ${input_residue_name_lowercase}.top ${input_molecule_name}/${input_molecule_name}.top
        mv ${input_residue_name_lowercase}.itp ${input_molecule_name}/${input_molecule_name}.itp
        mv ${input_residue_name_lowercase}.prm ${input_molecule_name}/${input_molecule_name}.prm
        mv ${input_residue_name_lowercase}_ini.pdb ${input_molecule_name}/${input_molecule_name}.pdb
        # mv {${input_residue_name_lowercase}.*,${input_residue_name_lowercase}_ini.pdb} ${input_residue_name_lowercase}
        ## PRINTING
        echo "COMPLETION -- MOVED ALL PREPARATION FILES TO: ${input_molecule_name}/prep_files"
        echo "MOVED ALL ITP, PRM, AND TOP FILES TO: ${input_molecule_name}"
    fi
  else
    echo "ERROR! There was something wrong with your input!"
    echo "The default function is: prep_charmm36_molecule MOLECULE_NAME RESIDUE_NAME"
    echo "Exiting here..."
  fi
}

### FUNCTION TO ADD MOLECULE
# The purpose of this function is to add a molecule to the correct directory
# We are assuming you have already used 'prep_charmm36_molecule' code.
# INPUTS:
#   $1: molecule name
#   $2: solutes or solvents (depending on which one you are adding)
# OUTPUTS:
#   This will copy over files from itp prm, etc.
function solvent_effects_add_molecule () {
    ## DEFINING INPUT VARIABLES
    input_molecule_name="$1"
    molecule_type="${2-solvents}"

    ## DEFINING PATH
    path_to_molecule="${INPUT_DIR}/${molecule_type}"

    ## MAKING DIRECTORY
    mkdir -p ${path_to_solvent}/${input_molecule_name}/setup_files

    ## COPYING
    cp -rv "${input_molecule_name}/"{${input_molecule_name}.itp,${input_molecule_name}.prm,${input_molecule_name}.pdb} ${path_to_solvent}/${input_molecule_name}/setup_files
}


### FUNCTION TO FIND BOX LENGTH BASED ON GRO FILE
# INPUTS:
#   $1 is the gro file
# OUTPUTS:
#   box_length: length of the box
#   box_volume: volume of the box
# USAGE: read box_length box_volume <<< "$(read_gro_box_length $GRO_FILE)"
function read_gro_box_length () {
    # DEFINING VARIABLES
    gro_file="$1"
    # FINDING BOX LENGTH
    box_length=$(tail ${gro_file} -n1 | awk '{print $1}')
    # FINDING BOX VOLUME
    box_volume=$(echo "${box_length}^3" | bc -l )
    # PRINTING
    echo "${box_length} ${box_volume}"
}
### FUNCTION TO EXPAND THE BOX
# INPUTS:
    # $1: GRO File
    # $2: Amount to expand in nm's (assuming cubic/rectangular)
# OUTPUTS:
    # Expanded gro file
function expandBox () {
    # The purpose of this function is that it takes the gro file and expands it by a given value
    # Input files
    gro_file="$1"
    expansionValue="$2" # Amount expanded in nms
    ## SEEING IF THERE IS ANY EXPANSION
    if (( $(awk 'BEGIN {print ("'${expansionValue}'" != "'0'")}') )); then # [ "${expansionValue}" -ne "0" ]
        # Get box vector and splitting into components
        output=$(tail -n 1 $gro_file)

        # create as array, can now access each component separately:
        array=($output)
        box_x=${array[0]}
        box_y=${array[1]}
        box_z=${array[2]}

        # Now, adding to your x, y, and z:
        new_x=$(awk "BEGIN {print $box_x + $expansionValue; exit}")
        new_y=$(awk "BEGIN {print $box_y + $expansionValue; exit}")
        new_z=$(awk "BEGIN {print $box_z + $expansionValue; exit}")

        # Using editconf to change the file (no center, box)
        gmx editconf -f "$gro_file" -o "$gro_file" -box $new_x $new_y $new_z -noc &>/dev/null

        # Printing what you did
        echo "--- Running expandBox function ---"
        echo "Changing the following gro file: ${gro_file}"
        echo "Changing x box size from ${box_x} to ${new_x}"
        echo "Changing y box size from ${box_y} to ${new_y}"
        echo "Changing z box size from ${box_z} to ${new_z}"
    else
        echo "Since the expansion was set to: ${expansionValue}, we are not doing anything!"
        echo "No expansion of the gro file was made!"
    fi
}

### FUNCTION TO CALCULATE MOLECULAR WEIGHT
# INPUTS:
#   $1: PDB file
# OUTPUTS:
#   Value for molecular weight
## USAGE: calc_pdb_molecular_weight urea_ini.pdb
function calc_pdb_molecular_weight () {
    ## DEFINING INPUT VARIABLE
    input_pdb="$1"
    
    ## REFERENCE FILE
    ref_file="/home/akchew/scratch/SideProjectHuber/prep_mixed_solvent/input_files/solvents/molecular_weights_ref.txt"

    ## GETTING THE ATOM LIST
    atom_list=$(head -n -1 "${input_pdb}" | awk '{print $3}' | sed 's/[0-9]*//g')
    
    ## GETTING REFERENCE FILE DATA WITHOUT COMMENTS
    ref_file_data="$(grep "^[^;]" ${ref_file})"
    
    ## DEFINING MOLECULAR WEIGHT VARIABLE
    molecular_weight=0
    
    ## LOOPING THROUGH THE LIST AND GETTING MOLECULAR WEIGHT, THEN ADDING IT
    while read -r current_atom; do
        ## FINDING THE ATOMIC WEIGHT
        # Removes all comments | finds atom name | prints mass | cuts all white space
        atom_weight=$(grep "^[^;]" "${ref_file}" | grep "${current_atom}" | awk -F',' '{print $2}' | tr -d ' ')        
        
        ## ADDING TO MOLECULAR WEIGHT
        # Adds the weights, removes all issues with windows, then makes math happen
        molecular_weight=$(echo "${molecular_weight} + ${atom_weight}" | tr -d $'\r' | bc )
    done <<< "${atom_list}"
    ## PRINTING MOLECULAR WEIGHT
    echo "${molecular_weight}"
}

##########################
### NAMING CONVENTIONS ###
##########################

### FUNCTION TO GET OUTPUTNAME 
# The purpose of this function is to find the input directory name
## INPUTS:
#   $1: mass fraction of water (e.g. 100)
#   $2: box length (e.g. 6) in nms
#   $3: solute residue name (e.g. ACE)
#   $4: water model (e.g. spce)
#   $5: cosolvent name (e.g. THF)
## OUTPUTS:
#   input_dir: name for input directory
## USAGE: input_dir=$(solvent_effects_find_input_dir_name ...)
function solvent_effects_find_input_dir_name () {
    ## DEFINING INPUT VARIABLES
    water_mass_frac_perc_="$1"
    box_length_="$2"
    solute_residue_name_="$3"
    water_model_="$4"
    cosolvent_name_="$5"
    ## SEEING IF YOU HAVE PURE WATER
    if [ "${water_mass_frac_perc_}" == 100 ]; then
        input_dir_=${box_length_}_nm_${solute_residue_name_}_${water_mass_frac_perc_}_WtPercWater_${water_model}_Pure
    else
        input_dir_=${box_length_}_nm_${solute_residue_name_}_${water_mass_frac_perc_}_WtPercWater_${water_model_}_${cosolvent_name_}
    fi
    ## PRINTING INPUT DIRECTORY
    echo "${input_dir_}"
}


## FUNCTION TO FIND PREPARATION FOLDER FOR MIXED SOLVENTS
# The purpose of this function is to find the preparation folder of a mixed solvent environment
## INPUTS:
#   $1: mass fraction of water (e.g. 0.5)
#   $2: solvent_two_residue_name: solvent 2 residue name (i.e. cosolvent)
## OUTPUTS:
#   prep_dir_name_: preparation directory name, e.g. 100_Water, 0_DIO, 10_Water_DIO
## USAGE: prep_dir_name="$(solvent_effects_find_prep_solvent_folder "${solvent_one_wt_frac}" "${solute_residue_name}")"
function solvent_effects_find_prep_solvent_folder () {
    ## DEFINING INPUT VARIABLES
    solvent_one_wt_frac_="$1"
    solvent_two_residue_name_="$2"
    
    ## FINDING PERCENTAGE OF WATER
    percent_solvent_one_wt_frac_=$(awk -v num=${solvent_one_wt_frac_} 'BEGIN{ printf "%d",num*100}') 
    
# Defining supposed directory name
# Checking if there is a cosolvent
if [ $(awk -v wt_frac=${solvent_one_wt_frac_} 'BEGIN{ if (wt_frac != 1.00 && wt_frac != 0.00) print 1; else print 0}') -eq 1 ]; then
    # Defining preparation directory name
    prep_dir_name_=${percent_solvent_one_wt_frac_}_Water_${solvent_two_residue_name_} 
    
    else
    ## CHECKING IF THERE IS WATER
    if [ $(awk -v wt_frac=${solvent_one_wt_frac_} 'BEGIN{ if (wt_frac == 0.00) print 1; else print 0}') -eq 1 ]; then
        # SITUATION WHERE NO WATER IS PRESENT
        # Defining preparation directory name with pure cosolvent
        prep_dir_name_=${percent_solvent_one_wt_frac_}_${solvent_two_residue_name_} 
            # e.g. 0_DIO

    else # Pure water
        # SITUATION WHEN 100% WATER IS PRESENT
        # Defining prep name with no cosolvent
        prep_dir_name_=${percent_solvent_one_wt_frac_}_Water

    fi
fi
    ## PRINTING
    echo "${prep_dir_name_}"

}


### FUNCTION TO GET THE NAME OF THE OUTPUT DIRECTORY
# The purpose of this function is to get the output directory name for solvent effects project
# The name is based on generalized name of 300.00_6_nm_XYL_HYD_100_WtPercWater_spce_GVL
## INPUTS:
#   $1: temperature of the system (e.g. 300.00)
#   $2: box length (e.g. 6, 7,) in nanometers
#   $3: name of the solute
#   $4: mass fraction of water (fraction, e.g 0.5)
#   $5: water model of your system (e.g. spce)
#   $6: name of the cosolvent (e.g. dioxane, etc.)
## OUTPUTS:
#   output_dir: name of the output directory
## USAGE: solvent_effects_get_output_dir_name 300.00 6 xylitol 1.00 spce dioxane
function solvent_effects_get_output_dir_name () {
    ## DEFINING THE INPUTS
    temperature_="$1"
    box_length_="$2"
    solute_name_="$3"
    solvent_water_mass_fraction_="$4"
    water_model_="$5"
    cosolvent_name_="$6"
    
    ## DEFINING NO SOLUTE NAME
    No_solute_name="NoSolute"
    
    ## COMPUTING SOLVENT WATER MASS FRACTION
    percent_solvent_one_wt_frac_=$(awk -v num=${solvent_water_mass_fraction_} 'BEGIN{ printf "%d",num*100}') 
    
    ## DEFINING OUTPUT DIRECTORY
   if [  "${solute_name_}" != "None" ]; then    output_dir="${temperature_}_${box_length_}_nm_${solute_name_}_${percent_solvent_one_wt_frac_}_WtPercWater_${water_model_}_${cosolvent_name_}"
   else
 output_dir="${temperature_}_${box_length_}_nm_${No_solute_name}_${percent_solvent_one_wt_frac_}_WtPercWater_${water_model_}_${cosolvent_name_}"
   fi
    
    ## DEFINING DIRECTORY IF YOU ONLY HAVE PURE WATER
    if [ $(awk -v wt_frac=${solvent_water_mass_fraction_} 'BEGIN{ if (wt_frac == 1.00) print 1; else print 0}') -ne 0 ]; then
           if [  "${solute_name_}" != "None" ]; then 
           output_dir="${temperature_}_${box_length_}_nm_${solute_name_}_${percent_solvent_one_wt_frac_}_WtPercWater_${water_model_}_Pure"
           else 
            output_dir="${temperature_}_${desired_box_length}_nm_${No_solute_name}_${percent_solvent_one_wt_frac_}_WtPercWater_${water_model}_Pure"
           fi
    fi
    ## PRINTING OUTPUT DIRECTORY
    echo "${output_dir}"
}


################################
### EDITING GRO FILE SCRIPTS ###
################################

### FUNCTION TO ADD A MOLECULE
# The purpose of this function is to add a solute to a pre-made box. 
# We will expand the gro file, add the solute, then update the topology. 
# Note that this function assumes that you ahve a solute in the PREP_SOLUTE_DIR. By default, we will try a large number of tries
# INPUTS:
#   $1: solute_name_: name of the solute
#   $2: solute_res_name_: solute residue name
#   $3: num_solute_: number of solutes to add
#   $4: input_gro_: input gro file name
#   $5: output_gro_: output gro file name
#   $6: expand_insert_dist_: distance in nm to expand the  box
#   $7: output_top_: output topology file
#   $8: scale_: scale to mulitple VDW radius
# OUTPUTS:
function solvent_effects_add_solute () {
    ### DEFINING INPUTS
    solute_name_="$1"
    solute_res_name_="$2"
    num_solute_="$3"
    input_gro_="$4"
    output_gro_="$5"
    expand_insert_dist_="$6"
    output_top_="$7"
    scale_="$8"

    ## DEFINING NUMBER OF TRIES TO ADD THE SOLUTE
    numTries=100000000 # A lot of tries
    
    ## COPYING OVER THE INPUT TO OUTPUT GRO FILE
    cp -r "${input_gro_}" "${output_gro_}"
    
    ## PRINTING
    echo "Adding ${solute_name_} into mixture"

    ## DEFINING PATH TO PDB FILE OF SOLUTE
    path2solutepdb="${PREP_SOLUTE_DIR}/${solute_name_}/${solute_name_}.pdb" # .gro

    ## EXPANDING GRO FILE TO MAKE SURE THE MOLECULE WILL FIT
    expandBox ${output_gro_} ${expand_insert_dist_}
    
    ## RUNNING GMX INSERTION
    if [ -z "${scale_}" ]; then
        gmx insert-molecules -f ${output_gro_} -ci ${path2solutepdb} -nmol ${num_solute_} -try ${numTries} -o ${output_gro_} # %.gro
    else
        gmx insert-molecules -f ${output_gro_} -ci ${path2solutepdb} -nmol ${num_solute_} -try ${numTries} -o ${output_gro_} -scale "${scale_}"
    fi

    ## CHECKING IF GMX INSERTION WORKED
    if [ ! -e ${output_gro_} ]; then # %.gro
        echo "Error!!! Insertion is incomplete. Please attempt to increase the number of tries!"
        echo "Stopping here at $(pwd)"
        exit
    fi

    ## FIXING TOPOLOGY FILE
    # Adding number of solutes
    echo "${solute_res_name_}    ${num_solute_}" >> ${output_top_}

}


### FUNCTION TO TRUNCATE AND CENTER
# The purpose of this function is to truncate and center a trajectory
# INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: residue name
#   $4: system name
#   $5: output xtc file
#   $6: beginning trajectory time
#   $7: end trajectory time
# OUTPUTS:
function truncate_and_center ()
{
    ## DEFINING  VARIABLES
    input_tpr_file="$1"
    input_xtc_file="$2"
    res_name="$3"
    system_name="$4"
    output_xtc_file="$5"
    beginning_time_ps="$6"
    end_time_ps="$7"
    ## DEFINING INDEX FILE
    index_file="index.ndx"

### CREATING INDEX FILE
gmx make_ndx -f "${input_tpr_file}" -o "${index_file}" << INPUT
q
INPUT

### CHECKING IF THE RESIDUE IS WITHIN (Checks if string is empty)
if [ -z "$(grep ${res_name} ${index_file})" ]; then
gmx make_ndx -f "${input_tpr_file}" -o ${index_file} << INPUT
r ${res_name}
q
INPUT
fi

## RUNNING CENTERING
gmx trjconv -s "${input_tpr_file}" \
            -f "${input_xtc_file}" \
            -o "${output_xtc_file}" \
            -pbc mol -center \
            -n ${index_file} \
            -b "${beginning_time_ps}" \
            -e "${end_time_ps}" << INPUT
${res_name}
${system_name}
INPUT

}

### FUNCTION TO READ THE FILE NAME
# The purpose of this function is to extract the name:
#   mdRun_433.15_6_nm_CYO_25_WtPercWater_spce_dmso
# This should output the temperature, box size, wt% water, residue names
# INPUTS:
#   $1: input name
# OUTPUTS:
#   output_array: output array  
#   433.15 6 25 dmso CYO
# USAGE:
#   read -a extract_array <<< $(extract_output_name ${name})
function extract_output_name () {
    ## DEFINING INPUT NAME
    input_name_="$1"
    # "mdRun_433.15_6_nm_CYO_25_WtPercWater_spce_dmso"
    
    ## SPLITTING ARRAY
    my_array=($(echo ${input_name_} | tr "_" "\n")) #"_"
    # mdRun 433.15 6 nm CYO 25 WtPercWater spce dmso
    # 0     1      2 3  4   5  6           7    8
    ## SPLITTING DETAILS
    temp_="${my_array[1]}"
    box_size_="${my_array[2]}"
    water_wt_perc="${my_array[5]}"
    cosolvent_res_name="${my_array[8]}"
    solute_res_name="${my_array[4]}"
    
    ## CORRECTING IF COSOLVENT IS GVL
    if [[ "${cosolvent_res_name}" == "GVL" ]]; then
        cosolvent_res_name="GVL_L"
    fi
    
    ## DECLARING OUTPUT ARRAY
    declare -a output_array=("${temp_}" \           
                             "${box_size_}" \      
                             "${water_wt_perc}" \  
                             "${cosolvent_res_name}" \
                             "${solute_res_name}" 
                             )
    
    ## PRINTING
    echo "${output_array[@]}"

}


