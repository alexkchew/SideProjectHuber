#!/bin/bash

# bash_rc.sh
# This folder contains all functions / global variables that is required for this project.

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="functions.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

## PRINTING
echo "-------- Reloading bash_rc.sh --------"

main_dir_path="${main_dir}/../../" # Current working directory

### MAIN DIRECTORIES
export CURRENTWD=$(cd ${main_dir_path}; pwd)

## CHECKING TO SEE IF WE ARE ON STAMPEDE
if [[ $(echo $HOSTNAME) == *"stampede"* ]]; then # Check if we are at the CHTC Supercomputers
    CURRENTWD="${WORK}/scratch/SideProjectHuber"
fi

## DEFINING DIRECTORIES
PATH2SCRIPTS="${CURRENTWD}/Scripts" # Where all script files are stored
PATH2BIN="${PATH2SCRIPTS}/bin" # Where all important global variables are stored

### LOADING GLOBAL VARIABLES
source "${PATH2BIN}/global_vars.sh"

### LOADING GLOBAL FUNCTIONS
source "${PATH2BIN}/functions.sh"

## DEFINING GENERAL RESEARCH PATH
GENERAL_RESEARCH_FUNCTIONS="${HOME}/bin/bashfiles/server_general_research_functions.sh"

## DEFINING SOLVENT EFFECTS PATH
SOLVENT_EFFECTS_FUNCTIONS="${HOME}/bin/bashfiles/solvent_effects_rc.sh"


### LOADING GLOBAL FUNCTIONS
source "${GENERAL_RESEARCH_FUNCTIONS}"
source "${SOLVENT_EFFECTS_FUNCTIONS}"