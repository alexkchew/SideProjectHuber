#!/bin/bash

# global_vars.sh
# This contains all global variables
# Created by: Alex K. Chew (alexkchew@gmail.com, 02/27/2018)

## PRINTING
echo "*** LOADING GLOBAL VARIABLES (global_vars.sh) ***"

### DEFINING GLOBAL VARIABLES ###
# CURRENTWD: Current working directory defined in bash_rc.sh

# PATH2BIN: Path to bin information defined in bash_rc.sh

## IMPORTANT FILES


## MAIN PATHS
# PATH2SCRIPTS: Path to all scripts defined in bash_rc.sh
# PATH2ALCHEMICALSCRIPTS: Path to alchemical analysis scripts
# PATH2POSTSIMSCRIPTS: Path to all post simulation scripts

## PREPARATION SCRIPTS
#   PATH2SCRIPTS_PREPSCRIPTS: Path to preparation scripts

## JOB LISTS
#   JOB_FILE: Contains all job files
#   JOB_FILE_SOLVENT_ENERGIES: job files for solvent energies (quick calcs)

## SWITCH SUBMIT SCRIPT
#   PATH2SWITCHSUBMIT: path to switch submit script

## INPUT DIRECTORIES
#   PREP_MIXED_COSOLVENTS: folder to where prep mixed cosolvents are


### DEFINING MAIN WORKING DIRECTORY
# Checking current hostname
if [[ $(hostname) == *"smic"* ]]; then # Check if we are in the smic server
    CURRENTWD="/work/achew/scratch/SideProjectHuber"
elif [[ $(hostname) == *"stampede"* ]]; then # Check if we are in the stampede server
    CURRENTWD="${WORK}/scratch/SideProjectHuber"
fi

### DEFINING IMPORTANT PATHS
PATH2SCRIPTS="${CURRENTWD}/Scripts" # Where all script files are stored
PATH2ALCHEMICALSCRIPTS="${PATH2SCRIPTS}/alchemical_setup"
PATH2POSTSIMSCRIPTS="${PATH2SCRIPTS}/post_extraction_scripts" # where all post extraction scripts are stored
PATH2SCRIPTS_PREPSCRIPTS="${PATH2SCRIPTS}/Prep_Scripts"
PATH2ANALYSIS_SCRIPTS="${PATH2SCRIPTS}/AnalysisScripts"
PATH2PREP="${CURRENTWD}/prep_mixed_solvent" # Where all preparation files are stored
PATH2SIM="${CURRENTWD}/Simulations" # Where all simulations are stored
PATH2ANALYSIS="${CURRENTWD}/Analysis" # Where all analysis is done

## DEFINING PATH FOR SCRIPTS ALCHEMICAL SETUP
PATH_ALCHEMICAL_SETUP="${PATH2SCRIPTS}/alchemical_setup"

### DEFINING WHERE SWITCH SUBMITS ARE
PATH2SWITCHSUBMIT="${PATH2SCRIPTS}/SubmitScripts"

### DEFINING INPUT_DIR PATHS
INPUT_DIR="${PATH2PREP}/input_files" # Where all input files are located
PREP_SOLVENT_DIR="${INPUT_DIR}/solvents" # Where all solvent information is stored
PREP_SOLUTE_DIR="${INPUT_DIR}/solutes" # Where all solute information is stored
PREP_SOLUTE_COMBINED_DIR="${INPUT_DIR}/solutes_alchemical" # Where all solute information is stored
PREP_TOP_DIR="${INPUT_DIR}/topology" # Where all topology is stored

### PREP MIXED SOLVENTS
PREP_MIXED_COSOLVENTS="${PATH2PREP}/prep_mixed_cosolvents"
PREP_MIXED_US_SIMS="${PATH2PREP}/prep_umbrella_sampling"

### PREP UMBRELLA SAMPLING SIMULATION
PREP_UMB_SIMS="${PATH2PREP}/prep_umbrella_sampling" # Main folder containing all umbrella sampling
PREP_HYD_SIMS="${PREP_UMB_SIMS}/0_hydronium_ion_sims" # Contains hydronium ion + solvent sims
PREP_HYD_SOLUTE_SIMS="${PREP_UMB_SIMS}/1_hydronium_ion_solute_sims" # Contains hydronium ion + solute sims
PREP_HYD_US="${PREP_UMB_SIMS}/2_pulling" # Contains pulling sims

### PREP UMBRELLA SAMPLING HYDRONIUM ION - CHLORINE
PREP_UMB_SIMS_HCL="${PATH2PREP}/prep_umbrella_sampling_HCL" # Main folder containing all umbrella sampling
PREP_HYD_SIMS_HCL="${PREP_UMB_SIMS_HCL}/0_hydronium_ion_chlorine_sims" # Contains hydronium ion + solvent sims
PREP_HYD_SOLUTE_SIMS_HCL="${PREP_UMB_SIMS_HCL}/1_hydronium_ion_solute_sims" # Contains hydronium ion + solute sims
PREP_HYD_US_HCL="${PREP_UMB_SIMS_HCL}/2_pulling" # Contains pulling sims

### DEFINING PATHS WITHIN INPUT FILE
SUBMISSION_DIR="${INPUT_DIR}/submission" # Where all submit files are stored
MDP_DIR="${INPUT_DIR}/mdp_files" # mdp file directory

### DEFINING IMPORTANT FILES
JOB_FILE="${PATH2SCRIPTS}/job_list.txt"
RESTART_JOB_FILE="${PATH2SCRIPTS}/reset_job.txt"
JOB_FILE_SOLVENT_ENERGIES="${PATH2SCRIPTS}/job_list_solvent_energies.txt"

## DEFINING PRE-DEFINED BASHRC
BASHRC_UMB_SAMPLING="${HOME}/bin/bashfiles/server_umbrella_sampling.sh"

## DEFINING PYTHON MODULES
MDBUILDER="${HOME}/bin/pythonfiles/modules/MDBuilder"
## DEFINING SCRIPTS WITHIN MDBUILDER
MDBUILDER_MOST_LIKELY_CONFIG="${MDBUILDER}/applications/solvent_effects/most_likely_configuration.py"
MDBUILDER_ATOM_NAME_MAPPING="${MDBUILDER}/alchemical_setup_tools/convert_atomnames_to_mapping.py"
MDBUILDER_FINDATOMTYPES="${MDBUILDER}/alchemical_setup_tools/find_atomtypes.py"

MDBUILDER_EXTRACTFF="${MDBUILDER}/alchemical_setup_tools/extract_ff_parameters.py"


## DEFINING FORCE FIELD NONBONDED FOLDER
FORCEFIELD_NONBONDED="${INPUT_DIR}/charmm36-nov2016.ff/ffnonbonded.itp"

## DEFINING ALCHEMICAL ANALYSIS SETUP
ALCHEMICAL_SETUP="${HOME}/bin/pythonfiles/alchemical-setup/alchemical-setup.py"

## FORCEFIELD INFO
PATH_CHARMM_FORCEFIELD_ITP="${INPUT_DIR}/charmm36-nov2016.ff/forcefield.itp"

## GETTING CODES FOR PYTHON IN CNN PYTHON CODES
CNN_PATH="$(cd ${CURRENTWD}/../3d_cnn_project; pwd)"
CNN_PYTHON_CODES="${CNN_PATH}/python_scripts"
## DEFINING LOOP GRID CODE
CNN_PYTHON_LOOP_GRID="${CNN_PYTHON_CODES}/loop_grid_interpolation.py"
## DEFINING PREDICTION CODE
CNN_PYTHON_PRED="${CNN_PYTHON_CODES}/prediction_post_training.py"