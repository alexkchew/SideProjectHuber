#!/bin/bash

# run_alchemical_setup.sh
# The purpose of this code is to run alchemical setup given that you have 
# a mapping between molecule 1 -> molecule 2. We will now use the alchemical-setup 
# tool, which can help us incorporate information about the topology. 
# 
# Written by: Alex K. Chew (02/22/2019)
# 
## RELOADING BIN FILE
source "../bin/bash_rc.sh"

############################### 
### USER DEFINED PARAMETERS ###
############################### 

## DEFINING MOLECULE NAMES
molecule_1_name="12-propanediol"
# molecule_2_name="acetone"
molecule_2_name="propanal"

##################################
### FILE DIRECTORY INFORMATION ###
##################################

## DEFINING FORCEFIELD ITP FILE
forcefield_itp_path="${PATH_CHARMM_FORCEFIELD_ITP}"

## DEFINING ALCHEMICAL SETUP SCRIPT LOCATION
alchemical_setup_script_loc="${PATH_ALCHEMICAL_SETUP}"

## DEFINING TOPOLOGY DETAILS
alchemical_topology_name="topology.top"
alchemical_topology_path="${alchemical_setup_script_loc}/${alchemical_topology_name}"

## DEFINING DIRECTORY OF SOLUTE
dir_solute="${PREP_SOLUTE_DIR}"

## DEFINING MAIN DIRECTORY FOR ALCHEMICAL ANALYSIS
dir_alchemical_solute="${PREP_SOLUTE_COMBINED_DIR}"

## COMPUTING COMBINED MOLECULE NAME
combined_name="$(combined_alchemical_molecule_name ${molecule_1_name} ${molecule_2_name})"

## DEFINING COMBINED DIR
path_combined_name="${dir_alchemical_solute}/${combined_name}"

## DEFINING PATH OF SOLUTES
path_molecule_1="${dir_solute}/${molecule_1_name}"
path_molecule_2="${dir_solute}/${molecule_2_name}"

## DEFINING FILE WITH FORCE FIELD PARAMETERS
ff_nonbonded_params="${FORCEFIELD_NONBONDED}"
# python_setup_find_atomtypes="${MDBUILDER_FINDATOMTYPES}"
python_extract_ff="${MDBUILDER_EXTRACTFF}"
python_alchemical_setup="${ALCHEMICAL_SETUP}"

## SETUP DIRECTORY NAME
setup_dir="alchemical_setup"
itp_dir="itp_files"

## MAPPING FILE
path_to_map="${path_combined_name}/map.txt"

## PATH TO SETUP
path_to_setup="${path_combined_name}/${setup_dir}"

########################
### DEFINING OUTPUTS ###
########################

## DEFINING OUTPUTS
output_prefix="alchemical_setup_output"

###################################
### MAIN SCRIPT 
###################################
# -------------------------------
#### CREATING DIRECTORY
#create_dir "${path_to_setup}" -f
#
### MOVING TO DIRECTORY
#cd "${path_to_setup}"
#
### CHECKING IF DIRECTORIES EXISTS
#stop_if_does_not_exist "${path_molecule_1}"
#stop_if_does_not_exist "${path_molecule_2}"
#stop_if_does_not_exist "${ff_nonbonded_params}"
#
### CREATING DIRECTORY FOR EACH
#mkdir -p "${itp_dir}/"{${molecule_1_name},${molecule_2_name}}
#
### COPYING FILES
#cp "${path_molecule_1}/${molecule_1_name}".{itp,prm} "${itp_dir}/${molecule_1_name}"
#cp "${path_molecule_2}/${molecule_2_name}".{itp,prm} "${itp_dir}/${molecule_2_name}"
#
### DEVELOPING TOPOLOGY TO RUN PRMMED ON
#sed "s/MOLECULE_NAME/${molecule_1_name}/g" "${alchemical_topology_path}" | sed "s#FORCEFIELD_PATH#${forcefield_itp_path}#g" > "${itp_dir}/${molecule_1_name}/${alchemical_topology_name}"
#sed "s/MOLECULE_NAME/${molecule_2_name}/g" "${alchemical_topology_path}" | sed "s#FORCEFIELD_PATH#${forcefield_itp_path}#g" > "${itp_dir}/${molecule_2_name}/${alchemical_topology_name}"
#
### CONVERTING EACH ITP AND PARAM FILE
#echo "Running python code to extract details: ${python_extract_ff}"
### MOLECULE 1
#python "${python_extract_ff}" -i "${itp_dir}/${molecule_1_name}/${alchemical_topology_name}" -o "${itp_dir}/${molecule_1_name}/${molecule_1_name}_prep.top"
### MOLECULE 2
#python "${python_extract_ff}" -i "${itp_dir}/${molecule_2_name}/${alchemical_topology_name}" -o "${itp_dir}/${molecule_2_name}/${molecule_2_name}_prep.top"
#
### COPYING GRO FILE
#cp "${path_molecule_1}/${molecule_1_name}.pdb" "${itp_dir}/${molecule_1_name}/${molecule_1_name}_prep.pdb"
#cp "${path_molecule_2}/${molecule_2_name}.pdb" "${itp_dir}/${molecule_2_name}/${molecule_2_name}_prep.pdb"
#
### CONVERTING PDB TO GRO
#solvent_effects_pdb_2_gro_ "${itp_dir}/${molecule_1_name}/${molecule_1_name}_prep.pdb" "${itp_dir}/${molecule_1_name}/${molecule_1_name}_prep.gro"
#solvent_effects_pdb_2_gro_ "${itp_dir}/${molecule_2_name}/${molecule_2_name}_prep.pdb" "${itp_dir}/${molecule_2_name}/${molecule_2_name}_prep.gro"

# -------------------------------

### RUNNING ALCHEMICAL ANALYSIS
python "${python_alchemical_setup}" -a "${path_to_setup}/${itp_dir}/${molecule_1_name}/${molecule_1_name}_prep.top" -b "${path_to_setup}/${itp_dir}/${molecule_2_name}/${molecule_2_name}_prep.top" -m "${path_to_map}" -o "${path_combined_name}/${output_prefix}" -v 1

# currently having issues getting number of columns correct for the dihedral angles -- stupid alchemical analysis tool
# Need to remove all the junk from the outputs of prmmed.


#python "${python_setup_find_atomtypes}" -f "${ff_nonbonded_params}" -i "${path_to_setup}/${itp_dir}/${molecule_1_name}/${molecule_1_name}.itp" -o "${path_to_setup}/${itp_dir}/${molecule_1_name}/${molecule_1_name}_prep.itp"
### MOLCULE 2
#python "${python_setup_find_atomtypes}" -f "${ff_nonbonded_params}" -i "${path_to_setup}/${itp_dir}/${molecule_2_name}/${molecule_2_name}.itp" -o "${path_to_setup}/${itp_dir}/${molecule_2_name}/${molecule_2_name}_prep.itp"
#
#
#





