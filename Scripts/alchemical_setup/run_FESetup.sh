#!/bin/bash

# run_FESetup.sh
# The purpose of this code is to run FESetup between two molecules. The idea is to 
# generate a combined configuration where you have states A and B. Note that FESetup 
# uses AMBER force field parameterization -- as a result, we would like to transform 
# the force field parameters to CHARMM. We can do this using the alchemical-setup
# package, which simply inputs the topology information and outputs a corrected 
# topology. The alchemical-setup needs a mapping way to get atomic map from molecule 1 
# to molecule 2, which is why we need FESetup. 
# 
# Written by: Alex K. Chew (02/11/2019)
#
# FUNCTIONS:
#   extract_map_info_from_FESetup_log:  extracts mapping information from FESetup.log

## RELOADING BIN FILE
source "../bin/bash_rc.sh"

#################
### FUNCTIONS ###
#################

### FUNCTION TO EXTRACT ALL THE NUMERICAL NUMBERS FOR MAPPING
# The purpose of this function is to extract all numerical numbers from 
# the mapping outputs of FESetup. Here, we assume you run FESetup and need the atomic 
# mapping between initial and final states. We also ignore all 'DU' atom mapping to avoid 
# issues with transforming to unknown atoms.
# INPUTS:
#   $1: log file
#   $2: output log file name and path
#   $3: path to python code to convert numerical numbers for wrapping
# OUTPUTS:
#   map log file containing mapping information
# USAGE: extract_map_info_from_FESetup_log morph.log output.map
function extract_map_info_from_FESetup_log () {
    ## DEFINING INPUTS
    log_file_="$1"
    output_file="$2"
    python_mapping="$3"

## FINDING SPECIFIC LINE NUMBER TO START
start_line_num="$(grep -n "Atom mapping between initial and final states" "${log_file_}" | grep -Eo '^[^:]+')"

## FINDING ENDING LINE
end_line_num=$(tail -n +${start_line_num} ${log_file_} | grep -n '^$' | grep -Eo '^[^:]+' | head -n1)

echo "Starting num: ${start_line_num}; end num: ${end_line_num}"

## USING WHILE LOOP
while read line; do
    ## RUNNING PYTHON
    echo "${line}"
    python "${python_mapping}" -s "${line}" -o "${output_file}"
done < <(tail -n +$((${start_line_num}+1)) ${log_file_} | head -n $((${end_line_num} - 2 )) )

}

############################### 
### USER DEFINED PARAMETERS ###
############################### 

## DEFINING MOLECULE NAMES
molecule_1_name="12-propanediol"
molecule_2_name="acetone"

## DEFINING WHETHER YOU WANT REWRITE
# True if you want to rewrite if existing
want_rewrite="False" # "True"

##################################
### FILE DIRECTORY INFORMATION ###
##################################

## DEFINING INPUT
FESetup_input_file="morph.in"
FESetup_output_file="morph.log"
FESetup_mapping_file="map.txt"
FESetup_input_path="${PATH2ALCHEMICALSCRIPTS}/${FESetup_input_file}"

## DEFINING DIRECTORY OF SOLUTE
dir_solute="${PREP_SOLUTE_DIR}"

## DEFINING MAIN DIRECTORY FOR ALCHEMICAL ANALYSIS
dir_alchemical_solute="${PREP_SOLUTE_COMBINED_DIR}"

## COMPUTING COMBINED MOLECULE NAME
combined_name="$(combined_alchemical_molecule_name ${molecule_1_name} ${molecule_2_name})"

## DEFINING COMBINED DIR
path_combined_name="${dir_alchemical_solute}/${combined_name}"

## DEFINING PATH OF SOLUTES
path_molecule_1="${dir_solute}/${molecule_1_name}"
path_molecule_2="${dir_solute}/${molecule_2_name}"

## DEFINING PYTHON PATH TO CONVERT ATOMNAMES
python_convert_atom_names="${MDBUILDER_ATOM_NAME_MAPPING}"

#############################################
### DEFINING OUTPUT DIRECTORY INFORMATION ###
#############################################

pdb_dir_name="ligand"
pdb_file_name="solute.pdb"

###################################
### MAIN SCRIPT 
###################################

## SEEING IF FILE EXISTS
if [ ! -e "${path_combined_name}" ] || [ "${want_rewrite}" == "True" ]; then
    ## CREATING DIRECTORY
    create_dir "${path_combined_name}" -f
    
    ## MOVING TO DIRECTORY
    cd "${path_combined_name}"
    
    ## CHECKING IF DIRECTORIES EXISTS
    stop_if_does_not_exist "${path_molecule_1}"
    stop_if_does_not_exist "${path_molecule_2}"
    
    ## COPYING OVER PDB FILES
    mkdir "${pdb_dir_name}"
    mkdir -p "${pdb_dir_name}"/{${molecule_1_name},${molecule_2_name}}
    cp "${path_molecule_1}/${molecule_1_name}.pdb" "${pdb_dir_name}/${molecule_1_name}/${pdb_file_name}"
    cp "${path_molecule_2}/${molecule_2_name}.pdb" "${pdb_dir_name}/${molecule_2_name}/${pdb_file_name}"
    
    ## COPYING OVER INPUT FILE
    cp "${FESetup_input_path}" "${path_combined_name}"
    
    ## USING SED TO UPDATE THE INPUT FILE
    sed -i "s/_MOLECULE1_/${molecule_1_name}/g" "${FESetup_input_file}"
    sed -i "s/_MOLECULE2_/${molecule_2_name}/g" "${FESetup_input_file}"
    sed -i "s/_BASEDIR_/${pdb_dir_name}/g" "${FESetup_input_file}"
    sed -i "s/_FILENAME_/${pdb_file_name}/g" "${FESetup_input_file}"
    sed -i "s/_LOGFILE_/${FESetup_output_file}/g" "${FESetup_input_file}"
    
    ## RUNNING FESetup
    echo "Running FESetup for: ${combined_name}"
    echo "Full path: ${path_combined_name}"
    FESetup "${FESetup_input_file}"
    
fi

## CREATING FILE IF DOES NOT EXIST
touch "${path_combined_name}/${FESetup_mapping_file}"
> "${path_combined_name}/${FESetup_mapping_file}"

echo "${path_combined_name}/${FESetup_mapping_file}"
### RUNNING EXTRACTION PROTOCAL
extract_map_info_from_FESetup_log "${path_combined_name}/${FESetup_output_file}" "${path_combined_name}/${FESetup_mapping_file}" "${python_convert_atom_names}"





