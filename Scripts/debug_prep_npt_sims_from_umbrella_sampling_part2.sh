#!/bin/bash

# debug_prep_npt_sims_from_umbrella_sampling_part2.sh
## FROM debug_prep_npt_sims_from_umbrella_sampling.sh
# The purpose of this script is to run npt simulations based on conformations of pulling simulations. The reason we need this script is because we want to know how long does it take to form a local domain. 
## NOW, we want to take that simulation, pull it to another position, and measure the amount of time it takes to equilibrate

## USAGE: bash debug_prep_npt_sims_from_umbrella_sampling_part2.sh

### RELOADING GLOBAL VARS AND FUNCTIONS
source "bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## NAME OF SOLUTE 1
solute_1_name="xylitol"
solute_1_res_name="XYL"

## NAME OF SOLUTE 2
solute_2_name="hydronium_LINCS_3" # "hydronium" # 
solute_2_res_name="HYD"

## DEFINE DESIRED BOX LENGTH
box_length="6"

## DEFINING DESIRED DISTANCES
desired_distance=0.50
# 1.95 for pure water
# 2.00 for everything else

## DEFINING TRAJECTORY TO GET DATA FROM
desired_input_trajectory_time="50000" # Time in ps to extract from input

## DEFINING DESIRED SPRING CONSTANT
desired_spring_constant="10000"

## DEFINING DESIRED TEMPERATURE
desired_temp="300.00" # Temperature for xylitol

## DEFINING DIRECTORY YOU ARE GETTING THE DATA FROM
Input_Sim_Dir="181002-XYL_testing_HYD_sims_other_solvents"

## DEFINING MAIN DIRECTORY TO RUN SIMULATIONS IN
Main_Sim_Dir="181008-XYL_testing_HYD_Part2"

## DEFINING PYTHON SCRIPT
python_find_configurations="${MDBUILDER}/umbrella_sampling/umbrella_sampling_select_configurations.py"

## DEFINING NUMBER OF CORES
num_cores="28"

############################
### PREDEFINED VARIABLES ###
############################

## DEFINING DESIRED SOLVENT NAMES
# solvent_water_mass_fraction="1.00" # Mass fraction of water
solvent_water_mass_fraction="0.10" # Mass fraction of water
cosolvent_name="dmso" # "dioxane" # Name of the cosolvent
cosolvent_res_name="dmso" # "DIO" # Residue name of the cosolvent

## TEMPERATURE TO RUN THIS EQUILIBRIUM STEP
temperature="300.00" # Kelvins

## WATER MODEL
water_model="spce"

## DEFINING FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## PULLING SIMULATIONS NAMES
pull_config_files="us_config"
pull_config_prefix="conf"
pull_summary_file="pull_summary.dat"
pull_topology_file="mixed_solv.top" # Topology file

######################################
### FINDING EQUILIBRATED DIRECTORY ###
######################################

## FINDING OUTPUT DIRECTORY FOR SOLUTE
solute_dir=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${solute_1_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 

## DEFINING FULL PATH TO OUTPUT DIRECTORY
full_path_hyd_solute_dir="${PATH2SIM}/${Input_Sim_Dir}/HYDTEST_${solute_dir}"

## CHECKING IF DIRECTORY EXISTS
check_dir_exist "${full_path_hyd_solute_dir}"
#
############################
### DEFINING INPUT FILES ###
############################

## INPUT GRO FILE
input_gro_file="mixed_solv_prod.gro"
input_xtc_file="mixed_solv_prod.xtc"
input_tpr_file="mixed_solv_prod.tpr"

## DEFINING MDP PATH
mdp_file_path="${MDP_DIR}/umbrella_sampling_hyd"

## DEFINING MDP FILES
mdp_em="em.mdp"
mdp_prod="debug_prep_npt_sims_from_umbrella_sampling_part2.mdp"

### DEFINING INPUTS FOR MDP FILES
mdp_prod_time="50000000" # 100 ns

## DEFINING SUBMISSION SCRIPT
input_submit_script="${SUBMISSION_DIR}/submit_debug_prep_npt_sims_from_umbrella_sampling.sh"
#
##############################
#### DEFINING OUTPUT FILES ###
##############################

## OUTPUT DIRECTORY
output_dir="HYDTEST2_${desired_distance}nm_${solute_dir}"
output_path="${PATH2SIM}/${Main_Sim_Dir}/${output_dir}" # FEP 

## OUTPUT PREFIX
output_prefix="mixed_solv"

## OUTPUT FILES
output_summary="conf_summary.csv"

## OUTPUT SUBMIT
output_submit_file="submit.sh"

## DEFINING OUTPUT SPRING CONSTANTS
output_spring_constant_file="spring_constants.dat"

###################
### MAIN SCRIPT ###
###################

## PRINTING
echo "-------------------------------------------------------------"
echo "CREATING DIRECTORY FOR: ${output_path}"
echo "CREATING FILES BASED ON INPUT DIR: ${Input_Sim_Dir}"
echo "FULL PATH OF INPUT: ${full_path_hyd_solute_dir}"
echo "-------------------------------------------------------------"

## CREATING OUTPUT DIRECTORY
create_dir "${output_path}" -f

## GOING INTO OUTPUT DIRECTORY
cd "${output_path}"

##########################
### COPYING OVER FILES ###
##########################
echo "*** COPYING OVER FILES ***"
## MDP FILES
cp -r "${mdp_file_path}"/${mdp_prod} "${output_path}"
## TOPOLOGY
cp -r "${full_path_hyd_solute_dir}/${pull_topology_file}" "${output_path}"
# ITP FILE
cp -r ${full_path_hyd_solute_dir}/{*.itp,*.prm} "${output_path}"
## FORCE FIELD
cp -r "${INPUT_DIR}/${forcefield_dir}" "${output_path}"
## SUBMISSION FILE (SINGLE WINDOW BASIS)
cp -r "${input_submit_script}" "${output_path}/${output_submit_file}"

#############################################
### EXTRACTION GRO FILE USING GMX TRJCONV ###
#############################################
echo "*** EXTRACTING DATA FROM ${desired_input_trajectory_time} ps ***"
echo "- INPUT DIR: ${full_path_hyd_solute_dir}"
echo "- INPUT TRAJECTORY: ${input_xtc_file}"
echo "- INPUT TPR: ${input_tpr_file}"
## USING TRJCONV
gmx trjconv -f "${full_path_hyd_solute_dir}/${input_xtc_file}" -s "${full_path_hyd_solute_dir}/${input_tpr_file}" -o "${output_path}/${output_prefix}.gro" -dump "${desired_input_trajectory_time}" << INPUTS
System
INPUTS

## GRO FILE
# cp -r "${full_path_hyd_solute_dir}/${input_gro_file}" "${output_path}/${output_prefix}.gro"

#####################
### EDITING FILES ###
#####################
echo "*** EDITING FILES USING SED ***"
## EQUILIBRATION MDP FILE
sed -i "s/_TEMPERATURE_/${desired_temp}/g" ${mdp_prod}
sed -i "s/_GROUP_1_NAME_/${solute_1_res_name}/g" ${mdp_prod}
sed -i "s/_GROUP_2_NAME_/${solute_2_res_name}/g" ${mdp_prod}
sed -i "s/_NSTEPS_/${mdp_prod_time}/g" ${mdp_prod}
sed -i "s/_PULLKCONSTANT_/${desired_spring_constant}/g" ${mdp_prod}
sed -i "s/_INITIAL_DIST_/${desired_distance}/g" ${mdp_prod}

## SUBMISSION FILE
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit_file}"
sed -i "s/_NPT_EQUIL_MDP_/${mdp_prod}/g" "${output_submit_file}"
sed -i "s/_OUTPUT_PREFIX_/${output_prefix}/g" "${output_submit_file}"
sed -i "s/_DIRECTORYNAME_/${output_dir}/g" "${output_submit_file}"

#########################
### UPDATING JOB LIST ###
#########################
## UPDATING DIRECTORY FOR JOB LIST
echo "${output_path}" >> ${JOB_FILE}

############################ PRINTING SUMMARY ############################
echo -e "\n************* SUMMARY *************"
echo "Creating solute-hydronium simulations taken from another simulation"
echo " - INPUT DIRECTORY: ${full_path_hyd_solute_dir}"
echo " - SOLUTE: ${solute_1_name} (${solute_1_res_name})"
echo " - HYDRONIUM_ION: ${solute_2_name} (${solute_2_res_name})" 
echo " - BOX LENGTH: ${box_length}"
echo " - MASS FRACTION OF WATER: ${solvent_water_mass_fraction}"
echo " - COSOLVENT: ${cosolvent_name}"
echo " - WORKING DIR: ${output_path}"

