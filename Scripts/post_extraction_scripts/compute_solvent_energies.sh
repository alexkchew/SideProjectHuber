#!/bin/bash

# compute_solvent_energies.sh
# In this script, we will generate energies of the solvent only by removing all solute contributions. By doing this, we can compute the total solvent energies in each system.

# Written by Alex K. Chew (12/11/2018)

## INPUTS:
#	• Solute residue name
#	• tpr file
#	• xtc file
#	• index file name
#	• production file MDP name
#	• submission file name
#	• output MDP file name for energy group (with charge)
#	• output MDP file name for energy group (no charge)
#   • Selection of interest

## OUTPUTS:
#	• Run energy groups with charge distribution
#	• Run energy groups without charge distribution

## ALGORITHMS:
#	• Add energy groups into MDP file
#	• Generate index file
#	• Create tpr file for energy group with charge
#	• --- NO CHARGED SIMS ---
#	• Check if no charge itp is available
#		○ Generate no charge atom itp file
#		○ Otherwise, skip and copy
#	• Edit topology file
#	• Generate TPR file for energy group with no charge
#	• Copy and paste submission file
#		○ Submission file should have gmx energy built within!

## FUNCTIONS:
#   create_solute_solvent_index: creates solute-solvent index file

## UPDATES:
#   20181217-Added "skip_energy_group_calc" functionality that can skip energy group calcs if already done!

### LOADING BASH RC ###
source "../bin/bash_rc.sh"; print_script_name

###########################
######## FUNCTIONS ########
###########################

### FUNCTION TO CREATE INDEX FILE FOR SOLUTE-SOLVENT ANALYSIS
# INPUTS:
#   $1: tpr file name
#   $2: index file name
#   $3: residue name of your solute
# OUTPUTS:
#   index file with your solute (SOLUTE), and "solvent" (SOLVENT)
#   NOTE: Solvent is defined as all atoms other than the solute
function create_solute_solvent_index () {
    ## DEFINING INPUTS:
    tpr_file_="$1"
    index_file_="$2"
    residue_name_="$3"
    
    ## RUNNNING GMX INDEX
gmx make_ndx -f "${tpr_file_}" -o "${index_file_}"  >/dev/null 2>&1  << INPUT
keep 0
r ${residue_name_}
! r ${residue_name_}
name 1 SOLUTE
name 2 SOLVENT
q
INPUT

## PRINTING
echo "-----------------------------"
## SEEING IF INDEX FILE EXISTS
if [ -e "${index_file_}" ]; then
    echo "CREATING INDEX FILE: ${index_file_}"
    echo "SOLUTE RESIDUE NAME: ${residue_name_}"
    echo "Note: This index file contains solute-solvent, where solvent is defined as everything but the solute"
    
else
    echo "Error! Index file ${index_file_} is not available!"
    echo "Check to see if create_solute_solvent_index function ran correctly!"
    echo "TPR file: ${tpr_file_}; Index file: ${index_file_}; Residue name: ${residue_name_}"
    echo "Pausing here so you can see the error"
    sleep 5
fi
echo "-----------------------------"
}

####################################
### DEFINING USER-DEFINED INPUTS ###
####################################

## DEFINING VARIABLES
#   $1: solute_solute_name: solute's name
#   $2: solute_residue_name: solute's residue name
#   $3: analysis_dir: full path to the analysis directory

# USAGE EXAMPLE: bash compute_solvent_energies.sh acetone ACE "/home/akchew/scratch/SideProjectHuber/Simulations/181107-PDO_DEHYDRATION_FULL_DATA_300NS/test/mdRun_433.15_6_nm_ACE_100_WtPercWater_spce_Pure"

## DEFINING SOLUTE RESIDUE NAME
solute_solute_name="$1" # "acetone"
solute_residue_name="$2" # "ACE"

## DEFINING MAIN DIRECTORY OF ANALYSIS
analysis_dir="$3" # "/home/akchew/scratch/SideProjectHuber/Simulations/181107-PDO_DEHYDRATION_FULL_DATA_300NS/test/mdRun_433.15_6_nm_ACE_100_WtPercWater_spce_Pure"

## DEFINING SOLUTE RESIDUE NAME
#solute_solute_name="acetone" # "acetone"
#solute_residue_name="ACE" # "ACE"
#
### DEFINING MAIN DIRECTORY OF ANALYSIS
#analysis_dir="/home/akchew/scratch/SideProjectHuber/Simulations/181107-PDO_DEHYDRATION_FULL_DATA_300NS/mdRun_433.15_6_nm_ACE_100_WtPercWater_spce_Pure" # "/home/akchew/scratch/SideProjectHuber/Simulations/181107-PDO_DEHYDRATION_FULL_DATA_300NS/test/mdRun_433.15_6_nm_ACE_100_WtPercWater_spce_Pure"

#######################
### DEFINING INPUTS ###
#######################

## DEFINING FUNCTIONS SCRIPTS
function_scripts="${GENERAL_RESEARCH_FUNCTIONS}"

## DEFINING PREFIX
input_prefix="mixed_solv_prod"

## DEFINING XTC AND TPR
input_gro_file="${input_prefix}.gro"
input_tpr_file="${input_prefix}.tpr"
input_xtc_file="${input_prefix}.xtc"

## FINDING TIME OF FIRST FRAME TO READ
first_frame="10000" # ps, time frame to begin reading the energy groups

## TOPOLOGY
input_top_file="mixed_solv.top"

## DEFINING PRODUCTION MDP FILE
input_mdp_file="production_LIQ.mdp"

## DEFINING SUBMISSION FILE NAME
input_submit_file="submit_compute_solvent_energies.sh"

## DEFINING SOLUTE AND SOLVENT NAME
solute_name="SOLUTE"
solvent_name="SOLVENT"
energy_group_name="${solute_name} ${solvent_name}"

## DEFINING INDEX FILE
index_file="solute_solvent.ndx"

## DEFINING ARRAY OF ENERGY GROUPS OF INTEREST
declare -a energy_of_interest=("Coulomb-14" "Coul.-recip." "Coulomb-(SR)" "LJ-SR:SOLVENT-SOLVENT" "LJ-14:SOLVENT-SOLVENT")

# printf '%s\n' "${energy_of_interest[@]}"

## DEFINING LOGICALS
skip_energy_group_calc="true" # True if you want to skip energy group calculations
# false by default

## DEFINING ITP FILE
input_solute_itp_file="${solute_solute_name}.itp"

########################
### DEFINING OUTPUTS ###
########################

## OUTPUT NOCHARGE ITP
output_solute_itp_file="${input_solute_itp_file%.itp}_nocharge.itp"

## DEFINING OUTPUT TOP IFLE
output_top_file="${input_top_file%.top}_nocharge.top"

## DEFINING OUTPUT SUBMIT FILE
output_submit_file="${input_submit_file}"

## DEFINING OUTPUT MDP FILE NAME
output_mdp_file="production_LIQ_energy_grps.mdp"

## DEFINING OUTPUT NAMES
output_prod_energy_grps="${input_prefix}_energy_grps"

## DEFINING OUTPUT ENERGY FILE
output_energy_file="${output_prod_energy_grps}.xvg"

## DEFINING OUTPUT EDR FILE
output_edr_file="${output_prod_energy_grps}.edr"

## DEFINING SUMMARY FILE
output_summary_file="${output_prod_energy_grps}.summary"

## DEFINING OUTPUT JOB NAME
output_job_name="solvent_energies_$(basename ${analysis_dir})"

######################
### DEFINING PATHS ###
######################

## INPUT PATH FOR SUBMISSION
path_input_submit_file="${SUBMISSION_DIR}/${input_submit_file}"

#################################################
################## MAIN SCRIPT ##################
#################################################

## GOING INTO DIRECTORY
cd "${analysis_dir}"

if [[ "${skip_energy_group_calc}" == "false" ]]; then

    ## PRINTING
    echo "ANALYSIS DIRECTORY: ${analysis_dir}"

    ## GENERATING INDEX FILE
    create_solute_solvent_index "${input_tpr_file}" "${index_file}" "${solute_residue_name}"

    ## EDITING MDP FILE TO INCLUDE ENERGY GROUPS (Assuming no energy groups were included)
    mdp_add_energy_groups "${input_mdp_file}" "${output_mdp_file}" "${energy_group_name}"

    ## CHECKING THE XTC FILE TOTAL TIME
    xtc_check_total_time_ps ${input_xtc_file} xtc_total_time

    ## CHECKING MDP OPTIONS
    mdp_nsteps=$(mdp_find_option_values ${output_mdp_file} "nsteps")
    mdp_dt=$(mdp_find_option_values ${output_mdp_file} "dt")

    ## CALCULATING TOTAL TIME FOR MDP
    mdp_check_total_time_ps=$(awk -v nsteps=${mdp_nsteps} -v dt=${mdp_dt} 'BEGIN{ printf "%.3f", nsteps*dt }')

    # Checking if the times match
    check_time_equality=$(awk 'BEGIN{ print "'${xtc_check_total_time_ps}'"!="'${mdp_check_total_time_ps}'" }')

    ## CORRECTING IF TIME EQUALITY DOES NOT MATCH
    if [ "${check_time_equality}" -eq 1 ];then # Probably unnecessary if statement
        ## NEED TO CORRECT MDP FILE TOTAL TIME
        nstep_correction=$(awk -v total_time=${xtc_total_time} -v dt=${mdp_dt} 'BEGIN{ printf "%d", total_time/dt }')
        ## FINDING LINE NUMBER FOR NSTEPS
        line_nsteps=$(grep -nE 'nsteps' "${output_mdp_file}" | tail -1  | sed 's/\([0-9]*\).*/\1/')
        ## ADDING CORRECTED VALUE
        sed -i ${line_nsteps}'ansteps =    '"${nstep_correction}" "${output_mdp_file}"
        ## REMOVING LINES
        sed -i ''${line_nsteps}'d' "${output_mdp_file}"
        echo "Correcting MDP file number of steps from ${mdp_nsteps} to ${nstep_correction}"
    else
        echo "MDP file matches your xtc file, continuing!"
    fi


    ## CHECKING IF SOLUTE ITP FILE EXISTS
    stop_if_does_not_exist "${input_solute_itp_file}"

    ## CONVERTING ITP FILE TO NO CHARGE
    convert_itp_nocharge "${input_solute_itp_file}" "${output_solute_itp_file}"

    ## EDITING TOPOLOGY FILE
    cp -r "${input_top_file}" "${output_top_file}"
    sed -i "s/${input_solute_itp_file}/${output_solute_itp_file}/g" "${output_top_file}"

    ## CREATING TPR FILE BASED ON MDP FILE
    echo "*** CREATING TPR FILE FOR ENERGY GROUPS ***"
    gmx grompp -f "${output_mdp_file}" -c "${input_gro_file}" -p "${output_top_file}" -o "${output_prod_energy_grps}.tpr" -n "${index_file}" >/dev/null 2>&1

    ## CHECKING IF TPR FILE EXISTS
    stop_if_does_not_exist "${output_prod_energy_grps}.tpr"

    ## COMPUTING NUMBER OF ATOMS FOR GRO FILE
    total_atoms=$(gro_count_total_atoms "${input_gro_file}")

    ## COMPUTING NUMBER OF CORES REQUIRED
    num_cores="$(compute_num_cores_required ${total_atoms})"
    echo "*** TOTAL ATOMS: ${total_atoms} ... USING ${num_cores} CORES ***"; sleep 1

    ## CREATING SUBMISSION FILE
    cp "${path_input_submit_file}" "${output_submit_file}"

    ## EDITING SUBMISSION FILE
    sed -i "s/_DIRECTORY_/${output_job_name}/g" "${output_submit_file}"
    sed -i "s/_TPRFILE_/${output_prod_energy_grps}.tpr/g" "${output_submit_file}"
    sed -i "s/_XTCFILE_/${input_xtc_file}/g" "${output_submit_file}"
    sed -i "s/_OUTPUTEDRFILE_/${output_edr_file}/g" "${output_submit_file}"
    sed -i "s/_OUTPUTENERGYFILE_/${output_energy_file}/g" "${output_submit_file}"
    sed -i "s/_NUMOFCORES_/${num_cores}/g" "${output_submit_file}"
    sed -i "s/_FIRSTTIMEFRAME_/${first_frame}/g" "${output_submit_file}"
    sed -i "s#_FUNCTIONSCRIPTS_#${function_scripts}#g" "${output_submit_file}"
    sed -i "s/_XTCFILETIMEPS_/${xtc_total_time}/g" "${output_submit_file}"
    sed -i "s/_OUTPUTSUMMARYFILE_/${output_summary_file}/g" "${output_submit_file}"

    ###############################
    ### EDITING SUBMISSION FILE ###
    ###############################

    ## LOOPING THROUGH AND ADDING TO EACH LINE
    for each_energy in "${energy_of_interest[@]}"; do
        sed -i "/_INPUTENERGYTYPES_/a ${each_energy}" "${output_submit_file}"
    done

    # FINDING LINE NUMBER OF ENERGY TYPES
    line_energy=$(grep -nE '_INPUTENERGYTYPES_' "${output_submit_file}" | tail -1  | sed 's/\([0-9]*\).*/\1/')

    ## DELETING ENERGY LINE
    sed -i ''${line_energy}'d' "${output_submit_file}"

    ####### ADDING TO JOB LIST
    echo "${analysis_dir}/${output_submit_file}" >> "${JOB_FILE_SOLVENT_ENERGIES}"

## SKIPPING ENERGY GROUPS
else
    ## CHECKING IF ENERGY, TPR, ETC. EXISTS
    stop_if_does_not_exist "${output_edr_file}"
    stop_if_does_not_exist "${output_prod_energy_grps}.tpr"
    
    ## DEFINING ENERGY TERMS
    energy_terms="$(printf '%s\n' "${energy_of_interest[@]}")"
    
    ## RUNNING GMX ENERGY
gmx energy -f "${output_edr_file}" -s "${output_prod_energy_grps}.tpr" -o "${output_energy_file}" -b "${first_frame}" > "${output_summary_file}" << INPUTS
${energy_terms}

INPUTS

fi

###############
### SUMMARY ###
###############
echo -e "\n*** SUMMARY ***"
echo "Generated solvent energetics computation"
echo "   --> Solute name: ${solute_solute_name}"
echo "   --> Residue name: ${solute_residue_name}"
echo "   --> New MDP file: ${output_mdp_file}"
echo "   --> Number of cores: ${num_cores}"
echo "   --> Job output name: ${output_job_name}"
echo "   --> Analysis directory: ${analysis_dir}"
echo "   --> Submission file: ${output_submit_file}"
