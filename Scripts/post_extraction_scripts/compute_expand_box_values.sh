#!/bin/bash

# compute_expand_box_values.sh
# In this script, we will use the expanded box simulation and 
# generate electrostatic / density profile for them. By doing so, 
# we can estimate the electrostatic potential and density

### FUNCTIONS:
#   compute_traj_com_z_dim: computes z-dimension from gmx traj

# Written by Alex K. Chew (01/16/2019)
## UPDATES:

### LOADING BASH RC ###
source "../bin/bash_rc.sh"; print_script_name

###########################
######## FUNCTIONS ########
###########################

### FUNCTION TO COMPUTE THE AVERAGE Z- COM
# The purpose of this function is to compute the z-component of a
# center of mass calculation from gmx traj.
# INPUTS:
#   $1: xvg file
# OUTPUTS:
#   ensemble average of the z-component
# USAGE:
#   compute_traj_com_z_dim coord.xvg
#   avg_z=$(compute_traj_com_z_dim coord.xvg)
function compute_traj_com_z_dim () {
    ## DEFINING INPUTS
    input_xvg_file_="$1"
    
    ## DEFINING EMPTY VALUE TO CAPTURE Z-DIMENSION
    zdim=0
    index=0 # index to keep tract of how many lines there were
    
    ## USING WHILE LOOP TO READ THE FILE
    while read -r line; do
        ## SKIPPING IF COMMENTS
        if [[ ${line} != \#* ]] && [[ ${line} != @* ]]; then
            ## FINDING LAST VALUE OF THE LINE
            last_value=$(echo ${line} | awk '{print $NF}')
            ## ADDING LAST VALUE TO Z DIMENSION
            zdim=$(echo "${zdim} + ${last_value}" | bc  )
            ## ADDING TO INDEX
            index=$((${index}+1))
        fi
    
    done < ${input_xvg_file_}
    ## FINALLY PRINTING THE DIVISION
    avg_z=$(awk -v z_value=${zdim} -v n_frames=${index} 'BEGIN{ printf "%.5f", z_value / n_frames }')
    ## PRINTING
    echo "${avg_z}"
}


####################################
### DEFINING USER-DEFINED INPUTS ###
####################################

## DEFINING VARIABLES
#   $1: analysis_dir: full path to the analysis directory

## DEFINING MAIN DIRECTORY OF ANALYSIS
analysis_dir="$1" #  "/home/akchew/scratch/SideProjectHuber/Analysis/190114-Expanded_NoSolute_Pure_Sims/NoSolute/Expand_8nm_300.00_6_nm_NoSolute_100_WtPercWater_spce_Pure"

#######################
### DEFINING INPUTS ###
#######################

## DEFINING FUNCTIONS SCRIPTS
function_scripts="${GENERAL_RESEARCH_FUNCTIONS}"

## DEFINING PREFIX
input_prefix="mixed_solv_prod"

## DEFINING XTC AND TPR
input_gro_file="${input_prefix}.gro"
input_tpr_file="${input_prefix}.tpr"
input_xtc_file="${input_prefix}_5_ns_whole.xtc"

## FINDING TIME OF FIRST FRAME TO READ
first_frame="5000" # ps, time frame to begin reading the energy groups

## DEFINING SYSTEM NAME
system_name="System"

## DEFINING BIN WIDTH
bin_width="0.02" # nm

## DEFINING INITIAL TRANSLATION -- Assuming we have a lot of buffer (e.g. 8 nm)
buffer_translation="2" # nm
# 0 means no initial translation prior to computing COM

########################
### DEFINING OUTPUTS ###
########################

## OUTPUT COM CALC
output_com_xvg="coord.xvg"
output_box_xvg="box.xvg"

## DEFINING BUFFER XTC FILE
buffer_xtc="${input_prefix}_5_ns_buffer.xtc"

## OUTPUT XVG FILE
output_potential_xvg="potential.xvg"
output_density_xvg="density.xvg"

## OUTPUT XTC FILE
output_xtc_file="${input_prefix}_5_ns_center.xtc"

############################################################################
### MAIN SCRIPT 
############################################################################

## GOING INTO DIRECTORY
cd "${analysis_dir}"

##############################
### PART 1: COM OF SOLVENT ###
##############################

## PRINTING
echo -e "\n*** Working directory: ${analysis_dir} ***"
echo -e "\n-------------------------"
echo "Computing COM of the z-dimension"
echo "Creating ${output_com_xvg} file"

## SEEING IF BUFFER IS REQUESTED
if [ "${buffer_translation}" -gt "0" ]; then
    echo "Buffer translation activated for ${buffer_translation} nm"
    echo "Creating buffer xtc file: ${buffer_xtc}"
    
## TRANSLATING
gmx trjconv -f "${input_xtc_file}" -s "${input_tpr_file}" -o "${buffer_xtc}" -pbc mol -trans 0 0 "${buffer_translation}" >/dev/null 2>&1 << INPUTS
${system_name}
INPUTS

    ## REDEFINING INPUT XTC FILE TO BUFFER FILE
    input_xtc_file="${buffer_xtc}"
fi

## USING TRAJ TO COMPUTE THE COM
gmx traj -f "${input_xtc_file}" -s "${input_tpr_file}" -com -ox "${output_com_xvg}" -ob "${output_box_xvg}" >/dev/null 2>&1 << INPUTS
${system_name}
INPUTS

## COMPUTING AVERAGE Z-DIMENSION
echo "Computing COM from ${output_com_xvg}..."
avg_z=$(compute_traj_com_z_dim ${output_com_xvg})

## PRINTING
echo "Average z COM found: ${avg_z} for ${system_name}"

##############################
### PART 2: MOVING SOLVENT ###
##############################

## PRINTING
echo -e "\n-------------------------"
echo "Moving all solvents based on COM to center of box (z-dimension)"

## FINDING THE CENTER OF THE BOX Z-DIMENSION
box_z_length=$(cat "${output_box_xvg}" | sed '/^#/d' | sed '/^@/d' | head -n1 | awk '{print $4}') # Opens, removes all lines begining with '#' and '@', then selects the first line, then prints column 4 (z-dimension size). Note that we are assuming NVT simulation (no box fluctuations)

## COMPUTING TRANSLATIONAL DISTANCE
trans_z=$(awk -v z_value=${avg_z} -v box_length=${box_z_length} 'BEGIN{ printf "%.5f", box_length/2.0 - z_value }') # desired center of box

## TRANSLATING
gmx trjconv -f "${input_xtc_file}" -s "${input_tpr_file}" -o "${output_xtc_file}" -pbc mol -trans 0 0 "${trans_z}" >/dev/null 2>&1 << INPUTS
${system_name}
INPUTS

## PRINTING
echo "Z-box length: ${box_z_length}"
echo "Translational z: ${trans_z}"
echo "Creating ${output_xtc_file} trajectory file, which has translationed information"

###################################################
### PART 3: COMPUTING GMX POTENTIAL AND DENSITY ###
###################################################

## PRINTING
echo -e "\n-------------------------"
echo "Computing gmx potential and gmx density"

## COMPUTING NUMBER OF BINS
num_bins=$(awk -v bin_w=${bin_width} -v box_length=${box_z_length} 'BEGIN{ printf "%d", box_length/bin_w }') # desired center of box
echo "Bin width set: ${bin_width}"
echo "Number of bins: ${num_bins}"

## COMPUTING GMX POTENTIAL
echo "Computing gmx potential...."
gmx potential -f "${output_xtc_file}" -s "${input_tpr_file}" -o "${output_potential_xvg}" -d Z -sl "${num_bins}"  >/dev/null 2>&1  << INPUTS
${system_name}
INPUTS

echo "Computing gmx density...."
## COMPUTING GMX DENSITY
gmx density -f "${output_xtc_file}"  -s "${input_tpr_file}" -o "${output_density_xvg}" -d Z -sl "${num_bins}"  >/dev/null 2>&1 << INPUTS
${system_name}
INPUTS

## REMOVING BUFFER TO SAVE SPACE
if [ "${buffer_translation}" -gt "0" ]; then
    rm "${buffer_xtc}"
fi

## CLEANING UP DUPLICATES
rm \#*