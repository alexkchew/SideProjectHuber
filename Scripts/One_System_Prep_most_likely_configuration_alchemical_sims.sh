#!/bin/bash

# One_System_Prep_most_likely_configuration_alchemical_sims.sh
# In this script, we compute the most likely configuration given a simulation 
# trajectory. The main idea here is that we are interested in a specific 
# solute and would like to optimize the trajectory on the basis of dihedral angles.
# To do this, we will tentatively assume that there is two dihedral angles (e.g. 1,2-PDO).
# As a result, we will not need to include additional user input. In the future, we 
# would ideally incorporate multiple dihedral angles to more accurately compute 
# the most likely configuration. In addition, this code runs an alchemical simulation.
# 
# Written by Alex K. Chew (02/01/2019)
# 
# FUNCTIONS:
# 
# 
# UPDATES:

### LOADING BASH RC ###
source "./bin/bash_rc.sh"; print_script_name

####################################
### DEFINING USER-DEFINED INPUTS ###
####################################

## DEFINING SOLUTE FULL NAME AND RESDIUE NAME
solute_name="12-propanediol"
solute_residue_name="PDO"  

solute_name_2="propanal"
solute_residue_name_2="PRO"

## DEFINING SYSTEM TEMPERATURE
system_temp="433.15"

## DEFINING WATER MASS FRACTION
solvent_one_wt_frac=$"1.00"

## DEFINING COSOLVENT
solvent_two_name="dioxane" 
solvent_two_residue_name="DIO" 

## DEFINING DESIRED BOX LENGTH
desired_box_length="6"

## DEFINING WATER MODEL
water_model="spce" 

## DEFINING MAIN SIMULATION DIRECTORY
Main_Sim_Dir="181107-PDO_DEHYDRATION_FULL_DATA_300NS"
# 181107-PDO_DEHYDRATION_FULL_DATA_300NS_OTHERSOLVENTS
# 181107-PDO_DEHYDRATION_FULL_DATA_300NS

## DEFINING OUTPUT SIM DIR
Output_Sim_Dir="190329-Testing_alchemical_mostlikely_configs" # "190207-PDO_Mostlikely_Configs_10000" 

## FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## DEFINING NUMBER OF CORES
num_cores="28"

## DEFINING INITIAL TIME TO LOOK IN
initial_time_frame="10000" # Initial time frame to look into

## DEFINING LOGICAL
# True if you want a hard restart in the computation of most likely configs
# Otherwise, False
want_recalc="False" # True

###############################
### FILE SYSTEM INFORMATION ###
###############################

## DEFINING GRO AND XTC FILE
input_prefix="mixed_solv_prod"
input_gro="${input_prefix}.gro"
input_xtc="${input_prefix}.xtc"
input_tpr="${input_prefix}.tpr"
input_top="mixed_solv.top"

## DEFINING OUTPUT DIRECTORY
output_dir_name=$(solvent_effects_get_output_dir_name "${system_temp}" "${desired_box_length}" "${solute_residue_name}" "${solvent_one_wt_frac}" "${water_model}" "${solvent_two_name}")

## DEFINING MDRUN OUTPUT
mdrun_output_dir_name="mdRun_${output_dir_name}"

## DEFINING WORKING SPACE
WorkSpace="${PATH2SIM}"

## DEFINING FULL PATH TO PREVIOUSLY COMPLETED SIMULATION
Main_Sim_Path="${WorkSpace}/${Main_Sim_Dir}/${mdrun_output_dir_name}"

## CHECKING IF PATH EXISTS
stop_if_does_not_exist "${Main_Sim_Path}"

## DEFINING OUTPUT SIMULATION PATH
output_dir_new_name="MostlikelyAlchemical_${output_dir_name}"
Output_Sim_Path="${WorkSpace}/${Output_Sim_Dir}/${output_dir_new_name}"

## DEFINING SETUP FILES FOLDER
setup_files="setup_files"

## DEFINING MDP FILE
mdp_file="alchemical_transformation.mdp"
mdp_parent_path="${MDP_DIR}/alchemical_transformation_fep"

## DEFINING MDP FILE TIME
mdp_file_time="100000000" # 200 ns

## DEFINING SUBMISSION FILE
submit_file_name="submit_most_likely_configuration.sh"
submit_file_path="${SUBMISSION_DIR}"

## DEFINING JOB FILE
job_file="${JOB_FILE}"


#############################
### DEFINING OUTPUT FILES ###
#############################

## DEFINING OUTPUT XTC FILE
output_xtc="${input_prefix}_center_solute_${initial_time_frame}ps.xtc"

## DEFINING TRANSFER SUMMARY
transfer_summary="transfer_summary.txt"

## DEFINING OUTPUT PREFIX
output_summary_prefix="most_likely_config"

## DEFINING SUMMARY FILE
output_summary_file="${output_summary_prefix}.summary"

## DEFINING MOST LIKELY CONFIG INFORMATION
degree_increment="5" # Degrees -- number of degrees increment in between dihedral angles

## DEFINING OUTPUT GRO
output_prefix="mixed_solv"
output_gro="${output_prefix}.gro"

## SUBMIT FILE
output_submit="submit.sh"

## DEFINING POSITION RESTRAINTS
solute_posre_value="10000"
output_solute_posre="solute_posre.itp"

## DEFINING INDEX FILE
res_index="res_index.ndx"

###############################################################
### MAIN SCRIPT 
###############################################################

## GOING INTO DIRECTORY
cd "${Main_Sim_Path}"

## CHECKING SIMULATION FILES
stop_if_does_not_exist "${input_gro}"
stop_if_does_not_exist "${input_xtc}"
stop_if_does_not_exist "${input_tpr}"

############################
### CENTERING THE SOLUTE ###
############################
## SEEING IF XTC FILE EXISTS AND WHETHER OR NOT YOU WANT FULL RECALCULATION
if [ ! -e "${output_xtc}" ] || [ "${want_recalc}" == "True" ]; then

## PRINTING
echo "Creating solute (residue ${solute_residue_name}) trajectory file ... (may take a while)"
echo "Starting from initial frame: ${initial_time_frame} ps"
## CREATING INDEX FILE OF RESIDUE
index_make_ndx_residue_with_system "${input_tpr}" "${res_index}" ${solute_residue_name}
## USING GMX TRJCONV TO CREATE AND CENTER ONLY THE SOLUTE
gmx trjconv -f "${input_xtc}" -s "${input_tpr}" -o "${output_xtc}" -n "${res_index}" -b "${initial_time_frame}" -pbc mol -center >/dev/null 2>&1 << INPUTS
${solute_residue_name}
System
INPUTS
sleep 5
else 
    echo "${output_xtc} already exists! Continuing script..."
fi
##############################################
### RUNNING MOST LIKELY CONFIGURATION CODE ###
##############################################
## CHECKING IF SUMMARY FILE ALREADY EXISTS
if [ ! -e "${output_summary_file}" ] || [ "${want_recalc}" == "True" ]; then
python3 ${MDBUILDER_MOST_LIKELY_CONFIG} -i "${Main_Sim_Path}" -o "${Main_Sim_Path}" -s "${output_summary_prefix}" -g "${input_gro}" -x "${output_xtc}" -n "${solute_residue_name}" -p 
else
    echo "${output_summary_file} already exists! Continuing script..."
fi

## FINDING NEAREST TRAJECTORY TIME
most_likely_time=$(head -n 2 "${output_summary_file}" | tail -n1 | awk '{print $4}')
echo "The most likely time found: ${most_likely_time} ps"; sleep 1

## CREATING DIRECTORY
create_dir "${Output_Sim_Path}" -f

## DUMPING NEAREST TRAJECTORY
echo "Creating ${output_gro} from ${output_xtc}..."
gmx trjconv -f "${output_xtc}" -s "${input_tpr}" -o "${Output_Sim_Path}/${output_gro}" -dump "${most_likely_time}" >/dev/null 2>&1 << INPUTS
System
INPUTS

## GOING TO OUTPUT SIMULATION DIRECTORY
cd "${Output_Sim_Path}"

## COPYING TOPOLOGY FILE, MDP FILES, ETC.
# TOPOLOGY
cp "${Main_Sim_Path}/${input_top}" "${Output_Sim_Path}"
# ITP/PRM FILES
cp "${Main_Sim_Path}/"{*.itp,*.prm} "${Output_Sim_Path}"
# FORCE FIELD
cp -r "${Main_Sim_Path}/${forcefield_dir}" "${Output_Sim_Path}"
# MDP FILE
cp -r "${mdp_parent_path}/${mdp_file}" "${Output_Sim_Path}"
# SUBMISSION FILE
cp -r "${submit_file_path}/${submit_file_name}" "${Output_Sim_Path}/${output_submit}"













#
####################################################
#### CREATING POSITION RESTRAINTS FOR THE SOLUTE ###
####################################################
### GROMACS GENRESTR -- Used to generate position restraints
#gmx genrestr -f "${output_gro}" -o "${output_solute_posre}" -fc "${solute_posre_value}" "${solute_posre_value}" "${solute_posre_value}" >/dev/null 2>&1 << INPUTS
#${solute_residue_name}
#INPUTS
#
### CORRECTING POSRE FILE
#itp_fix_genrestr_numbering "${output_solute_posre}"
#
### ADDING POSITION RESTRAINTS TO TOPOLOGY FILE
#topology_add_include_specific "${input_top}" "${solute_name}.itp" "#include \"${output_solute_posre}\""
#
#########################################
#### EDITING MDP AND SUBMISSION FILES ###
#########################################
### MDP FILES
#sed -i "s/NSTEPS/${mdp_file_time}/g" ${mdp_file}
#sed -i "s/TEMPERATURE/${system_temp}/g" ${mdp_file}
#
### SUBMISSION FILE
#sed -i "s/_DIRECTORY_/${output_dir_new_name}/g" ${output_submit}
#sed -i "s/_NUMCORES_/${num_cores}/g" ${output_submit}
#sed -i "s/_TPRFILE_/${output_prefix}_prod/g" ${output_submit}
#
############################################
#### CREATING TPR GIVEN TOPOLOGY AND GRO ###
############################################
#echo "*** Creating tpr file ***"
#gmx grompp -f ${mdp_file} -c ${output_gro} -p ${input_top} -o ${output_prefix}_prod -maxwarn 1
#
### ADDING SUBMISSION SCRIPT INTO JOB LIST
#echo "${Output_Sim_Path}/${output_submit}" >> "${job_file}"
#
##########################################
#### CREATING SUMMARY FILE OF TRANSFER ###
##########################################
#echo "transfer_summary.txt" > "${transfer_summary}"
#echo "This file contains summary of transferring a most likely configuration file into this directory" >> "${transfer_summary}"
#echo "------------------------------" >> "${transfer_summary}"
#echo "Main directory path: ${Main_Sim_Path}" >> "${transfer_summary}"
#echo "Main xtc file: ${output_xtc}" >> "${transfer_summary}"
#echo "Optimal likely structure found in: ${most_likely_time} ps" >> "${transfer_summary}"
#echo "Current directory path: ${Output_Sim_Path}" >> "${transfer_summary}"
#
################ PRINTING SUMMARY ###############
#echo "********** SUMMARY **********"
#echo "Created most likely configurations for the following:"
#echo "Solute name/residue: ${solute_name} / ${solute_residue_name}"
#echo "System temperature: ${system_temp}"
#echo "Water mass fraction: ${solvent_one_wt_frac}"
#echo "Selected cosolvent name/residue: ${solvent_two_name} / ${solvent_two_residue_name}"
#echo "Time taken configuration: ${most_likely_time} ps"
#echo "Box length: ${desired_box_length} nm"
#echo "Main simulation directory: ${Main_Sim_Dir}"
#echo -e "\nCurrent simulation directory: ${Output_Sim_Dir}"
#echo "Path: ${Output_Sim_Path}"
#echo "Jobs have been saved at: ${job_file} "
