#!/bin/bash 
# COMET Submit Script (submit.sh)

#SBATCH --partition=compute
#SBATCH --job-name="JOB_NAME"
#SBATCH --output="slurm.%j.%N.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --export=ALL
#SBATCH -t 48:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A mit155
#SBATCH --no-requeue

# Defining number of threads
NumberOfThreads="24" # Number of threads

#Set the number of openmp threads
export OMP_NUM_THREADS=${NumberOfThreads}

#Now list your executable command (or a string of them).

# ---- RUN SCRIPTS BELOW ----- #

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
echo "----- RUNNING GROMACS COMMANDS -----"

### RUNNING GROMACS ###

gro_file="mixed_solv.gro"
input_top="mixed_solv.top"
output_prefix="mixed_solv"

# Defining mdp scripts
em_mdp="minim_LIQ.mdp"
equil_mdp="equil_LIQ.mdp"
prod_mdp="production_LIQ.mdp"

if [ ! -e "${output_prefix}_equil.gro" ]; then

    # Need to minimize and equilibrate
    # MINIMIZE
    gmx grompp -f ${em_mdp} -c ${gro_file} -p ${input_top} -o ${output_prefix}_em -maxwarn 1
    gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em

    # EQUILIBRATION
    gmx grompp -f ${equil_mdp} -c ${output_prefix}_em -p ${input_top} -o ${output_prefix}_equil -maxwarn 1
    gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_equil
fi

# MD PRODUCTION
if [ ! -e "${output_prefix}_prod.tpr" ]; then
    gmx grompp -f ${prod_mdp} -c ${output_prefix}_equil.gro -p ${input_top} -o ${output_prefix}_prod -maxwarn 1
fi

if [ ! -e "${output_prefix}_prod.gro" ]; then
    gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_prod
fi