
# This script is designed for umbrella sampling simulation in COMET

#SBATCH --partition=compute
#SBATCH --job-name="JOB_NAME"
#SBATCH --output="slurm.%j.%N.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --export=ALL
#SBATCH -t 48:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A mit161
#SBATCH --no-requeue

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="24" # number of cores you want to run with

## SETTING NUMBER OF OPENMP THREADS
# export OMP_NUM_THREADS=${num_cores}

## LOADING GROMACS
echo "----- LOADING GROMACS ------"
# module purge all
module load gromacs/2016.3
source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
# echo "LOADING GROMACS 2016.3 -- NOTE: This needs gmx_mpi commands! Make sure to run those!"
echo "----- RUNNING GROMACS COMMANDS -----"

### RUNNING GROMACS ###
