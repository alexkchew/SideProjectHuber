#!/bin/bash 
# CHTC_HPC Submit Script (submit.sh)

#SBATCH --partition=univ2	# default "univ", if not specified
#SBATCH --time=5-00:00:00		# run time in days-hh:mm:ss
#SBATCH --nodes=1			# require 1 nodes
#SBATCH --ntasks-per-node=20            # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=16        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME


# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
num_cores="20" # number of cores you want to run with
LastAlpha="17"
ScriptDir="4_Submission_Files"

for currentJobIndex in $(seq 0 $LastAlpha); do
    # Echoing
    echo "---------------------------BEGIN---------------------------"
    echo "Running Alpha: ${currentJobIndex}"
    
    # Running each index
    bash ${ScriptDir}/submit_lambda_${currentJobIndex}.sh "${num_cores}"
done

echo "---------------------------END---------------------------"