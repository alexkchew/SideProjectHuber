#!/bin/bash

# CHTC_HPC Submit Script (batch_submit.sh)
# The main idea here is that we want to batch the jobs in addition to switch submission
# VARIABLES:
#   _JOBNAME_ <-- job name for the batch
#   _NUMNODES_ <-- number of nodes

#SBATCH --partition=univ,univ2		# default "univ", if not specified
#SBATCH --time=2-00:00:00		# run time in days-hh:mm:ss
#SBATCH --nodes=_NUMNODES_			# require 1 nodes
#SBATCH --ntasks-per-node=16            # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=16        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J _JOBNAME_

## DEFINING NUMBER OF CORES TO RUN THIS ON
export num_cores="16" # number of cores you want to run with

### RUNNING GROMACS ###
