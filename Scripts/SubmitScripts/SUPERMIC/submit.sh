#!/bin/bash
# ** submit.sh script for SUPERMIC server ** #
# Allocation about 20 CPUs per node
# Max Walltime: 72 hours (3 days)
# Queue: workq
# Run by: qsub submit.sh

#PBS -q workq
#PBS -A TG-ENG170011
#PBS -l walltime=70:00:00
#PBS -l nodes=1:ppn=20
#PBS -V
#PBS -N JOB_NAME
#PBS -j oe
#PBS -o job_output.out
#PBS -M akchew@wisc.edu
#PBS -m abe

# module load impi/4.1.3.048/intel64  # load impi

export TASKS_PER_HOST=20 # number of MPI tasks per host
export THREADS_HOST=1    # number of OpenMP threads spawned by each task on the host
export TASKS_PER_MIC=30  # number of MPI tasks per MIC
export THREADS_MIC=1     # number of OpenMP threads spawned by each task on the MIC

# GOING TO DIRECTORY
cd "$PBS_O_WORKDIR"        # go to where your PBS job is submitted if necessary

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
source "/home/achew/gromacs-thread/bin/GMXRC"; echo "Completed..."
echo "----- RUNNING GROMACS COMMANDS -----"

# RUN SCRIPTS BELOW 
NumberOfThreads="20" # Number of threads

gro_file="mixed_solv.gro"
input_top="mixed_solv.top"
output_prefix="mixed_solv"

# Defining mdp scripts
em_mdp="minim_LIQ.mdp"
equil_mdp="equil_LIQ.mdp"
prod_mdp="production_LIQ.mdp"

if [ ! -e "${output_prefix}_equil.gro" ]; then

# Need to minimize and equilibrate

# MINIMIZE
gmx grompp -f ${em_mdp} -c ${gro_file} -p ${input_top} -o ${output_prefix}_em -maxwarn 1
gmx mdrun -v -nt ${NumberOfThreads} -deffnm ${output_prefix}_em

# EQUILIBRATION
gmx grompp -f ${equil_mdp} -c ${output_prefix}_em -p ${input_top} -o ${output_prefix}_equil -maxwarn 1
gmx mdrun -v -nt ${NumberOfThreads} -deffnm ${output_prefix}_equil

fi


# MD PRODUCTION
if [ ! -e "${output_prefix}_prod.tpr" ]; then
gmx grompp -f ${prod_mdp} -c ${output_prefix}_equil.gro -p ${input_top} -o ${output_prefix}_prod -maxwarn 1
fi

gmx mdrun -v -nt ${NumberOfThreads} -deffnm ${output_prefix}_prod


