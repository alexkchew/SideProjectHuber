#!/bin/bash 
# submit_KL_most_likely_config.sh
# SWARM Submit Script (submit.sh)
## VARIABLES
#   JOB_NAME <-- job name

#SBATCH -p compute
#SBATCH -t 1000:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=28        # total number of "tasks" (cores) requested
#SBATCH --ntasks-per-node=28              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

# Defining number of threads
num_cores="28" # Number of threads

#Set the number of openmp threads
# export OMP_NUM_THREADS=${num_cores}
echo "----- RUNNING GROMACS COMMANDS -----"

### RUNNING GROMACS ###
# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
tpr_file_prefix="mixed_solv_prod"

## RUNNING MD SIMULATION
if [ ! -e "${tpr_file_prefix}.gro" ]; then
	## CHECKING IF A TRAJECTORY WAS ALREADY MADE
	if [ ! -e "${tpr_file_prefix}.xtc" ]; then
		gmx mdrun -v -nt ${num_cores} -deffnm "${tpr_file_prefix}"
	else
		## RESTARTING JOB SINCE IT DID NOT FINISH
		gmx mdrun -v -nt ${num_cores} -s ${tpr_file_prefix}.tpr -cpi ${tpr_file_prefix}.cpt -deffnm ${tpr_file_prefix} -append
	fi

fi