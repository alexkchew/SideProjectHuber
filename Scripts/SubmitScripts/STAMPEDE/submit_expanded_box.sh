#!/bin/bash 
# STAMPEDE Submit Script (submit_expanded_box.sh)

#SBATCH -p normal
#SBATCH -J JOB_NAME
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --export=ALL
#SBATCH -t 48:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="64" # number of cores you want to run with

echo "----- LOADING GROMACS ------"
# module load gcc/7.1.0
# source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
module load intel/18.0.2
module load impi/18.0.2
module load gromacs/2016.4

## DEFINING OUTPUT PREFIX
output_prefix="mixed_solv_prod"

## RUNNING GROMACS
gmx mdrun -v -nt "${num_cores}" -deffnm ${output_prefix}

