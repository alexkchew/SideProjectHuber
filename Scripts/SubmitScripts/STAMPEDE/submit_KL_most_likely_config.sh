#!/bin/bash 
# submit_KL_most_likely_config.sh
# STAMPEDE Submit Script (submit.sh)
## VARIABLES
#   JOB_NAME <-- job name


#SBATCH -p normal
#SBATCH -J JOB_NAME
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --export=ALL
#SBATCH -t 48:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

echo "----- LOADING GROMACS ------"
# module load gcc/7.1.0
# source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
module load intel/18.0.2
module load impi/18.0.2
module load gromacs/2016.4

# Defining number of threads
num_cores="64" # Number of threads

#Set the number of openmp threads
# export OMP_NUM_THREADS=${num_cores}
echo "----- RUNNING GROMACS COMMANDS -----"

### RUNNING GROMACS ###
# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
tpr_file_prefix="mixed_solv_prod"

## RUNNING MD SIMULATION
if [ ! -e "${tpr_file_prefix}.gro" ]; then
	## CHECKING IF A TRAJECTORY WAS ALREADY MADE
	if [ ! -e "${tpr_file_prefix}.xtc" ]; then
		gmx mdrun -v -nt ${num_cores} -deffnm "${tpr_file_prefix}"
	else
		## RESTARTING JOB SINCE IT DID NOT FINISH
		gmx mdrun -v -nt ${num_cores} -s ${tpr_file_prefix}.tpr -cpi ${tpr_file_prefix}.cpt -deffnm ${tpr_file_prefix} -append
	fi

fi