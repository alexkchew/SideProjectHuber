
# This script is designed for free energy perturbation simulations in STAMPEDE
# Updated script for 10 hrs
#SBATCH -p normal
#SBATCH -J JOB_NAME
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=48
#SBATCH --export=ALL
#SBATCH -t 48:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

## LOADING GROMACS
# LOADING GROMACS
echo "----- LOADING GROMACS ------"
# module load gcc/7.1.0
# source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
module load intel/18.0.2
module load impi/18.0.2
module load gromacs/2016.4

# Defining number of threads
num_cores="48" # Number of threads

#Set the number of openmp threads
# export OMP_NUM_THREADS=${num_cores}
echo "----- RUNNING GROMACS COMMANDS -----"

### RUNNING GROMACS ###
