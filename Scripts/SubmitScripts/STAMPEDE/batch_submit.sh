#!/bin/bash

# CHTC_HPC Submit Script (batch_submit.sh)
# The main idea here is that we want to batch the jobs in addition to switch submission
# VARIABLES:
#   _JOBNAME_ <-- job name for the batch
#   _NUMNODES_ <-- number of nodes

#SBATCH -p skx-normal
#SBATCH -J _JOBNAME_
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=_NUMNODES_
#SBATCH --ntasks-per-node=48
#SBATCH --export=ALL
#SBATCH -t 18:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

## LOADING GROMACS
# LOADING GROMACS
echo "----- LOADING GROMACS ------"
# module load gcc/7.1.0
# source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
module load intel/18.0.2
module load impi/18.0.2
module load gromacs/2016.3

# Defining number of threads
export num_cores="48" # Number of threads

#Set the number of openmp threads
# export OMP_NUM_THREADS=${num_cores}
echo "----- RUNNING GROMACS COMMANDS -----"
