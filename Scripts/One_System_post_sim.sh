#!/bin/bash

# One_System_post_sim.sh
# Written by Alex Chew (07/18/2017)
# The purpose of this script is to simply take your current simulation and run something out of it. This script was initially written to post-process TPR files for a different GROMACS version. 
# Usage: bash One_System_post_sim.sh tBuOH tBuOH 363.15 0.5 GVL_L GVLL 6 spce
# $1 is the mole fraction of water
# $2 is the name of the file
# $3 is the system temperature
# $4 is the weight fraction of water
# $5 is the name of the co-solvent
# $6 is the residue name of the co-solvent
# $7 is the desired box length
# $8 is Water model (tip3p, spce, spc, etc.)

##* -- Updates -- *##
# 170803 - Setting maxwarn = 1 for grompping
# 170911 - Added functionality to extract ns/day information
# 170922 - Added functionality to extract atom and volume from simulations
# 170924 - Fixed volume to read from the trajectory!
# 181211 - Fixing up code to include bashrc functions

### NOTE: Use post-processing load gromacs thread 5_0_1 if you want hydrogen bonding lifetimes!

### LOADING BASH RC ###
source "./bin/bash_rc.sh"; print_script_name

### USER-DEFINED FUNCTIONS ###

## Functions for summary ##

### FUNCTION TO EXTRACT SUMMARY INFORMATION FOR SOLVENT ENERGETICS
# The purpose of this function is to extract solvent energies
# INPUTS:
#   $1: summary file
# OUTPUTS:
#   
function extract_solvent_energies () {
    ## DEFINING INPUTS:
    input_summary_="$1"
    
    ## GREPPING OUT USEFUL ENERGIES
    coulomb_14=$(grep "Coulomb-14" "${input_summary_}" | awk '{print $2}')
    coulomb_energies=$(grep "Coulomb (SR)" "${input_summary_}" | awk '{print $3}')
    coulomb_recip_energies=$(grep "Coul. recip." "${input_summary_}"   | awk '{print $3}')
    
    LJ_energy_SR=$(grep "LJ-SR:SOLVENT-SOLVENT" "${input_summary_}"   | awk '{print $2}')
    LJ_energy_14=$(grep "LJ-14:SOLVENT-SOLVENT" "${input_summary_}"   | awk '{print $2}')
    
    
    ## PRINTING RESULTS
    echo "${coulomb_14}:${coulomb_energies}:${coulomb_recip_energies}:${LJ_energy_SR}:${LJ_energy_14}"

}

## EXTRACTING DIRECTORY INFORMATION ##
# The purpose of this function is to get a directory, then extract information from it
# $1 is the directory name, e.g. mdRun_403.15_6_nm_CEL_75_WtPercWater_spce_dioxane
# USAGE:
#       IFS=":"; read residue_name temp mass_frac cosolvent_name <<< "$(extract_dir_info ${Simulation_dir})"; IFS="${OLDIFS}"
function extract_dir_info () {
    # Residue name
    residue_name=$(echo "$1" | cut -d '_' -f5)
    
    # Temperature
    temp=$(echo "$1" | cut -d '_' -f2)
    
    # Mass Fraction of Water
    mass_frac=$(echo "$1" | cut -d '_' -f6)
    
    # Cosolvent Name
    cosolvent_name=$(echo "$1" | cut -d '_' -f9)
    
    # Printing as a variable
    echo "${residue_name}:${temp}:${mass_frac}:${cosolvent_name}"
}

## EXTRACTING LOG INFORMATION FROM FILE ##
# The purpose of this function is to look at a log file and get the performance
# $1 is the path to your current log file
function extract_log () {
    # Performance (ns/day)
    performance=$(tail "$1" | grep "Performance" | awk '{print $2}')
    # Number of atoms
    num_atoms=$(grep -E "There are:" "$1" | tail -n1 | awk '{print $(NF-1)}')
    # Host name (final one)
    host_name=$(grep -E "Host" "$1" | tail -n1 | awk '{print $2}')
    
    # Running external function to correctly read the host_name
    read clean_host_name <<< "$(read_hostname ${host_name})"
    
    echo "${performance}:${num_atoms}:${clean_host_name}"
}

### EXTRACTING .OUT INFORMATION ###
# The purpose of this function is to look at an output file and extract it accordingly
# $1 is the *.out file
# $2 is the tpr file
function extract_out () {

    ## LEGEND ## -- Checking for executable gmx
    # Since ACI and Comet are the same, we can use the fact that comet always has a "comet" within the name

    # SWARM
    # /app/gromacs_2016/bin/gmx

    # Stampede
    # /home1/04920/tg842191/gromacs_local_install/gromacs-2016/gromacs-thread/bin/gmx
    # /opt/apps/intel17/impi17_0/gromacs/2016.3/bin/mdrun_mpi - Stampede ibrun

    # Comet
    # /home/akchew/gromacs_local_install/gromacs-2016/gromacs-thread/bin/gmx

    # SMIC
    # /home/achew/gromacs-thread/bin/gmx

    # ACI
    # /home/akchew/gromacs_local_install/gromacs-2016/gromacs-thread/bin/gmx

    # First checking if it's comet
    if [[ $1 == *"comet"* ]]; then
        hostname="comet"
        
    # SUPERMIC
    elif [[ $1 == *"job_output.out"* ]]; then
        hostname="smic"
    else
        # Checking current executable
        GMX_executable_path=$(grep -E "Executable" "$1" | tail -n1 | awk '{print $2}')

        # STAMPEDE
        if [[ ${GMX_executable_path} == *"tg842191"* ]]; then
            hostname="stampede"
        # STAMPEDE ibrun
        elif [[ ${GMX_executable_path} == *"/opt/apps/intel17/impi17_0/gromacs/2016.3/bin/mdrun_mpi"* ]]; then
            hostname="stampede_ibrun"
        # SWARM    
        elif [[ ${GMX_executable_path} == *"/app/gromacs_2016/bin/gmx"* ]]; then
            hostname="swarm"
        # ACI
        elif [[ ${GMX_executable_path} == *"/home/akchew/gromacs_local_install/gromacs-2016/gromacs-thread/bin/gmx"* ]]; then
            hostname="ACI"

        # Nothing
        else
            hostname="Cannot_ID"
        fi
    fi

    # Now, finding the performance, etc.
    performance=$(grep "Performance" "$1" | tail -n1 | awk '{print $2}')

    # Finding total number of atoms using TPR file
    num_atoms=$(gmx dump -s "$2" 2> /dev/null | grep -E "natoms" | awk {'print $NF'})
    
    # Printing
    echo "${performance}:${num_atoms}:${hostname}"

}


### FUNCTION TO ADD FIRST LINE TO FILE ##
# The purpose of this script is to check if file exists. If it not, then simply add the first line to the file.
# $1 is the full path to your file
function add_first_line () {
    if [[ ! -e "$1" ]]; then
        echo "PATH, File, Residue_name, Temperature, Water_Mass_Frac, Cosolvent, Host, Performance(ns/day), Num_Atoms" > "$1"
        
    fi
}

# Generalized from the original function
# $1 is the full path to your file
# $2 is the variables you want to add on, e.g. $2 =" var1, var2 ", etc.
function add_first_line_general () {
    if [[ ! -e "$1" ]]; then
        echo "PATH, Residue_name, Temperature, Water_Mass_Frac, Cosolvent, $2" > "$1"
        
    fi
}


## FUNCTION TO READ THE CORRECT HOSTNAME FROM LOG FILE ##
# The purpose of this function is to take your hostname and correctly identify it
# $1 is your output from finding the host name
function read_hostname () {
    if [[ "$1" == *"stampede"* ]]; then 
        echo "stampede" 
    elif [[ "$1" == *"swarm"* ]]; then
        echo "swarm"; 
    elif [[ "$1" == *"smic"* ]]; then
        echo "smic"
    elif [[ "$1" == *"comet"* ]]; then
        echo "comet"
    elif [[ "$1" == *"aci"* ]]; then
        echo "aci"
    else
        echo "Does_Not_Exist"
    fi
}


### FUNCTION TO READ TPR FILE AND OUTPUT NUMBER OF MOLECULES ###
# $1: TPR file name
#function find_num_molecules () {
#
#
#    # Printing what you found
#    echo "(${species[@]}):(${num_of_species[@]})"
#}
### FUNCTION TO SEE WHAT TYPE OF SPECIES YOU HAVE ###
# $1: Input from species function
function check_species () {

    ## FUNCTION TO CHECK IF VALUE IS WITHIN AN ARRAY
    function contains() {
        local n=$#
        local value=${!n}
        for ((i=1;i < $#;i++)) {
            if [ "${!i}" == "${value}" ]; then
                echo "y"
                return 0
            fi
        }
        echo "n"
        return 1
    }
    ## DEFINING DEFAULT ARRAYS ##
    declare -a Cosolvent_Array=("GVLL" "THF" "DIO")
    Water_Residue_Name="SOL"
    
    
    # Checking if it is a cosolvent
    if [ "$(contains "${Cosolvent_Array[@]}" "$1")" == "y" ]; then
        currentSpecies="Cosolvent"
    elif [ "$1" == "${Water_Residue_Name}" ]; then
        currentSpecies="Water"
    else
        currentSpecies="Reactant"
    fi
    
    echo "$currentSpecies"

}


### FUNCTION TO EXTRACT VOLUME DATA ###
# $1: Input file as an xvg from gmx traj
function get_volume_xvg () {
    # Start by finding when the data begins
    line_data_begins=$(grep -nE "@" "$1" | tail -n1 | sed "s/:.*//")

    # Then, let's find the last data
    line_data_ends=$(grep -nE "*" "$1" | tail -n1 | sed "s/:.*//")

    # Getting the data
    my_data=$(sed -n "$(( ${line_data_begins} + 1)),${line_data_ends}p" "$1" | awk '{print $2}' )
    
    # Calculating volume
    avg_volume=$(echo ${my_data} | awk '{ total += $2^3; count++ } END { print total/count }') # <-- can be used to average to get values
    
    # Printing
    echo "${avg_volume}"
}

###########################
##### INPUT VARIABLES #####
###########################

## RELOADING BIN FILE
source "bin/bash_rc.sh"; print_script_name

# Saving deliminator #
OLDIFS="$IFS"

### USER DEFINED PARAMETERS ###
solute_name="$1" # "sorbitol"
solute_residue_name="$2" #"SOR"
system_temp="$3" #363.15 # System temperature, edited in mdp file labelled "TEMPERATURE"
solvent_one_wt_frac="$4" #1.0 # fraction of first solvent (water)

## DEFINING COSOLVENT
solvent_two_name="$5" #dioxane # Co-solvent molecule name   # GVL_L, dioxane, tetrahydrofuran
solvent_two_residue_name="$6" #DIO # Cosolvent residue name # GVLL, DIO, THF

## DEFINING BOX LENGTH
desired_box_length="$7"  #4 nms

## DEFINING WATER MODEL
water_model="$8" # tip3p, spce, spc, etc. (original tip3p) "spce"

## DEFINING MAIN SIMULATION DIRECTORY
specific_sim_dir="$9"

## DEFINING MAIN ANALYSIS DIRECTORY
specific_analysis_dir="${10}" #"170724-TBUOH_XYL_LGA_prod_extend"

## DEFINING OUTPUT DIRECTORY
percent_solvent_one_wt_frac=$(awk -v num=$solvent_one_wt_frac 'BEGIN{ printf "%d",num*100}') 
output_dir=${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_$solvent_two_name # Can be $2 if you'd like

# Finding current date
currentDate=$(date +'%y%m%d')

###############################
### DEFINING DIRECTORY NAME ###
###############################
# Checking if there is a cosolvent
if [ $(awk -v wt_frac=$solvent_one_wt_frac 'BEGIN{ if (wt_frac != 1.00 && wt_frac != 0.00) print 1; else print 0}') -eq 1 ]; then
    # Defining preparation directory name
    prep_dir_name=${percent_solvent_one_wt_frac}_Water_$solvent_two_residue_name 
    
    else
    # Checking if there is water
    if [ $(awk -v wt_frac=$solvent_one_wt_frac 'BEGIN{ if (wt_frac == 0.00) print 1; else print 0}') -eq 1 ]; then
    # Defining preparation directory name with pure cosolvent
    prep_dir_name=${percent_solvent_one_wt_frac}_$solvent_two_residue_name 

        else # Pure water
        # Defining prep name with no cosolvent
        prep_dir_name=${percent_solvent_one_wt_frac}_Water
    
    # Changing output directory
    output_dir=${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_Pure
    
    fi
fi

##############################
### DEFINING WORKING PATHS ###
##############################

## DEFINING WORKSPACE
WorkSpace="${PATH2SIM}/${specific_sim_dir}" # _100

## DEFINING SIMULATION DIRECTORIES
Simulation_dir=mdRun_${system_temp}_${output_dir} # Directory where all the runs will take place
path2Sim_dir=${WorkSpace}/${Simulation_dir} # Defining path structure

# Checking if simulation directory exists
if [ ! -e $path2Sim_dir ]; then # Checking if solute exists
    echo "No simulation directory! Check the following simulation directory:"
    echo -e "${path2Sim_dir}\n"
    echo "Exiting...."
    exit
fi

################
### LOGICALS ###
################
## TPR EXTENSION ## -- Used for hydrogen bonding
wantTPR_extended="False" # True if you want TPR file extended from prod_extend for h-bond lifetimes

## SIMULATION PERFORMANCE ## -- Used to get the performance of simulations (i.e. ns/day)
wantSim_Performance="False" # True If you want performance information about your jobs
csvSim_Perf_Output="${currentDate}-Sim_Prod_Extract.csv"
csvSim_Pref_AllOutFiles="${currentDate}-Sim_Prod_Extract_OutFiles.csv"

## SYSTEM SIZE / DIMENSIONS, ETC. ##
wantSim_Size="False" # True if you want the system size (i.e. number of atoms, etc.)
Sim_Size_Output="${currentDate}-Sim_System_Size_Extract.csv"
first_frame="10000" # ps, first frame to start checking volume

## SOLVENT ENERGIES (ANALYSIS)
wantSim_Energies="False" # True if you want solvent energies
## DEFINING SCRIPT NAMES
compute_solvent_energies_script="compute_solvent_energies.sh"
## DEFINING FULL PATH TO SOLVENT ENERGIES
path_compute_solvent_energies_script="${PATH2POSTSIMSCRIPTS}/${compute_solvent_energies_script}"

## SOLVENT ENERGIES (EXTRACTION)
## EXTRACTION OF SOLVENT ENERGIES
wantSim_Energies_extraction="True"
## DEFINING OUTPUT NAME
output_solvent_energies="solvent_energies.csv"
## DEFINING PATH
path_to_output_solvent_energies="${WorkSpace}/${output_solvent_energies}"
## DEFINING OUTPUT SUMMARY FILE
output_summary_file="mixed_solv_prod_energy_grps.summary"
## DEFINING INCOMPLETE_JOBS SCRIPT
incomplete_solvent_energies_job="incomplete_job_list.txt"


## DIRECTORY AND FILES
forcefield_dir="charmm36-nov2016.ff"
Sim_Perf_dir="${PATH2SCRIPTS}/Sim_Performance" # Directory for simulation performances

## OUTPUT ANALYSIS DIRECTORY
main_analysis_dir="${PATH2ANALYSIS}"

# Defining files within the directory
prod_gro="mixed_solv_prod.gro"
prod_xtc="mixed_solv_prod.xtc"
prod_tpr="mixed_solv_prod.tpr"
prod_log="mixed_solv_prod.log"
prod_edr="mixed_solv_prod.edr"
top_file="mixed_solv.top"

# Defining output files
tpr_file="mixed_prod_extended.tpr"

# MDP Files
em_mdp="minim_LIQ.mdp" # Energy minimization mdp file
equil_mdp="equil_LIQ.mdp" # Equilibration mdp file
prod_mdp="production_LIQ.mdp" # MD Production mdp file
prod_extended_mdp="prod_extend/prod_5_ns_xtcout_50_nstlist_5/production_LIQ_extended.mdp"

#############################################################################
################################# MAIN CODE #################################
#############################################################################

# Going into working directory
cd "${path2Sim_dir}"

### RUN COMMANDS FOR EACH SIMULATION ###

############################
####### TPR EXTENDED #######
############################

### TPR_EXTENDED: This simply goes into every simulation, take the prod_extend, create a new TPR file, then copy it to your simulation folder (i.e. specific_analysis_dir)
if [[ ${wantTPR_extended} == "True" ]]; then

    # Defining analysis output directory
    path2analysis_dir="${main_analysis_dir}/${specific_analysis_dir}/${solute_residue_name}/${Simulation_dir}"

    # Sleeping to make sure everything is right
    echo "***Check your system, initiating in 2 seconds***"
    echo "Creating TPR file, ${tpr_file}, in the following directory:"
    echo -e "${path2Sim_dir}\n"
    echo -e "Copying TPR file to ${path2analysis_dir} \n"
    sleep 2; 

    # Running GMX to get a TPR file for production extend
    gmx grompp -f ${prod_extended_mdp} -p ${top_file} -c ${prod_gro} -o ${tpr_file} -maxwarn 1
    #
    ## Then, copying the tpr file to the corresponding analysis directory
    cp -r ${tpr_file} ${path2analysis_dir} # Copying tpr file
    cp -r ${solute_name}.itp ${path2analysis_dir} # Copying itp file
    rm ${tpr_file} # Removing tpr file

fi

###############################
####### PRODUCTION INFO #######
###############################

### SIMULATION PRODUCTION DETAILS ###
if [[ ${wantSim_Performance} == "True" ]]; then
    # Printing
    echo "Performing simulation analysis for performance ns/day..."
    
    # Defining current working directory for storing information
    sim_perf_full_path="${Sim_Perf_dir}"
    sim_perf_csv_full_path="${sim_perf_full_path}/${csvSim_Perf_Output}"
    sim_perf_csv_AllOut_full_path="${sim_perf_full_path}/${csvSim_Pref_AllOutFiles}"
    
    # Creating directory to store all files in 
    if [[ ! -e ${sim_perf_full_path} ]]; then
        mkdir -p "${sim_perf_full_path}"
    fi
    
    # Start by checking to see if the text file was already written
    add_first_line ${sim_perf_csv_full_path}
    add_first_line ${sim_perf_csv_AllOut_full_path}
    
    # Now, we simply extract the details to get the details
    
    # Extracting data from the file
    IFS=":"; read residue_name temp mass_frac cosolvent_name <<< "$(extract_dir_info ${Simulation_dir})"; IFS="${OLDIFS}"
    
    # Reading from the production files
    IFS=":"; read performance num_atoms clean_host_name <<< "$(extract_log ${prod_log})"; IFS="${OLDIFS}"
    
    # Now, writing to your file
    echo "${path2Sim_dir},${prod_log}, ${residue_name}, ${temp}, ${mass_frac}, ${cosolvent_name}, ${clean_host_name}, ${performance}, ${num_atoms}" >> ${sim_perf_csv_full_path}
    
    # Print what you did
    echo -e "Copied details, performance, hostname to: ${sim_perf_csv_full_path} \n"
    
    echo "--- Looking into output file ---"
    # Now, going through all the output files -- used as a way to find more performance things
    for currentOutputFile in $(ls *.out); do
        echo "Working on ${currentOutputFile}"
        
        # Extracting information from each output file and inserting into our databank
        IFS=":"; read performance num_atoms host_name <<< "$(extract_out ${currentOutputFile} ${prod_tpr})"; IFS="${OLDIFS}"
        
        # Loading into your file
        echo "${path2Sim_dir},${currentOutputFile}, ${residue_name}, ${temp}, ${mass_frac}, ${cosolvent_name}, ${host_name}, ${performance}, ${num_atoms}" >> ${sim_perf_csv_AllOut_full_path}
    
    done
    
fi

###########################
####### SYSTEM SIZE #######
###########################
if [[ ${wantSim_Size} == "True" ]]; then
    # Defining output files
    volume_xvg_file="Volume.xvg"

    # Printing
    echo "Looking into simulations to see the system details!"
    
    # Defining desired general output
    sim_size_output_string="N_water, N_cosolvent, Volume(nm3)"
    
    # Defining full path for output
    path2Sim_Size="${script_dir}/${Sim_Size_Output}"
    
    # Adding a first line to the output
    add_first_line_general "${path2Sim_Size}" "${sim_size_output_string}"
    
    ### FINDING DIRECTORY INFORMATION ###
    IFS=":"; read residue_name temp mass_frac cosolvent_name <<< "$(extract_dir_info ${Simulation_dir})"; IFS="${OLDIFS}"
    
    ### NUMBER OF MOLECULES ###
    # Using tpr file to get all molecule types
    # Start by finding the different species
    declare -a species=($(gmx dump -s "${prod_tpr}" 2> /dev/null | grep "moltype" | grep "=" | awk '{print $NF}' | sed "s#\"##g"))
    
    # Then find the number of molecules for each component
    declare -a num_of_species=($(gmx dump -s "${prod_tpr}" 2> /dev/null | grep -E "#molecules" | awk '{print $NF}'))
    
    total_species=$(( ${#species} - 1 )) # Finding total number of species
    echo ${total_species}
    
    # Now, we will loop through each species to see if it is a cosolvent, reactant, etc.
    num_cosolvent=0 # Default values
    num_water=0 # Default values
    
    for eachSpeciesIndex in $(seq 0 ${total_species}); do
        # Defining species
        currentSpecies=${species[eachSpeciesIndex]}; echo "Checking current species: ${currentSpecies}"
        
        # Checking the species
        read check_species_result <<< "$(check_species ${currentSpecies})"
        echo $check_species_result
        if [ "${check_species_result}" == "Cosolvent" ]; then
            num_cosolvent=${num_of_species[eachSpeciesIndex]}
        elif [ "${check_species_result}" == "Water" ]; then
            num_water=${num_of_species[eachSpeciesIndex]}
        fi # Not checking for reactants
        
    done
    
    ### VOLUME ###
    # Running gmx traj to get the volume
gmx traj -f ${prod_xtc} -s ${prod_tpr} -ob ${volume_xvg_file} -b ${first_frame} << INPUT
System
INPUT

    # Now given that you have the file, we need to extract the data
    #gmx energy -f "${prod_edr}" > ${volume_xvg_file} 2>&1 << INPUT
    #Volume
    #
    #INPUT

    # Grepping to get volume
#    volume=$(grep -E "Volume" ${volume_xvg_file} | grep "nm" | awk '{print $2}')
    read volume <<< "$(get_volume_xvg ${volume_xvg_file})"
    
    # Printing to text file
    echo "${path2Sim_dir}, ${residue_name}, ${temp}, ${mass_frac}, ${cosolvent_name}, ${num_water}, ${num_cosolvent}, ${volume}" >> ${path2Sim_Size}
    
fi

################################ 
####### SOLVENT ENERGIES #######
################################

### COMPUTING SOLVENT ENERGIES
if [[ ${wantSim_Energies} == "True" ]]; then
    ## GOING TO POST DIRECTORY
    cd "${PATH2POSTSIMSCRIPTS}"
    
    ## RUNNING SOLVENT ENERGIES SCRIPT
    bash "${path_compute_solvent_energies_script}" "${solute_name}" "${solute_residue_name}" "${path2Sim_dir}"
    
    ## GOING BACK TO CURRENT WORKING DIRECTORY
    cd "$path2Sim_dir"
    
fi

### EXTRACTION OF SOLVENT ENERGIES
if [[ ${wantSim_Energies_extraction} == "True" ]]; then
    ## DEFINING FULL PATH TO SUMMARY FILE
    path_to_summary_file="${path2Sim_dir}/${output_summary_file}"
    ## SEEING IF SUMMARY FILE EXISTS
    if [ ! -e "${path_to_summary_file}" ]; then
        echo "Error! Path does not exist: ${path_to_summary_file}"
        echo "Perhaps you forgot to run the computing of solvent energies"
        echo "Outputting simulation path to ${incomplete_solvent_energies_job}"
        echo "${path2Sim_dir}" >> "${PATH2SCRIPTS}/${incomplete_solvent_energies_job}"
        echo "Pausing so you can see this error!"
        sleep 3
    else
        ## PRINTING
        echo "Working on analysis for: ${path_to_summary_file}"
        ## DEFINING OUTPUT FILE
        add_first_line_general "${path_to_output_solvent_energies}" "Coulomb-14, Coulomb (SR)[kJ/mol], Coul. recip.[kJ/mol], LJ-SR:SOLVENT-SOLVENT, LJ-14:SOLVENT-SOLVENT"
        ## READING RESIDUE NAME, ETC.
        IFS=":"; read residue_name temp mass_frac cosolvent_name <<< "$(extract_dir_info ${Simulation_dir})"; IFS="${OLDIFS}"

        ## READING COULOMB ENERGIES
        IFS=":"; read coulomb_14 coulomb_energies coulomb_recip_energies LJ_energy_SR LJ_energy_14 <<< "$(extract_solvent_energies ${path_to_summary_file})"; IFS="${OLDIFS}"
        ## PRINTING TO FILE
        echo "${path2Sim_dir},${residue_name},${temp},${mass_frac},${cosolvent_name},${coulomb_14},${coulomb_energies},${coulomb_recip_energies},${LJ_energy_SR},${LJ_energy_14}" >> "${path_to_output_solvent_energies}"
    fi
fi


## GOING BACK TO WORKING DIRECTORY
cd "$CURRENTWD" # Probably unnecessary

# Printing results
echo "------ Summary ------"
echo "Solute Molecule: ${solute_name} ( ${solute_residue_name} )"
echo "Solvent Molecule: ${solvent_two_name} ( ${solvent_two_residue_name} )"
echo "Weight Fraction of Water: ${solvent_one_wt_frac}"
echo "Full Simulation Working Directory: ${path2Sim_dir}"
echo "Temperature (K): ${system_temp}"
