#!/bin/bash

# quick_solvent_net_predictions.sh
# The purpose of this script is to run through SolventNet predictions for a single directory
# INPUTS:
#   $1: input_sim_path: input path directory
#   $2: input simulation name
# Written by: Alex K. Chew (10/11/2019)

### LOADING BASH RC ###
source "bin/bash_rc.sh"
#######################

#######################
### DEFINING INPUTS ###
#######################

## INPUT SIM
input_sim_path="$1"
# "/home/akchew/scratch/SideProjectHuber/Simulations/191010-Cyclohexanol_4ns"

## DEFINING INPUT NAME
input_sim_name="$2"
# "mdRun_433.15_6_nm_CYO_25_WtPercWater_spce_dmso"

############################
### DEFAULT INPUT VALUES ###
############################

## PREFIX
input_prefix="mixed_solv_prod"

## GRO AND XTC
input_gro_file="${input_prefix}.gro"
input_xtc_file="${input_prefix}.xtc"
input_tpr_file="${input_prefix}.tpr"

## CENTERING TRAJECTORY DETAILS
centered_trunc_traj_beginning_time="0" # 0 ns
centered_trunc_traj_end_time="4000" # 4 ns
system_selection="System"
centered_trunc_traj_output_xtc="${input_prefix}_center_${centered_trunc_traj_beginning_time}_${centered_trunc_traj_end_time}.xtc"

## DEFAULT VOXEL DETAILS
box_size=4.0
box_inc="0.2"
box_map_type="3channel_oxy"
## NORMALIZATION
normalization="maxima"

######################
### DEFINING PATHS ###
######################

## DEFINING PATH OF SIMULATION
path_to_sim="${input_sim_path}/${input_sim_name}"

## DEFINING PATH TO LOOP INTERPOLATION
path_loop_grid="${CNN_PYTHON_LOOP_GRID}"

## STOP IF DOES NOT EXIST
stop_if_does_not_exist "${path_to_sim}"

###################
### MAIN SCRIPT ###
###################

## EXTRACTING NAME
read -a extract_array <<< $(extract_output_name ${input_sim_name})

## EXTRACTING DETAILS
residue_name=${extract_array[4]}
temp=${extract_array[0]}
sim_box_size=${extract_array[1]}
water_wt_perc=${extract_array[2]}
cosolvent_res_name=${extract_array[3]}

## DEFINING VARIABLES FOR VOXELATION
solutes="${residue_name}"
temperatures="${temp}"
mass_frac="${water_wt_perc}"
input_traj_loc="${path_to_sim}"
gro_file="${input_gro_file}"
xtc_file="${centered_trunc_traj_output_xtc}"
cosolvents="${cosolvent_res_name}"
output_database_loc="${path_to_sim}"
pickle_name="${solutes}_${temperatures}_${cosolvents}_${mass_frac}"
picklesuffix="_SolventNet.pickle"

## DEFINING FULL PICKLE NAME
full_pickle_name="${pickle_name}${picklesuffix}"

## GOING INTO DIRECTORY
cd "${path_to_sim}"

## CHECKING IF FILES ARE ALREADY COMPLETE
if [[ ! -e "${full_pickle_name}" ]] || [[ ! -e "${centered_trunc_traj_output_xtc}" ]]; then

########################################
## PART 1: CENTERING REACTANT 
########################################
echo "1: Centering reactant"

## TRUNCATING AND CENTERING
if [ ! -e "${centered_trunc_traj_output_xtc}" ]; then
    truncate_and_center "${input_tpr_file}" "${input_xtc_file}" "${residue_name}" "${system_selection}" "${centered_trunc_traj_output_xtc}" "${centered_trunc_traj_beginning_time}" "${centered_trunc_traj_end_time}"
fi

echo "------------------------------"

########################################
## PART 2: GENERATING VOXEL REPS
########################################
echo "2: Getting voxel representations"

## PRINTING
echo "--- Creating database: ${output_database_loc} ---"
echo "Solutes: ${solutes}"
echo "Cosolvents: ${cosolvents}"
echo "Temperatures: ${temperatures}"
echo "Mass fraction: ${mass_frac}"
echo "Trajectory location: ${input_traj_loc}"
echo "XTC file: ${xtc_file}"

## SEEING IF PICKLE FILE EXISTS
if [ ! -e "${full_pickle_name}" ]; then

## PRINTING PYTHON
echo "Running python..."
## PAUSING
sleep 3

## RUNNING PYTHON CODE FOR VOXEL REPS
/usr/bin/python3.4 "${path_loop_grid}"  -i "${input_sim_path}" \
                                        -o "${output_database_loc}" \
                                        -g "${gro_file}" \
                                        --xtcfile "${xtc_file}" \
                                        -c "${box_inc}" \
                                        -b "${box_size}" \
                                        -t "${box_map_type}" \
                                        -n "${normalization}" \
                                        -m "${mass_frac}" \
                                        --solvent "${cosolvents}" \
                                        -s "${solutes}" \
                                        --temp "${temperatures}" \
                                        --fullpath \
                                        --picklesuffix "${picklesuffix}" \
                                        --skipcosnaming \
                                        --specifysolvents

else
    echo "--> ${full_pickle_name} exists!"

fi

if [ ! -e "${full_pickle_name}" ]; then
    echo "Error! For some reason, this pickle did not work:"
    echo "${full_pickle_name}"
    echo "Pausing here so you can see it!"
    sleep 5
fi

echo "------------------------------"

## REMOVING ANY ADDITIONAL VALUES
if [[ -e \#* ]]; then
    rm \#*    
fi


else
    echo "${input_sim_name} has been voxelated! Continuing..."
fi
