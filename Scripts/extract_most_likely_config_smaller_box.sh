#!/bin/bash

# extract_most_likely_config_smaller_box.sh
# The purpose of this code is to extract most likely configurations and generate a smaller simulation box size. The utility is to run additional ab initio calculations with a smaller box size.

## RELOADING BIN FILE
source "bin/bash_rc.sh"

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT SIM
declare -a input_sim_array=("Mostlikely_433.15_6_nm_PDO_100_WtPercWater_spce_Pure") 
# "Mostlikely_433.15_6_nm_PDO_10_WtPercWater_spce_dmso"
# "Mostlikely_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane"
# Mostlikely_433.15_6_nm_PDO_10_WtPercWater_spce_dmso
# "Mostlikely_433.15_6_nm_PDO_100_WtPercWater_spce_Pure"
## DEFINING FRAME ARRAY
declare -a desired_frames_ps_of_interest=("150000" "200000")
# "150000" "200000"
# "199440" "183670" "170660" GAUCHE PDO
# "199440" "183670" "170660" GAUCHE PDO
# "71680" "119260" "73080" TRANS PDO

## DEFINING BOX LENGTH
declare -a desired_box_length_nm_array=("0.5") #  "1.5" # "1.0" "1.5"

## DEFINING BOX TYPE ARRAY
declare -a box_type_array=("spherical" "rectangular") #  "rectangular"
#  "rectangular"

## LOOPING
for input_sim in ${input_sim_array[@]}; do
    ## LOOPING THE BOX LENGTH
    for desired_box_length_nm in ${desired_box_length_nm_array[@]}; do
        ## LOOPING THROUGH DESIRD FRAME OF INTEREST
        for desired_frame_ps in ${desired_frames_ps_of_interest[@]}; do
            ## LOOPING THROUGH BOX TYPE
            for box_type in ${box_type_array[@]}; do

## DEFINING INPUT PATH
main_sim_folder="190922-PDO_most_likely_gauche"
# FINAL_PDO_DEHYDRATION
input_path="${PATH2SIM}/${main_sim_folder}/${input_sim}"

## DEFINING RESIDUENAME
residue_name="PDO"

## DEFINING OUTPUT DIRECTORY NAME
output_dir="191118-extract_most_likely_0.5nm_gauche_pure_water"
# "191023-extract_most_likely_config_zhizhang_0.5nm_gauche"
# "191008-extract_most_likely_config_zhizhang_0.5nm"

## DEFINING INPUT GRO AND XTC FILE
input_tpr="mixed_solv_prod.tpr"
input_gro="mixed_solv_prod.gro"
input_xtc="mixed_solv_prod.xtc"

## DEFINING DESIRED FRAME
# desired_frame_ps="200000"

## DEFINING DISTANCE TO EDGE
dist_to_edge="0.05"

#############################
### DEFINING OUTPUT FILES ###
#############################

## DEFINING OUTPUT FILE NAME
input_basename=$(basename ${input_path})
output_file="${box_type}-${desired_box_length_nm}_nm-${desired_frame_ps}_ps-${input_basename}"

## DEFINING OUTPUT PATH
output_path="${PATH2SIM}/${output_dir}/${output_file}"

## DEFINING PREFIX
prefix="mixed_solv"

## OUTPUT GRO
new_gro_file="${prefix}.gro"
new_tpr_file="${prefix}.tpr"

## DEFINING INDEX FILE
index_file="residue_with_system.ndx"
index_file_trim="trim.ndx"

###################
### MAIN SCRIPT ###
###################

## DEFINING INDEX FILE
index_make_ndx_residue_with_system "${input_path}/${input_tpr}" "${input_path}/${index_file}" "${residue_name}"

## CREATING DIRECTORY
create_dir "${output_path}" -f

## GO TO OUTPUT PATH
cd "${output_path}"

## DUMPING NEAREST TRAJECTORY
echo "Creating ${new_gro_file} from ${input_path}..."
gmx trjconv -f "${input_path}/${input_xtc}" \
            -s "${input_path}/${input_tpr}" \
            -o "${output_path}/${new_gro_file}" \
            -dump "${desired_frame_ps}" \
            -n "${input_path}/${index_file}" \
            -pbc mol -center << INPUTS
${residue_name}
System
INPUTS

## COPYING TPR FILE
cp "${input_path}/${input_tpr}" "${output_path}/${new_tpr_file}"

## FINDING GRO FILE SIZE
read -a box_size <<< $(gro_measure_box_size "${new_gro_file}")

## GETTING LOWER AND UPPER LIMIT
lower_limit=$(awk -v L="${box_size[0]}" -v dx="${desired_box_length_nm}" 'BEGIN{ printf "%.5f", L/2 - dx /2 }') 
upper_limit=$(awk -v L="${box_size[0]}" -v dx="${desired_box_length_nm}" 'BEGIN{ printf "%.5f", L/2 + dx /2 }') 

## DEFINING NEW GRO FILE
file_trim="${new_gro_file%.gro}_trim.gro"

## GMX SELECT TO GENERATE INDEX FILE 
# Example of round one(ROUND)
if [[ "${box_type}" == "spherical" ]]; then
    ## SELECTING SPHERICAL NM
    gmx select -f "${new_gro_file}" -s "${new_tpr_file}" -on "${index_file_trim}" -select "group "System" and same residue as within ${desired_box_length_nm} of resname ${residue_name}"
    ## TRJCONV TO GET THE NEW SELECTION
    gmx trjconv -f "${new_gro_file}" -s "${new_tpr_file}" -n "${index_file_trim}" -o "${file_trim}" -center
    
elif [[ "${box_type}" == "rectangular" ]]; then
    ## SELECTING BOX
    gmx select -f "${new_gro_file}" -s "${new_tpr_file}" -on "${index_file_trim}" -select "group "System" and same residue as all and x > ${lower_limit} and x < ${upper_limit} and y > ${lower_limit} and y < ${upper_limit} and z > ${lower_limit} and z < ${upper_limit}"
    
    ## TRJCONV TO GET THE NEW SELECTION
    gmx trjconv -f "${new_gro_file}" -s "${new_tpr_file}" -n "${index_file_trim}" -o "${file_trim}" -box "${desired_box_length_nm}" "${desired_box_length_nm}" "${desired_box_length_nm}" -center
fi

## DEFINING FILE WITH EXPANDED
file_box_expand="${new_gro_file%.gro}_trim_resized.gro"

## DISTANCE TO EDGE
gmx editconf -f "${file_trim}" -o "${file_box_expand}" -d "${dist_to_edge}" 
## PDB FILE
gmx editconf -f "${file_trim}" -o "${file_box_expand%.gro}.pdb" -d "${dist_to_edge}" 
            done
        done
    done
done
