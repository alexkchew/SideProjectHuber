#!/bin/bash

# One_System_Prep_prod_extension.sh
# Written by Alex Chew (06/21/2017)
# The purpose of this script is to take your cosolvent mixture and extend the simulation to capture dynamical effects

# Usage: bash One_System_Prep_prod_extension.sh tBuOH tBuOH 363.15 0.25 GVL_L GVLL 6 spce
# $1 is the mole fraction of water
# $2 is the name of the file
# $3 is the system temperature
# $4 is the weight fraction of water 
# $5 is the name of the co-solvent
# $6 is the residue name of the co-solvent
# $7 is the desired box length
# $8 is Water model (tip3p, spce, spc, etc.)
# $9 is the simulation directory

##* -- Updates -- *##
# 170731 - Switched off velocity creation for trr files
# 180514 - Added simulation directory variable

echo -e "\n ----- One_System_Prep_prod_extension ----- \n"

## RELOADING BIN FILE
source "bin/bash_rc.sh"

# -- USER DEFINED PARAMETERS -- #
solute_name="$1" # "sorbitol"
# solute_residue_name="$2" #"SOR"

### GETTING RESIDUE NAME
## DEFINING SOLUTE ITP
solute_itp_file="${PREP_SOLUTE_DIR}/${solute_name}/${solute_name}.itp"
## GETTING RESIDUE NAME
solute_residue_name=$(itp_get_resname ${solvent_itp_file})

system_temp="$2" #363.15 # System temperature, edited in mdp file labelled "TEMPERATURE"
solvent_one_wt_frac="$3" #1.0 # fraction of first solvent (water)

# Defining co-solvent
solvent_two_name="$4" #dioxane # Co-solvent molecule name   # GVL_L, dioxane, tetrahydrofuran
solvent_two_residue_name="$5" #DIO # Cosolvent residue name # GVLL, DIO, THF

# Defining desired box length
desired_box_length="$6"  #4 nms

# Defining water model
water_model="$7" # tip3p, spce, spc, etc. (original tip3p) "spce"

## DEFINING SIMULATION DIRECTORY
main_sim_dir="$8"

## Defining OUTPUT Directory ##
percent_solvent_one_wt_frac=$(awk -v num=$solvent_one_wt_frac 'BEGIN{ printf "%d",num*100}') 

# Checking if we even have a cosolvent
if [ $(awk -v wt_frac=$solvent_one_wt_frac 'BEGIN{ if (wt_frac != 1.00) print 1; else print 0}') -eq 1 ]; then

output_dir=${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_${solvent_two_name}
    
else
output_dir=${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_Pure
    
fi

## Defining Important directories ##

## MAIN DIRECTORIES ##

## SIMULATION DIRECTORIES ##

# INPUT FILES #
forcefield_name="charmm36-nov2016.ff"
forcefield_dir="$INPUT_DIR/$forcefield_name" # Force field parameters

## MDP Files ##
prod_extend_mdp="production_LIQ_extended.mdp" # MD production extension file
path2prod_extend_mdp="${MDP_DIR}/${prod_extend_mdp}"

# In MDP file, defining times -- Changes NSTEPS in mdp file -- Assumes dt = 0.002 fs
ProdTime="2500000" # "2500000" # steps in ps, equivalent to 5 ns -- 9 ns
nstxtcout="50" # Amount of times you would like to export xtc files, equivalent to 0.1 ps
nstvout="0" # Amount of times you would like to export velocities
nstlist="5" # Neighbor list update frequency -- optimal for GPUs //equivalent to 0.01 ps

# Converting the times
ProdTime_ns=$(awk -v prod_time=${ProdTime} 'BEGIN{ printf "%d", prod_time*0.002/1000 }') # ns

# GRO and TOP files
input_gro="mixed_solv_prod.gro" # Assuming job was complete
input_top="mixed_solv.top"

# Submission script
output_submit_name="submit.sh"
input_submit_name="submit_prod_extend.sh"
submitScriptInput="$INPUT_DIR/submission/$input_submit_name"

## OUTPUT DIRECTORIES ##

# Name of directory for this type of job
production_extend_dir="prod_extend"
# Generating path to current job
production_extend_dir_full_path="${production_extend_dir}/prod_${ProdTime_ns}_ns_xtcout_${nstxtcout}_nstlist_${nstlist}"
# Getting basename for name seen in submission
production_basename=$(basename ${production_extend_dir_full_path})

# OUTPUT FILES #
output_prefix="mixed_solv_prod"
output_tpr="mixed_solv_prod.tpr"

### MAIN SCRIPT ###

# Defining working directory
WorkSpace="${PATH2SIM}/${main_sim_dir}" # ${solute_name} # _100

# Defining simulation directory
specific_sim_dir=mdRun_${system_temp}_${output_dir} # Directory where all the runs will take place
path2Sim_dir="$WorkSpace/${specific_sim_dir}/" # Defining path structure

# Checking if the simulation exists
if [ ! -e $path2Sim_dir ]; then
    echo "Simulation path does not exist! We need a fully equilibrated cosolvent system with solute to ensure that "
    echo "Check: $path2Sim_dir"
    echo "--- Exiting now! ---"
    exit
fi

# Defining new working directory
Prod_Extension_Work_Space="$path2Sim_dir/${production_extend_dir_full_path}"

# Checking if new workspace was already created. If so, delete it!
if [ -e $Prod_Extension_Work_Space ]; then
    echo "Error! Current work space is existing"
    echo "Path: $Prod_Extension_Work_Space"
#    echo "Will delete duplicate directory in 5 seconds"
#    sleep 3; echo "... 2 seconds"; sleep 2
#    echo "Deleting $Prod_Extension_Work_Space"
#    rm -r $Prod_Extension_Work_Space

    echo "Exiting here!"
    exit
fi

# Checking if gro / top file exists from previous production
if [[ ! -e "$path2Sim_dir/${input_gro}" || ! -e "$path2Sim_dir/${input_top}" ]]; then
    echo "Error, no GRO file or top file from previous production run!"
    echo "Check gro: $path2Sim_dir/${input_gro}"
    echo "Check top: $path2Sim_dir/${input_top}"
    echo "Exiting now...."
    exit
fi

# Making workspace
mkdir -p "$Prod_Extension_Work_Space"

# Going to workspace
cd "$Prod_Extension_Work_Space"

# Copying over mdp file with adjusted options

# Adjusting temperature
sed "s/TEMPERATURE/$system_temp/g" ${path2prod_extend_mdp} > ./${prod_extend_mdp}

# Adjusting output values
sed -i "s/NSTXTCOUT/$nstxtcout/g" ./${prod_extend_mdp}
sed -i "s/NSTVOUT/$nstvout/g" ./${prod_extend_mdp}
sed -i "s/NSTLIST/$nstlist/g" ./${prod_extend_mdp}
sed -i "s/NSTEPS/${ProdTime}/g" ./${prod_extend_mdp}

# Using GROMACs to generate a tpr file
gmx grompp -f ./${prod_extend_mdp} -c ${path2Sim_dir}/${input_gro} -p ${path2Sim_dir}/${input_top} -o ${output_prefix} -maxwarn 1

# Adding submission file
sed "s/DIRECTORY/${production_basename}_${output_dir}/g" ${submitScriptInput} > ./${output_submit_name}

# Adding to job list
echo "$Prod_Extension_Work_Space" >> "${JOB_FILE}"

## Printing results ##
echo "------ Summary ------"
echo "Extended production simulation by ${ProdTime_ns} ns"
echo "Solute Molecule: $solute_name ( $solute_residue_name )"
echo "Solvent Molecule: $solvent_two_name ( $solvent_two_residue_name )"
echo "Weight Fraction of Water: $solvent_one_wt_frac"
echo "Full Simulation Working Directory: $Prod_Extension_Work_Space"
echo "Temperature (K): $system_temp"
echo "Submit job by using list in: ${JOB_FILE}"
