#!/bin/bash

# One_System_Prep_hydronium.sh
# Written by Alex Chew (06/02/2017)
# The purpose of this script is to take your cosolvent mixture with solute, add hydronium and chloride ion, then have a submit script for equilibrium and production runs

# HCl is from the following reference:
# Bonthuis, D. J., Mamatkulov, S. I. & Netz, R. R. Optimization of classical nonpolarizable force fields for OH- and H3O+. J. Chem. Phys. 144, (2016).


# Usage: bash One_System_Prep_hydronium.sh tBuOH tBuOH 363.15 0.5 GVL_L GVLL 6 spce
# $1 is the mole fraction of water
# $2 is the name of the file
# $3 is the system temperature
# $4 is the weight fraction of water 
# $5 is the name of the co-solvent
# $6 is the residue name of the co-solvent
# $7 is the desired box length
# $8 is Water model (tip3p, spce, spc, etc.)
# $9 is number of hydronium ions desired to add

##* -- Updates -- *##
# 17-06-02: Completed hydronium chloride addition script
# 17-06-08: Addition of hydronium ions

## RELOADING BIN FILE
source "bin/bash_rc.sh"

### Functions ###

function expandBox () {
    # The purpose of this function is that it takes the gro file and expands it by a given value
    # $1: GRO File
    # $2: Amount to expand in nm's (assuming cubic/rectangular)
    
    # Input files
    gro_file="$1"
    expansionValue="$2" # Amount expanded in nms
    
    # Get box vector and splitting into components
    output=$(tail -n 1 $gro_file)
    
    # create as array, can now access each component separately:
    array=($output)
    box_x=${array[0]}
    box_y=${array[1]}
    box_z=${array[2]}
    
    # Now, adding to your x, y, and z:
    new_x=$(awk "BEGIN {print $box_x + $expansionValue; exit}")
    new_y=$(awk "BEGIN {print $box_y + $expansionValue; exit}")
    new_z=$(awk "BEGIN {print $box_z + $expansionValue; exit}")
    
    # Using editconf to change the file (no center, box)
    gmx editconf -f "$gro_file" -o "$gro_file" -box $new_x $new_y $new_z -noc &>/dev/null
    
    # Printing what you did
    echo "--- Running expandBox function ---"
    echo "Changing the following gro file: ${gro_file}"
    echo "Changing x box size from ${box_x} to ${new_x}"
    echo "Changing y box size from ${box_y} to ${new_y}"
    echo "Changing z box size from ${box_z} to ${new_z}"
}


# -- USER DEFINED PARAMETERS -- #
solute_name="$1" # "sorbitol"
solute_residue_name="$2" #"SOR"
system_temp="$3" #363.15 # System temperature, edited in mdp file labelled "TEMPERATURE"
solvent_one_wt_frac="$4" #1.0 # fraction of first solvent (water)

# Defining co-solvent
solvent_two_name="$5" #dioxane # Co-solvent molecule name   # GVL_L, dioxane, tetrahydrofuran
solvent_two_residue_name="$6" #DIO # Cosolvent residue name # GVLL, DIO, THF

# Defining desired box length
desired_box_length="$7"  #4 nms

# Defining water model
water_model="$8" # tip3p, spce, spc, etc. (original tip3p) "spce"

# Number of hydronium ions added
numOfAddition="$9" # Number of hydronium ion added

# Output directories
percent_solvent_one_wt_frac=$(awk -v num=$solvent_one_wt_frac 'BEGIN{ printf "%d",num*100}') 


# Checking if we even have a cosolvent
if [ $(awk -v wt_frac=$solvent_one_wt_frac 'BEGIN{ if (wt_frac != 1.00) print 1; else print 0}') -eq 1 ]; then

output_dir=${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_${solvent_two_name}
    
else
output_dir=${desired_box_length}_nm_${solute_residue_name}_${percent_solvent_one_wt_frac}_WtPercWater_${water_model}_Pure
    
fi

## Defining Important directories ##

# Current working directory
currentWD="/home/akchew/scratch/SideProjectHuber" # current working directory

## MAIN DIRECTORIES ##
script_dir="$currentWD/Scripts"
job_list_file="job_list.txt"

## INPUT DIRECTORIES ##
prep_dir="$currentWD/prep_mixed_solvent" # Preparation directory
input_files="$prep_dir/input_files" # Main input file
solvent_dir="$input_files/solvents" # Solvents directory
solute_dir="$input_files/solutes" # Solutes directory
mdp_dir="$input_files/mdp_files" # mdp directory
HCL_dir="$input_files/hydronium_chloride" # Directory for itp, gro files for HCl

# INPUT FILES #
forcefield_name="charmm36-nov2016.ff"
forcefield_dir="$input_files/$forcefield_name" # Force field parameters

## MDP Files ##
em_mdp="minim_LIQ.mdp" # Energy minimization mdp file
equil_mdp="hydronium_chloride_equil.mdp" # Equilibration mdp file
prod_mdp="hydronium_chloride_prod.mdp" # MD Production mdp file

path2em_mdp="$mdp_dir/$em_mdp"
path2equil_mdp="$mdp_dir/$equil_mdp"
path2prod_mdp="$mdp_dir/$prod_mdp"

# In MDP file, defining times -- Changes NSTEPS in mdp file -- Assumes dt = 0.002 fs
EquilTime="1000000" # steps, equivalent to 2000 ps (2 ns)
ProdTime="50000000" # ps, -- 10 ns equivalent to 100 ns ("50000000")

# GRO and TOP files
input_gro="mixed_solv_prod.gro" # Assuming job was complete
input_top="mixed_solv.top"
path2hydronium_gro="$HCL_dir/hydronium.gro" # Path to hydronium ion gro file
path2chlorine_gro="$HCL_dir/chlorine.gro" # Path to chloride ion gro file

# ITP Files
input_itp="hydronium_chloride.itp" # Input file name
path2input_itp="$HCL_dir/$input_itp" # Path to input

# Submission script
submitScriptName="submit.sh"
submitScriptInput="$input_files/submission/$submitScriptName"

# Input Parameters
numTries="100000000" # A lot of tries for gmx-insert
expand_insert_dist="0.2" # nms, used for expansion, insertion, then allow NPT to take over

# Hydronium chloride residue names
hydronium_res_name="HYD"
chlorine_res_name="CLL"

## OUTPUT DIRECTORIES ##
simulation_dir="Simulations" # Folder where all simulations will take place
hydronium_chloride_output_dir="hydronium_chloride_${numOfAddition}"
hydronium_chloride_output_abr="HCl_${numOfAddition}" # Abbreviation for job name

# OUTPUT FILES #
output_gro="mixed_solv.gro"
output_top="mixed_solv.top"

### MAIN SCRIPT ###

# Defining working directory
WorkSpace="$currentWD/$simulation_dir/${solute_name}" # _100

# Defining simulation directory
Simulation_dir=mdRun_${system_temp}_${output_dir} # Directory where all the runs will take place
path2Sim_dir=$WorkSpace/$Simulation_dir # Defining path structure

# Checking if the simulation exists
if [ ! -e $path2Sim_dir ]; then
    echo "Simulation path does not exist! We need a fully equilibrated cosolvent system with solute to ensure that "
    echo "Check: $path2Sim_dir"
    echo "--- Exiting now! ---"
    exit
fi

# Defining new working directory
hydronium_Work_Space="$path2Sim_dir/$hydronium_chloride_output_dir"

# Checking if new workspace was already created. If so, delete it!
if [ -e $hydronium_Work_Space ]; then
    echo "Error! Hydronium chloride work space is existing"
    echo "Path: $hydronium_Work_Space"
    echo "Will delete duplicate directory in 5 seconds"
    sleep 3; echo "... 2 seconds"; sleep 2
    echo "Deleting $hydronium_Work_Space"
    rm -r $hydronium_Work_Space
fi

# Checking if gro / top file exists from previous production
if [[ ! -e "$path2Sim_dir/${input_gro}" || ! -e "$path2Sim_dir/${input_top}" ]]; then
    echo "Error, no GRO file or top file from previous production run!"
    echo "Check gro: $path2Sim_dir/${input_gro}"
    echo "Check top: $path2Sim_dir/${input_top}"
    echo "Exiting now...."
    exit
fi

# Making workspace
mkdir -p "$hydronium_Work_Space"
echo -e "\n ----- One_System_Prep_hydronium.sh ----- \n"

# Going to workspace
cd "$hydronium_Work_Space"

# Copying gro and top files from previous directory
echo "Copying gro and top file from $path2Sim_dir to $hydronium_Work_Space"
cp -r "$path2Sim_dir/${input_gro}" "$hydronium_Work_Space/$output_gro"
cp -r "$path2Sim_dir/${input_top}" "$hydronium_Work_Space/$output_top"

## Editing gro + top file ##

## Finding line to add to topology file to include parameters for hydrogen chloride after parameters
lineNum2forcefield=$(grep -nr ".prm" "$output_top"  | tail -n1 | grep -Eo '^[^:]+' )
line2add=$(( $lineNum2forcefield+1 ))

sed -i "${line2add}a \ " $output_top
sed -i "${line2add}a #include \"$input_itp\"" $output_top # Parameter
sed -i "${line2add}a ; Including params for hydronium chloride ion" $output_top

# Finding line with ions.itp and commenting that out, we won't need it
line_num_ion_itp=$(grep -nr "${forcefield_name}/ions.itp" $output_top | grep -Eo '^[^:]+')
sed -i "${line_num_ion_itp}s/^/; /" ${output_top}

# Expanding gro box and adding molecules
expandBox ${output_gro} ${expand_insert_dist}

#-- Adding molecules --#
gmx insert-molecules -f $output_gro -ci $path2hydronium_gro -nmol $numOfAddition -try $numTries -o ${output_gro} # Hydronium ion
gmx insert-molecules -f $output_gro -ci $path2chlorine_gro -nmol $numOfAddition -try $numTries -o ${output_gro} # chlorine ion

# Adding number HCl to topology file
echo "$hydronium_res_name    $numOfAddition" >> $output_top
echo "$chlorine_res_name    $numOfAddition" >> $output_top

# Now that our system is set up, we need to copy files
echo "Copying itp files, parameters, forcefield, and mdp files"
# itp + parameters for solute
cp -r "$solute_dir/$solute_name/"{*.itp,*.prm} ./
cp -r "$path2input_itp" ./ # Hydronium ion itp file
# itp + parameters for solvent
if [ $(awk -v wt_frac=$solvent_one_wt_frac 'BEGIN{ if (wt_frac != 1.00) print 1; else print 0}') -eq 1 ]; then cp -r "$solvent_dir/$solvent_two_name/"{*.itp,*.prm} ./ ; fi
# mdp files
cp -r "$path2em_mdp" ./
cp -r "$path2equil_mdp" ./
cp -r "$path2prod_mdp" ./
# force field
cp -r "$forcefield_dir" ./

## Editing MDP files
# Editing number of steps
sed -i "s/NSTEPS/${EquilTime}/g" ./$equil_mdp
sed -i "s/NSTEPS/${ProdTime}/g" $prod_mdp

# Editing temperatures
sed -i "s/TEMPERATURE/$system_temp/g" $equil_mdp
sed -i "s/TEMPERATURE/$system_temp/g" $prod_mdp

## Submission Script ##
sed "s/sideProject/${hydronium_chloride_output_abr}_${output_dir}/g" ${submitScriptInput} > $submitScriptName

# Editing the names of the mdp files
sed -i "s/EM_MDP/${em_mdp}/g" $submitScriptName
sed -i "s/EQUIL_MDP/${equil_mdp}/g" $submitScriptName
sed -i "s/PROD_MDP/${prod_mdp}/g" $submitScriptName

# Cleaning up workspace by deleting all the useless things (i.e. saved files)
rm *\#

# Submitting the job -- by putting it into a list, then allow user to edit
if [ ! -e $script_dir ]; then
    mkdir -p "$currentWD/$script_dir"
fi
echo "$hydronium_Work_Space" >> "$script_dir/$job_list_file"

# Printing results
echo "------ Summary ------"
echo "Added $numOfAddition hydronium ion and chloride"
echo "Solute Molecule: $solute_name ( $solute_residue_name )"
echo "Solvent Molecule: $solvent_two_name ( $solvent_two_residue_name )"
echo "Weight Fraction of Water: $solvent_one_wt_frac"
echo "Full Simulation Working Directory: $hydronium_Work_Space"
echo "Temperature (K): $system_temp"
echo "Submit job by using list in: $job_list_file"