#!/bin/bash

# debug_prep_npt_sims_from_umbrella_sampling.sh
# The purpose of this script is to run npt simulations based on conformations of pulling simulations. The reason we need this script is because we want to know how long does it take to form a local domain. 

## USAGE: bash debug_prep_npt_sims_from_umbrella_sampling.sh

### RELOADING GLOBAL VARS AND FUNCTIONS
source "bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## NAME OF SOLUTE 1
solute_1_name="xylitol"
solute_1_res_name="XYL"

## NAME OF SOLUTE 2
solute_2_name="hydronium_LINCS_3" # "hydronium" # 
solute_2_res_name="HYD"

## DEFINE DESIRED BOX LENGTH
box_length="6"

## DEFINING DESIRED DISTANCES
desired_distance=1.95
# 1.95 for pure water
# 2.00 for everything else

## DEFINING DESIRED SPRING CONSTANT
desired_spring_constant="20000"

## DEFINING DESIRED TEMPERATURE
desired_temp="300.00" # Temperature for xylitol

## DEFINING MAIN DIRECTORY TO RUN SIMULATIONS IN
Main_Sim_Dir="181002-XYL_testing_HYD_sims_other_solvents" 

## DEFINING PYTHON SCRIPT
python_find_configurations="${MDBUILDER}/umbrella_sampling/umbrella_sampling_select_configurations.py"

## DEFINING NUMBER OF CORES
num_cores="28"

############################
### PREDEFINED VARIABLES ###
############################

## DEFINING DESIRED SOLVENT NAMES
# solvent_water_mass_fraction="1.00" # Mass fraction of water
solvent_water_mass_fraction="1.00" # Mass fraction of water
cosolvent_name="dmso" # "dioxane" # Name of the cosolvent
cosolvent_res_name="dmso" # "DIO" # Residue name of the cosolvent

## TEMPERATURE TO RUN THIS EQUILIBRIUM STEP
temperature="300.00" # Kelvins

## WATER MODEL
water_model="spce"

## DEFINING FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## PULLING SIMULATIONS NAMES
pull_config_files="us_config"
pull_config_prefix="conf"
pull_summary_file="pull_summary.dat"
pull_topology_file="mixed_solv.top" # Topology file

##############################
### FINDING PULL DIRECTORY ###
##############################

## FINDING DIRECTORY
pull_dir=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${solute_2_res_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 

## DEFINING DIRECTORY TO FIND HYD ION DIRECTORY
full_path_hyd_solute_dir="${PREP_HYD_US}/PULL_${solute_1_name}_${pull_dir}"

## CHECKING IF DIRECTORY EXISTS
check_dir_exist "${full_path_hyd_solute_dir}"

############################
### DEFINING INPUT FILES ###
############################

## DEFINING MDP PATH
mdp_file_path="${MDP_DIR}/umbrella_sampling_hyd"

## DEFINING MDP FILES
mdp_em="em.mdp"
mdp_prod="debug_prep_npt_sims_from_umbrella_sampling.mdp"

### DEFINING INPUTS FOR MDP FILES
mdp_prod_time="50000000" # 100 ns

## DEFINING SUBMISSION SCRIPT
input_submit_script="${SUBMISSION_DIR}/submit_debug_prep_npt_sims_from_umbrella_sampling.sh"

#############################
### DEFINING OUTPUT FILES ###
#############################

## FINDING OUTPUT DIRECTORY FOR SOLUTE
solute_dir=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${solute_1_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 

## OUTPUT DIRECTORY
output_dir="HYDTEST_${solute_dir}"
output_path="${PATH2SIM}/${Main_Sim_Dir}/${output_dir}" # FEP 

## OUTPUT PREFIX
output_prefix="mixed_solv"

## OUTPUT FILES
output_summary="conf_summary.csv"

## OUTPUT SUBMIT
output_submit_file="submit.sh"

## DEFINING OUTPUT SPRING CONSTANTS
output_spring_constant_file="spring_constants.dat"

###################
### MAIN SCRIPT ###
###################

## PRINTING
echo "-------------------------------------------------------------"
echo "CREATING DIRECTORY FOR: ${output_path}"
echo "GENERATING CONFIGURATIONS BASED ON ${full_path_hyd_solute_dir}"
echo "-------------------------------------------------------------"

## CREATING OUTPUT DIRECTORY
create_dir "${output_path}" -f

## GOING INTO OUTPUT DIRECTORY
cd "${output_path}"

##########################
### COPYING OVER FILES ###
##########################
echo "*** COPYING OVER FILES ***"
## MDP FILES
cp -r "${mdp_file_path}"/{${mdp_em},${mdp_prod}} "${output_path}"
## TOPOLOGY
cp -r "${full_path_hyd_solute_dir}/${pull_topology_file}" "${output_path}"
# ITP FILE
cp -r ${full_path_hyd_solute_dir}/{*.itp,*.prm} "${output_path}"
## FORCE FIELD
cp -r "${INPUT_DIR}/${forcefield_dir}" "${output_path}"
## SUBMISSION FILE (SINGLE WINDOW BASIS)
cp -r "${input_submit_script}" "${output_path}/${output_submit_file}"

#####################
### EDITING FILES ###
#####################
echo "*** EDITING FILES USING SED ***"
## EQUILIBRATION MDP FILE
sed -i "s/_TEMPERATURE_/${desired_temp}/g" ${mdp_prod}
sed -i "s/_GROUP_1_NAME_/${solute_1_res_name}/g" ${mdp_prod}
sed -i "s/_GROUP_2_NAME_/${solute_2_res_name}/g" ${mdp_prod}
sed -i "s/_NSTEPS_/${mdp_prod_time}/g" ${mdp_prod}
sed -i "s/_PULLKCONSTANT_/${desired_spring_constant}/g" ${mdp_prod}

## SUBMISSION FILE
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit_file}"
sed -i "s/_NPT_EQUIL_MDP_/${mdp_prod}/g" "${output_submit_file}"
sed -i "s/_OUTPUT_PREFIX_/${output_prefix}/g" "${output_submit_file}"
sed -i "s/_DIRECTORYNAME_/${output_dir}/g" "${output_submit_file}"

#################################
### GENERATING CONFIGURATIONS ###
#################################
## DEFINING VARIABLES FOR PYTHON SCRIPTS
output_configs="${output_path}"
umb_desired_distances=${desired_distance}

### CREATING CONFIGURATIONS
python3 "${python_find_configurations}" --imain "${full_path_hyd_solute_dir}" --isum "${pull_summary_file}" --icon "${pull_config_files}" --ipref "${pull_config_prefix}" --omain "${output_configs}" --osum "${output_summary}" --opref "${pull_config_prefix}" --configs "${umb_desired_distances}"

## DEFINING EXPECTED NAME
expected_name=${pull_config_prefix}0_desired*.gro

## CHANGING THE NAME
mv ${expected_name} "${output_prefix}.gro"

###########################
### ENERGY MINIMIZATION ###
###########################

## SKIPPING ENERGY MINIMIZATION
# cp "${output_prefix}.gro" "${output_prefix}_em.gro"

##############################################
#echo -e "\n*** ENERGY MINIMIZING ***"
### CREATING GROMPP FILE
#gmx grompp -f ${mdp_em} -c "${output_prefix}.gro" -p ${pull_topology_file} -o "${output_prefix}"_em.tpr 
### RUNNING ENERGY MINIMIZATION
#gmx mdrun -nt 1 -v -deffnm "${output_prefix}"_em

#########################
### UPDATING JOB LIST ###
#########################
## UPDATING DIRECTORY FOR JOB LIST
echo "${output_path}" >> ${JOB_FILE}

############################ PRINTING SUMMARY ############################
echo -e "\n************* SUMMARY *************"
echo "Creating solute-hydronium simulations"
echo " - SOLUTE: ${solute_1_name} (${solute_1_res_name})"
echo " - HYDRONIUM_ION: ${solute_2_name} (${solute_2_res_name})" 
echo " - BOX LENGTH: ${box_length}"
echo " - MASS FRACTION OF WATER: ${solvent_water_mass_fraction}"
echo " - COSOLVENT: ${cosolvent_name}"
echo " - WORKING DIR: ${output_path}"

