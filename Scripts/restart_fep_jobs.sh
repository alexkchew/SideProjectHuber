#!/bin/bash

# restart_fep_job.sh
# The purpose of this script is to restart fep jobs for multiple FEP simulations.
# Here, the FEP jobs can be extended based on production simulation time

## USAGE: bash restart_fep_job.sh sim_folder_name

# Written by Alex Chew (01/11/2019)

## ** Updates ** ##

## DEFINING VARIABLES
# $1: simulation_folder_name

## SOURCING GLOBAL VARIABLES
source "bin/bash_rc.sh"

## PRINTING SCRIPT NAME
print_script_name

#########################################################
### FUNCTIONS 
#########################################################


#########################################################
### MAIN SCRIPT
#########################################################


################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING DIRECTORY
simulation_folder_name="$1" # "190110-Extended_HYD_FEP"

## DEFINING DESIRED PRODUCTION TIME
desired_prod_time="11000.000"
# "5000.000" # "11000.000"

## DEFINING NUMBER OF SPLITS
num_splits="6"

## DEFINING OUTPUT SUBMIT EXTENSION
output_submit="extend_submit"

## DEFINING OUTPUT JOB LIST
output_job_list="${RESTART_JOB_FILE}"

## DEFINING NUMBER OF CORES
num_cores="28"

## DEFINING PATH TO SIMULATION FOLDER
simulation_folder_path="${PATH2SIM}/${simulation_folder_name}"

##################################
### DEFINING SUBMISSION SCRIPT ###
##################################
## SUBMISSION SCRIPT NAME
submit_script_per_lambda="submit_free_energy_per_lambda_single_reference_extended.sh"
submit_script_split_windows="submit_free_energy_full_split_windows_extended.sh"
## DEFINING PATH TO SCRIPT
submit_script_per_lambda_path="${SUBMISSION_DIR}/${submit_script_per_lambda}"
submit_script_split_windows_path="${SUBMISSION_DIR}/${submit_script_split_windows}"
## STOPPING IF THIS DOES NOT EXIST
stop_if_does_not_exist "${submit_script_per_lambda_path}"

### LOOPING THROUGH EACH DIRECTORY

## LOOPING THROUGH ALL SIMULATION FOLDERS WITHIN FILE
for fep_simulation_folder_name in $(ls -d ${simulation_folder_path}/*/ ); do
    ## REWRITING VARIABLE TO GET BASENAME
    fep_simulation_folder_name=$(basename ${fep_simulation_folder_name})
    ## DEFINING SPECIFIC FILE Comment off if you want to run for one simulation
    #   fep_simulation_folder_name="FEP_300.00_6_nm_HYD_100_WtPercWater_spce_Pure"

    ####################################
    ### DEFINING DIRECTORY STRUCTURE ###
    ####################################
    ## DEFINING STRUCTURE
    FEP_path="${PATH2SIM}/${simulation_folder_name}/${fep_simulation_folder_name}"
    lambda_files="3_lambda"
    submit_files="4_Submission_Files"
    md_folder="4_MD"

    ############################################
    ### COPYING OVER LAMBDA SUBMISSION FILES ###
    ############################################

    ## PRINTING
    echo -e "\n--- COPYING LAMBDA SUBMISSION FILE ---"

    ## CHECKING IF ALL PATHS ARE WORKING
    stop_if_does_not_exist "${FEP_path}/${submit_files}"

    ## DEFINING OUTPUT SUBMISSION LAMBDA PATH
    output_submit_lambda="${FEP_path}/${submit_files}/${submit_script_per_lambda}"

    ## COPYING OVER FILE
    cp -rv "${submit_script_per_lambda_path}" "${output_submit_lambda}"

    ## EDITING FILE
    echo "  --> Editing submission file to have production time: ${desired_prod_time}"
    echo "  --> Submission path: ${output_submit_lambda}"
    sed -i "s/_PRODSIMTIME_/${desired_prod_time}/g" "${output_submit_lambda}"

    #############################################
    ### GENERATING FULL SPLIT SUBMISSION FILE ###
    #############################################

    ## PRINTING
    echo -e "\n--- GENERATING SPLIT SUBMISSION FILES ---"

    ## CHECKING IF PATHS ARE WORKING
    stop_if_does_not_exist "${FEP_path}/${lambda_files}"

    ## FINDING TOTAL NUMBER OF WINDOWS
    total_lambda_windows=$(find "${FEP_path}/${lambda_files}" -maxdepth 1 -name "Lambda"* -type d | wc -l)

    ## SEEING IF TOTAL LAMBDA WINDOWS IS ZERO
    if [ "${total_lambda_windows}" == 0 ]; then
    	echo "Error! ${total_lambda_windows} is zero!"
    	echo "Perhaps check if lambdas were made!"
    	echo "FEP path: ${FEP_path}"
    	exit 1
    fi

    ## FINDING EXTREME LAMBDA VALUES
    initial_lambda_window="0"
    final_lambda_window=$(( ${total_lambda_windows} - 1 ))

    ## FINDING SEQUENCE OF VALUES
    generate_split_array "${initial_lambda_window}" "${final_lambda_window}" "${num_splits}"
 
    ## LOOPING THROUGH EACH SPLIT AND CREATING SUBMISSION SCRIPT
    for each_split in $(seq 0 $((${num_split}-1)) ); do
        ## PRINTING
        echo "Creating submission script for split: ${each_split} of $((${num_split}-1))"
        ## DEFINING INITIAL AND FINAL SPLIT
        initial_split_window="${_initial_window_array_[each_split]}"
        final_split_window="${_final_window_array_[each_split]}"

        ## DEFINING OUTPUT SUBMISSION PATH
        output_submission_path="${FEP_path}/${output_submit}_${each_split}.sh"

        ## CREATING A SUBMISSION SCRIPT
        cp -r "${submit_script_split_windows_path}" "${output_submission_path}"

        ## EDITING SUBMISSION SCRIPT
        sed -i "s/FIRSTALPHA/${initial_split_window}/g" "${output_submission_path}" 
        sed -i "s/LASTALPHA/${final_split_window}/g" "${output_submission_path}"
        sed -i "s/DIRECTORYNAME/${fep_simulation_folder_name}_${each_split}/g" "${output_submission_path}"
        sed -i "s/NUMBER_OF_CORES/${num_cores}/g" "${output_submission_path}"

        ## ADDING TO JOB LIST
        echo "${output_submission_path}" >> "${output_job_list}"

    done

    # Printing what we have done
    echo "----------- SUMMARY -----------"
    echo "  We have restarted jobs for: ${fep_simulation_folder_name}"
    echo "  Total production simulation time: ${desired_prod_time}"
    echo "  Number of splits: ${num_splits}"
    echo "  See job listing in: ${output_job_list}"
    echo "  Full simulation path: ${FEP_path}"
    echo "  Best of luck with the restarting FEP sims"
    
done
