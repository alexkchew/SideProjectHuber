#!/bin/bash

# prep_umbrella_sampling_1_hydronium_solute_sims.sh
# The purpose of this script is to take the hydronium ion in mixed-solvent environment, add a solute, then equilibrate in an NPT ensemble. This script serves as a way to start generating configurations.

# Written by: Alex K. Chew (alexkchew@gmail.com, 09/11/2018)

## ALGORITHM:
#   - Use previous mixed-solvent environment with water/cosolvent and hydronium ion
#   - Then, we add the solute, updating topology
#   - Finally, we equilibrate the system in an NPT ensemble

## USAGE: bash prep_umbrella_sampling_1_hydronium_solute_sims.sh

# *** UPDATES *** #


### RELOADING GLOBAL VARS AND FUNCTIONS
source "bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING LOGICALS
want_chlorine_ion="True" # True if you want chlorine ion within the system

## DEFINING NAME OF CHLORINE
counter_ion_name="chlorine"
counter_ion_res_name="CLL"

## NAME OF SOLUTE 1
# solute_1_name="xylitol"
# solute_1_res_name="XYL"
solute_1_name="12-propanediol"
solute_1_res_name="PDO"

## NAME OF SOLUTE 2
solute_2_name="hydronium_LINCS_3" # "hydronium_LINCS_2" # "hydronium" # 
solute_2_res_name="HYD"

## DEFINE DESIRED BOX LENGTH
box_length="6" # "6" # 9

## DEFINING DEFAULT SCALE FOR INSERTION
insertion_scale="0.58" # Default is 0.57

############################
### PREDEFINED VARIABLES ###
############################

## DEFINING DESIRED SOLVENT NAMES
# solvent_water_mass_fraction="1.00" # Mass fraction of water
solvent_water_mass_fraction="0.10" # Mass fraction of water
cosolvent_name="dioxane" # "dioxane" # Name of the cosolvent
cosolvent_res_name="DIO" # Residue name of the cosolvent

## TEMPERATURE FOR THE EQUILIBRIUM STEP
equil_temp="300.00"

## TEMPERATURE TO RUN THIS EQUILIBRIUM STEP
temperature="300.00" # Kelvins
if [ "${solute_1_name}" == "12-propanediol" ]; then
    temperature="433.15" # Kelvins
fi
## TEMPORARY, FIX LATER
temperature="300.00" # Kelvins

## WATER MODEL
water_model="spce"

## DEFINING FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## DEFINING INSERTION USED FOR EXPANSION
expand_insert_dist="0.1" # "0.3" # "0.3" # nms, used for expansion, insertion, then allow NPT to take over

## RESIDUE NAME OF HYDRONIUM ION
hyd_res_name="HYD"

## FINDING DIRECTORY
hyd_ion_dir=$(solvent_effects_get_output_dir_name "${equil_temp}" "${box_length}" "${hyd_res_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 

if [ "${want_chlorine_ion}" == "True" ]; then
    prep_hyd_dir="${PREP_HYD_SIMS_HCL}"
else
    prep_hyd_dir="${PREP_HYD_SIMS}"
fi

## DEFINING DIRECTORY TO FIND HYD ION DIRECTORY
full_path_hyd_ion_dir="${prep_hyd_dir}/${hyd_ion_dir}"

## CHECKING IF DIRECTORY EXISTS
check_dir_exist "${full_path_hyd_ion_dir}"

############################
### DEFINING INPUT FILES ###
############################
# This section defines the input files from the hydronium ion simulations
## INPUT FILE
input_gro_file="mixed_solv_equil_1_npt.gro"
input_top_file="mixed_solv.top"
## INPUT PATH
input_gro_path="${full_path_hyd_ion_dir}/${input_gro_file}"
input_top_path="${full_path_hyd_ion_dir}/${input_top_file}"


## CHECKING IF GRO AND TOP FILE EXISTS
if [[ ! -e "${input_gro_path}" || ! -e "${input_top_path}" ]]; then
    echo "Error, no GRO file or top preparation file!"
    echo "Check gro: ${input_gro_path}"
    echo "Check top: ${input_top_path}"
    echo "Exiting now...."
    exit
fi

#################################
### DEFINING OUTPUT VARIABLES ###
#################################
## DEFINING OUTPUT DIRECTORY NAME
output_dir_name=$(solvent_effects_get_output_dir_name "${temperature}" "${box_length}" "${hyd_res_name}" "${solvent_water_mass_fraction}" "${water_model}" "${cosolvent_name}") 
### DEFINING OUTPUT DIRECTORY
output_dir="PREPHYD_${solute_1_name}_${output_dir_name}"

## OUTPUT FILES
output_gro="mixed_solv.gro"
output_top="mixed_solv.top"

##########################
### DEFINING MDP FILES ###
##########################

## DEFINING MDP PATH
mdp_file_path="${MDP_DIR}/umbrella_sampling_hyd" # 2

## DEFINING MDP FILES
em_mdp="minim_LIQ.mdp" # "em.mdp"                         # Energy minimization mdp file
em_mdp="em.mdp"
equil_npt_mdp="initial_equil_npt.mdp"   # Initial npt equilibration

## INPUTS FOR MDP FILE
equil_npt_n_steps="2000000"     # 2 ns NPT equilibration (Note! This uses a 0.001 time step)

################################
### DEFINING SUBMISSION FILE ###
################################

## DEFINING SUBMISSION FILE
submit_input_file_name="submit_prep_umbrella_sampling_1_hydronium_solute_sims.sh"
submit_output_file_name="submit.sh"
submit_file_path="${SUBMISSION_DIR}/${submit_input_file_name}"

###############################################
### DEFINING SIMULATION WORKING DIRECTORIES ###
###############################################
## DEFINING WORKING DIRECTORY
if [ "${want_chlorine_ion}" == "True" ]; then
    WorkSpace="${PREP_HYD_SOLUTE_SIMS_HCL}"
else
    WorkSpace="${PREP_HYD_SOLUTE_SIMS}"
fi

# Checking if working directory exists
if [ ! -e ${WorkSpace} ]; then # Checking if solute exists
    mkdir -p ${WorkSpace}
fi

## DEFINING FULL PATH
path2Sim_dir=${WorkSpace}/${output_dir} # Defining path structure

## CREATING SIM DIRECTORY
create_dir "${path2Sim_dir}" -f

## GOING INTO THE DIRECTORY
cd "${path2Sim_dir}"

##########################################
### COPYING MIXED-SOLVENT ENVIRONMENTS ###
##########################################
echo -e "\n***COPYING OVER GRO AND TOP FILE FROM: ${prep_gro_file_path}***"
# Gro file
cp -r ${input_gro_path} "${path2Sim_dir}/${output_gro}"
# Top file
cp -r ${input_top_path} "${path2Sim_dir}/${output_top}"
# ITP FILE
cp -r ${full_path_hyd_ion_dir}/{*.itp,*.prm}  "${path2Sim_dir}"

## COPYING SOLUTE INFORMATION
cp -rv "${PREP_SOLUTE_DIR}/${solute_1_name}/"{*.itp,*.prm} ./

#####################
### ADDING SOLUTE ###
#####################

## DEFINING GRO FILE OF INSERTED
output_insert_gro=${output_gro%.gro}_inserted.gro

## DEFINING NUMBER OF SOLUTES TO ADD (DEFAULTS)
solute_1_num_to_add="1"
#
## ADDING SOLUTE 1
solvent_effects_add_solute "${solute_1_name}" "${solute_1_res_name}" "${solute_1_num_to_add}" "${output_gro}" "${output_insert_gro}" "${expand_insert_dist}" "${output_top}" "${insertion_scale}"

## CLEANING UP DIRECTORY FROM COPIES
rm \#*
## CREATING SETUP FILES DIRECTORY
setup_files_dir="setup_files"
mkdir ${setup_files_dir}
## MOVING GRO FILES TO SETUP FILES
mv *.gro ${setup_files_dir}
cp -r ${setup_files_dir}/${output_insert_gro} ./${output_gro}


#############################################
### ADDING PARAMETERS AND ITP TO TOPOLOGY ###
#############################################
echo -e "\n*** EDITING TOPOLOGY FILE TO INCLUDE ITP AND PRMS ***"

### ADDING PARAMETERS AND ITP TO TOPOLOGY
## ITP FILE
topology_add_include_specific "${output_top}" "${solute_2_name}.itp" "#include \"${solute_1_name}.itp\""

## PRM FILE
topology_add_include_specific "${output_top}" "${solute_2_name}.prm" "#include \"${solute_1_name}.prm\""

###############################
### COPYING ALL INPUT FILES ###
###############################
## FORCE FIELD FILES
echo -e "\n*** COPYING OVER FORCE FIELD (${forcefield_dir}) ***"
cp -r "${INPUT_DIR}/${forcefield_dir}" ./

## MDP FILE
echo -e "\n*** COPYING OVER MDP FILES ***"
cp -rv "${mdp_file_path}/"{${em_mdp},${equil_npt_mdp}} ./

### SUBMISSION FILE
echo -e "\n*** COPYING OVER SUBMISSION FILES ***"
cp -rv "${submit_file_path}" ./"${submit_output_file_name}"
#
###########################
### EDITING INPUT FILES ###
########################### 

## ADJUSTING EQUILIBRATION MDP FOR NPT EQUILIBRATION
sed -i "s/NSTEPS/${equil_npt_n_steps}/g" ${equil_npt_mdp}
sed -i "s/TEMPERATURE/${temperature}/g" ${equil_npt_mdp}

## ADJUSTING SUBMISSION FILE
sed -i "s/DIRECTORY/${output_dir}/g" ${submit_output_file_name}
sed -i "s/NPT_EQUIL_MDP/${equil_npt_mdp}/g" ${submit_output_file_name}

############################
#### ENERGY MINIMIZATION ###
############################
echo -e "\n*** ENERGY MINIMIZING ***"
## CREATING GROMPP FILE
gmx grompp -f ${em_mdp} -c ${output_gro} -p ${output_top} -o "${output_gro%.gro}"_em.tpr 
## RUNNING ENERGY MINIMIZATION
gmx mdrun -nt 1 -v -deffnm "${output_gro%.gro}"_em

## MAKING MOLECULES WHOLE
gmx trjconv -s "${output_gro%.gro}"_em.tpr -f "${output_gro%.gro}"_em.gro -o "${output_gro%.gro}"_em.gro -pbc mol << INPUTS
System
INPUTS

#########################
### UPDATING JOB LIST ###
#########################
## UPDATING DIRECTORY FOR JOB LIST
echo "${path2Sim_dir}" >> ${JOB_FILE}

############################ PRINTING SUMMARY ############################
echo -e "\n************* SUMMARY *************"
echo "Generated solute-hydronium in mixed-solvent systems simulations for the following:"
echo "- SOLUTE: ${solute_1_name} (${solute_1_res_name})" 
echo "- HYDRONIUM_ION: ${solute_2_name} (${solute_2_res_name})" 
echo "- BOX LENGTH: ${box_length}"
echo "- MASS FRACTION OF WATER: ${solvent_water_mass_fraction}"
echo "- COSOLVENT: ${cosolvent_name}"
echo "- WORKING DIR: ${path2Sim_dir}"
