#!/bin/bash

# analysis_full_extraction.sh
# This bash script will run the following scripts:
#   analysis_extraction.sh -- extract xtc files
#   analysis_xtctruncate.sh -- truncates the xtc files
# This will completely run extraction for a specific directory.
# CREATED ON: 05/06/2018
# AUTHOR: Alex K. Chew (alexkchew@gmail.com)

## LOADING LIGAND BUILDER VARIABLES / FUNCIONS
source "bin/bash_rc.sh"

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING INPUT DIRECTORY (SIMULATION FOLDER WITH MANY SIMULATIONS)
input_dir_name="190925-2ns_mixed_solvent_with_FRU_HMF"
# "190922-PDO_most_likely_gauche"

## DEFINING OUTPUT DIRECTORY
output_dir_name="190925-2ns_mixed_solvent_with_FRU_HMF"
# "190922-PDO_most_likely_gauche"

## PROD EXTEND
alternative_extraction="prod_extend" # Turn on if you want prod extend
alternative_extraction="" 

## DEFINING LOGICAL FOR EXPANDED SIMS
expanded_sims="False" # True if you have expanded sims

## LOGICALS
want_xtc="True"
want_ndx="True"
want_pdb="True"
want_split="False"

##################################
### DEFINING IMPORTANT SCRIPTS ###
##################################

## DEFINING  VARIABLES
timeOfCut="10000"
xtc_trunc_file="mixed_solv_prod_10_ns_whole.xtc"

if [[ "${expanded_sims}" == "True" ]]; then
    ## CHANGING TRUNCATION TIME
    timeOfCut="5000"
    xtc_trunc_file="mixed_solv_prod_5_ns_whole.xtc"
fi

bs_analysis_extract="${PATH2SCRIPTS}/analysis_extraction.sh"
bs_analysis_xtc_trunc="${PATH2SCRIPTS}/analysis_xtctruncate.sh"

## RUNNING EXTRACTION PROTOCOL
bash "${bs_analysis_extract}" "${input_dir_name}" "${output_dir_name}" "${alternative_extraction}"

## RUNNING XTC TRUNCATION PROTOCOL
bash "${bs_analysis_xtc_trunc}" "${want_xtc}" "${want_ndx}" "${want_pdb}" "${want_split}" "${output_dir_name}" "${timeOfCut}" "${xtc_trunc_file}"

###############
### SUMMARY ###
###############
echo "------- SUMMARY -------"
echo "INPUT DIRECTORY: ${input_dir_name}"
echo "OUTPUT DIRECTORY: ${output_dir_name}"
