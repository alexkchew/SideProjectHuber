#!/bin/bash

# prep_free_energy.sh
# We would like to generate a script to equilibrate our systems prior to free energy calculations. For now, let's solvate the system with a fixed number of molecules, then energy minimize and equilibrate at a temperature.
# Then, add a random molecule that we would like to use

# Testing hydronium parameters based on Netz and coworkers paper:
# Bonthuis, D. J., Mamatkulov, S. I. & Netz, R. R. Optimization of classical nonpolarizable force fields for OH- and H3O+. J. Chem. Phys. 144, (2016).

# Usage: prep_free_energy.sh 

# Written by Alex Chew (06-15-2017)

##* -- Updates -- *##
# 17-06-16: Added minimization scripts, etc.

# -- USER DEFINED PARAMETERS -- #

# Defining number of water molecules
num_solv_one="506" # Number of molecules used in   #4 nms

# Defining water model
water_model="spce" # tip3p, spce, spc, etc. (original tip3p) "spce"

# Defining temperature / pressure
system_temp="300" # Previously 500 Kelvins, used for equilibration -- high temperature for faster equilibration

### DEFINING IMPORTANT DIRECTORIES (BEGIN) ###

# Important directories / Files
# Specifying input files
currentWD="/home/akchew/scratch/SideProjectHuber" # Working directory
prepdir="$currentWD/prep_mixed_solvent"
input_files="$prepdir/input_files"
output_prefix="mixed_solv" # Prefix for gro and top files
currentprepdir="$prepdir/prep_free_energy" # Preparation directory for this script

# Defining output directory
output_dir="$currentprepdir/${num_solv_one}_${water_model}"

# Solute / solvent / forcefield /mdp files
solute_dir="$input_files/solutes" # Solute directory
solvent_dir="$input_files/solvents" # Solvent directory
forcefield_dir="$input_files/charmm36-nov2016.ff"
mdp_dir="$input_files/mdp_files" # mdp file directory

# Defining information files
molecular_wt_info="$solvent_dir/molecular_weights.info" # Stores molecular weight information
volume_info="$solvent_dir/molecular_volume.info" # Stores volume information
solute_PDB_file="$solute_dir/$solute_name/$solute_name.pdb" # pdb file for solute
top_input_file="$input_files/topology/mixed_solv_free_energy.top" # cosolvent input topology file

# Defining submission script for equilibration
# Submission script
submitScript="submit_prep_free_energy.sh" # Submission script name
submitScript_loc="$input_files/submission/$submitScript" # Location
output_submit_script="submit.sh" # submission script output

# MDP Files
# Minimization
minim_mdp="$mdp_dir/minim_LIQ.mdp"
# Equilibration
equil_mdp_file_name="equil_cosolvent_mixing.mdp"
equil_mdp="$mdp_dir/$equil_mdp_file_name"

# Specifying output files
top_output_file="${output_prefix}.top"

# Defining job list file
scriptDir="$currentWD/Scripts" # Script directory
job_list_file="$scriptDir/job_list.txt"

### DEFINING IMPORTANT DIRECTORIES (END) ###

## --- CALCULATION OF BOX SIZE (START) --- ##
solvent_one_name="spc216"
solvent_one_residue_name="SOL"
solvent_one_file=spc216.gro
solvent_one_vol=0.09 # units of nm3 / molecule; estimated from input gro file box size with 1.5x fudge factor
solvent_one_MW=18.01 # g/mol, molecular weight of water

# Using molar volume to get total volume
solvent_one_total_vol=$(awk -v vol=$solvent_one_vol -v tot=$num_solv_one  'BEGIN{ printf "%d",tot*vol}')

# Now, finding box length
box_length=$(awk -v vol=$solvent_one_total_vol 'BEGIN{ printf "%.2f",vol^(0.33)}')  # nm

## --- CALCULATION OF BOX SIZE (END) --- ##

##*** INITIATING SYSTEM ***##

# Checking if the output directory exists
if [ -e $output_dir ]; then
    echo "$output_dir already exists, removing duplicates!"
    echo "Waiting 5 seconds until deletion..."
    sleep 5
    rm -r $output_dir
fi
# Creating directory for us to work in
mkdir -p ${output_dir}

# Going into directory
cd "$output_dir"

# Copying important files for preparation (itps, force fields, etc.)
# Force field
cp -r "$forcefield_dir" $output_dir

## SYSTEM CREATION ##
# Adding topology file
cp ${top_input_file} ${output_dir}/${top_output_file}

# Fixing topology file
# sed -i "s/SOLVENT/$solvent_two_name/g" $top_output_file # Fixing solvent name
sed -i "s/WATER_MODEL/$water_model/g" $top_output_file # Fixing water model

# Solvating with pure water
gmx solvate -cs ${solvent_dir}/${solvent_one_name}/${solvent_one_name}.gro -o ${output_prefix}.gro -maxsol $num_solv_one -box $box_length $box_length $box_length

# Adding to topology
echo "$solvent_one_residue_name    $num_solv_one" >> ${top_output_file}

# Minimize with solvent #1 (Water)
gmx grompp -f $minim_mdp -c ${output_prefix}.gro -p ${top_output_file} -o ${output_prefix}_em -maxwarn 1
gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em

## Equilibration ##

# Copy equilibration mdp file and changing temperature
sed "s/TEMPERATURE/$system_temp/g" $equil_mdp > ./$equil_mdp_file_name

# Creating a job script
output_dir_name=$(basename $output_dir) # Getting the base name
sed "s/DIRECTORY/$output_dir_name/g" $submitScript_loc > ./$output_submit_script

# Printing
echo "Writing to job file: $output_dir"

# Writing output directory to jobfile
echo "$output_dir" >> $job_list_file