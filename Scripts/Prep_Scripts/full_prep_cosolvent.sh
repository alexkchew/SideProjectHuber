#!/bin/bash

# full_prep_cosolvent.sh
# The purpose of this script is to take prep_cosolvent.sh and expand by checking the folders and correspondingly submit jobs that are not complete. 

# Usage: bash full_prep_cosolvent.sh

# Written by Alex Chew (05-12-2017)

##*-- Updates --*##
# 170512-Updated to include box length
# 17-06-01: Added hydronium ion addition and availability to change the desired water model (originally tip3p)

## RELOADING BIN FILE
source "../bin/bash_rc.sh"

## DEFINING IMPORTANT SCRIPTS
prep_cosolvent_name="prep_cosolvent.sh" # Script to generate one system
prep_cosolvent_path="${PATH2SCRIPTS_PREPSCRIPTS}/$prep_cosolvent_name"

#################################
### DECLARING ARRAY VARIABLES ###
#################################

## DEFINING COSOLVENTS
# "GVL_L" "tetrahydrofuran" "dioxane" "dmso" "2-butanone" "sulfolone"
#  "N-methyl-2-pyrrolidone"
declare -a cosolvent_name=("2-butanone" "NN-dimethylacetamide" "NN-dimethylformamide" "N-methyl-2-pyrrolidone" "sulfolone" "dioxane" "dmso" "GVL_L" "tetrahydrofuran" "tetramethylenesulfoxide" "ethylenecarbonate" "acetone" "acetonitrile")  # Co-solvent name 

# "2-butanone" "NN-dimethylacetamide" "NN-dimethylformamide" "N-methyl-2-pyrrolidone" "sulfolone" "dioxane" "dimethyl sulfoxide" "GVL_L" "tetrahydrofuran" "tetramethylenesulfoxide" "ethylenecarbonate" "acetone" "acetonitrile"

# "GVLL" "THF" "DIO" "dmso"
# NO LONGER NEED COSOLVENT RESIDUE NAME
# declare -a cosolvent_resname=("ACN")  # Residue name 

## SOLVENT NAME LIST
#   GVL_L GVLL
#   tetrahydrofuran THF
#   dioxane DIO
#   dmso dmso
#   NN-dimethylacetamide DMA
#   NN-dimethylformamide DMF
#   N-methyl-2-pyrrolidone NMP
#   urea    URE
#   2-butanone BUT
#   sulfolone SUL
#   ethylenecarbonate ETH
#   tetramethylenesulfoxide TET
#   acetone ACE
#   acetonitrile ACN

## Weight fractions
# "0.00" "0.10" "0.25" "0.50" "0.75" "0.85" "0.90" "1.00"
# "0.56" "0.35" "0.12"
declare -a solvent_one_wt_frac=("0.25") # "0.00" "0.05" "0.50" "0.25" "0.75"  "1.00" "0.05" "0.5"  "1.00" "0.10"
# "0.25" "0.50" "0.75"

## Desired box lengths
# "4" "6" "8" "10" "12"
declare -a desire_box_lengths=("6")

## Declaring water model
water_model="spce" # tip3p, spce, spc, etc. (original tip3p) "spce"


# Finding total number of co-solvents
totalCosolvents=$((${#cosolvent_name[@]}-1))

# Using for-loop to create multiple systems
for currentCoSolvent_Num in $(seq 0 $totalCosolvents); do

    # Defining the cosolvent
    cosolvent_current_name="${cosolvent_name[currentCoSolvent_Num]}"

    # Varying the weight fractions
    for currentWtFrac in ${solvent_one_wt_frac[@]}; do
        # Varying box length
        for currentBoxLen in ${desire_box_lengths[@]}; do
        
            # Running script
            bash "${prep_cosolvent_path}" "${currentWtFrac}" "${cosolvent_current_name}" "${currentBoxLen}" "${water_model}"
        
        done
        
    done
    
done

# Printing what you did
echo "------SUMMARY------"
echo "Created systems for ${#cosolvent_name[@]} cosolvents. Their residue names are: ${cosolvent_resname[@]}"
echo "We varied the weight fraction of water: ${solvent_one_wt_frac[@]}"
echo "We varied the box length: ${desire_box_lengths[@]}"
echo "We used the following water model: ${water_model}"
echo "Now, we can use the script in ${JOB_FILE} to submit jobs. Good luck!"
