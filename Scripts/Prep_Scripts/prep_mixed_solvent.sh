#!/bin/bash

# prep_mixed_solvent.sh
# PLAN FOR THIS:
# User specifies mole fractions of two input solvents. Assume first mole fraction always > 0. 
# User species total number of solvent molecules.
# User specifies molar volume of each solvent.
# Program solvates desired molecule in box, sized based on molar volume of first solvent. 
# Program creates second box of solvent based on molar volume.
# Program concatenates box; makes input files (top files)

# Usage: prep_mixed_solvent.sh solute_name solute_residue_name output_dir mol_frac solvent_two_name solvent_two_residue_name
# Example: bash prep_mixed_solvent.sh levoglucosan LGA testing 0.05 tetrahydrofuran THF
# $1 - solute file (Name of all the pdb / itp)
# $2 - solute name (RESIDUE NAME)
# #3 - output_directory
# $4 - Mass fraction of water
# $5 - Co-solvent molecule name
# $6 - Co-solvent residue name

# Originally written by Reid Van Lehn
# Edited by Alex Chew (03/24/2017)
# 170511 - Creating alternative script to account for volume instead of molecules

##* -- Updates -- *##
# 2017-03-24: Adding temperature, changing mdp files for longer run times.
# 2017-03-24: Adding variable solvents (dioxane, GVL, and THF)
# 2017-03-24: Conversion of mole fraction to mass fraction
# 2017-03-28: Added volumes to co-solvents
# 2017-03-29: Added additional energy minimizations
# 2017-04-05: Changed fudge factor from 1.5 to 2.0 for volume of solvent#2 (Succeeds with 5% wt water)

# -- USER DEFINED PARAMETERS -- #
solute_name="$1"
solute_residue_name="$2"
output_dir="$3" # output directory
solvent_one_weight_frac="$4" # fraction of first solvent
solvent_two_name="$5" # Co-solvent molecule name
solvent_two_residue_name="$6" # Co-solvent residue name

# Important directories / Files
# Specifying input files
currentWD=$(pwd) # Current working directory
input_files="$currentWD/input_files"
output_prefix="mixed_solv" # Prefix for gro and top files

solute_dir="$input_files/solutes" # Solute directory
solvent_dir="$input_files/solvents" # Solvent directory
forcefield_dir="$input_files/charmm36-nov2016.ff"
mdp_dir="$input_files/mdp_files" # mdp file directory

molecular_wt_info="$solvent_dir/molecular_weights.info" # Stores molecular weight information
volume_info="$solvent_dir/molecular_volume.info" # Stores volume information
solute_PDB_file="$solute_dir/$solute_name/$solute_name.pdb" # pdb file for solute
top_input_file="$input_files/mixed_solv.top"

# MDP File
minim_mdp="$mdp_dir/minim_LIQ.mdp"

# Specifying output files
top_output_file=mixed_solv.top

## --- CALCULATION OF BOX SIZE (START) --- ##
# By default
solute_vol=0.38 # nm3/ molecule; estimated from editconf with 0 distance to box wall >> gmx editconf -f fructose.pdb -o fructose.gro -d 0.000 -bt cubic

solvent_one_name="spc216"
solvent_one_residue_name="SOL"
solvent_one_file=spc216.gro
solvent_one_vol=0.09 # units of nm3 / molecule; estimated from input gro file box size with 1.5x fudge factor
solvent_one_MW=18.01 # g/mol, molecular weight of water

# Found beforehand
solvent_two_vol_init=$(grep "^$solvent_two_name" $volume_info | awk '{print $NF}') # units of nm3 / molecule; approximated from input gro file box size

if [ -z "$solvent_two_vol_init" ]; then 
    echo "Error! No co-solvent volume!"; 
    echo "Stopping here, please check $volume_info"
    echo "All co-solvents need volumes to find the correct box size"
    echo "Stopping job now!"
    exit
fi

# Adding a 2.0 fudge factor
solvent_two_vol=$(awk -v vol_init=$solvent_two_vol_init 'BEGIN{ printf "%.2f", vol_init*2.0 }') # 1.5 failed a 5% weight fraction of water

## Conversion of weight fraction into mole fraction

# First, we need the molecular weight of the solute
solvent_two_MW=$(grep "^$solvent_two_name" $molecular_wt_info | awk '{print $NF}')

# Checking to make sure the MW is there
if [ -z "$solvent_two_MW" ]; then 
    echo "Error! No co-solvent molecular weight"; 
    echo "Stopping here, please check $molecular_wt_info"
    echo "All co-solvents need molecular weights for conversions of mass fractions to mole fractions"
    echo "Stopping job now!"
    exit
fi

# Now, getting total mass / fraction of solvent 2
solvent_two_weight_frac=$(awk -v frac_one=$solvent_one_weight_frac 'BEGIN{ printf "%.2f",1-frac_one}') 
total_mass=$(awk -v frac_one=$solvent_one_weight_frac -v frac_two=$solvent_two_weight_frac -v MW_1=$solvent_one_MW -v MW_2=$solvent_two_MW 'BEGIN{ printf "%.5f", frac_one/MW_1 + frac_two/MW_2  }') 

# Now, getting the moles of water
solvent_one_mol_frac=$(awk -v frac_one=$solvent_one_weight_frac -v MW_1=$solvent_one_MW -v tot_mass=$total_mass 'BEGIN{ printf "%.2f", frac_one/MW_1/tot_mass }') 

total_mol_solvent=5000

# INITIAL BOX SIZE CALCULATIONS - note that this will always truncate any decimals # Ensures 100% though
num_solv_one=$(awk -v num=$total_mol_solvent -v frac=$solvent_one_mol_frac 'BEGIN{ printf "%d",num*frac}') 
num_solv_two=$(awk -v total=$total_mol_solvent -v num_one=$num_solv_one 'BEGIN{ printf "%d",total-num_one}') 

solv_vol_one=$(awk -v num=$num_solv_one -v vol=$solvent_one_vol 'BEGIN{ printf "%.2f",num*vol}') 
solv_vol_two=$(awk -v num=$num_solv_two -v vol=$solvent_two_vol 'BEGIN{ printf "%.2f",num*vol}') 
total_vol=$(awk -v vol1=$solv_vol_one -v vol2=$solv_vol_two -v vol3=$solute_vol 'BEGIN{ printf "%.2f",vol1+vol2+vol3}') 

# Get box dimensions corresponding to this total volume
box_dim=$(awk -v size=$total_vol 'BEGIN{ printf "%.4f",size^0.33}') 

## --- CALCULATION OF BOX SIZE (END) --- ##

# Making new directory for our job

# Making sure there is no duplicate output directory
if [ -e $output_dir ]; then
    echo "$output_dir already exists, removing duplicates!"
    rm -r $output_dir
fi
mkdir -p $output_dir

# Going into directory
cd "$output_dir"

# Copying important files for preparation (itps, force fields, etc.)
# itp + parameters for solute
cp -rv "$solute_dir/$solute_name/"{*.itp,*.prm} ./
# itp + parameters for solvent
cp -rv "$solvent_dir/$solvent_two_name/"{*.itp,*.prm} ./
# force field
cp -r "$forcefield_dir" ./

# Adjusting volume of the box and adding solute
gmx editconf -f ${solute_PDB_file} -o ${output_prefix}.gro -bt cubic -box $box_dim $box_dim $box_dim -c

# Adding topology file
cp ${top_input_file} ${top_output_file}

# Fixing topology outputfile
sed -i "s/SOLVENT/$solvent_two_name/g" $top_output_file
sed -i "s/SOLUTE/$solute_name/g" $top_output_file

# cat correct  numbers to end of file (in correct order) Insertion into topology file
echo "$solute_residue_name    1" >> ${top_output_file}

# Minimize the solute by itself
gmx grompp -f $minim_mdp -c ${output_prefix}.gro -p ${top_output_file} -o ${output_prefix}_em1 -maxwarn 1
gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em1


# Naming next energy minimization
output_prefix_em2="${output_prefix}_em2"

# Add second solvent
if [ $(awk -v wt_frac=$solvent_one_weight_frac 'BEGIN{ if (wt_frac != 1.00) print 1; else print 0}') -eq 1 ]; then

    # Solvating
    gmx solvate -cp ${output_prefix}_em1.gro -cs ${solvent_dir}/${solvent_two_name}/${solvent_two_name}.gro -o ${output_prefix}.gro -maxsol $num_solv_two
    
    # Adding to topology
    echo "$solvent_two_residue_name    $num_solv_two" >> ${top_output_file}
    
    # Minimize solute with solvent#2
    gmx grompp -f $minim_mdp -c ${output_prefix}.gro -p ${top_output_file} -o $output_prefix_em2 -maxwarn 1
    gmx mdrun -v -nt 1 -deffnm $output_prefix_em2
    
    else
    # Simply change the name of gro file
    cp ${output_prefix}_em1.gro $output_prefix_em2.gro
    
fi

# Add First Solvent
gmx solvate -cp $output_prefix_em2.gro -cs ${solvent_dir}/${solvent_one_name}/${solvent_one_name}.gro -o ${output_prefix}.gro -maxsol $num_solv_one

# Solvent #1, Water -- Adding to topology
echo "$solvent_one_residue_name    $num_solv_one" >> ${top_output_file}

# Minimize with solvent #1 (Water)
gmx grompp -f $minim_mdp -c ${output_prefix}.gro -p ${top_output_file} -o ${output_prefix}_em3 -maxwarn 1
gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em3

# Making sure energy minimization is complete
if [ ! -e ${output_prefix}_em3.gro ]; then
    echo "Error! Energy minimization for water is not complete. This implies that you have some sort of error in your system. Stopping here!"
    echo "*** For debugging ***"
    echo "- Please check prep_solvent.sh and see if you have equilibrated the system long enough. Typically, we equilibrate at 500 ps, but you can increase that to 1.5 ns by changing the input variable, want_long_equil to True. Then, you re-run the prep_solvent.sh script."
    echo "- Please check the molecular volume. Generally, we fudge by 2 to ensure the molecule fits. It may be the case that the fudging is insufficient (although typically, the issue really is the input gro file for the solvent via prep_solvent.sh script)"
    echo "--- Error location ---"
    echo "Solute name: ${solute_name}"
    echo "Residue name: ${solute_residue_name}"
    echo "Output directory: ${output_dir}"
    echo "... Pausing 5 seconds so you can see the error"
    sleep 5
    exit
fi


# No periodic boundary (Probably unnecessary)
gmx editconf -f ${output_prefix}_em3.gro -nopbc -o ${output_prefix}.gro # -bt cubic -d 0.000 

# Cleaning up local directory
mkdir -p setup_files
rm \#* # Removes all temporary files
mv * setup_files

# Keep the gro and top file
mv setup_files/$top_output_file ./
mv setup_files/${output_prefix}.gro ./

# output what we did
echo "   ************* SUMMARY  *************"
echo " Solute: $solute_residue_name"
echo " Added $num_solv_one molecules of $solvent_one_name "
echo " Added $num_solv_two molecules of $solvent_two_name "
echo " Total box dimensions: $box_dim x $box_dim x $box_dim"
