#!/bin/bash

# prep_cosolvent.sh
# PLAN FOR THIS:
# We would like a mixed cosolvent system. Let's create a small case and then use genconf to expand it. Before genconf, we will equilibrate the system heavily. 

## INPUTS ##

# Usage: prep_cosolvent.sh solute_name solute_residue_name output_dir mol_frac solvent_two_name solvent_two_residue_name
# Example: bash prep_cosolvent.sh 0.5 GVL_L GVLL 6 spce
# $1 - Mass fraction of water
# $2 - Co-solvent molecule name
# $3 - Desired box length (nm)
# $4 - Water model (tip3p, spce, spc, etc.)

# Written by Alex Chew (05-11-2017)

##* -- Updates -- *##
# 17-05-12: Updated to include different box lengths
# 17-05-17: Updated to include 100% Water weight fraction
# 17-05-26: Fixing topology file to remove parameter and itp
# 17-06-01: Added hydronium ion addition and availability to change the desired water model (originally tip3p)
# 17-06-13: Added ability to get 0% water
# 19-10-04: Updating code to take in global variables. Removing residue name requirement

## RELOADING BIN FILE
source "../bin/bash_rc.sh"

# -- USER DEFINED PARAMETERS -- #
solvent_one_weight_frac="$1" # fraction of first solvent
solvent_two_name="$2" # Co-solvent molecule name
# solvent_two_residue_name="$3" # Co-solvent residue name

# Defining desired box length
desired_box_length="$3"  #4 nms

# Defining water model
water_model="$4" # tip3p, spce, spc, etc. (original tip3p) "spce"

# Defining temperature
system_temp="300" # 300 K is the default

## DEFINING IF YOU WANT TO OVERWRITE
want_overwrite=false
# True if you want to overwrite previous files

# Getting percentage out of 100
percent_solvent_one_wt_frac=$(awk -v num=$solvent_one_weight_frac 'BEGIN{ printf "%d",num*100}')

###################
### DIRECTORIES ###
###################
currentWD="${CURRENTWD}" # current working directory
prepdir="${PATH2PREP}"
input_files="${INPUT_DIR}"
output_prefix="mixed_solv" # Prefix for gro and top files

# Solute / solvent / forcefield /mdp files
solute_dir="${PREP_SOLUTE_DIR}" # Solute directory
solvent_dir="${PREP_SOLVENT_DIR}" # Solvent directory
forcefield_dir="$input_files/charmm36-nov2016.ff"
mdp_dir="${MDP_DIR}" # mdp file directory

## DEFINING SOLVENT ITP FILE
solvent_itp_file="${PREP_SOLVENT_DIR}/${solvent_two_name}/${solvent_two_name}.itp"

## GETTING RESIDUE NAME
solvent_two_residue_name=$(itp_get_resname ${solvent_itp_file})

########################
### OUTPUT DIRECTORY ###
########################
# Checking if there is a cosolvent
if [ $(awk -v wt_frac=$solvent_one_weight_frac 'BEGIN{ if (wt_frac != 1.00 && wt_frac != 0.00) print 1; else print 0}') -eq 1 ]; then
    output_dir_name=${percent_solvent_one_wt_frac}_Water_${solvent_two_residue_name} 
    
    else
    # Checking if there is water
    if [ $(awk -v wt_frac=$solvent_one_weight_frac 'BEGIN{ if (wt_frac == 0.00) print 1; else print 0}') -eq 1 ]; then
        echo "Running this part!"
        output_dir_name=${percent_solvent_one_wt_frac}_${solvent_two_residue_name}

    else # Pure water
        output_dir_name=${percent_solvent_one_wt_frac}_Water
    
    fi
fi

## PRINTING
echo "Current Water is: ${percent_solvent_one_wt_frac}"
echo "Output directory name is: ${output_dir_name}"

# Defining information files
molecular_wt_info="${solvent_dir}/molecular_weights.info" # Stores molecular weight information
volume_info="${solvent_dir}/molecular_volume.info" # Stores volume information
top_input_file="${input_files}/topology/mixed_solv_cosolvent.top" # cosolvent input topology file

# Specifying mixing cosolvent directory
cosolvent_dir="${prepdir}/prep_mixed_cosolvents"

# Defining submission script for equilibration
# Submission script
submitScript="submit_cosolvent.sh" # Submission script name
submitScript_loc="$input_files/submission/$submitScript" # Location
output_submit_script="submit.sh" # submission script output

# Refining output directory for full path
output_dir="$cosolvent_dir/${water_model}/${desired_box_length}nm/$output_dir_name"

# MDP Files
# Minimization
minim_mdp="$mdp_dir/minim_LIQ.mdp"
# Equilibration
equil_mdp_file_name="equil_cosolvent_mixing.mdp"
equil_mdp="$mdp_dir/$equil_mdp_file_name"

# Specifying output files
top_output_file=mixed_solv.top

# Defining job list file
scriptDir="$currentWD/Scripts" # Script directory
job_list_file="$scriptDir/job_list.txt"

## --- CALCULATION OF BOX SIZE (START) --- ##
solvent_one_name="spc216"
solvent_one_residue_name="SOL"
solvent_one_file="spc216.gro"
solvent_one_vol="0.09" # units of nm3 / molecule; estimated from input gro file box size with 1.5x fudge factor
solvent_one_MW="18.01" # g/mol, molecular weight of water

# Found beforehand, molecular volume
solvent_two_vol_init=$(grep "^$solvent_two_name" $volume_info | awk '{print $NF}') # units of nm3 / molecule; approximated from input gro file box size

if [ -z "$solvent_two_vol_init" ]; then 
    echo "Error! No co-solvent volume!"; 
    echo "Stopping here, please check $volume_info"
    echo "All co-solvents need volumes to find the correct box size"
    echo "Stopping job now!"
    exit
fi

# Adding a 2.0 fudge factor, molecular volume
solvent_two_vol=$(awk -v vol_init=$solvent_two_vol_init 'BEGIN{ printf "%.2f", vol_init*2 }') # 1.5 failed a 5% weight fraction of water

## Conversion of weight fraction into mole fraction

# First, we need the molecular weight of the solute
solvent_two_MW=$(grep "^$solvent_two_name" $molecular_wt_info | awk '{print $NF}')

# Checking to make sure the MW is there
if [ -z "$solvent_two_MW" ]; then 
    echo "Error! No co-solvent molecular weight"; 
    echo "Stopping here, please check $molecular_wt_info"
    echo "All co-solvents need molecular weights for conversions of mass fractions to mole fractions"
    echo "Stopping job now!"
    exit
fi

# Now, getting total mass / fraction of solvent 2
solvent_two_weight_frac=$(awk -v frac_one=$solvent_one_weight_frac 'BEGIN{ printf "%.2f",1-frac_one}') 
total_mass=$(awk -v frac_one=$solvent_one_weight_frac -v frac_two=$solvent_two_weight_frac -v MW_1=$solvent_one_MW -v MW_2=$solvent_two_MW 'BEGIN{ printf "%.5f", frac_one/MW_1 + frac_two/MW_2  }') 

# Now, getting the moles of water
solvent_one_mol_frac=$(awk -v frac_one=$solvent_one_weight_frac -v MW_1=$solvent_one_MW -v tot_mass=$total_mass 'BEGIN{ printf "%.2f", frac_one/MW_1/tot_mass }') 

## --- INITIAL BOX SIZE CALCULATIONS (BEGIN) --- ## 
# Note that this will always truncate any decimals; ensures 100% though

# Finding total volume
total_vol=$(awk -v vol=$desired_box_length 'BEGIN{ printf "%.2f",vol^3}') 

echo "Total Volume is $total_vol"

## Total number of molecules ##
if [ $(awk -v wt_frac=$solvent_one_weight_frac 'BEGIN{ if (wt_frac != 0.00) print 1; else print 0}') -eq 1 ]; then
    # Finding total number of solvent one
    num_solv_one=$(awk -v totvol=$total_vol -v vol_solvent_1=$solvent_one_vol -v vol_solvent_2=$solvent_two_vol -v mol_frac_1=$solvent_one_mol_frac 'BEGIN{ printf "%d",totvol/(vol_solvent_1+(1-mol_frac_1)/mol_frac_1*vol_solvent_2)}') 
    # Finding total number for solvent two
    num_solv_two=$(awk -v num_solv_1=$num_solv_one -v mol_frac_1=$solvent_one_mol_frac 'BEGIN{ printf "%d",num_solv_1*(1-mol_frac_1)/mol_frac_1}') 
else # No Water
    num_solv_one=0
    num_solv_two=$(awk -v totalVol=$total_vol -v sol_2_vol=$solvent_two_vol 'BEGIN{ printf "%d",totalVol/sol_2_vol}') 
fi

## RINTING
echo "Number of solvent 1 is $num_solv_one"
echo "Number of solvent 2 is $num_solv_two"

### --- INITIAL BOX SIZE CALCULATIONS (END) --- ##

## CHECKING IF OUTPUT DIRECTORY EXISTS
if [[ ! -e "${output_dir}" ]] || [[ "${want_overwrite}" == true ]]; then

## CREATING DIRECTORY
create_dir "${output_dir}" -f

# Going into directory
cd "${output_dir}"

# Copying important files for preparation (itps, force fields, etc.)

# itp + parameters for solvent
cp -rv "${solvent_dir}/${solvent_two_name}/"{${solvent_two_name}.itp,${solvent_two_name}.prm} ./
# force field
cp -r "${forcefield_dir}" ./

## SYSTEM CREATION ##

# Adding topology file
cp ${top_input_file} ${top_output_file}

# Fixing topology file
sed -i "s/SOLVENT/$solvent_two_name/g" $top_output_file # Fixing solvent name
sed -i "s/WATER_MODEL/$water_model/g" $top_output_file # Fixing water model

## Creating GRO File ##

# Checking if we have 100% water
if [ $(awk -v wt_frac=$solvent_one_weight_frac 'BEGIN{ if (wt_frac != 1.00 && wt_frac != 0.00) print 1; else print 0}') -eq 1 ]; then

    ## Solvent 2: Cosolvent ## Added first because it's bulkier

    # Solvating
    gmx solvate -cs ${solvent_dir}/${solvent_two_name}/${solvent_two_name}.gro -o ${output_prefix}.gro -maxsol $num_solv_two -box $desired_box_length $desired_box_length $desired_box_length

    # Adding to topology
    echo "$solvent_two_residue_name    $num_solv_two" >> ${top_output_file}
    
    # Now, minimize the solvent
    gmx grompp -f $minim_mdp -c ${output_prefix}.gro -p ${top_output_file} -o ${output_prefix}_em1 -maxwarn 1
    gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em1
    
    ## Solvent 1: Water ##
    # Adding water
    gmx solvate -cp ${output_prefix}_em1.gro -cs ${solvent_dir}/${solvent_one_name}/${solvent_one_name}.gro -o ${output_prefix}.gro -maxsol $num_solv_one
    
    # Adding to topology
    echo "$solvent_one_residue_name    $num_solv_one" >> ${top_output_file}
    
    else
    
    # Checking if there is water
    if [ $(awk -v wt_frac=$solvent_one_weight_frac 'BEGIN{ if (wt_frac == 0.00) print 1; else print 0}') -eq 1 ]; then

        # Editing topology to remove water itp or prms
        line_num_water_itp=$(grep -n "${water_model}.itp" ${top_output_file} | grep -Eo '^[^:]+')
        # Commenting out itp / prms for water
        sed -i "${line_num_water_itp}s/^/; /" ${top_output_file}

        ## Only solvent 2, cosolvent ##
        # Solvating
        gmx solvate -cs ${solvent_dir}/${solvent_two_name}/${solvent_two_name}.gro -o ${output_prefix}.gro -maxsol $num_solv_two -box $desired_box_length $desired_box_length $desired_box_length

        # Adding to topology
        echo "$solvent_two_residue_name    $num_solv_two" >> ${top_output_file}

        # Now, minimize the solvent
        gmx grompp -f $minim_mdp -c ${output_prefix}.gro -p ${top_output_file} -o ${output_prefix}_em1 -maxwarn 1
        gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em1

        # Rewriting gro file
        if [ -e ${output_prefix_em1.gro} ]; then
            rm ${output_prefix}.gro; mv ${output_prefix}_em1.gro ${output_prefix}.gro
        else
            echo "Error, check the 0 percent water case!"
            echo "Exiting..."
            exit
        fi
    
    else # No co-solvent (pure water)
    
        # Editing topology to remove cosolvent itp or prms
        line_num_cosolvent_itp=$(grep -n "${solvent_two_name}.itp" ${top_output_file} | grep -Eo '^[^:]+')
        line_num_cosolvent_prm=$(grep -n "${solvent_two_name}.prm" ${top_output_file} | grep -Eo '^[^:]+')
        # Commenting out itp / prms for cosolvents
        sed -i "${line_num_cosolvent_itp}s/^/; /" ${top_output_file}
        sed -i "${line_num_cosolvent_prm}s/^/; /" ${top_output_file}

        # Adding water
        gmx solvate -cs ${solvent_dir}/${solvent_one_name}/${solvent_one_name}.gro -o ${output_prefix}.gro -maxsol $num_solv_one -box $desired_box_length $desired_box_length $desired_box_length

        # Adding to topology
        echo "$solvent_one_residue_name    $num_solv_one" >> ${top_output_file}

    fi
fi

# Minimize with solvent #1 (Water)
gmx grompp -f $minim_mdp -c ${output_prefix}.gro -p ${top_output_file} -o ${output_prefix}_em2 -maxwarn 1
gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em2

# Making sure energy minimization is complete
if [ ! -e ${output_prefix}_em2.gro ]; then
    echo "Error! Energy minimization for water is not complete. This implies that you have some sort of error in your system. Stopping here!"
    echo "Working directory: $(pwd)"
    echo "Pausing so you can see this error!"
    sleep 5
    exit
fi

## Equilibration ##

# Copy equilibration mdp file and changing temperature
sed "s/TEMPERATURE/$system_temp/g" $equil_mdp > ./$equil_mdp_file_name

# Creating a job script
sed "s/DIRECTORY/$output_dir_name/g" $submitScript_loc > ./$output_submit_script

# Printing
echo "Writing to job file: $output_dir"

# Writing output directory to jobfile
echo "$output_dir" >> $job_list_file

## OUTPUTTING
echo "   ************* SUMMARY  *************"
echo " Added ${num_solv_one} molecules of ${solvent_one_name} "
echo " Added ${num_solv_two} molecules of ${solvent_two_name} "
echo " Total box dimensions: ${desired_box_length} x ${desired_box_length} x ${desired_box_length}"
echo " --- Starting equilibration addition ---"

else
    echo "${output_dir} is available! No jobs are submited."
fi
    