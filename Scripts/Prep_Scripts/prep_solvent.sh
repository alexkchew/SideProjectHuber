#!/bin/bash

# prep_solvent.sh
# The purpose of this script is to take the output from CHARMM gui to output solvent details. Furthermore, this script will run energy minimization and 500 ps NPT equilibration on the solvent so subsequent simulations would work correctly.

# Note that each solvent should have a "setup_files" where the *.itp, *.prm, and *.pdb files are placed.
# Written by: Alex K. Chew (alexkchew@gmail.com, 02/27/2018)

## ALGORITHM:
# 1. We will find volumes of a single solvent
# 2. Using that volume, expand to a box of 125 solvent molecules
# 3. Run energy minimization
# 4. Run equilibration
# 5. Output volume in the molecular_volume.info
# 6. Output final equilibrated gro file
# 7. Clean up directories

## USAGE: bash prep_solvent.sh urea URE

## VARIABLES:
# $1: solvent_name: Name of the solvent
# $2: solvent_residue_name: residue name of the solvent

# *** UPDATES *** #
# 180227 - Completed draft of preparation solvents
# 180330 - Updated to include variables
# 180423 - Updated to enable changes to run time parameters for NPT equil


# -- USER DEFINED PARAMETERS (BEGIN) -- #
solvent_name="$1" # Name of the solvent
solvent_residue_name="$2" # Name of the residue name

### DEFINING LOGICALS
want_long_equil="True" # True if you want longer equilibration (1.5 ns instead of 500 ps)

# -- USER DEFINED PARAMETERS (END) -- #

### LOCAL FUNCTIONS

## FUNCTION TO CHECK INTEGRITY OF SETUP FILE
# $1: Full path to setup file
function check_setup_file () {
    # DEFINING VARIABLES
    setup_file_path="$1" # file path to setup file
    # PRINTING
    echo "Checking integrity of setup file........"
    echo "Setup file directory: $1"
    
    # CHECKING FILES
    check_itp="$(check_file_exist ${setup_file_path}/*.itp)"
    check_pdb="$(check_file_exist ${setup_file_path}/*.pdb)"
    check_prm="$(check_file_exist ${setup_file_path}/*.prm)"
    echo "ITP file exists: ${check_itp}"
    echo "PDB file exists: ${check_pdb}"
    echo "PRM file exists: ${check_prm}"
    
    if [ "${check_itp}" == "True" ] && [ "${check_pdb}" == "True" ] && [ "${check_prm}" == "True" ]; then
        echo "Integrity intact, itp, pdb, and prm file is correctly located"
    else
        echo "ERROR! Check setup files. You should have an itp, pdb, and prm file... stopping here."
        exit
    fi
}

## FUNCTION TO CHECK MOLECULAR VOLUME TO SEE IF ENTRY EXISTS
# INPUTS:
#   $1: name of the molecule
#   $2: path of the file
# OUTPUTS:
#   True/False
# USAGE: read logical <<< $(check_molecule_info $MOLECULE_NAME $FILE_PATH)
function check_molecule_info () {
    # DEFINING VARIABLES
    molecule_name="$1"
    file_path="$2"
    
    # GREPPING TO SEE EXISTANCE IN FIRST LINE
    line_location=$(grep -nE "^${molecule_name}" "${file_path}" | sed 's/\([0-9]*\).*/\1/')
    
    # SEEING IF MOLECULAR INFORMATION IS THERE!
    if [ -z "${line_location}" ]; then
        echo "False" # LINE DOES NOT EXIST
    else
        echo "True" # LINE DOES EXIST
    fi
}

### FUNCTION TO ROUND UP
# INPUTS:
#   $1: Value to round
#   $2: Number of decimal places
# OUTPUTS:
#   Rounded value
# USAGE: roundup $VALUE $NUM_DEC_PLACES
function roundup() 
{ echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+1.0)/(10^$2)" | bc)
); };

### THE PURPOSE OF THIS FUNCTION IS TO WRITE INTO MOLECULAR INFO INTO A FILE
# INPUTS:
#   $1: molecular_file: File of interest
#   $2: name: Name of molecule you are interested in
#   $3: value: Value accompanied with the molecule
# USAGE: write_molecular_info $MOLECULAR_FILE $SOLUTE_NAME $VALUE
function write_molecular_info () {
    # DEFINING VARIABLES
    molecular_file="$1"
    name="$2"
    value="$3"
    
    # READING FILE TO SEE IF MOLECULE EXISTS
    read within_molecule_vol <<< $(check_molecule_info ${name} ${molecular_file})
    if [ "${within_molecule_vol}" == "False" ]; then
        echo "Adding line to ${molecular_file}"
        printf "%s, %0.2f\n" "${name}" "${value}" >> "${molecular_file}"
    else
        echo "${molecular_file} already has ${value} -- doing nothing"
    fi
}

### FUNCTION TO CALCULATE MOLECULAR WEIGHT
# INPUTS:
#   $1: PDB file
#   $2: reference file which has all atom names and molecular weights
# OUTPUTS:
#   Value for molecular weight
## USAGE: calc_pdb_molecular_weight urea_ini.pdb
function calc_pdb_molecular_weight () {
    ## DEFINING INPUT VARIABLE
    input_pdb="$1" # INPUT PDB
    ref_file="$2" # REFERENCE FILE
    
    ## REFERENCE FILE
    ref_file="/home/akchew/scratch/SideProjectHuber/prep_mixed_solvent/input_files/solvents/molecular_weights_ref.txt"

    ## GETTING THE ATOM LIST
    atom_list=$(head -n -1 "${input_pdb}" | awk '{print $3}' | sed 's/[0-9]*//g')
    
    ## GETTING REFERENCE FILE DATA WITHOUT COMMENTS
    ref_file_data="$(grep "^[^;]" ${ref_file})"
    
    ## DEFINING MOLECULAR WEIGHT VARIABLE
    molecular_weight=0
    
    ## LOOPING THROUGH THE LIST AND GETTING MOLECULAR WEIGHT, THEN ADDING IT
    while read -r current_atom; do
        ## FINDING THE ATOMIC WEIGHT
        # Removes all comments | finds atom name | prints mass | cuts all white space
        atom_weight=$(grep "^[^;]" "${ref_file}" | grep "${current_atom}" | awk -F',' '{print $2}' | tr -d ' ')        
        
        ## ADDING TO MOLECULAR WEIGHT
        # Adds the weights, removes all issues with windows, then makes math happen
        molecular_weight=$(echo "${molecular_weight} + ${atom_weight}" | tr -d $'\r' | bc )
    done <<< "${atom_list}"
    ## PRINTING MOLECULAR WEIGHT
    echo "${molecular_weight}"
}


### RELOADING GLOBAL VARS AND FUNCTIONS
source "../bin/bash_rc.sh"

### PRINTING SCRIPT NAME
print_script_name

### PRE-DEFINED VARIABLES
molecular_volume_file="${PREP_SOLVENT_DIR}/molecular_volume.info" # File where we store all molecular volumes
molecular_weight_file="${PREP_SOLVENT_DIR}/molecular_weights.info"
molecular_weight_ref="${PREP_SOLVENT_DIR}/molecular_weights_ref.txt" # Where you will find all atom names and corresponding atom mass
num_of_res="125" # Number of residues you want for your final .gro file
force_field="charmm36-nov2016.ff" # Force field
prep_folder="${PREP_SOLVENT_DIR}/prep_solvents"
top_folder="${prep_folder}/topology"
mdp_folder="${prep_folder}/mdp"

## DEFINING FUDGE FACTOR
fudge_fact="2" # 1.5 

### PRE-DEFINED TOP/MDP FILES
input_top_file="prep_solvent.top"
input_em_mdp_file="minim_LIQ.mdp"
input_equil_mdp_file="equil_LIQ.mdp"

### PRE-DEFINED MDP RUN-TIME PARAMETERS
if [ "${want_long_equil}" == "True" ]; then
    equil_n_steps="3000000" # 3000000 -- 6 ns Default: 250000 for 500 ps
    # equil_n_steps="750000" # Default: 250000 for 500 ps
    # 750000 -- 1.5 ns for more difficult to equilibrate
else
    equil_n_steps="250000"
fi

### DEFINING DIRECTORIES
# SOLVENT DIRECTORIES
solvent_dir="${PREP_SOLVENT_DIR}/${solvent_name}" # Main directory for current solvent
solvent_setup_dir="${solvent_dir}/setup_files" # Setup directory for current solvent
solvent_sim_dir="${solvent_dir}/em_equil" # Directory for energy minimization and equilibration

# OTHER INPUT DIRECTORIES
force_field_dir="${INPUT_DIR}/${force_field}"

## CHECKING INTEGRITY OF SETUP FILE
check_setup_file "${solvent_setup_dir}"

## CHECKING IF THE ENERGY/MINIMIZATION DIRECTORY EXISTS
create_dir "${solvent_sim_dir}" -f

## GOING INTO DIRECTORY
cd "${solvent_sim_dir}"

######################################################
#### -- SHRINKING AND FINDING MOLECULAR VOLUME -- ####
######################################################

## RUNNING EDITCONF TO GET MOLECULAR VOLUME
gmx editconf -f "${solvent_setup_dir}"/*.pdb -o "${solvent_sim_dir}/${solvent_name}_shrink.gro" -d 0.000 -bt cubic

## READING BOX LENGTH AND BOX VOLUME
read box_length molecular_volume <<< "$(read_gro_box_length ${solvent_name}_shrink.gro)"

## ROUNDING MOLECULAR VOLUME
rounded_molecular_volume=$(roundup ${molecular_volume} 2)

## WRITING MOLECULAR VOLUME TO INFO FILE
write_molecular_info "${molecular_volume_file}" "${solvent_name}" "${rounded_molecular_volume}"

####################################
### CALCULATING MOLECULAR WEIGHT ###
####################################

## CALCULATING MOLECULAR WEIGHT OF THE PDB FILE
molecular_wt=$(calc_pdb_molecular_weight "${solvent_setup_dir}"/*.pdb ${molecular_weight_ref})

## STORING INTO MOLECULAR WEIGHT FILE
write_molecular_info "${molecular_weight_file}" "${solvent_name}" "${molecular_wt}"

#########################################################################
#### -- SOLVATING AND RUNNING ENERGY MINIMIZATION + EQUILIBRATION -- ####
#########################################################################

## FINDING NEW VOLUME / LENGTH
new_volume=$(echo "${num_of_res}*${molecular_volume}" | bc -l)
new_length=$(awk -v vol=$new_volume -v fudge_fact=${fudge_fact} 'BEGIN{ printf "%.4f",vol**(1/3)*fudge_fact}') # Fudging by 50%

## ROUNDING LENGTH UP TO ENSURE ALL MOLECULES WILL FIT
round_length=$(roundup ${new_length} 0)

## RUNNING SOLVATION
gmx solvate -cs "${solvent_name}_shrink.gro" -o "${solvent_name}_${num_of_res}.gro" -maxsol "${num_of_res}" -box "${round_length}" "${round_length}" "${round_length}"

## COPYING FORCEFIELD FILES 
echo "COPYING FORCE FIELD FILES, ITP, TOPOLOGY, PRM, ETC."
# Force fields
cp -r "${force_field_dir}" "${solvent_sim_dir}" 
# ITP files
cp -r "${solvent_setup_dir}"/*.itp "${solvent_sim_dir}/${solvent_name}.itp"
# PRM files
cp -r "${solvent_setup_dir}"/*.prm "${solvent_sim_dir}/${solvent_name}.prm"
# TOP files
cp -r "${top_folder}/${input_top_file}" "${solvent_sim_dir}/${input_top_file}"
# MDP FILES
cp -r "${mdp_folder}/"{${input_em_mdp_file},${input_equil_mdp_file}} "${solvent_sim_dir}"

## FIXING MDP FILE DETAILS
sed -i "s/NSTEPS/${equil_n_steps}/g" "${input_equil_mdp_file}"

## FIXING FILES THAT WERE COPIED
sed -i "s/MOLECULE_NAME/${solvent_name}/g" "${input_top_file}"
echo "   ${solvent_residue_name}    ${num_of_res}" >> "${input_top_file}"

## RUNNING ENERGY MINIMIZATION
gmx grompp -f ${input_em_mdp_file} -c "${solvent_name}_${num_of_res}.gro" -p "${input_top_file}" -o "${solvent_name}_${num_of_res}_em"
gmx mdrun -v -nt 1 -deffnm "${solvent_name}_${num_of_res}_em"

## RUNNING EQUILIBRATION
gmx grompp -f "${input_equil_mdp_file}" -c "${solvent_name}_${num_of_res}_em.gro" -p "${input_top_file}" -o "${solvent_name}_${num_of_res}_equil"
gmx mdrun -v -nt 12 -deffnm "${solvent_name}_${num_of_res}_equil"

## LASTLY, COPYING EQUILIBRATED FILES TO MAIN DIRECTORY
# GRO FILE
cp -r "${solvent_name}_${num_of_res}_equil.gro" "${solvent_dir}/${solvent_name}.gro"
# ITP FILE
cp -r "${solvent_name}.itp" "${solvent_dir}/${solvent_name}.itp"
# PRM FILE
cp -r "${solvent_name}.prm" "${solvent_dir}/${solvent_name}.prm"

### PRINTING SUMMARY
echo "----- SUMMARY -----"
echo "Worked on solvent: ${solvent_name}"
echo "Molecular volume: ${rounded_molecular_volume} nm^3"
echo "Molecular weight: ${molecular_wt}"
echo "Total number of steps for equilibration: ${equil_n_steps}"
echo "Total solvent residues desired: ${num_of_res}"
echo "*gro, *prm, and *itp located in ${solvent_dir}"
echo "YOU'RE ALL GOOD TO GO! Best of luck!"