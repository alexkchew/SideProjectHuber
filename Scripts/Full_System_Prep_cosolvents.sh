#!/bin/bash

# Full_System_Prep_cosolvents.sh
# Written by Alex Chew (Updated 05/11/2017)
# The purpose of this script is to take the One_System_Prep_cosolvents.sh and expand it to generate the intial state of our system. Once the systems are set up, we can use another script to submit the jobs easily. The main focus of this script is to set up all the systems so we can just submit them on the fly.

# This script will use for-loops to generate our systems in a fast pace. Optimally, we do this for one solute at a time at varying concentrations of co-solvents. ** Important ** Make sure to check the directories of One_System_Prep.sh to ensure that there is no errors in directories.

# Usage: bash Full_System_Prep_cosolvents.sh

##*-- Updates --*##
# 17-05-12: Updated to include box length
# 17-06-01: Added hydronium ion addition and availability to change the desired water model (originally tip3p)s
# 17-06-20: Added temperature descriptions
# 17-06-21: Added extension of production MD
# 17-07-17: Added remainder of the solutes temperatures
# 17-07-18: Fixed temperature for xylitol (433.15 K -> 403.15 K)

# Defining important directories
#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
bashrc_name="bash_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/bin/${bashrc_name}"
## PRINTING NAME
print_script_name
## ONE SYSTEM SCRIPT NAME

### NEED TO UPDATE
# REMOVAL OF RESIDUE 1 NAME
# REMOVAL OF RESIDUE 2 NAME - solvent_two_residue_name

one_system_script_name="One_System_Prep_cosolvents.sh" # Script to generate one system
# Script to generate one system for free energy perturbation simulations (assuming One_System_Prep_cosolvents.sh was completed)
# one_system_script_name="One_System_Prep_continuation_free_energy.sh" 

# Script to generate most likely configurations (assuming One_System_Prep_cosolvents.sh was completed)
# one_system_script_name="One_System_Prep_most_likely_configuration.sh" 

# ----------------


# one_system_script_name="One_System_Prep_expand_box.sh" # Script to generat expanded system




# one_system_script_name="One_System_Prep_hydronium.sh" # Script to run hydronium ion
# one_system_script_name="One_System_Prep_prod_extension.sh" # Script to extend production runs
# one_system_script_name="One_System_post_sim.sh" # Script to generate one system
job_list_file="$PATH2SCRIPTS/job_list.txt"

## DEFINING MAIN SIMULATION DIRECTORY
# main_sim_dir="181107-PDO_DEHYDRATION_FULL_DATA_300NS_OTHERSOLVENTS" # "180706-HMF_LEV_FRU_in_DMSO_DIO"
# main_sim_dir="190309-PDO_DMSO_Sims" # "180706-HMF_LEV_FRU_in_DMSO_DIO"
main_sim_dir="181107-PDO_DEHYDRATION_FULL_DATA_300NS" # "180706-HMF_LEV_FRU_in_DMSO_DIO"
main_sim_dir="FINAL_PDO_DEHYDRATION/Normal_MD_300_ns_sims" ## <-- simulation with all PDO sims
main_sim_dir="20201019-Pure_Mixed_Solvents" ## <-- simulation with all PDO sims

# main_sim_dir="190925-2ns_mixed_solvent_with_FRU_HMF" ## <-- simulation with
# main_sim_dir="190925-4ns_mixed_solvent_with_FRU_HMF" ## <-- simulation with


#main_sim_dir="191004-FRU_HMF_FEP" ## <-- simulation with
#main_sim_dir="191015-Cyclohexanol_4ns_expanded_FEP" ## <-- simulation with
#main_sim_dir="191022-FRU_HMF_LEV_FEP_STAMPEDE" ## <-- simulation with


# main_sim_dir="190227-PDO_Most_likely_GVL_THF" # "180706-HMF_LEV_FRU_in_DMSO_DIO"
# main_sim_dir="190713-GLY_BTL_sims" # "180706-HMF_LEV_FRU_in_DMSO_DIO

## DEFINING OUTPUT SIM DIR
output_sim_dir="190713-GLY_BTL_sims_most_likely"
# output_sim_dir="20201019-Pure_Mixed_Solvents"
# "190922-PDO_most_likely_gauche"
# EXPANDED SIMULATION PARAMETERS
expand_dist="8" # nm expansion

## DEFINING ANALYSIS DIRECTORY
# main_analysis_dir="180521-20_HMF_rerun_10ns_prod_extend"

## Declaring array variables
## Solutes
# "tBuOH" "levoglucosan" "xylitol" "cellobiose" "ETBE" "propanediol" "fructose" "12-propanediol" "propanal" "acetone"
# "acetone" "propanal" "12-propanediol" "fructose" "5-hydroxymethylfurfural" "1-hydroxy-2-butanone" "fru-int-2" "levulinic" "6-hydroxy-25-dioxohexanal" "hydroxyacetone" "hydronium" "hydronium_LINCS_3" "None" "glucose" "chlorine" "12-propanediol" "acetone"
# "12-propanediol" "acetone" "propanal" "NoSolute"
# "hydronium_LINCS_3" "chlorine"
# cis-13-cyclohexanediol
# "12-propanediol" "propanal" "acetone"
# "fructose" "5-hydroxymethylfurfural" "levulinic" "LEV"
# "cyclohexanol" "cyclohexene"
# "trans-13-cyclohexanediol"
# "fructose" "5-hydroxymethylfurfural" "levulinic"
# "fructose" "12-propanediol" 
# "cis-12-cyclohexanediol" "trans-12-cyclohexanediol"
declare -a solute_names=("xylitol") # cyclohexanol cyclohexene
# "tBuOH" 
# "NoSolute"
# "12-propanediol"
# levulinic
# "fructose" "5-hydroxymethylfurfural" 
# "fructose" "5-hydroxymethylfurfural") #  Folder name within Simulations
# "glycerol" "1butanol"
# For fructose project: "fructose" "tBuOH"
## Residue Names
#  "tBuOH" "LGA" "XYL" "CEL" "ETBE" "PDO" "FRU" "HMF" "FRI"
# "ACE" "PRO" "LEV" "HDH" "HYA" "HYD" "None" "GLU" "CLL" "HYD" "CLL" "PDO" "ACE"
# "PDO" "ACE" "PRO"
# "HYD" "CLL"
# CCH
# "PDO" "PRO" "ACE"
# "CYO" "CYE"
# "TCH"
# "FRU" "PDO" "tBuOH"
# "CHD" "THD"
# "GLY" "BTL"
# declare -a solute_residue_names=("FRU" "HMF") # <-- DEPRECIATED
## Solute temperatures
# "363.15" "403.15" "403.15" "403.15" "343.15" "433.15" "373.15"
# "300.00" "300.00"
# "373.15" "363.15" <-- fRU + tBuOH
# MeCN: 363.15 K -- TBA
#       393.15 K -- PDO
#       373.15 K -- FRU
# "373.15" "393.15" "363.15"
declare -a solute_temps=("363.15")  # 433.15 300.00 # "433.15"
# "433.15"
# "403.15" "403.15" "403.15"
# "403.15" "403.15"
# 403.15 <-- FRU /HMF for Ted's project
# FOR ACE/WATER WITH ALI: "398.15" K -- updated to 393.15 K
## NOTE: solute_names, solute_residue_names, and solute_temps must  be the same length!

## DECLARING TRIALS
# declare -a trials=("2")

# TEMPERATURE LIST
# 300.00 K - HYD
# 343.15 K - ETBE
# 363.15 K - tBuOH
# 373.15 K - FRU, FRI, LEV, HDH, HMF
# 403.15 K - LGA, CEL, XYL
# 433.15 K - PDO, SOR, ACE, GLY, HYA
# 22-dimethyl-13-dioxan-5-ol DMI 433.15

## SOLVENT NAME LIST
#   GVL_L GVLL
#   tetrahydrofuran THF
#   dioxane DIO
#   dmso dmso
#   NN-dimethylacetamide DMA
#   NN-dimethylformamide DMF
#   N-methyl-2-pyrrolidone NMP
#   urea    URE
#   2-butanone BUT
#   sulfolone SUL
## NEW TEMPERATURES FOR HMF AND LA: 125 C 398.15

## Solvents

# Mass fraction of water
# "0.00" "0.05" "0.10" "0.25" "0.50" "0.75" "0.85" "0.90" "1.00"
# "0.05" "0.10" "0.25" "0.50" "0.75" "0.85" "0.90" "1.00"
# "0.20" "0.40" "0.60" "0.80" "1.00"  "0.40" "0.60" "0.80" "1.00"
# "1.00" "0.75" "0.56" "0.35" "0.12"
# PDO dehydration: "0.05" "0.10" "0.25" "0.50" "0.75" "0.85" "0.90" "1.00"
# Hydronium ion project: "0.00" "1.00"
# "0.10" "0.25" "0.50" "0.75"
# "0.75" "0.56" "0.35" "0.12" --- Muhammad's project
# "0.75" "0.56" "0.35" "0.12"
# "0.10" "0.25" "0.50" "0.75"
declare -a solvent_one_wt_fractions=("0.10")  #  "1.00"
# "1.00"
# "0.10"
#  "1.00"
# "0.25"
# "0.10"
# "0.20" "0.40" "0.60" "0.80"  "0.25" "0.50"
# "0.02" "0.05" "0.10" "0.25" "0.50" "0.75"
# "0.10" "1.00"
# "0.75"
#  "0.40" "0.60" "0.80" "1.00" "0.10"
## SOLVENT NAME LISTs -- NO LONGER NECESSARY
#   GVL_L GVLL
#   tetrahydrofuran THF
#   dioxane DIO
#   dmso dmso
#   NN-dimethylacetamide DMA
#   NN-dimethylformamide DMF
#   N-methyl-2-pyrrolidone NMP
#   urea    URE
#   2-butanone BUT
#   sulfolone SUL
#   ethylenecarbonate ETH
#   tetramethylenesulfoxide TET
#   acetone ACE
#   acetonitrile ACN

# Cosolvent names + residue names
# Hydronium ion: "dioxane" "tetrahydrofuran" "GVL_L" "N-methyl-2-pyrrolidone" "acetone" "dmso"
# "dioxane" "dmso" "GVL_L" "acetone" "tetrahydrofuran"
# "dioxane" "dmso" "GVL_L" "acetone" "tetrahydrofuran"
#  "dioxane" "acetone" "GVL_L" "tetrahydrofuran"
# acetonitrile ACN
declare -a solvent_two_molecules_names=("dioxane" "dmso") # "dmso"
# "tetrahydrofuran" "GVL_L" "acetone"   "acetonitrile"
#  "GVL_L" "tetrahydrofuran"
# "acetone"  "dmso"

# "2-butanone" "NN-dimethylacetamide" "NN-dimethylformamide" "N-methyl-2-pyrrolidone" "sulfolone" "tetramethylenesulfoxide" "ethylenecarbonate" "acetonitrile"

# "2-butanone" "NN-dimethylacetamide" "NN-dimethylformamide" "N-methyl-2-pyrrolidone" "sulfolone" "dioxane" "dimethyl sulfoxide" "GVL_L" "tetrahydrofuran" "tetramethylenesulfoxide" "ethylenecarbonate" "acetone" "acetonitrile"
# "dmso"
# "2-butanone" "NN-dimethylacetamide" "NN-dimethylformamide" "N-methyl-2-pyrrolidone" "sulfolone" "dioxane" "dimethyl sulfoxide" "GVL_L" "tetrahydrofuran" "tetramethylenesulfoxide" "ethylenecarbonate" "acetone" "acetonitrile"
## COSOLVENT NAMES MASS
# "2-butanone" "NN-dimethylacetamide" "NN-dimethylformamide" "N-methyl-2-pyrrolidone" "sulfolone" "dioxane" "dimethyl sulfoxide" "GVL_L" "tetrahydrofuran" "tetramethylenesulfoxide" "ethylenecarbonate" "acetone" "acetonitrile"


#   PDO studies
#   "dioxane" "dmso"
#   "GVL_L" "N-methyl-2-pyrrolidone" "tetrahydrofuran" "tetramethylenesulfoxide"
#   HYD studies
#   "dioxane" "tetrahydrofuran" "GVL_L" "N-methyl-2-pyrrolidone" "acetone" "dmso"
#   "DIO" "THF" "GVLL" "NMP" "ACE" "dmso"
# "DIO" "dmso" "GVLL" "ACE" "THF" "ACN"
# declare -a solvent_two_molecules_residue_names=("dmso") # "ACE" "THF" "GVLL"  "dmso"
#   "DIO" "dmso"
#   "GVLL" "NMP" "THF" "TET"

## Desired box lengths
# "4" "6" "8"
declare -a desire_box_lengths=("6")

## Number of hydronium ions desired
declare -a desire_hyd_ions=("1") # DEPRECIATED

# Finding total solutes
totalSolutes=$((${#solute_names[@]}-1))
totalSolvents=$((${#solvent_two_molecules_names[@]}-1))

## Declaring water model
water_model="spce" # tip3p, spce, spc, etc. (original tip3p) "spce"

# Using for-loop to create multiple systems
for current_solute_count in $(seq 0 $totalSolutes); do

    # Create a variable to ensure 100% runs only one time!
    check_100="False"

    # Defining solute
    solute_name="${solute_names[current_solute_count]}"

    # Defining system temperature (K)
    system_temp="${solute_temps[current_solute_count]}"

    # Varying weight fractions
    for wt_frac in "${solvent_one_wt_fractions[@]}"; do

        # Varying solvents
        for current_solvent_two_count in $(seq 0 $totalSolvents); do
            solvent_two_name="${solvent_two_molecules_names[current_solvent_two_count]}"
            # Varying box length
            for currentBoxLen in ${desire_box_lengths[@]}; do
                # Check if we have 100% Weight fraction of pure water
                if [[ $(awk -v wt_frac=$wt_frac 'BEGIN{ if (wt_frac != 1.00) print 1; else print 0}') -eq 1 || $check_100 == "False" ]]; then
                    # Varying hydronium ion
                    if [[ $one_system_script_name == "One_System_Prep_hydronium.sh" ]]; then
                        for currentHydNum in ${desire_hyd_ions[@]}; do
                            # Using One_System_Prep to explore the system space
                            bash ${PATH2SCRIPTS}/${one_system_script_name} ${solute_name} ${system_temp} ${wt_frac} ${solvent_two_name} ${currentBoxLen} ${water_model} ${currentHydNum}
                        done
                    # Just pure cosolvents
                    elif [[ $one_system_script_name == "One_System_Prep_cosolvents.sh" ]] || [[ ${one_system_script_name} == "One_System_post_sim.sh" ]] || [[ ${one_system_script_name} == "One_System_Prep_continuation_free_energy.sh" ]]; then
                        bash ${PATH2SCRIPTS}/${one_system_script_name} ${solute_name} ${system_temp} ${wt_frac} ${solvent_two_name} ${currentBoxLen} ${water_model} "${main_sim_dir}" "${main_analysis_dir}"
                    elif [[ $one_system_script_name == "One_System_Prep_prod_extension.sh" ]]; then
                        bash $PATH2SCRIPTS/$one_system_script_name ${solute_name} ${system_temp} ${wt_frac} ${solvent_two_name} ${solvent_two_residue_name} ${currentBoxLen} ${water_model} "${main_sim_dir}"
                    ## EXPANDED BOX SIMULATIONS
                    elif [[ $one_system_script_name == "One_System_Prep_expand_box.sh" ]]; then
                        bash $PATH2SCRIPTS/$one_system_script_name "${solute_name}" "${system_temp}" "${wt_frac}" "${solvent_two_name}" "${solvent_two_residue_name}" "${currentBoxLen}" "${water_model}" "${expand_dist}" "${main_sim_dir}" "${output_sim_dir}"
                    ## EXPANDED BOX SIMULATIONS
                    elif [[ $one_system_script_name == "One_System_Prep_most_likely_configuration.sh" ]]; then
                        bash $PATH2SCRIPTS/${one_system_script_name} "${solute_name}" "${system_temp}" "${wt_frac}" "${solvent_two_name}" "${currentBoxLen}" "${water_model}" "${main_sim_dir}" "${output_sim_dir}"
                    fi
                    # Changing check_100
                    if [[ $(awk -v wt_frac=$wt_frac 'BEGIN{ if (wt_frac == 1.00) print 1; else print 0}') -eq 1 && $check_100 == "False" ]]; then
                        check_100="True"
                    fi
                fi

                # Echoing what you did
                echo "Running command: bash $PATH2SCRIPTS/$one_system_script_name $solute_name  $system_temp $wt_frac $solvent_two_name $solvent_two_residue_name $currentBoxLen $water_model"
            done
        done
    done
done

# Printing what you did
echo "------ FULL SUMMARY------"
echo "SCRIPT NAME: ${one_system_script_name}"
echo "Created systems for ${#solute_names[@]} solutes: ${solute_names[@]}"
echo "We varied the weight fraction of water: ${solvent_one_wt_fractions[@]}"
echo "The system temperatures were: ${solute_temps[@]}"
echo "The total number of solvents varied is: ${#solvent_two_molecules_names[@]}"
echo "The solvents were: ${solvent_two_molecules_names[@]} with residue names: ${solvent_two_molecules_residue_names[@]}"
echo "Box length was varied: ${desire_box_lengths[@]}"
echo "Now, we can use the script in $job_list_file to submit jobs. Good luck!"


# --- Array Commands, useful for for-loops --- #

### now loop through the above array
#for i in "${arr[@]}"
#do
#   echo "$i"
#   # or do whatever with individual element of the array
#done
#
## You can access them using echo "${arr[0]}", "${arr[1]}" also
 