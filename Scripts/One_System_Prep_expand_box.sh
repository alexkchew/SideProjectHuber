#!/bin/bash


# One_System_Prep_expand_box.sh
# Written by: Alex K. Chew (01/14/2019)
# The purpose of this script is to take your pre-designated solvent box, expand the box, then rerun the simulation

# USAGE: 
#    bash One_System_Prep_expand_box NoSolute None 300.00 1.00 dioxane DIO 6 spce 4 main_sim_dir output_sim_dir

# NOTES:
#   - This system is designed to measure the electrostatic potential across a vacuum and solvent
#   - This system assumes you have already run "One_System_Prep_cosolvents.sh" script
#   - The idea behind rerunning from a previously designated script is to ensure that the equilibration is complete. Otherwise, you will need to run an NPT equilibration separately.

## RELOADING BIN FILE
source "bin/bash_rc.sh"

#######################
### INPUT VARIABLES ###
#######################

## DEFINING SOLUTE FULL NAME AND RESDIUE NAME
solute_name="$1" # "NoSolute" # "sorbitol"
# solute_residue_name="$2" # "None" #"SOR"

### GETTING RESIDUE NAME
## DEFINING SOLUTE ITP
solute_itp_file="${PREP_SOLUTE_DIR}/${solute_name}/${solute_name}.itp"
## GETTING RESIDUE NAME
solute_residue_name=$(itp_get_resname ${solvent_itp_file})

## DEFINING SYSTEM TEMPERATURE
system_temp="$2" # "300.00" #363.15 # System temperature, edited in mdp file labelled "TEMPERATURE"

## DEFINING WATER MASS FRACTION
solvent_one_wt_frac="$3" # "1.00" #1.0 # fraction of first solvent (water)

## DEFINING COSOLVENT
solvent_two_name="$4" # "dioxane" #dioxane # Co-solvent molecule name   # GVL_L, dioxane, tetrahydrofuran
solvent_two_residue_name="$5" # "DIO" #DIO # Cosolvent residue name # GVLL, DIO, THF

## DEFINING DESIRED BOX LENGTH
desired_box_length="$6" # "6"  #4 nms

## DEFINING WATER MODEL
water_model="$7" # "spce" # tip3p, spce, spc, etc. (original tip3p) "spce"

## DEFINING TOTAL EXPANSION DISTANCE
expand_dist="$8" # "4" # nm, how much to expand in z-direction

## DEFINING MAIN SIMULATION DIRECTORY
Main_Sim_Dir="${10}" # "190114-NoSolute_Pure_Sims"

## DEFINING OUTPUT SIM DIR
Output_Sim_Dir=${11} # "190114-Expanded_NoSolute_Pure_Sims"

## FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## DEFINING PRODUCTION TIME
mdp_prod_time="10000000" # 20 ns

## DEFINING NUMBER OF CORES
num_cores="28"

###############################
### FILE SYSTEM INFORMATION ###
###############################

## GRO AND XTC FILE
input_prefix="mixed_solv_prod"
input_gro_file="${input_prefix}.gro"
# input_xtc_file="${input_prefix}.xtc"
input_top_file="mixed_solv.top"

## DEFINING OUTPUT PREFIX
output_prefix="mixed_solv_prod"
output_gro_file="${output_prefix}.gro"
# output_xtc_file="${output_prefix}.xtc"

## DEFINING OUTPUT DIRECTORY
output_dir_name=$(solvent_effects_get_output_dir_name "${system_temp}" "${desired_box_length}" "${solute_residue_name}" "${solvent_one_wt_frac}" "${water_model}" "${solvent_two_name}")

## DEFINING MDRUN OUTPUT
mdrun_output_dir_name="mdRun_${output_dir_name}"

## DEFINING WORKING SPACE
WorkSpace="${PATH2SIM}"

## DEFINING FULL PATH TO PREVIOUSLY COMPLETED SIMULATION
Main_Sim_Path="${WorkSpace}/${Main_Sim_Dir}/${mdrun_output_dir_name}"

## CHECKING IF PATH EXISTS
stop_if_does_not_exist "${Main_Sim_Path}"

## DEFINING OUTPUT SIMULATION PATH
output_dir_new_name="Expand_${expand_dist}nm_${output_dir_name}"
Output_Sim_Path="${WorkSpace}/${Output_Sim_Dir}/${output_dir_new_name}"

## DEFINING SETUP FILES FOLDER
setup_files="setup_files"

## DEFINING MDP FILE
mdp_prod_file="production_LIQ_NVT.mdp"

## DEFINING SUBMISSION FILE
input_submit_file="submit_expand_box.sh"
output_submit_file="submit.sh"

############################################################
### MAIN SCRIPT 
############################################################

## CREATING DIRECTORY
create_dir "${Output_Sim_Path}" -f

## MOVING TO THE DIRECTORY
cd "${Output_Sim_Path}"

## COPYING OVER FILES
# FORCE FIELD
cp -r "${Main_Sim_Path}/${forcefield_dir}" "${Output_Sim_Path}"
# ITP FILES
cp -r "${Main_Sim_Path}/"{*.prm,*.itp} "${Output_Sim_Path}"
# TOP FILE
cp -r "${Main_Sim_Path}/${input_top_file}" "${Output_Sim_Path}"
# GRO FILE
cp -r "${Main_Sim_Path}/${input_gro_file}" "${Output_Sim_Path}/${output_gro_file}"

##########################
### EXPANDING GRO FILE ###
##########################

## READING GRO FILE DISTANCE
read -a box_size <<< $(gro_measure_box_size "${Output_Sim_Path}/${output_gro_file}")

## COMPUTING BOX SIZE INCREASE
box_size_z=$(awk -v box_1=${box_size[2]} -v box_2=${expand_dist} 'BEGIN{ printf "%.5f", box_1 + box_2 }')

## RESIZING THE BOX
resize_box_gro_name="${output_prefix}_resize.gro"
## USING EDITCONF (Silenced)
gmx editconf -f "${Output_Sim_Path}/${output_gro_file}" -o "${Output_Sim_Path}/${resize_box_gro_name}" -box ${box_size[0]} ${box_size[1]} ${box_size_z} -noc &>/dev/null

## REORGANIZING
mkdir -p "${Output_Sim_Path}/${setup_files}"
## MOVING FILES
mv "${Output_Sim_Path}/${output_gro_file}" "${Output_Sim_Path}/${setup_files}"
mv "${Output_Sim_Path}/${resize_box_gro_name}" "${Output_Sim_Path}/${output_gro_file}"
# NOTE: We are renamimg the resized gro file to the output gro file

## PRINTING
echo -e "\n---------------------------------"
echo "Reading GRO file: ${output_gro_file}"
echo "Z-Dimension is: ${box_size[2]} nm"
echo "Increasing box size by ${expand_dist} nm"
echo "Total z-dimension is: ${box_size_z} nm"
echo "Creating gro file: ${resize_box_gro_name}"
echo "Renaming ${resize_box_gro_name} to ${output_gro_file}"

###########################
### MDP FILE GENERATION ###
###########################

## COPYING MDP FILE
cp -r "${MDP_DIR}/${mdp_prod_file}" "${Output_Sim_Path}"

## CHANGING MDP OPTIONS
sed -i "s/NSTEPS/${mdp_prod_time}/g" ${mdp_prod_file}
sed -i "s/TEMPERATURE/${system_temp}/g" ${mdp_prod_file}

## PRINTING
echo -e "\n---------------------------------"
echo "Copying over MDP file: ${mdp_prod_file}"
echo "Number of steps: ${mdp_prod_time}"
echo "Temperature of system: ${system_temp} K"

#########################
### SUBMISSION SCRIPT ###
#########################

## COPYING SUBMISSION FILE
cp -r "${SUBMISSION_DIR}/${input_submit_file}" "${Output_Sim_Path}/${output_submit_file}"

## EDITING SUBMISSION FILE
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" ${output_submit_file}
sed -i "s/_JOBNAME_/${output_dir_new_name}/g" ${output_submit_file}
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" ${output_submit_file}

## CREATING TPR FILE (SILENCING)
gmx grompp -f "${mdp_prod_file}" -c "${output_gro_file}" -p ${input_top_file} -o ${output_prefix} -maxwarn 1 
# &>/dev/null

## PRINTING
echo -e "\n---------------------------------"
echo "Copying over submit file: ${input_submit_file} -> ${output_submit_file}"
echo "Creating TPR file: ${output_prefix}.tpr"

## CHECKING IF TPR FILE EXISTS
stop_if_does_not_exist "${output_prefix}.tpr"

## ADDING TO JOB LIST
echo "Adding job to: ${JOB_FILE}"
echo "${Output_Sim_Path}/${output_submit_file}" >> "${JOB_FILE}"

