#!/bin/bash

# One_System_Prep_continuation_free_energy.sh
# The purpose of this script is to take a previously solvated system and do free energy perturbation on one of the residues (HENCE "continuation")

# AUTHORS:
#   Alex K. Chew (alexkchew@gmail.com)

# HCl is from the following reference:
# Bonthuis, D. J., Mamatkulov, S. I. & Netz, R. R. Optimization of classical nonpolarizable force fields for OH- and H3O+. J. Chem. Phys. 144, (2016).
# We use the free energy details in this reference to ensure our simulations are accurate.

# INPUTS:
#   $1: name of the solute
#   $2: system temperature
#   $3: weight fraction of water
#   $4: name of the cosolvent
#   $5: initial box length
#   $6: water model
#   $7 is Simulation directory

# USAGE: bash One_System_Prep_continuation_free_energy.sh hydronium HYD 433.15 0.10 dmso dmso 6 spce 180601-fep_testing

##* -- Updates -- *##
# 2018-06-01: Edited script to manage free energy calculations for new solute systmes (no longer single continuation)

## RELOADING BIN FILE
source "bin/bash_rc.sh"

## PRINTING SCRIPT NAME
print_script_name

#################
### FUNCTIONS ###
#################

### FUNCTION TO CHECK INTEGRITY OF THE INPUT FILE
# INPUTS:
#   $1: Full path to the directory
#   $2: GRO file
#   $3: TOP file
#   $4: FORCE FIELD file
# USAGE: check_input_file PATH GRO_FILE TOP_FILE FORCE_FIELD
function check_input_file () {
    ## DEFINING INPUTS
    input_path="$1"
    input_gro="$2"
    input_top="$3"
    forcefield="$4"

    ## CHECKING IF FILES EXISTS
    check_gro="$(check_file_exist ${input_path}/${input_gro})"
    check_top="$(check_file_exist ${input_path}/${input_top})"
    check_forcefield="$(check_file_exist ${input_path}/${forcefield})"

    ## PRINTING SUMMARY
    echo "*** CHECKING INTEGRITY OF THE INPUT FILES ***"
    echo "Input directory path: ${input_path}"
    echo "${input_gro} exists: ${check_gro}"
    echo "${input_top} exists: ${check_top}"
    echo "${forcefield} exists: ${check_forcefield}"

    ## SEEING IF WE CAN CONTINUE
    if [ "${check_gro}" == "True" ] && [ "${check_top}" == "True" ] && [ "${check_forcefield}" == "True" ]; then
        echo "Input integrity intact: gro, top, forcefield is correctly available"
    else
        echo "ERROR! Check input files! You should have a gro, top, and forcefield file. stopping here!"
        echo "Input path: ${input_path}"
        exit
    fi
}

######################
### DEFINING INPUT ###
######################

## SOLUTE
solute_name="$1" # "acetone"
# solute_residue_name="$2" # "ACE"

## TEMPERATURE
system_temp="$2" # Temperature in K

## COMPOSITION
water_mass_frac="$3" # mass fraction of water available

## COSOLVENT
cosolvent_name="$4" # dmso dioxane
# cosolvent_residue_name="$5" # dmso DIO

## DEFINING SYSTEM PARAMETERS
box_length="$5" # nm box length
water_model="$6" # tip3p, spce, spc, etc. (original tip3p) "spce"

## DEFINING MAIN SIMULATION DIRECTORY
Main_Sim_Dir="$7"

## DEFINING NUMBER OF TIMES TO SPLIT THE WINDOWS
num_split="6" # Denotes the number of splits you want for N number of lambdas (i.e. N/num_split === number of independent nodes you want to run the simulation on)

### GETTING RESIDUE NAME
## DEFINING SOLUTE ITP
solute_itp_file="${PREP_SOLUTE_DIR}/${solute_name}/${solute_name}.itp"
## GETTING RESIDUE NAME
solute_residue_name=$(itp_get_resname ${solute_itp_file})

## GETTING RESIDUE NAME OF THE COSOLVENT
## DEFINING SOLVENT ITP FILE
solvent_itp_file="${PREP_SOLVENT_DIR}/${cosolvent_name}/${cosolvent_name}.itp"
## GETTING RESIDUE NAME
cosolvent_residue_name=$(itp_get_resname ${solvent_itp_file})

## DEFINING NUMBER OF SOLUTES
numSolute="1"

## DEFINIGN INSERTION EXPANDING
expand_insert_dist="0.2" # 0.3 nms, used for expansion, insertion, then allow NPT to take over "0.1"

## DEFINING LOGICALS
want_continuation="False"
    ## IF True: Continues from previous simulation
    ## IF False: Uses the simulation from prep_mixed_cosolvents

## FINDING MASS FRACTION OF WATER IN PERCENTAGE
water_mass_frac_perc=$(awk -v num=${water_mass_frac} 'BEGIN{ printf "%d",num*100}')

## DEFINING FORCE FIELD
forcefield_dir="charmm36-nov2016.ff"

## DEFINING INPUT DIRECTORY NAME
input_dir=$(solvent_effects_find_input_dir_name "${water_mass_frac_perc}" "${box_length}" "${solute_residue_name}" "${water_model}" "${cosolvent_name}")

## DEFINING SIMULATION DIRECTORY
Simulation_dir=mdRun_${system_temp}_${input_dir} # Directory

## DEFINING OUTPUT PREFIX
output_prefix="mixed_solv" # Prefix for gro and top files

## DEFINING INPUT GRO AND TOP
if [ "${want_continuation}" == "True" ]; then
    input_gro="${output_prefix}_prod.gro"
    input_top="${output_prefix}.top"
elif [ "${want_continuation}" == "False" ]; then
    output_prefix="mixed_solv" # Prefix for gro and top files
    input_gro="${output_prefix}_equil.gro"
    input_top="${output_prefix}.top"
fi

## DEFINING RUN PARAMETERS
num_cores="28" # Number of cores to run the simulation on

############################
### DEFINING DIRECTORIES ###
############################

### DIRECTORY STRUCTURE ###
# -------------------------- #
##### MAIN_SIMULATION_DIR
######## SOLUTE_NAME
########### INPUT_DIRECTORIES (HAS GRO, ITP, TOP files, etc.)
# -------------------------- #
if [ "${want_continuation}" == "True" ]; then

    ## DEFINING PATH TO INPUT DIRECTORY
    input_dir_path="${PATH2SIM}"

    ## DEFINING FULL PATH TO INPUT
    input_full_path="${input_dir_path}/${solute_name}/${Simulation_dir}"

elif [ "${want_continuation}" == "False" ]; then

    ## DEFINING INPUT PREP MIXED SOLVENTS
    input_dir_path="${PREP_MIXED_COSOLVENTS}/${water_model}"

    ## FINDING PREPARATION DIRECTORY
    prep_dir_name="$(solvent_effects_find_prep_solvent_folder "${water_mass_frac}" "${cosolvent_residue_name}")"

    ## DEFINING FULL PATH TO INPUT
    input_full_path="${input_dir_path}/${box_length}nm/${prep_dir_name}"


fi

## CHECKING IF THE FILE DIRECTORY EXISTS
input_exist=$(check_file_exist "${input_full_path}")

############################
### DEFINING INPUT FILES ###
############################

## MDP DIRECTORY
mdp_dir="${MDP_DIR}/free_energy" # Directory where you will look into for MDP files
if [ "${solute_name}" == "hydronium_LINCS_2" ] || [ "${solute_name}" == "hydronium_LINCS_3" ] || [ "${solute_name}" == "chlorine" ]; then ## CHANGING DIRECTORY IF HYDRONIUM IS INCLUDED <-- Change this later!
    ## These mdp file are the same as free energy, except using SHAKE instead of LINCS
    # mdp_dir="${MDP_DIR}/free_energy_hydronium_LINCS" # Directory where you will look into for MDP files
    mdp_dir="${MDP_DIR}/free_energy_hydronium_LINCS_3" # Directory where you will look into for MDP files
    # mdp_dir="${MDP_DIR}/free_energy_hydronium" # Directory where you will look into for MDP files
fi
## MDP FILES
mdp_em_1="${mdp_dir}/em_steep.mdp"
# mdp_em_2="${mdp_dir}/em_l-bfgs.mdp"
mdp_nvt="${mdp_dir}/nvt.mdp"
mdp_npt="${mdp_dir}/npt.mdp"
mdp_md="${mdp_dir}/md.mdp"

## DEFINING MDP TIMES
mdp_md_prod_steps="2500000" # 5 ns
# "5500000" # 11 ns
# "2500000" -- 5 ns

# CREATING MDP ARRAY
declare -a mdp_array=(${mdp_em_1} ${mdp_nvt} ${mdp_npt} ${mdp_md}) # ${mdp_em_2}

## SUBMISSION FILES
submit_per_lambda_name="submit_free_energy_per_lambda_single_reference_extended.sh"
submit_per_lambda="${SUBMISSION_DIR}/${submit_per_lambda_name}" # submit_free_energy_per_lambda.sh # For individual lambdas
full_submit_script="${SUBMISSION_DIR}/submit_free_energy_full_split_windows_extended.sh" # submit_free_energy_full.sh # Full script to run each lambda

#############################
### DEFINING OUTPUT FILES ###
#############################

## OUTPUT DIRECTORY
output_dir="FEP_${system_temp}_${input_dir}"
output_dir="${PATH2SIM}/${Main_Sim_Dir}/${output_dir}" # FEP ${solute_name}/

## OUTPUT FILES
output_gro="${output_prefix}.gro"
output_top="${input_top}"
output_submit="submit_lambda.sh" # name of output script
output_submit_full="extend_submit.sh" # Matching Monitor job

## JOB FILE
job_list_file="${PATH2SCRIPTS}/job_list.txt"

###################
### MAIN SCRIPT ###
###################

## RUNNING ONLY IF INPUT FILE EXISTS
if [ "${input_exist}" = "True" ]; then
    ## CHECKING THE INPUT PATH
    check_input_file "${input_full_path}" "${input_gro}" "${input_top}" "${forcefield_dir}"

    ## FINDING TOTAL NUMBER OF LAMBDA VALUES
    numLambda=$(grep "vdw_lambdas" ${mdp_md} | cut -d "=" -f 2 | wc -w) #
    numLambda_accountzero=$(( $numLambda-1 )) # Accounting that we start from 0

    ## CHECKING IF LAMBDA IS EMPTY
    if [ -z "${numLambda}" ]; then
        echo "Error!!! Incorrect number of lambda values. Check ${mdp_md}..."
        echo "Exiting now..."; sleep 3
        exit
    else
        echo "Total number of lambdas: ${numLambda}"
    fi

    ############################
    ### CREATING DIRECTORIES ###
    ############################

    ## CREATING OUTPUT DIRECTORY
    create_dir "${output_dir}" -f

    ## GOING INTO DIRECTORY
    cd "${output_dir}"

    ## DEFINING DIRECTORY STRUCTURE
    output_molecule="${output_dir}/1_Molecule" # Location for the molecule GRO, TOP, FORCEFILEDS
    output_mdp="${output_dir}/2_MDP" # Location of MDP files
    output_lambda="${output_dir}/3_lambda" # Location of lambda files (each step)
    output_submission="${output_dir}/4_Submission_Files" # Location of Submission Files
    output_analysis="${output_dir}/5_Analysis" # Location of Analysis (empty)

    ## CREATING DIRECTORIES
    mkdir -p ${output_molecule} ${output_mdp} ${output_ff} ${output_lambda} ${output_submission} ${output_analysis}

    #####################
    ### COPYING FILES ###
    #####################

    ## GRO, TOP, AND FORCEFIELD FILES
    echo "*** COPYING GRO, TOP AND FORCE FIELD FILES TO MOLECULE DIRECTORY ***"
    cp -r "${input_full_path}"/${input_gro} "${output_molecule}/${output_gro}" # Gro file
    cp -r "${input_full_path}"/${input_top} "${output_molecule}/${output_top}"
    cp -r "${input_full_path}"/${forcefield_dir} "${output_molecule}" # Topology file

    ## ITP / PRM FILES
    echo "*** COPYING ALL ITP/PRM FILES TO MOLECULE DIRECTORY ***"
    cp -r "${input_full_path}"/{*.prm,*.itp} "${output_molecule}"

    #####################
    ### ADDING SOLUTE ###
    #####################
    ## SEEING IF YOU WANT CONTINUATION
    if [ "${want_continuation}" == "False" ]; then
        ## GOING INTO DIRECTORY
        cd "${output_molecule}"

        ## ADDING SOLUTES
        solvent_effects_add_solute "${solute_name}" "${solute_residue_name}" "${numSolute}" "${output_gro}" "${output_gro}" "${expand_insert_dist}" "${output_top}"

        ## CHECKING IF YOU WANT A SOLUTE
        if [  "${solute_name}" != "None" ]; then
            # itp + parameters for solute
            cp -rv "${PREP_SOLUTE_DIR}/${solute_name}/"{*.itp,*.prm} "${output_molecule}"
        fi

        #############################################
        ### ADDING PARAMETERS AND ITP TO TOPOLOGY ###
        #############################################

        ## Finding line to add to
        lineNum2system=$(grep -nr "\[ system \]" $output_top | sed 's/\([0-9]*\).*/\1/') #
        ## Now, adding lines to topology
        line2add=$(( $lineNum2system-1 ))
        sed -i "${line2add}a \ " ${output_top} # Blank line
        ## CHECKING IF YOU WANT A SOLUTE
        if [  "${solute_name}" != "None" ]; then
            sed -i "${line2add}a #include \"$solute_name.itp\"" ${output_top} # itp file
            sed -i "4a #include \"${solute_name}.prm\"" ${output_top} # Parameter
            sed -i "${line2add}a ; Include topology for solute, ${solute_name}" ${output_top} # adding comment
        fi

        ## GOING BACK TO DIRECTORY
        cd "${output_dir}"

    fi

    ##########################
    ### CREATING MDP FILES ###
    ##########################

    ## DEFINING DIRECTORY STRUCTURE
    output_mdp_em1="${output_mdp}/1_EM"
    # output_mdp_em2="${output_mdp}/2_EM"
    output_mdp_nvt="${output_mdp}/2_NVT"
    output_mdp_npt="${output_mdp}/3_NPT"
    output_mdp_md="${output_mdp}/4_MD"
    ## DEFINING ARRAY
    declare -a output_mdp_array=(${output_mdp_em1} ${output_mdp_nvt} ${output_mdp_npt} ${output_mdp_md}) #  ${output_mdp_em2}
    ## CREATING DIRECTORIES
    mkdir -p ${output_mdp_em1} ${output_mdp_em2} ${output_mdp_nvt} ${output_mdp_npt} ${output_mdp_md}

    ## CREATING SUBMISSION FILES
    newSubmissionFileName="${output_submit%.*}.sh"

    ## COPYING OVER SUBMISSION FILE
    cp -r "${submit_per_lambda}" "${output_submission}/${submit_per_lambda_name}" # ${newSubmissionFileName}" ## DEPRECIATED

    ## CONVERTING SUBMISSION FILE FROM DOS TO UNIX
    dos2unix "${output_submission}/${submit_per_lambda_name}"

    ## LOOPING THROUGH EACH LAMBDA VALUE (CREATING MDP FILES)
    for currentLambda in $(seq 0 "$numLambda_accountzero"); do
        ## PRINTING
        echo "WORKING ON MDP FILES FOR LAMBDA: ${currentLambda}"
        ## LOOPING THROUGH EACH MDP FILE
        for currentMDP in $(seq 0 $(( ${#output_mdp_array[@]}-1 ))); do
            ## GETTING BASENAME OF SCRIPT
            currentMDPName=$(basename ${mdp_array[currentMDP]})
            currentMDPName_nomdp=${currentMDPName%.*}

            ## COPYING MDP FILE
            sed "s/MOLECULETYPE/${solute_residue_name}/g" ${mdp_array[currentMDP]} | sed "s/INITIAL_LAMBDA_STATE/${currentLambda}/g" | sed "s/TEMPERATURE/${system_temp}/g" > "${output_mdp_array[currentMDP]}/${currentMDPName_nomdp}_${currentLambda}.mdp"

            ## EDITING CONTINUATION
            if [ "${currentLambda}" == "0" ]; then
                if [ "${currentMDPName}" == "$(basename ${mdp_em_1})" ] || [ "${currentMDPName}" == "$(basename ${mdp_nvt})" ]; then
                    sed -i "s/CONTINUATION/no/g" "${output_mdp_array[currentMDP]}/${currentMDPName_nomdp}_${currentLambda}.mdp"
                    ## GENERATING VELOCITIES
                    if [ "${currentMDPName}" == "$(basename ${mdp_nvt})" ]; then
                        sed -i "s/GEN_VEL/yes/g" "${output_mdp_array[currentMDP]}/${currentMDPName_nomdp}_${currentLambda}.mdp"
                        fi
                fi

            else
                if [ "${currentMDPName}" == "$(basename ${mdp_em_1})" ] || [ "${currentMDPName}" == "$(basename ${mdp_nvt})" ]; then
                    sed -i "s/CONTINUATION/yes/g" "${output_mdp_array[currentMDP]}/${currentMDPName_nomdp}_${currentLambda}.mdp"
                    if [ "${currentMDPName}" == "$(basename ${mdp_nvt})" ]; then
                        sed -i "s/GEN_VEL/no/g" "${output_mdp_array[currentMDP]}/${currentMDPName_nomdp}_${currentLambda}.mdp"
                        fi
                fi
            fi

            ## EDITING PRODUCTION CODE
            if [ "${currentMDPName}" == "$(basename ${mdp_md})" ]; then
                    sed -i "s/_NSTEPS_/${mdp_md_prod_steps}/g" "${output_mdp_array[currentMDP]}/${currentMDPName_nomdp}_${currentLambda}.mdp"
            fi



        done

    done

    #################################
    ### COPYING SUBMISSION SCRIPT ###
    #################################

    # DIVIDING UP WINDOWS INTO MULTIPLE JOBS
    ## FINDING DIVIDED VALUE WHILE ROUNDING DOWN
    divided_value="$((${numLambda_accountzero} / ${num_split}))"

    ## FINDING DIRECTORY NAME
    output_dir_name=$(basename ${output_dir})

    ## LOOPING THROUGH EACH SPLIT
    for each_split_index in $(seq 0 $((${num_split}-1)) ); do
        ## DEFINING EACH LAMBDA
        ### INITIAL SPLIT
        if [ "${each_split_index}" == 0 ]; then
            initial_lambda="$((${each_split_index}*${divided_value}))"
            final_lambda="$(( (${each_split_index}+1)*${divided_value}))"
        ### FINAL SPLIT
        elif [ "${each_split_index}" == "$((${num_split}-1))" ]; then
            initial_lambda="$((${final_lambda}+1))"
            final_lambda="${numLambda_accountzero}"
        ### MIDDLE SPLITS
        else
            initial_lambda="$((${final_lambda}+1))"
            final_lambda="$((${initial_lambda}+${divided_value}))"
        fi

        ## DEFINING SUBMISISON SCRIPT
        each_split_submission_script="${output_dir}/${output_submit_full%.sh}_${each_split_index}.sh"

        ## EDITING SUBMISSION SCRIPT
        sed "s/FIRSTALPHA/${initial_lambda}/g" ${full_submit_script} > "${each_split_submission_script}"
        sed -i "s/LASTALPHA/${final_lambda}/g" "${each_split_submission_script}"
        sed -i "s/DIRECTORYNAME/${output_dir_name}_${each_split_index}/g" "${each_split_submission_script}"
        sed -i "s/NUMBER_OF_CORES/${num_cores}/g" "${each_split_submission_script}"

        ## ADDING TO JOB LIST
        # if [ ${each_split_index} == 0 ]; then
        echo "${each_split_submission_script}" >> "${job_list_file}"
        # fi

        done

    ## COPYING DIRECTORY TO JOB LIST
    # echo "${output_dir}" >> "${job_list_file}"

## INPUT FILE DOES NOT EXIST
else
    echo "ERROR! Input file does not exist!"
    echo "Please check input file path: ${input_full_path}"
    echo "Script was not run!"
    echo "Pausing for 3 seconds..."
    sleep 3
    exit
fi

### PRINTING SUMMARY
echo -e "\n***----- SUMMARY -----***"
echo "*** Prepared free energy perturbation simulations ***"
echo "INPUT DIRECTORY: ${input_dir}"
echo "OUTPUT DIRECTORY PATH: ${output_dir}"
echo "Solute: ${solute_name} (${solute_residue_name})"
echo "Cosolvent: ${cosolvent_name} (${cosolvent_residue_name})"
echo "System temperature: ${system_temp}"
echo "Mass fraction of water: ${water_mass_frac}"
echo "Initial box length: ${box_length}"
echo "Water model: ${water_model}"
echo "Force field directory: ${forcefield_dir}"
echo -e "\n----- free energy parameters -----"
echo "Number of lambdas: ${numLambda_accountzero}"
