#!/bin/bash

# post_extraction.sh
# This bash script will run post extraction protocols. This is useful if you want to run additional gmx commands without invoking truncation. 
# USAGE: bash post_extraction.sh
# Written by Alex K. Chew (alexkchew@gmail.com, 03/06/2018)

## ** UPDATES ** ##
# 180307 - Updated script to run correctly on server
# 180312 - Updated script to check index file for residue

### LOADING BASH RC ###
source "bin/bash_rc.sh"

### FUNCTION TO USE TRJCONV AND FIX ROTATION + TRANSLATION
# INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: residue name
#   $4: system name
#   $5: output xtc file
function rot_trans_ {
    ## DEFINING  VARIABLES
    input_tpr_file="$1"
    input_xtc_file="$2"
    res_name="$3"
    system_name="$4"
    output_xtc_file="$5"
    ## DEFINING INDEX FILE
    index_file="index.ndx"

### CREATING INDEX FILE
gmx make_ndx -f "${input_tpr_file}" -o "${index_file}" << INPUT
q
INPUT

### CHECKING IF THE RESIDUE IS WITHIN (Checks if string is empty)
if [ -z "$(grep ${res_name} ${index_file})" ]; then
gmx make_ndx -f "${input_tpr_file}" -o ${index_file} << INPUT
r ${res_name}
q
INPUT
fi

### EXTRACTION AND PLACES CENTER OF MASSES WITHIN BOX
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file}" -o "${input_xtc_file%.xtc}_center" -pbc mol -center -n ${index_file} << INPUT
${res_name}
${system_name}
INPUT
# CENTERING ON RESIDUE, OUTPUTTING SYSTEM

### RESTRAINING ROTATION AND TRANSLATIONAL DEGREES OF FREEDOM
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file%.xtc}_center" -o "${output_xtc_file}" -fit rot+trans -center -n ${index_file} << INPUT
${res_name}
${res_name}
${system_name}
INPUT
# CENTERING AND ROT+TRANS ON RESIDUE, OUTPUTTING SYSTEM
}


### FUNCTION TO USE TRJCONV TO SHORTEN THE TIME
# INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: system name
#   $4: output xtc file
#   $5: trunction time in ps (lower bound) -- first frame to read
#   $6: trunction_time_last_Frame in ps (upper bound) -- last frame to read
function trunc_ {
    ## DEFINING VARIABLES
    input_tpr_file="$1"
    input_xtc_file="$2"
    system_name="$3"
    output_xtc_file="$4"
    truncation_time_first_frame="${5:-0}"
    trunction_time_last_Frame=${6:-0}
    
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file}" -o "${output_xtc_file}" -b "${truncation_time_first_frame}" -e "${trunction_time_last_Frame}" -pbc mol << INPUT
${system_name}
INPUT
}

### FUNCTION TO RUN GMX DIPOLE FUNCTION
# The purpose of this function is to calculate the dipoles of the system. In turn, you will get the dielectric constant.
# INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: output file name
#   $4: atom selection for gmx dipoles
# OUTPUTS:
function calc_gmx_dipoles_ {
    ## DEFINING INPUT VARIABLES
    input_tpr_file_="$1"
    input_xtc_file_="$2"
    output_file_="$3"
    atom_selection_="$4"
    
    ## PRINTING
    echo -e "\n----------------------------------"
    echo "Working on gmx dipole at: $(pwd)"
    echo "See ${output_file_} if you want more information!"
    
    ## RUNNING GMX DIPOLE
gmx dipoles -f "${input_xtc_file_}" -s "${input_tpr_file_}" > "${output_file_}" 2>&1 << INPUTS 
${atom_selection_}
INPUTS
#  
}

### FUNCTION TO RUN THE DIPOLE FUNCTION MULTIPLE TIMES 
# The purpose of the function is to calculate dipoles while varying time to get the sampling time
## INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: prefix to the names within folder
#   $4: atom selection for gmx dipoles
#   $5: when to begin reading for gmx dipoles in picoseconds
#   $6: time intervals to consider for sampling time (ps)
#   $7: last frame to read gmx dipoles
#   $8: folder to store all sampling time in
## OUTPUTS:
#   folder with gmx dipoles calculated at various sampling times 
function calc_gmx_dipoles_sampling_time_ {
    ## DEFINING INPUT VARIABLES
    input_tpr_file_="$1"
    input_xtc_file_="$2"
    dipole_sampling_prefix_="$3"
    atom_selection_="$4"
    dipole_begin_frame_ps_="$5"
    dipole_interval_ps_="$6"
    dipole_end_frame_ps_="$7"
    dipole_sampling_folder_="$8"
    
    ## FINDING TOTAL NUMBER OF INTERVALS
    total_intervals_=$(awk -v begin_frame=${dipole_begin_frame_ps_} -v end_frame=${dipole_end_frame_ps_} -v interval=${dipole_interval_ps_} 'BEGIN{ printf "%d",(end_frame - begin_frame)/interval }') 
    #  
    # echo "Total intervals: ${total_intervals_}"
    ## CREATING FOLDER FOR SAMPLING
    create_dir "${dipole_sampling_folder_}" -f
    
    ## LOOPING THROUGH EACH INTERVAL
    for each_interval_index in $(seq 1 ${total_intervals_}); do
        ## FINDING BEGIN TIME
        current_begin_time=$(awk -v end_frame="${dipole_end_frame_ps_}" -v current_interval_index="${each_interval_index}" -v interval="${dipole_interval_ps_}" 'BEGIN{ printf "%d",end_frame-current_interval_index*interval}') 
        ## DEFINING CURRENT NAME
        current_file_name="${dipole_sampling_prefix_}_${each_interval_index}_${current_begin_time}_${dipole_end_frame_ps_}.info"
        ## PRINTING
        echo -e "\n----------------------------------"
        echo "Working on gmx dipole at: $(pwd), interval index ${each_interval_index} of ${total_intervals_}"
        echo "See ${current_file_name} if you want more information!"
        ## RUNNING GMX DIPOLE
gmx dipoles -f "${input_xtc_file_}" -s "${input_tpr_file_}" -b "${current_begin_time}" -e "${dipole_end_frame_ps_}" > "${dipole_sampling_folder_}/${current_file_name}" 2>&1 << INPUTS 
${atom_selection_}
INPUTS
    
    done
    
}


### PRINTING SCRIPT NAME
print_script_name

## DEFINING SCRIPT NAMES
compute_solvent_energies_script="compute_solvent_energies.sh"
compute_expand_box_values_script="compute_expand_box_values.sh"

#################################
### DEFINING INPUT PARAMETERS ###
#################################

### DEFINING MAIN ANALYSIS DIRECTORY (ASSUMING IT IS IN ANALYSIS FOLDER)
# Main_Extract_Dir="190116-Expanded_NoSolute_DIO_DMSO_Sims"
# Main_Extract_Dir="190314-PDO_Most_likely_25_DIO_DMSO"




## ALL 7 MOLECULES
Main_Extract_Dir="170814-7Molecules_200ns_Full"
declare -a Categories=("ETBE" "tBuOH" "PDO" "LGA" "FRU" "CEL" "XYL") # "PDO"  "PRO"  "ACE" "NoSolute"

###### DEFINING PATH OF SIM DIRECTORY
path_to_sim_directories=${PATH2ANALYSIS}
# path_to_sim_directories="/home/akchew/scratch/storage/2019_3d_cnn/MD_simulations/DMSO_MeCN_ACE_sims"

## DMSO
Main_Extract_Dir="SOLVENTNET_DMSO"
# "190305-TBA_FRU_MIXED_SOLV_DMSO"
declare -a Categories=("tBuOH" "FRU" "PDO") # "PDO"  "PRO"  "ACE" "NoSolute"

## MECN
Main_Extract_Dir="SOLVENTNET_MECN" # "190614-Run_MeCN"
declare -a Categories=("tBuOH" "FRU" "PDO") # "PDO"  "PRO"  "ACE" "NoSolute"

## ACE
Main_Extract_Dir="SOLVENTNET_ACE" # "190612-FRU_HMF_Run"
declare -a Categories=("FRU" "GLU") # "PDO"  "PRO"  "ACE" "NoSolute"

## PURE WATER
Main_Extract_Dir="2020_For_Alex_S_Pure_Water_Ref" # "190612-FRU_HMF_Run"
declare -a Categories=("NoSolute") # "PDO"  "PRO"  "ACE" "NoSolute"


### GLY BTL
# Main_Extract_Dir="190922-PDO_most_likely_gauche"
# declare -a Categories=("PDO") # "PDO"  "PRO"  "ACE" "NoSolute"

#### FRU AND HMF IN DMSO
# Main_Extract_Dir="190925-2ns_mixed_solvent_with_FRU_HMF"
# declare -a Categories=("FRU" "HMF")

## THD CHD
# Main_Extract_Dir="190621-THD_CHD_Most_likely"
# declare -a Categories=("CHD" "THD") # "PDO"  "PRO"  "ACE" "NoSolute"

# "ETBE" "tBuOH" "PDO" "LGA" "FRU" "CEL" "XYL"
# "ETBE" "tBuOH" "PDO" "LGA" "FRU" "CEL" "XYL" 
# "tBuOH" "FRU" "PDO"
# declare -a Categories=("PDO") # "PDO"  "PRO"  "ACE" "NoSolute"
# Directories within Main_Extract_Dir "PDO"   "PRO" "ACE"  "PRO"  "ACE"

## DECLARING SOLUTE NAMES

#"mdRun_433.15_6_nm_ACE_10_WtPercWater_spce_dioxane" "mdRun_433.15_6_nm_ACE_10_WtPercWater_spce_dmso" "mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane" "mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dmso" "mdRun_433.15_6_nm_PRO_10_WtPercWater_spce_dioxane" "mdRun_433.15_6_nm_PRO_10_WtPercWater_spce_dmso"

### DEFINING INPUT FILES
input_gro_file="mixed_solv_prod.gro" # Structure file
input_tpr_file="mixed_solv_prod.tpr" # Job file
input_xtc_file="mixed_solv_prod.xtc" # Trajectory file
# input_xtc_file="mixed_solv_prod_10_ns_whole.xtc" # Trajectory file
# input_xtc_file="mixed_solv_prod_10_ns_whole.xtc" # Trajectory file
# input_xtc_file="mixed_solv_prod_last_1_ns.xtc" # Trajectory file
# input_xtc_file="mixed_solv_prod_10ns_to_100ns_pbcmol.xtc" # Trajectory file

### DEFINING OUTPUT FILES
# output_xtc_file="mixed_solv_prod_10ns_to_100ns_pbcmol_rot_trans_center.xtc"
output_xtc_file="mixed_solv_prod_0ns_to_200ns_pbcmol.xtc"
# output_xtc_file="mixed_solv_prod_10ns_to_200ns_pbcmol.xtc"
# output_xtc_file="mixed_solv_prod_10ns_to_100ns_pbcmol_rot_trans_center.xtc"
# output_xtc_file="mixed_solv_prod_10ns_to_100ns_pbcmol.xtc"
# output_xtc_file="mixed_solv_prod_10_ns_whole.xtc"
# output_xtc_file="mixed_solv_prod_last_1_ns_center_rot_trans_center.xtc"
# output_xtc_file="mixed_solv_prod_last_1_ns.xtc"
#   "mixed_solv_prod_10ns_to_200ns_pbcmol.xtc" --- 10 ns to 200 ns

### DEFINING JOB INFORMATION
job_type="CENTERED_TRUNCATED_TRAJ"
    # "CENTERED_TRUNCATED_TRAJ" # CENTERED_TRUNCATED_TRAJ <-- used to center and truncate trajecotries
    # ROT_TRANS: This is designed for ROT_TRANS fixing for spatial mapping purposes
    # TRUNC: This is designed to truncate the trajectories
    # HBOND: Runs hydrogen bonding calculations
    # HBOND_EXTRACT: Extract hydrogen bonding calculations
    # DIPOLE: This is designed to calculate dielectric constants using gmx dipole
    # DIPOLE_SAMPLING: This is designed to calculate dielectric constants and find the sampling time required to converge to the correct dielectric constant
    # EXPAND_BOX_VALUES: This is designed to compute electrostatic potentials and densities for an expanded box of solvent molecules
    # CENTERED_TRUNCATED_TRAJ: This is designed to center the last 
    
## DEFINING JOB SCRIPTS
job_hbond="${PATH2ANALYSIS_SCRIPTS}/gromacs_hbond.sh"
job_hbond_extract="${PATH2ANALYSIS_SCRIPTS}/gromacs_hbond_extract.sh"
    
#### ROT_TRANS DEFINITIONS
system_selection="System"
# declare -a residue_name=("PDO" "PDO")
residue_name="${Categories[@]}" # Residue names used for centering purposes

#### TRUNCTION DEFINITIONS
truncation_time_first_frame="10000.000" # ps truncation
trunction_time_last_frame="200000.000" # ps ending

#### DIPOLE DEFINITIONS
dipole_output_file="gmx_dipole_summary.txt"
dipole_atom_selection="System"

#### DIPOLE SAMPLING DEFINITIONS
dipole_begin_frame_ps="10000" # ps
dipole_interval_ps="5000" # ps interval
dipole_end_frame_ps="50000" # ps last frame to look at
dipole_sampling_folder="gmx_dipoles_sampling_time" # folder to output gmx dipoles
dipole_sampling_prefix="gmx_dipoles_sampling"

#### DEFININITIONS FOR CENTERED_TRUNCATED_TRAJ
#centered_trunc_traj_output_xtc="mixed_solv_prod_last_10_ns_centered.xtc"
#centered_trunc_traj_beginning_time="190000" # ps
#centered_trunc_traj_end_time="200000" # ps

## NEW CENTERED TRAJ WITH 190 NS WORTH OF DATA
## FOR 20 NS TRAINING (SOLVENTNET)

## FOR 4 NS
centered_trunc_traj_output_xtc="mixed_solv_prod_first_4_ns_centered.xtc"
## FOR 2 NS
centered_trunc_traj_output_xtc="mixed_solv_prod_first_2_ns_centered.xtc"
centered_trunc_traj_output_xtc="mixed_solv_prod_0ns_to_200ns_pbcmol.xtc"
centered_trunc_traj_output_xtc="mixed_solv_prod_0ns_to_200ns_pbcmol.xtc"

centered_trunc_traj_output_xtc="mixed_solv_prod_first_20_ns_centered_with_10ns.xtc"

# centered_trunc_traj_output_xtc="mixed_solv_prod_first_100_ns_centered_with_10ns.xtc"
# centered_trunc_traj_beginning_time="10000" # "10000" # ps # 100000
centered_trunc_traj_beginning_time="0" # "10000" # ps # 100000
# centered_trunc_traj_beginning_time="150000" # "10000" # ps
# centered_trunc_traj_end_time="42000" # "50000" # ps 200000
# centered_trunc_traj_end_time="110000" # "50000" # ps 200000
# centered_trunc_traj_end_time="4000" # "32000"
# centered_trunc_traj_end_time="200000"
centered_trunc_traj_end_time="20000"
# "2000" # "32000"
# centered_trunc_traj_end_time="20000" # "32000"

######################
### DEFINING PATHS ###
######################

### PATH TO MAIN EXTRACTION DIRECTORYs
path_main_extract_dir="${path_to_sim_directories}/${Main_Extract_Dir}"

###################
### MAIN SCRIPT ###
###################

## ITERATION COUNTER
ITER=0

## LOOPING THROUGH EACH CATEGORY
for each_category in "${Categories[@]}"; do
    ## DEFINING PATH TO CATEGORY
    path_to_categories="${path_main_extract_dir}/${each_category}"
    
    ## DEFINING EACH RESIDUE
    if [[ "${each_category}" == "NoSolute" ]]; then
        current_residue="System"
    else
        current_residue=${each_category} # "${residue_name[${ITER}]}"
    fi
    echo "Working on current residue: ${current_residue}"
    
    ## LOOPING THROUGH EACH DIRECTORY WITHIN THE CATEGORY
    for each_file in $(ls ${path_to_categories}); do
        ## DEFINING FULL PATH
        path_to_each_file="${path_to_categories}/${each_file}"
        
        ## GOING INTO DIRECTORY
        cd "${path_to_each_file}"
        
        ########################
        ### GROMACS COMMANDS ###
        ########################
        
        ### TRUNC
        if [ "${job_type}" == "TRUNC" ]; then
            ### TRUNCATING THE SYSTEM
            trunc_ "${input_tpr_file}" "${input_xtc_file}" "${system_selection}" "${output_xtc_file}" "${truncation_time_first_frame}" "${trunction_time_last_frame}"
        
        ### ROT_TRANS
        elif [ "${job_type}" == "ROT_TRANS" ]; then
            ### RESTRAINING ROTATION AND TRANSLATIONAL DEGREES OF FREEDOM
            rot_trans_ "${input_tpr_file}" "${input_xtc_file}" "${current_residue}" "${system_selection}" "${output_xtc_file}"
        #### HBONDS
        elif [ "${job_type}" == "HBOND" ]; then
            ### RUNNING SCRIPT
            bash "${job_hbond}" "${path_to_each_file}" "${each_category}"
        #### HBONDS EXTRACT
        elif [ "${job_type}" == "HBOND_EXTRACT" ]; then
            ### RUNNING SCRIPT
            bash "${job_hbond_extract}" "${path_to_each_file}" "${each_category}"
        
        ### DIPOLE
        elif [ "${job_type}" == "DIPOLE" ]; then
            #### RUNNING DIPOLE SCRIPT
            calc_gmx_dipoles_ "${input_tpr_file}" "${input_xtc_file}" "${dipole_output_file}" "${dipole_atom_selection}"        
        ### DIPOLE SAMPLING TIME
        elif [ "${job_type}" == "DIPOLE_SAMPLING" ]; then
            ## RUNNING SAMPLING SCRIPT
            echo "calc_gmx_dipoles_sampling_time_ "${input_tpr_file}" "${input_xtc_file}" "${dipole_sampling_prefix}" "${dipole_atom_selection}" "${dipole_begin_frame_ps}" "${dipole_interval_ps}" "${dipole_end_frame_ps}" "${dipole_sampling_folder}""
            calc_gmx_dipoles_sampling_time_ "${input_tpr_file}" "${input_xtc_file}" "${dipole_sampling_prefix}" "${dipole_atom_selection}" "${dipole_begin_frame_ps}" "${dipole_interval_ps}" "${dipole_end_frame_ps}" "${dipole_sampling_folder}" 
        elif [ "${job_type}" == "EXPAND_BOX_VALUES" ]; then
            ## GOING TO POST SCRIPT DIRECTORY
            cd "${PATH2POSTSIMSCRIPTS}"
            bash "${compute_expand_box_values_script}" "${path_to_each_file}"
        
        ## TRUNCATED CENTER JOBS    
        elif [ "${job_type}" == "CENTERED_TRUNCATED_TRAJ" ]; then
            truncate_and_center "${input_tpr_file}" "${input_xtc_file}" "${current_residue}" "${system_selection}" "${centered_trunc_traj_output_xtc}" "${centered_trunc_traj_beginning_time}" "${centered_trunc_traj_end_time}"
        fi
        
        # sleep 5
        
    
    done
    
    ## INCREASING IN ITERATION
    ITER="$((ITER+1))"

done

###############
### SUMMARY ###
###############

echo "----------SUMMARY----------"
echo "Main extraction directory: ${Main_Extract_Dir}"
echo "Categories: ${Categories[@]}"
echo "Residue names: ${residue_name[@]}"
echo "Job type: ${job_type}"


