#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 1000:00:00
#SBATCH -J ANALYSIS_HBOND
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
# python analysisTools_samplingtime_server.py
#python analysisTools_samplingtime_server.py 2&>1 | tee logfile.log

# stdbuf -oL python analysisTools_samplingtime.py > log
#stdbuf -oL python analysisTools_equiltime.py > log
#stdbuf -oL python analysisTools_radial_dist.py > log

# Running Bash Script
#bash analysisTools_xtcTruncate.sh True True True

# Running analysis tool
bash analysisTools_HBond.sh