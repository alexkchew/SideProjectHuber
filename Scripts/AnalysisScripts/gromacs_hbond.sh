#!/bin/bash

# gromacs_hbond.sh
# This bash script will look into hydrogen bonding between reactant and product using gmx hbond
# Written by Alex Chew (04/16/2018)

## USAGE EXAMPLE : bash gromacs_hbond.sh /home/akchew/scratch/SideProjectHuber/Analysis/180413-PDO_PRO_COMBINED/PRO/mdRun_433.15_6_nm_PRO_10_WtPercWater_spce_GVL_L PRO

## VARIABLES:
# $1: Full path to the analysis directory
# $2: Residue name

# sed '/^#/d' hbnum_total_PDO_DIO.xvg  | sed '/^@/d' | awk '{print $2}' |  less

# Printing
echo "----- gromacs_hbond.sh ------"

## RELOADING PROFILE
source "/home/akchew/scratch/SideProjectHuber/Scripts/bin/bash_rc.sh"

#################
### FUNCTIONS ###
#################

### FUNCTION TO SEE IF SOLVENT IS WITHIN THE INDEX
# INPUTS:
#   $1: residue name
#   $2: index file
function check_index_ () {
    l_res_name="$1"
    l_index_file="$2"
    if [ -z "$(grep ${l_res_name} ${l_index_file})" ]; then 
        echo "False"
    else
        echo "True"
    fi
}

## SAVING DELIMINATOR
OLDIFS="$IFS"

##############
### INPUTS ###
##############

## DIRECTORIES
#analysis_dir="180416-PDO_ACE_PRO_DIO_DMSO_Other_Mass_Frac"
#category_dir="PDO"
#specific_dir="mdRun_433.15_6_nm_PDO_25_WtPercWater_spce_dioxane"

# analysis_dir="$1"
# specific_dir="$3"

## DEFINING PATHS
path_analysis_dir="$1"
category_dir="$2"

## DEIFNING RESIDUE NAME
residue_name=${category_dir}

## FILES
gro_file="mixed_solv_prod.gro"
tpr_file="mixed_solv_prod.tpr"
xtc_file="mixed_solv_prod_10_ns_whole.xtc"
index_file="index.ndx"

## DEFINING SOLVENTS
declare -a solvent_names=('SOL' 'GVLL' 'dmso' 'DIO' 'THF' 'URE' 'DMF' 'DMA' 'NMP' 'BUT' 'SUL')

## LOGICALS
## TOTAL NUMBER OF HYDROGEN BONDS
want_hbnum_total="True" # True if you want total number of hydrogen bonds between two groups
want_hbnum_each_OH_group="True" # True if you want each individual OH group

###############
### OUTPUTS ###
###############

## DIRECTORY NAME FOR THE OUTPUTS
output_dir="output_hbond"

## NUMBER OF HYDROGEN BONDS (TOTAL)
hbond_num_file_prefix="hbnum_total"

## NUMBER OF HYDROGEN BONDS PER HYDROXYL
hbond_each_hydroxyl_prefix="hbnum_hydroyxyl"

###################
### MAIN SCRIPT ###
###################

## GOING TO DIRECTORY
cd "${path_analysis_dir}"

## CREATING DIRECTORY FOR OUTPUT
mkdir -p "${path_analysis_dir}/${output_dir}"

## LOOPING THROUGH THE SOLVENT LIST
for each_solvent in "${solvent_names[@]}"; do

    ## SEEING IF THERE IS AN INDEX FILE
    if [ ! -e ${index_file} ]; then
gmx make_ndx -f "${tpr_file}" -o ${index_file} << INPUT
q
INPUT
    fi
    ## FIRST CHECK IF THE SOLVENT IS WITHIN THE INDEX
    solvent_isin=$(check_index_ "${each_solvent}" "${index_file}")
    
    
    ## IF SOLVENT WITHIN INDEX, RUN GMX HBOND
    if [ "${solvent_isin}" == "True" ]; then
        echo "*** Running GMX HBond between ${residue_name} and ${each_solvent} ***"; sleep 2
        ########################
        ### RUNNING EACH JOB ###
        ########################

        #### TOTAL NUMBER OF HYDROGEN BONDS
        if [ "${want_hbnum_total}" == "True" ]; then
### CHECKING IF THE SOLUTE IS WITHIN
if [ -z "$(grep ${residue_name} ${index_file})" ]; then # Checks if string is empty
echo "RESIDUE NAME NOT FOUND -- ADDING TO INDEX FILE"
gmx make_ndx -f "${tpr_file}" -n ${index_file} -o ${index_file} << INPUT
r ${residue_name}
q
INPUT
fi      
        ## TOTAL NUMBER OF HYDROGEN BONDS
gmx hbond -f "${xtc_file}" -s "${tpr_file}" -num  "${output_dir}/${hbond_num_file_prefix}_${residue_name}_${each_solvent}.xvg" -n ${index_file} << INPUTS
${residue_name}
${each_solvent}
INPUTS
        fi
        
    fi
    
done








