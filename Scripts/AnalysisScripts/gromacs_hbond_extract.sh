#!/bin/bash

# gromacs_hbond_extract.sh
# This bash script will use the output of gromacs_hbond.sh and calculate the average hydrogen bond / std

# CREATED ON: 04/17/2018
# WRITTEN BY: Alex K. Chew (alexkchew@gmail.com)

# Printing
echo "----- gromacs_hbond_extract.sh ------"

## RELOADING PROFILE
source "/home/akchew/scratch/SideProjectHuber/Scripts/bin/bash_rc.sh"

#######################
### INPUT VARIABLES ###
#######################

## PYTHON SCRIPT TO EXTRACT HYDROGEN BONDING
python_script="${PATH2ANALYSIS_SCRIPTS}/gromacs_hbond_extract.py"

## DEFINING DIRECTORY STRUCTURE
path_analysis_dir="$1"
category_dir="$2"

## EXTRACTING PATH TO ANALYSIS DIRECTORY
path_parent_analysis_dir=$(dirname ${path_analysis_dir})

## EXTRACTING NAME OF THE ANALYSIS DIRECTORY
analysis_dir_name=$(basename ${path_analysis_dir})

## DEFINING INPUT NAMES
hbond_num_file_prefix="hbnum_total"
hbond_each_hydroxyl_prefix="hbnum_hydroyxyl"

###############
### OUTPUTS ###
###############

## DIRECTORY NAME FOR THE OUTPUTS
output_dir="output_hbond"

## OUTPUT CSV NAME
output_csv_name="output_gromacs_hbond.csv"

## DEFINING FULL PATH TO OUTPUT CSV
output_csv_path="${PATH2ANALYSIS_SCRIPTS}/${output_csv_name}"

###################
### MAIN SCRIPT ###
###################

## FINDING XVG FILE
xvg_file_paths=$(ls "${path_analysis_dir}/${output_dir}/${hbond_num_file_prefix}"*)

## LOOPING THROUGH THE SOLVENT LIST
for each_xvg_output in ${xvg_file_paths}; do
    xvg_file_name=$(basename "${each_xvg_output}")
    echo "EXPORTING FROM: ${xvg_file_name}"
    ## RUNNING PYTHON SCRIPT
    python3 ${python_script} --path "${path_parent_analysis_dir}" --dir "${analysis_dir_name}" --xvg "${xvg_file_name}"  --out "${output_csv_path}"
done


