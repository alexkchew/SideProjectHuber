#!/bin/bash

# analysisTools_HBond.sh
# This bash script will take the output from analysisTools_Extraction.sh and develop index.ndx & use gmx hbond to develop hydrogen bonds.
# Written by Alex Chew (03/18/2017)
##* -- Updates -- *##
# 2017-04-05: Fixed up a lot of the residue naming issues -- adding extra co-solvents
# 2017-07-16: Fixing up the hydrogen bond analysis to include Chandler kinetics
# 2017-07-17: Automating script to output Chandler kinetics
# 2017-07-25: Adding the hydrogen bonding summary -- number of h-bonds, hbonds vs time
# 2017-08-07: Adding functionality to get OH hydrogen bonding lifetimes
# 2017-08-09: Partitioning functionality to incorporate generalized OH
# 2017-08-26: Creating partitioning of hydrogen bonding for average and standard deviation

### NOTE! NEED EXTENDED TPR -- RUN One_System_post_sim.sh before this script!

# -- USER-SPECIFIC PARAMETERS -- #

# Printing
echo "----- analysisTools_HBond.sh ------"

# Saving deliminator #
OLDIFS="$IFS"

# Creating logicals
skipGMX="False" # True if you want to skip the gmx hbond (may produce errors if you haven't run this script already)
wantSummary="True" # True if you want to extract all the data 
wantIndex="False" # True if you want index groups
wantGeneralOH="True" # True if you want the analysis of the entire OH as well! -- Does not overwrite "wantOHGroups"
wantOHGroups="False" # True if you want analysis of each individual hydroxyl group

## SPLIT HBONDING ##
wantSplitOH="True" # True if you want splitting hydrogen bonds
split_time="2000" # "2000" # "4000" # "6000" # "2000" "4500 # ps of splitting
# total_sim_time="5000" # "13000" # "9000" # "5000" # ps of ending
total_sim_time="5000" # "13000" # "9000" # "5000" "10000" # ps of ending

## Summary characteristics ##
wantHBondvsTime="False" # True if you want hydrogen bonds over time for each file

# Loading GROMACS #
load_gromacs_5_0_1_thread #load_gromacs_2016_3 # Loading gromacs 2016.3 for hydrogen bonding analysis

## INPUT PARAMETERS ##

# "170717-TBUOH_XYL_LGA_prod_extend" 
# "170807-7Molecules_prod_extend" 
specific_analysis_dir="190309-PDO_DMSO_Sims_prod_extend" # Directory where you want specifically the analysis to occur
# "170826-7Molecules_200ns_Combed_WithITP_3Solvents_prod_extend"

# Defining SUMMARY File Name
summary_file_name="hbond_summary_5_0_1.csv" # For generalized OH groups
summary_file_each_OH="hbond_summary_5_0_1_eachOH.csv"
hbond_vs_time_file_name="hbond_vs_time_summary_5_0_1.csv"
summary_file_split="hbond_summary_split.csv" # For splitting hydrogen bond lifetimes


# Extraction directories within your folder
# "CEL" "ETBE" "PDO" "FRU" "tBuOH" "LGA" "XYL"
declare -a extractDirList=("PDO")  # "levoglucosan" "tBuOH" "sorbitol" "xylitol" "LGA" 
residue_name=( "${extractDirList[@]}" )
solute_name=( "${extractDirList[@]}" )
#declare -a residue_name=("tBuOH" "FRU") # Used for indexing, Residue name  "LGA" 
# "cellobiose" "ETBE" "propanediol" "fructose" "tBuOH" "levoglucosan" "xylitol"
# declare -a solute_name=("tBuOH" "FRU") # Used to look for itp files

#declare -a extractDirList=("ETBE" "PDO")  # "levoglucosan" "tBuOH" "sorbitol" "xylitol" "LGA" 
#declare -a residue_name=("ETBE" "PDO")  # "levoglucosan" "tBuOH" "sorbitol" "xylitol" "LGA" 
#declare -a solute_name=("ETBE" "propanediol") # Used to look for itp files

# Finding total number of extraction directories
totalExtractionDir=$((${#extractDirList[@]}-1))

# First and last frame to read in trajectory in ps
first_frame="1000" # ps

# Index in the index file
onlyWater="SOL" # Water typically SOL

# Defining directory structure
mainDir="/home/akchew/scratch/SideProjectHuber" # Main directory for side project
path2FullAnalysis="${mainDir}/Analysis" # Directory for analysis (general)
#specific_analysis_dir="cellobiose" # Directory where you want specifically the analysis to occur

# Defining extraction files (Assuming you have same names)
groFile="mixed_solv_prod.gro"
tprFile="mixed_prod_extended.tpr" # "mixed_solv_prod.tpr" # Used for indexing, etc.
xtcFile="mixed_solv_prod.xtc"
indexFile="index.ndx" # Used for H-bond groups
output_alcohol_file="alcohol_names.txt" # Used to specify alcohol groups

# Defining production input file names

production_file_start="mdRun_"

# Defining analysis folder you would like this to come out of
analysis_folder="hbond_analysis_Ver_5_0_1_Hbond_split" # We will create a folder of this type within your folder

## Defining OUTPUT Files ##
hbond_num_file_name="hbnum_total.xvg"
hbond_ac_file_name="hbac.xvg"
hbond_output_file_name="hbond_output.txt"

# Defining files within analysis folder
hbond_num_file="${analysis_folder}/${hbond_num_file_name}"
hbond_ac_file="${analysis_folder}/${hbond_ac_file_name}" # Chandler kinetics
hbond_output_file="${analysis_folder}/${hbond_output_file_name}" # Textfile where you output the final script values to

# Split directory
split_dir="${analysis_folder}/hbond_split" # Directory where all splitting will take place


# Defining directory for analysis
Path2AnalysisWorkingDir="$path2FullAnalysis/$specific_analysis_dir"

if [ ! -e $Path2AnalysisWorkingDir ] # Checking if analysis file is there
    then
        echo "Error! No analysis file!!! Stopping now!"
        echo "Missing path: ${Path2AnalysisWorkingDir}"
        echo "May need to run analysisTools_Extraction.sh"
    exit
fi


# Start by finding all the possible splits -- Split OHs
if [[ ${wantSplitOH} == "True" ]]; then
    echo "Calculating number of splits required"
    
    # Creating a time sequence
    time_sequence=($(seq ${first_frame} ${split_time} ${total_sim_time}))
    
    # Finding total iterations required
    total_iter=$((${#time_sequence[@]}-1))
    
    echo "For splitting, total iterations required is: ${total_iter}"
    echo "This is based on your input of ${first_frame} ps as your first frame with ${split_time} ps as your increment and ${total_sim_time} ps as your final time"
    
fi




### GROMACS EXTRACTION ###
if [[ ${skipGMX} != "True" ]]; then
    echo "Running GMX HBOND ANALYSIS"


    # Looping through each analysis directory
    for currentAnalysis_dir_count in $(seq 0 $totalExtractionDir); do

        # Defining parameters for extraction
        FilesRequired=$(echo $Path2AnalysisWorkingDir/${extractDirList[currentAnalysis_dir_count]}/$production_file_start*)

        # Defining residue/solute
        currentResidueName=${residue_name[currentAnalysis_dir_count]}
        currentSoluteName=${solute_name[currentAnalysis_dir_count]}

        # -- Starting the analysis tools -- #
        for currentSeq in $FilesRequired; do
            echo "Working hydrogen bonds in: $currentSeq"
            cd $currentSeq
            
            # Checking for existance of all files
            if [[ ! -f ${tprFile} && ! -f ${xtcFile} ]]; then
                echo "Error! Did not find the following TPR / XTC File Required"
                echo "TPR File: ${tprFile}"
                echo "XTC File: ${xtcFile}"
                echo "Stopping script here: $(pwd)"
                exit
            fi

            # Checking if the analysis folder exists. If it does, remove and recreate
#            if [[ -e ${analysis_folder} ]]; then
#                echo "${analysis_folder} already exists, removing duplicates!"
#                echo "Waiting 5 seconds until deletion..."
#                sleep 5
#                rm -rv ${analysis_folder}
#            fi

            # Making directory
            mkdir -p ${analysis_folder}; echo "Making ${analysis_folder} directory and running gmx hbond"

            # Keeping track of how long it took
            start=`date +%s`
            
            # Checking if you want an index file
            if [[ ${wantIndex} == "True" ]]; then
                # Creating index file
gmx make_ndx -f ${tprFile} -o ${indexFile} << INPUT
q
INPUT
            fi
            
            ## GENERAL HYDROGEN BONDING ##
            
            if [[ ${wantGeneralOH} == "True" ]]; then
            echo "--> Running generalized OH analysis"
            # Printing
            echo "See all results in ${currentSeq}/${hbond_output_file} , not showing them here!"
# Running gmx hbond
gmx hbond -f "$xtcFile" -s "$tprFile"  -num $hbond_num_file -ac ${hbond_ac_file} -b ${first_frame}  > ${hbond_output_file} 2>&1 << HBOND_INPUT
$currentResidueName
$onlyWater
HBOND_INPUT

            fi
            
            ## SPLIT OH GROUPS -- AVG + STD ##
            if [[ ${wantSplitOH} == "True" ]]; then
                echo "--> Running SPLIT OH data collection -- used for averaging and STD"
                
                # Start by creating directory
                mkdir -p ${split_dir}; echo "Making split directory: ${split_dir}"
                
                # Then, iterating each possible iteration and saving
                for currentSplit in $(seq 0 $((${total_iter} -1 ))); do
                
                    # Printing
                    echo "Working on split: ${currentSplit} out of ${total_iter}"
                    
                    # Defining the time bounds
                        # Lower bound
                        lower_bound_time=${time_sequence[${currentSplit}]}
                        # Upper Bound
                        upper_bound_time=${time_sequence[$((${currentSplit}+1))]}
                        
                    # Printing our bounds
                    echo "Working on ${lower_bound_time} ps to ${upper_bound_time} ps"
                
                    # Creating a new directory
                    currentSplitOHDir="${split_dir}/${lower_bound_time}_${upper_bound_time}"
                    mkdir -p ${currentSplitOHDir}
                
                    # Running gmx hbond for specific directory
                    echo "See all results in ${currentSeq}/${currentSplitOHDir} , not showing them here!"
                    
# Running gmx hbond
gmx hbond -f "$xtcFile" -s "$tprFile"  -num ${currentSplitOHDir}/${hbond_num_file_name} -ac ${currentSplitOHDir}/${hbond_ac_file_name} -b ${lower_bound_time} -e ${upper_bound_time}  > ${currentSplitOHDir}/${hbond_output_file_name} 2>&1 << HBOND_INPUT
$currentResidueName
$onlyWater
HBOND_INPUT
                       
                    
                done
                
            fi
            
            
            # Check if you want OH groups only!
            if [[ ${wantOHGroups} == "True" ]]; then
                # Now, running the getOH Script
                getOHindex ${indexFile} ${currentSoluteName}.itp ${groFile} ${output_alcohol_file}
                
                # We can then loop to create a directory, then run the analysis within that directory
                for currentAlcohol in $(cat ${output_alcohol_file});do
                    # Printing
                    echo "Working on alcohol: ${currentAlcohol}"
                    
                    # Check if the current directory exists. If so, remove it
                    # Checking if the analysis folder exists. If it does, remove and recreate
                    if [[ -e ${currentAlcohol} ]]; then
                        echo "${currentAlcohol} already exists, removing duplicates!"
                        echo "Waiting 5 seconds until deletion..."
                        sleep 5
                        rm -rv ${currentAlcohol}
                    fi
                    
                    currentAlcoholDir=${analysis_folder}/${currentAlcohol}
                    
                    # Start by creating a directory
                    mkdir -p ${currentAlcoholDir}
                    
                    # Then, run gmx hbond
                    echo "See all results in ${currentAlcoholDir} , not showing them here!"
gmx hbond -f "${xtcFile}" -s "${tprFile}"  -n ${indexFile} -num "${currentAlcoholDir}/${hbond_num_file_name}" -ac "${currentAlcoholDir}/${hbond_ac_file_name}" -b "${first_frame}" > "${currentAlcoholDir}/${hbond_output_file_name}" 2>&1 << HBOND_INPUT
$currentAlcohol
$onlyWater
HBOND_INPUT

sleep 5; echo "Waiting for 5 seconds"
                
                done
            
            fi
            


            # Checking time
            end=`date +%s`
            runtime=$((end-start))
            echo -e "Total runtime for hydrogen bonding analysis was: ${runtime} seconds \n"

            # Going back to previous directory
            cd $Path2AnalysisWorkingDir # Probably not necessary, but just in case

        done
    done

    # Printing what we have done
    echo "------------------"
    echo "Generated hydrogen bonds located in $Path2AnalysisWorkingDir"
    echo "Worked on the following analysis directories: ${extractDirList[@]}"
    echo "Good luck with the analysis!"
    
    
    else # Do not want to use gmx hbond
    echo "Skipping GMX HBond..."
fi

### SUMMARY SECTION ###

## Functions for summary ##

### FUNCTION TO EXTRACT THE DIRECTORY INFORMATION
# The purpose of this function is to get a directory, then extract information from it
# $1 is the directory name, e.g. mdRun_403.15_6_nm_CEL_75_WtPercWater_spce_dioxane
function extract_dir_info () {
    # Residue name
    residue_name=$(echo "$1" | cut -d '_' -f5)
    
    # Temperature
    temp=$(echo "$1" | cut -d '_' -f2)
    
    # Mass Fraction of Water
    mass_frac=$(echo "$1" | cut -d '_' -f6)
    
    # Cosolvent Name
    cosolvent_name=$(echo "$1" | cut -d '_' -f9)
    
    # Printing as a variable
    echo "${residue_name}:${temp}:${mass_frac}:${cosolvent_name}"
}


# The purpose of this script is to look into your output file and export information
# $1 is the directory name
function extract_hbond_output () {
    # Finding the data
    k_forward=$( grep -E "Forward" "$1" | awk {'print $2'})
    hbond_lifetime=$( grep -E "Forward" "$1" | awk {'print $3'})
    k_backward=$( grep -E "Backward" "$1" | awk {'print $2'})
    hbond_lifetime_reverse=$( grep -E "Backward" "$1" | awk {'print $3'})

    # Average hydrogen bond per time frame
    avg_hbond=$(grep -E "Average number of hbonds per timeframe" "$1" | awk {'print $7'})
    
    # Printing as variables
    echo "${k_forward}:${hbond_lifetime}:${k_backward}:${hbond_lifetime_reverse}:${avg_hbond}"
}

# The purpose of this script is to look for the directory and check for it. If it exists, delete it!
# $1 is the full path to your directory
function removeifexists () {
    if [ -e "$1" ] # Checking if analysis file is there
    then
        echo "Duplicate file!"
        echo "Full path: $1"
        echo "Removing in 5 seconds..."
        sleep 5; rm -rv $1
    fi
}



## MAIN SCRIPT SUMMARY ##

if [[ ${wantSummary} == "True" ]]; then
    # The purpose of the summary is to simply go through all the documents that had gmx hbond done, then output k (forward constant) and the corresponding time constant (tau)
    echo "----- RUNNING GMX HBOND SUMMARY -----"
    
    # Location of the summary_file
    path2summary="${Path2AnalysisWorkingDir}/${summary_file_name}"
    path2hbond_vs_time_summary="${Path2AnalysisWorkingDir}/${hbond_vs_time_file_name}"
    path2summary_eachOH="${Path2AnalysisWorkingDir}/${summary_file_each_OH}"
    path2summary_split="${Path2AnalysisWorkingDir}/${summary_file_split}"
    
    # Checking if any of the files already exist; remove if necessary
    removeifexists "$path2summary"
    removeifexists "$path2hbond_vs_time_summary"
    removeifexists "$path2summary_eachOH"
    removeifexists "$path2summary_split"
    
    #### ADDING FIRST LINE TO SUMMARIES ####
    
    ## Generalized OH (no splitting) ##
    if [[ ${wantGeneralOH} == "True" ]]; then
    
    # Adding the first line to the summary
    echo "PATH, Residue_name, Temperature, Water_Mass_Frac, Cosolvent, Forward_Rate_Constant, HBond_Lifetime_Forward (ps), Backward_Rate_Constant, HBond_Lifetime_Backward (ps), Average_HBond_Per_Timeframe" > ${path2summary}
    
    fi
    
    ## Splitting OH ##
    if [[ ${wantSplitOH} == "True" ]]; then
        current_split_OH="PATH, Residue_name, Temperature, Water_Mass_Frac, Cosolvent"
        additional_string="HBond_Lifetime_Forward (ps)"
        # "Forward_Rate_Constant, HBond_Lifetime_Forward (ps), Backward_Rate_Constant, HBond_Lifetime_Backward (ps), Average_HBond_Per_Timeframe"
    
    
        # Looping to continuously add as columns
        for currentSplit in $(seq 0 $((${total_iter} -1 ))); do
        
            # Editing the additional string
            temp_addn_string=$(echo ${additional_string} | sed "s/,/_${currentSplit},/g;s/$/_${currentSplit}/" )
        
            # Adding to the string
            current_split_OH=$(echo "${current_split_OH}, ${temp_addn_string}")
        
        done
        
        # At the end, adding to first line in summary
        echo "${current_split_OH}" > "${path2summary_split}"
        
    fi
    
    
    ## Varying OH groups ##
    if [[ ${wantOHGroups} == "True" ]]; then
        
        # Adding first line to summary
        echo "PATH, Residue_name, Temperature, Water_Mass_Frac, Cosolvent, Alcohol_Label, Forward_Rate_Constant, HBond_Lifetime_Forward (ps), Backward_Rate_Constant, HBond_Lifetime_Backward (ps), Average_HBond_Per_Timeframe" > ${path2summary_eachOH}

    fi
    
    #### LOOPING THROUGH EACH FOLDER #### 
    first_hbond_vs_time=False # Will changed to True later
    
    # Going through each file and looking at the hydrogen bonding analysis
    # Looping through each analysis directory
    for currentAnalysis_dir_count in $(seq 0 ${totalExtractionDir}); do

        # Defining parameters for extraction
        FilesRequired=$(echo ${Path2AnalysisWorkingDir}/${extractDirList[currentAnalysis_dir_count]}/${production_file_start}*)
        IFS=" " # Setting deliminator to spaces

        # -- Starting the summary tools -- #
        for currentSeq in ${FilesRequired}; do
        
            # Printing current working directory
            echo "------------------------------------------"
            echo "Working on current directory: ${currentSeq}"
            
            # Going into directory
            cd ${currentSeq}
            
            # Getting directory name
            dir_name=$(basename ${currentSeq})
            
            # Extracting data from the file
            IFS=":"; read residue_name temp mass_frac cosolvent_name <<< "$(extract_dir_info ${dir_name})"
            
            echo "Current residue name: ${residue_name}"
            echo "Current temperature (K): ${temp}"
            echo "Current mass frac. of water: ${mass_frac}"
            echo "Cosolvent: ${cosolvent_name}"
            
            ## Generalized OH Hydrogen Bonding Data ##
            if [[ ${wantGeneralOH} == "True" ]]; then
                echo "--> Running generalized OH analysis"
            
                # Extracting the data
                IFS=":"
                read k_forward hbond_lifetime k_backward hbond_lifetime_reverse avg_hbond <<< "$(extract_hbond_output ${hbond_output_file})"
                
                # Start by printing the point of working directory
                echo "${currentSeq},${residue_name},${temp},${mass_frac},${cosolvent_name},${k_forward},${hbond_lifetime},${k_backward},${hbond_lifetime_reverse},${avg_hbond}" >> ${path2summary}

                # Printing what we did
                echo -e "Copying forward rate (${k_forward}) and lifetime (${hbond_lifetime}) to ${path2summary}... \n"

                if [[ ${wantHBondvsTime} == "True" ]]; then
                    echo -e "--- Starting hydrogen bond versus time extraction... \n"  

                    # For the very first time
                    if [[ ${first_hbond_vs_time} == "False" ]]; then
                        echo "Creating a hydrogen bond versus time file at ${path2hbond_vs_time_summary}"
                        echo "Hydrogen_Bonding_Vs_Time" > ${path2hbond_vs_time_summary}
                        first_hbond_vs_time="True" # Turning off this function
                    fi

                    # Writing current PWD
                    echo "${currentSeq}" >> ${path2hbond_vs_time_summary}

                    # Getting first and last line numbers
                    hbond_first_line=$(grep -n "s1 legend" ${hbond_num_file} | grep -Eo '^[^:]+')
                    hbond_first_line=$(( ${hbond_first_line} + 1 )) # Adding one
                    hbond_last_line=$(wc -l ${hbond_num_file}  | awk {'print $1'})

                    # Now, getting the data and extracting
                    sed -n ${hbond_first_line},${hbond_last_line}p ${hbond_num_file} | awk {'print $1, $2'} >> ${path2hbond_vs_time_summary}
                    echo -e "Copying line number ${hbond_first_line} to ${hbond_last_line} and splitting to first + second columns \n"

                    # Adding empty line
                    echo "" >> ${path2hbond_vs_time_summary}

                fi
            
            fi
            
            ## Splitting OH Hydrogen Bonding Data ##
            if [[ ${wantSplitOH} == "True" ]]; then
                echo "--> Running SPLIT OH analysis -- used for averaging and STD"
            
                # Creating a data string
                data_string="${currentSeq},${residue_name},${temp},${mass_frac},${cosolvent_name}"
                
                # Looping through each split, extracting data, adding to data_stirng
                IFS="${OLDIFS}" # Setting deliminator to spaces
                for currentSplit in $(seq 0 $((${total_iter}-1 ))); do
                    # Printing
                    echo "Working on split: ${currentSplit} out of ${total_iter}"
                    
                    # Defining the time bounds
                        IFS="${OLDIFS}" # Setting deliminator to spaces
                        # Lower bound
                        lower_bound_time=${time_sequence[${currentSplit}]}
                        # Upper Bound
                        upper_bound_time=${time_sequence[$((${currentSplit}+1))]}
                        
                    # Printing our bounds
                    echo "Working on ${lower_bound_time} ps to ${upper_bound_time} ps"
                
                    # Defining current working dorectory
                    currentSplitOHDir="${split_dir}/${lower_bound_time}_${upper_bound_time}"
                    
                    # Now, extracting data for the working directory
                    IFS=":"
                    read k_forward hbond_lifetime k_backward hbond_lifetime_reverse avg_hbond <<< "$(extract_hbond_output ${currentSeq}/${currentSplitOHDir}/${hbond_output_file_name})"
                    
                    # Adding to the current string
                    data_string=$(echo "${data_string}, ${hbond_lifetime}")
                    # $(echo "${data_string}, ${k_forward},${hbond_lifetime},${k_backward},${hbond_lifetime_reverse},${avg_hbond}")
                
                done
                
                # Now, adding to the summary file
                echo "${data_string}" >> "${path2summary_split}"
                
                # Printing what we did
                echo -e "Copying data: ${data_string} to ${path2summary_split}... \n"
                
            
            fi
            
            
            ## Specific OH Hydrogen Bonding Data ##
            
            if [[ ${wantOHGroups} == "True" ]]; then
                echo "--> Running individual OH analysis"
                
                # Checking if you have an alcohol file
                if [[ -e ${output_alcohol_file} ]]; then
                
                    # Editing delimitor -- fixing to default
                    IFS=$' \t\n'
                    for currentAlcohol in $(cat ${output_alcohol_file});do
                        # Printing
                        echo "Working on alcohol: ${currentAlcohol}"

                        # Defining directory
                        currentAlcoholDir="${analysis_folder}/${currentAlcohol}"
                        
                        # Going to directory
                        cd ${currentAlcoholDir}

                        # Extracting the data
                        IFS=":"
                        read k_forward hbond_lifetime k_backward hbond_lifetime_reverse avg_hbond <<< "$(extract_hbond_output "${hbond_output_file_name}")"
                        
                        
                        # Printing to file
                        echo "${currentSeq},${residue_name},${temp},${mass_frac},${cosolvent_name},${currentAlcohol},${k_forward},${hbond_lifetime},${k_backward},${hbond_lifetime_reverse},${avg_hbond}" >> ${path2summary_eachOH}
                        
                        # Going back to current working directory
                        cd ${currentSeq}
                    done
                
                else
                    echo "No alcohol file, ${output_alcohol_file}, not going through OH's"
                fi
                
                
            fi
            
            # Going back to previous directory
            cd ${Path2AnalysisWorkingDir} # Probably not necessary, but just in case
        
        done
        
    done

    # Printing what we have done
    echo "------------------"
    echo "Generated SUMMARY in $path2summary"
    echo "Worked on the following analysis directories: ${extractDirList[@]}"
    echo "Good luck with the summary!"

fi

## ARCHIVE ##
## Defining output files
#hbondFile_total="hbnum_total.xvg"
#hbondFile_waterOnly="hbnum_water.xvg"
#hbondFile_GVLOnly="hbnum_GVL.xvg"

# Index in the index file
#moleculeOfInterest=$residue_name # Molecule of interest
#onlyWater="SOL" # Water typically SOL
#
## Specific indexes
#notMolecule="! r ${moleculeOfInterest}" # Everything else other than solute
#onlySolventTwo="! r ${residue_name} & ! r ${onlyWater}" # Only interactions with solvent two
#
## Creating codenames for the specific indexes
#notMolecule_cb="!$(echo ${moleculeOfInterest} | tr [a-z] [A-Z])" # Callback
#onlySolventTwo_cb="${notMolecule_cb}_&_!${onlyWater}"


## Creating an index
## Normally: gmx make_ndx -f mixed_solv_frac_04_prod.tpr -o index.ndx
#gmx make_ndx -f "$tprFile" -o "$indexFile" << INPUT # Here document
#${notMolecule}
#${onlySolventTwo}
#q
#INPUT
#
## Now, using hydrogen bonds -- input 2 atoms, creates hbnum.xvg
### Total, GVL + water ##
#gmx hbond -f "$xtcFile" -s "$tprFile" -n $indexFile -num $hbondFile_total << HBOND_INPUT
#$moleculeOfInterest
#$notMolecule_cb
#HBOND_INPUT
#
### Only with water ##
#gmx hbond -f "$xtcFile" -s "$tprFile" -n $indexFile -num $hbondFile_waterOnly << HBOND_INPUT
#$moleculeOfInterest
#$onlyWater
#HBOND_INPUT
#
### Only with GVLL ##
#gmx hbond -f "$xtcFile" -s "$tprFile" -n $indexFile -num $hbondFile_GVLOnly << HBOND_INPUT
#$moleculeOfInterest
#$onlySolventTwo_cb
#HBOND_INPUT
#    