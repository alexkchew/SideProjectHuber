# -*- coding: utf-8 -*-
"""
extract_hbond_multi_traj_analysis.py
The purpose of this script is to analyze the data from the multi_traj_analysis_tool.py for hydrogen bonds

Author(s):
    Alex K. Chew (alexkchew@gmail.com)
"""

### IMPORTING FUNCTION TO GET TRAJ
from MDDescriptors.traj_tools.multi_traj import load_multi_traj_pickle
### IMPORTING FUNCTION TO FIND ALL PICKLES
from MDDescriptors.traj_tools.multi_traj import find_multi_traj_pickle, load_multi_traj_pickles
import sys
## IMPORTING DECODER FUNCTIONS
from MDDescriptors.core.decoder import decode_name

import numpy as np


#%% MAIN SCRIPT
if __name__ == "__main__":
    # gmx hbond -s mixed_solv_prod.tpr -f mixed_solv_prod_10_ns_whole.xtc -num

    from MDDescriptors.application.solvent_effects.h_bond import calc_hbond
    ## DEFINING CLASS
    Descriptor_class = calc_hbond
    
    ## DEFINING DATE
    Date='180405'
    
    
    #%%
    ## DEFINING DESIRED DIRECTORY
    # Pickle_loading_file=r"mdRun_433.15_6_nm_ACE_50_WtPercWater_spce_dmso"
    
    # Pickle_loading_file=r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_tetrahydrofuran"
    # Pickle_loading_file=r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_GVL_L"
    # Pickle_loading_file=r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane"
    
    Pickle_loading_file=r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane"
    # Pickle_loading_file=r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_NN-dimethylacetamide"
    # Pickle_loading_file=r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_NN-dimethylformamide"
    # Pickle_loading_file = r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_urea"
    # Pickle_loading_file = r"mdRun_433.15_6_nm_PDO_100_WtPercWater_spce_Pure"
    '''
    mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_NN-dimethylacetamide
    mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_NN-dimethylformamide
    mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_urea
    '''
    
    #### SINGLE TRAJ ANALYSIS
    multi_traj_results = load_multi_traj_pickle(Date, Descriptor_class, Pickle_loading_file )
    ### EXTRACTING THE RESULTS
    hbond = multi_traj_results
    
    print(np.mean(hbond.hbond_btn_residues['PDO-HOH']))
    print(np.mean(hbond.hbond_btn_residues['HOH-HOH']))
    print(np.mean(hbond.hbond_solute_donor['PDO-O2-HOH']))
    print(np.mean(hbond.hbond_solute_donor['PDO-O1-HOH']))
    
    print(np.mean(hbond.hbond_solute_donor['PDO-O1-HOH']) / np.mean(hbond.hbond_solute_donor['PDO-O2-HOH']))
    
    print(np.mean(hbond.hbond_btn_residues['HOH' + '-' + hbond.solvent_name[-1]]))
    
    #%%
    
    import MDDescriptors.core.plot_tools as plot_tools
    import os
    import os.path # checks if file exists
    
    ### DEFINING GLOBAL PLOTTING VARIABLES
    FONT_SIZE=16
    FONT_NAME="Arial"    
    
    ### DEFINING COLOR LIST
    COLOR_LIST=['k','b','r','g','m','y','k','w']
    
    ### DEFINING LINE STYLE
    LINE_STYLE={
                "linewidth": 1.4, # width of lines
                }
    
    ### DEFINING SAVING STYLE
    DPI_LEVEL=600
    
    # plot_tools.create_plot
    
    
    ### FUNCTION TO CONGLOMERATE AVERAGE AND STD INTO A LIST
    def convert_avg_std_labels(label_list, avg_list, std_list):
        '''
        The purpose of this function is to convert the labels, average, std into two lists. For example:
            ['label_1', 'label_2'], [avg_1, avg_2], [std_1, std_2] -> ['label_1', 'label_1(err)' ,'label_2', 'label_2(err)'] and [avg_1,std_1, avg_2, std_2]
        This is important for exporitng to CSV file, where you need each line one by one. You can also create a table using pandas, but I'm not doing that here.
        INPUTS:
            label_list: [list] labels of the values. The labels should match the array of average and standard deviation
            avg_list: [list] list of average values for each label
            std_list: [list] list of standard deviations corresponding to the average labels
        OUTPUTS:
            label_avg_std_list: [list] labels with both the original label and error (denoted by label(err) )
            avg_std_list: [list] values corresponding to average, the standard deviation, respectively
        '''
        ## FIRST FIND THE LENGTH OF THE INPUTS
        label_length = len(label_list)
        avg_length = len(avg_list)
        std_length = len(std_list)
        
        ## CHECKING LENGTHS OF LABELS + AVG + STD
        if label_length != avg_length or label_length != std_length:
            print("ERROR! Label length does not match average or std length. The lengths are:")
            print("label length: %d, average length: %d, std length: %d"%(label_length, avg_length, std_length))
            print("STOPPING HERE, Check the average, std, label lengths!")
            sys.exit()
        
        ## CREATING LIST WITH ERRORS
        label_avg_std_list, avg_std_list = [], []
        
        ## LOOPING THROUGH EACH LABEL
        for index in range(label_length):
            ## STORING THE LABELS + ERROR
            label_avg_std_list.extend([label_list[index], label_list[index] + '(err)' ])
            ## STORING AVERAGE AND STD VALUE
            avg_std_list.extend( [avg_list[index], std_list[index] ] )
                
        return label_avg_std_list, avg_std_list
    
    ###############################################
    ### CLASS FUNCTION TO PLOT HYDROGEN BONDING ###
    ###############################################
    class plot_hbond:
        '''
        The purpose of this function is to plot the average number of hydrogen bonds for a particular solvent system
        INPUTS:
            hbond: hbond from calc_hbond class
            pickle_name: name of the pickle -- contains important information about the name of the system you care about
            decode_type: decoding type for the pickle name
            export_text_file_name: name of text file to export to
        OUTPUTS:
            
        '''
        ### INITIATING
        def __init__(self, hbond, pickle_name, decode_type = 'solvent_effects', export_text_file_name = 'export.csv', labels = None):
            ## STORING INPUT VARIABLES
            self.hbond = hbond
            self.pickle_name = pickle_name
            self.export_text_file_name = export_text_file_name
            
            ## DECODING THE NAME
            self.decoded_name = decode_name(self.pickle_name, decode_type)
            
            ## FINDING DECODED NAME AS LIST
            self.decoded_name_list, self.decoded_value_list = convert_dict_to_list(self.decoded_name)
            
            ## EXPORTING AVERAGE HYDROGEN BONDS
            self.export_avg_hbond_each_solute_heavy_atoms(labels)
            
            
        ### FUNCTION TO WRITE OUT THE HBONDING DETAILS
        def export_avg_hbond_each_solute_heavy_atoms(self, labels = None ):
            '''
            The purpose of this function is to export the average hydrogen bonding to a text files
            INPUTS:
                self: class object
            OUTPUTS:
                text file using the export_text_file_name variable 
            '''
            ## FINDING KEYS TO DATA
            key_data = [ each_key for each_key in self.hbond.hbond_solute_donor.keys() if 'HOH' in each_key]
            
            ## FINDING AVERAGE HYDROGEN BONDS
            average_h_bonds = [ np.mean(self.hbond.hbond_solute_donor[each_key], axis=0) for each_key in key_data]
            error_h_bonds = [ np.std(self.hbond.hbond_solute_donor[each_key], axis=0) for each_key in key_data]
            
            ## COMBINING KEYS AND DATA
            label_avg_std_list, avg_std_list = convert_avg_std_labels( key_data, average_h_bonds, error_h_bonds )
            ## Storing the labels
            self.labels = label_avg_std_list
            
            ## COMBINING DECODED NAMES FOR EXPORTION
            full_label_list = self.decoded_name_list + label_avg_std_list
            full_value_list = self.decoded_value_list  + avg_std_list
            
            ## CHECKING IF LABELS ARE MATCHIN
            if labels is not None:
                if set(self.labels) != set(labels):
                    print("ERROR, LABELS ARE NOT MATCHING FOR CSV FILE. CHECK IT!")
                    print("Previous labels:" )
                    print(labels)
                    print("New labels:"%(self.labels))
            
            ## OPENING FILE AND WRITING
            print("WRITING TO FILE: %s"%(self.export_text_file_name))
            ## WRITING LABELS
            if os.path.exists(self.export_text_file_name) is False:
                with open(self.export_text_file_name, 'w+') as file:
                    ## WRITING THE LABELS
                    for each_label in full_label_list:
                        file.write(str(each_label))
                        file.write(', ')
                    ## WRITING EMPTY LINE
                    file.write('\n')
            
            ## WRITING VALUES
            with open(self.export_text_file_name, 'a') as file:
                ## WRITING THE VALUES
                for each_value in full_value_list:
                    file.write(str(each_value))
                    file.write(', ')
                ## WRITING EMPTY LINE
                file.write('\n')
            
            return

        ### PLOTTING AVERAGE HBOND BETWEEN EACH SOLUTE HYDROGENS
        def plot_avg_hbond_each_solute_heavy_atoms(figs = None, axs = None):
            '''
            This will plot the average number of hydrogen bonds for a solute (each heavy atom) to water
            INPUTS:
                self: class object
            OUTPUTS:
                fig, ax: figure and axis
            '''
            ## FINDING KEYS TO DATA
            key_data = [ each_key for each_key in hbond.hbond_solute_donor.keys() if 'HOH' in each_key]
            
            if figs == None or axs == None:
                figs = []; axs = []
                for each_acceptor in range(len(hbond.acceptors_solute)):
                    fig, ax = plot_tools.create_plot()

                    ## DEFINING X AND Y AXIS
                    ## DRAWING LABELS
                    # ax.set_xlabel('Solvent system',fontname=FONT_NAME,fontsize = FONT_SIZE)
                    ax.set_ylabel('Average number of hydrogen bonds',fontname=FONT_NAME,fontsize = FONT_SIZE)
                    ## DRAWING TITLE
                    ax.set_title(key_data[each_acceptor])
                    ## APPENDING
                    figs.append(fig); axs.append(ax)
                    ## DEFINING CURRENT POSITION
                    current_position = 1
                    
                    
            ## FINDING DATA FROM INPUTS
            try:
                label=[ solvent_name for solvent_name in hbond.solvent_name if solvent_name != 'HOH' ][0]
            ## IF THERE IS NO SUCH SOLVENT NAME, THEN WE HAVE A PURE WATER CASE
            except:
                label='HOH'
            average_h_bonds = [ np.mean(hbond.hbond_solute_donor[each_key], axis=0) for each_key in key_data]
            error_h_bonds = [ np.std(hbond.hbond_solute_donor[each_key], axis=0) for each_key in key_data]
                        
            ## ADDING TO EACH FIGURE
            for each_acceptor in range(len(hbond.acceptors_solute)):
                axs[each_acceptor].bar(current_position, average_h_bonds[each_acceptor], align='center', alpha=1.0, yerr=error_h_bonds[each_acceptor])
                axs[each_acceptor].set_xticks([current_position], [label])
            
            return figs, axs
            
    ### FUNCTION TO CONVERT DICTIONARY TO A LIST        
    def convert_dict_to_list(dictionary):
        '''
        The purpose of this function is to convert a dictionary to a list. For example:
            {'var1': 10, 'var2':20} --> ['var1', 'var2'] and [10, 20]
        This function is useful when outputting a dictionary to a csv file, where you need the labels and values in a line-by-line fashion.
        INPUTS:
            dictionary: [dict] dictionary that you want to convert to two lists
        OUTPUTS:
            labels: [list] list of labels
            values: [list] values of the dictionary labels
        '''
        ## CHECKING IF YOU HAVE SUPPLIED A DICTIONARY
        if type(dictionary) is not dict:
            print("ERROR, You supplied another type other than dictionary to conversion of dictionary to list!")
            print("Type: %s"%(type(dictionary)))
            print("STOPPING HERE TO PREVENT ERRORS. Please check your inputs to convert_dict_to_list function")
            sys.exit()
        
        ## CONVERTING THE KEYS TO A LIST OF STRINGS
        labels = list( dictionary.keys() )
        ## CONVERTING VALUES INTO LIST
        values = [ dictionary[each_key] for each_key in labels]
        return labels, values
  
    
    
    #%%
    
    ### RUNNING HBOND PLOT
    hbond_plot = plot_hbond(hbond, Pickle_loading_file)
    
    
    #%%

    
    
    ####################################################################
    ### CLASS TO EXTRACT MULTIPLE TRAJECTORIES TO A SINGLE TEXT FILE ###
    ####################################################################
    class multi_traj_export_csv:
        '''
        The purpose of this class is to take an exporting class, and combine the results of the exports into a single text file. We will take the inputs for the exporting text file, loop through each text file and run it accordingly
        INPUTS:
            traj_results: list of results as a list given by "load_multi_traj_pickles"
            list_of_pickles: list of opickles from the "load_multi_traj_pickles"
            export_class: plotting class/function
                NOTE: We assume that the export class will have output functionalities
            decoder_type: way to decode your directory name
            export_text_file_name: name of text file to export to
        OUTPUTS:
            a csv file based on export_text_file_name

        ALGORITHM:
            - Create a text file
            - Run exporting text file script
            - Loop until all pickles are complete
        NOTES:
            export_class.label: the export class must have a corresponding label to it. This ensures that the export is correct!
        '''
        ### INITIALIZATION
        def __init__(self, traj_results, list_of_pickles, export_class, decoder_type = None, export_text_file_name = 'export.csv'):
            ## STORING FILE NAME
            self.export_text_file_name = export_text_file_name
            
            ## REMOVING ANY FILE THAT IS ALREADY EXISTING FOR THE EXPORTATION OF TEXT FILES
            self.remove_file(self.export_text_file_name)
            
            ## CREATING BLANK LABEL
            label = None
            ## LOOPING THROUGH EACH PICKLE AND USING EXPORT FUNCTION
            for index, each_pickle in enumerate(list_of_pickles):
                ## PRINTING
                print("EXPORTING FOR %s"%(each_pickle))
                ## RUNING EXPORT CLASS
                exported_class = export_class(traj_results[index], list_of_pickles[index], decoder_type, export_text_file_name, label )
                label = exported_class.labels
            return
            
        ### FUNCTION TO CHECK IF FILE EXISTS, THEN REMOVES IF TRUE
        def remove_file(self, text_file):
            '''
            The purpose of this function is to remove any file
            INPUTS:
                self: class object
            OUTPUTS:
                removed file
            '''
            if os.path.exists(text_file) is True:
                print("Initial %s found! Removing..."%(text_file))
                os.remove(text_file)
            return
        
        
        
    ## RUNNING MULTI TRAJECTORY
    traj_results, list_of_pickles = load_multi_traj_pickles( Date, Descriptor_class)
    ### RUNNING MULTI TRAJECTORY EXPORT
    multi_export = multi_traj_export_csv( traj_results = traj_results,
                                             list_of_pickles = list_of_pickles,
                                             export_class = plot_hbond,
                                             decoder_type = 'solvent_effects',
                                             export_text_file_name = 'export.csv',
                                            )
    
    
    
    
    
    
    #%%
    plot_rdf_solute_solvent = plot_rdf(rdf)
    plot_rdf_solute_solvent.plot_rdf_solute_oxygen_to_solvent(True)
    # plot_rdf_solute_solvent.plot_rdf_solute_solvent(True)
    # plot_rdf_solute_solvent.plot_rdf_solute_solvent(True)
    
    
    #%%
    ##### MULTI TRAJ ANALYSIS
    list_of_pickles = find_multi_traj_pickle(Date, Descriptor_class)

    rdfs=[]
    
    for Pickle_loading_file in list_of_pickles:
        ### EXTRACTING THE DATA
        multi_traj_results = load_multi_traj_pickle(Date, Descriptor_class, Pickle_loading_file )
        ### EXTRACTING THE RDF
        rdf = multi_traj_results
        ### TURNING OFF SOLUTE AND SOLVENT COM
        rdf.solute_COM = []; rdf.solvent_COM = []
        ### STORING THE RDFS
        rdfs.append(rdf)
    
    
    ### PLOTTING
    # plot_rdf(rdf)
    
    
    #%%
    ### DEFINING GLOBAL PLOTTING VARIABLES
    FONT_SIZE=16
    FONT_NAME="Arial"    
    
    ### DEFINING COLOR LIST
    COLOR_LIST=['k','b','r','g','m','y','k','w']
    
    ### DEFINING LINE STYLE
    LINE_STYLE={
                "linewidth": 1.4, # width of lines
                }
    ### DEFINING SAVING STYLE
    DPI_LEVEL=600
    
    ### IMPORTING MODULES
    import matplotlib.pyplot as plt
    from MDDescriptors.core.plot_tools import save_fig_png # code to save figures
    
    #########################################################################
    ### CLASS FUNCTION TO PLOTTING MULTIPLE RADIAL DISTRIBUTION FUNCTIONS ###
    #########################################################################
    class multi_plot_rdf:
        '''
        The purpose of this class is to take multiple class_rdf classes and plot it accordingly
        INPUTS:
            rdfs: list of calc_rdf classes
            names: list of names associated with each rdf
            decode_type: string denoting way to decode the names
        OUTPUTS:
            ## INPUTS
                self.rdfs: rdfs
                self.names: Names of the directories
                self.decode_type: decoding type for the directories
            ## DECODING
                self.names_decoded: decoded names
                self.unique_solute_names: unique solute names
            
            
        FUNCTIONS:
            find_unique: finds unique decoders
            convert_water_to_cosolvent_mass_frac: converts water mass fraction to cosolvent
            
        ACTIVE FUNCTIONS:
            plot_rdf_solute_solvent_multiple_mass_frac: Plots rdf solute to solvent for multiple mass fractions
            plot_rdf_solute_oxy_to_solvent_multiple_mass_frac: plots rdf of solute's oxygen to solvent for multiple mass fractions
        '''
        ### INITIALIZATION
        def __init__(self, rdfs, names, decode_type='solvent_effects'):
            ## DEFINING ORGANIZATION LEVELS
            self.organization_levels = [ 'solute_residue_name', 'cosolvent_name', 'mass_frac_water' ]
            
            ## STORING INPUTS
            self.rdfs = rdfs
            self.names = names
            self.decode_type = decode_type
            
            ## DECODING NAMES
            self.names_decoded = [decode_name(name=name,decode_type=decode_type) for name in self.names]
            
            ## FINDING UNIQUE SOLUTE NAMES
            self.unique_solute_names = self.find_unique('solute_residue_name')
            
            ## PLOTTING RDF FOR DIFFERENT MASS FRACTIONS
            # self.plot_rdf_solute_solvent_multiple_mass_frac()

        ### FUNCTION TO FIND ALL UNIQUE RESIDUES
        def find_unique(self,decoding_name):
            '''
            The purpose of this function is to find all unique solutes
            INPUTS:
                self: class property
                decoding_name: decoding name
                    e.g. 'solute_residue_name', etc.
            OUTPUTS:
                unique_names: unique names
            '''
            unique_names = list(set([each_decoded_name[decoding_name] for each_decoded_name in self.names_decoded]))
            return unique_names
            
        ### FUNCTION TO CREATE RDF PLOT
        def create_rdf_plot(self):
            '''
            The purpose of this function is to generate a figure for you to add your RDFs.
            Inputs:
                fontSize: Size of font for x and y labels
                fontName: Name of the font
            Output:
                fig: Figure to print
                ax: Axes to plot on
            '''
            ## CREATING PLOT
            fig = plt.figure() 
            ax = fig.add_subplot(111)
        
            ## DRAWING LABELS
            ax.set_xlabel('r (nm)',fontname=FONT_NAME,fontsize = FONT_SIZE)
            ax.set_ylabel('Radial Distribution Function',fontname=FONT_NAME,fontsize = FONT_SIZE)
            
            # Drawing ideal gas line
            ax.axhline(y=1, linewidth=1, color='black', linestyle='--')
            
            return fig, ax
        
        ### FUNCTION TO CONVERT MASS FRACTION FROM WATER TO COSOLVENT
        @staticmethod
        def convert_water_to_cosolvent_mass_frac(mass_frac_water_perc):
            '''
            The purpose of this script is to convert mass fraction from water to cosolvent
            INPUTS:
                mass_frac_water_perc: mass fraction of water (as a percent, e.g. 10)
            OUTPUTS:
                mass_frac_cosolvent: mass fraction of cosolvent (e.g. 0.90)
            '''
            return (100 - mass_frac_water_perc)/float(100)
        
        ### FUNCTION TO PLOT FOR DIFFERENT MASS FRACTIONS
        def plot_rdf_solute_solvent_multiple_mass_frac(self, save_fig=False):
            '''
            The purpose of this function is to plot the solute to solvent for multiple mass fractions
            INPUTS:
                self: class object
                save_fig: True if you want to save all the figures
            OUTPUTS:
                plot of RDF vs distance for different mass fractions of solvents
            '''
            ## LOOPING THROUGH EACH SOLUTE
            for each_solute in self.unique_solute_names:
                ## LOOPING THROUGH EACH COSOLVENT
                for each_solvent in self.find_unique('cosolvent_name'):
                    ## EXCLUDING IF PURE CASE
                    if each_solvent != 'Pure':
                        ## FINDING ALL INDICES THAT HAVE THIS SOLUTE AND SOLVENT
                        mass_frac_indices = [index for index, name_decoded in enumerate(self.names_decoded) \
                                             if name_decoded['solute_residue_name']==each_solute and name_decoded['cosolvent_name'] ==each_solvent]
                        ## FINDING ALL MASS FRACTIONS
                        water_mass_frac_values = [ self.names_decoded[index]['mass_frac_water'] for index in mass_frac_indices]
                        ## SORT BY THE SMALLEST MASS FRACTION OF WATER
                        water_mass_frac_values, mass_frac_indices = (list(t) for t in zip(*sorted(zip(water_mass_frac_values, mass_frac_indices))))
                        ## GETTING MASS FRACTION OF COSOLVENT
                        cosolvent_mass_frac_values = [ self.convert_water_to_cosolvent_mass_frac(each_mass_perc) for each_mass_perc in water_mass_frac_values ]                                                
                        
                        ## RDF -- SOLUTE - SOLVENT
                        for solvent_index,each_solvent_name in enumerate(rdfs[mass_frac_indices[0]].solvent_name):
                            ## CREATING RDF PLOT
                            fig, ax = self.create_rdf_plot()
                            ## SETTING THE TITLE
                            ax.set_title("%s --- %s"%(each_solute, each_solvent_name))
                            ## LOOPING THROUGH EACH MASS FRACTION AND PLOTTING
                            for each_mass_frac in range(len(mass_frac_indices)):
                                ## GETTING DATA INDEX
                                data_index = mass_frac_indices[each_mass_frac]
                                ## GETTING G_R AND R
                                g_r = self.rdfs[data_index].rdf_g_r[solvent_index]
                                r   = self.rdfs[data_index].rdf_r[solvent_index]
                                ## PLOTTING G_R VS R
                                ax.plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac],
                                                label= "Cosolvent mass frac: %.2f"%(cosolvent_mass_frac_values[each_mass_frac]),
                                                **LINE_STYLE)
                            ## ADDING PLOT IF 100% WATER EXISTS
                            pure_water_index = [index for index, name_decoded in enumerate(self.names_decoded) \
                                                 if name_decoded['solute_residue_name']==each_solute and \
                                                 name_decoded['cosolvent_name'] == 'Pure' and \
                                                 name_decoded['mass_frac_water'] == 100
                                                 ]
                            if len(pure_water_index) !=0 and each_solvent_name == 'HOH':
                                ## GETTING G_R AND R
                                g_r = self.rdfs[pure_water_index[0]].rdf_g_r[0]
                                r   = self.rdfs[pure_water_index[0]].rdf_r[0]
                                ## PLOTTING G_R VS R
                                ax.plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac+1],
                                                label= "Cosolvent mass frac: %.2f"%(0),
                                                **LINE_STYLE)
                            ## CREATING LEGEND
                            ax.legend()
                            ## LABELING FIGURE
                            label = "RDF_mass_frac_%s_%s_%s"%(each_solute, each_solvent,each_solvent_name)
                            ## SAVING FIGURE
                            save_fig_png(fig, label, save_fig, dpi=DPI_LEVEL)
                return
            
        ### FUNCTION TO PLOT OXYGENS
        def plot_rdf_solute_oxy_to_solvent_multiple_mass_frac(self, save_fig=False):
            '''
            The purpose of this function is to plot the solute to solvent for multiple mass fractions
            INPUTS:
                self: class object
                save_fig: True if you want to save all the figures
            OUTPUTS:
                plot of RDF vs distance for different mass fractions of solvents
            '''
            ## LOOPING THROUGH EACH SOLUTE
            for each_solute in self.unique_solute_names:
                ## LOOPING THROUGH EACH COSOLVENT
                for each_solvent in self.find_unique('cosolvent_name'):
                    ## EXCLUDING IF PURE CASE
                    if each_solvent != 'Pure':
                        ## FINDING ALL INDICES THAT HAVE THIS SOLUTE AND SOLVENT
                        mass_frac_indices = [index for index, name_decoded in enumerate(self.names_decoded) \
                                             if name_decoded['solute_residue_name']==each_solute and name_decoded['cosolvent_name'] ==each_solvent]
                        ## FINDING ALL MASS FRACTIONS
                        water_mass_frac_values = [ self.names_decoded[index]['mass_frac_water'] for index in mass_frac_indices]
                        ## SORT BY THE SMALLEST MASS FRACTION OF WATER
                        water_mass_frac_values, mass_frac_indices = (list(t) for t in zip(*sorted(zip(water_mass_frac_values, mass_frac_indices))))
                        ## GETTING MASS FRACTION OF COSOLVENT
                        cosolvent_mass_frac_values = [ self.convert_water_to_cosolvent_mass_frac(each_mass_perc) for each_mass_perc in water_mass_frac_values ]                                                
                        
                        ## CREATING FIGURE AND AXIS
                        figs_axs = [ [[self.create_rdf_plot()][0] for index in range(len(rdfs[mass_frac_indices[0]].solvent_name))]  # Vary by solvent name
                                        for atomname in range(len(rdfs[mass_frac_indices[0]].rdf_oxy_names)) ] # Vary by atom solute name
                        ### LOOPING OVER EACH ATOM NAME
                        for atom_index, atomname in enumerate(rdfs[mass_frac_indices[0]].rdf_oxy_names):
                            
                            ## LOOPING OVER EACH SOLVENT
                            for solvent_index,each_solvent_name in enumerate(rdfs[mass_frac_indices[0]].solvent_name):
                                ## SETTING THE TITLE
                                figs_axs[atom_index][solvent_index][1].set_title("%s-%s --- %s"%(each_solute,atomname, each_solvent_name))
                                ## LOOPING THROUGH EACH MASS FRACTION AND PLOTTING
                                for each_mass_frac in range(len(mass_frac_indices)):
                                    ## GETTING DATA INDEX
                                    data_index = mass_frac_indices[each_mass_frac]
                                    ## GETTING G_R AND R
                                    g_r = self.rdfs[data_index].rdf_oxy_g_r[solvent_index][atom_index]
                                    r   = self.rdfs[data_index].rdf_oxy_r[solvent_index][atom_index]
                                
                                    ## PLOTTING G_R VS R
                                    figs_axs[atom_index][solvent_index][1].plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac],
                                                    label= "Cosolvent mass frac: %.2f"%(cosolvent_mass_frac_values[each_mass_frac]),
                                                    **LINE_STYLE)
                                ## ADDING PLOT IF 100% WATER EXISTS
                                pure_water_index = [index for index, name_decoded in enumerate(self.names_decoded) \
                                                     if name_decoded['solute_residue_name']==each_solute and \
                                                     name_decoded['cosolvent_name'] == 'Pure' and \
                                                     name_decoded['mass_frac_water'] == 100
                                                     ]
                                if len(pure_water_index) !=0 and each_solvent_name == 'HOH':
                                    ## GETTING G_R AND R
                                    g_r = self.rdfs[pure_water_index[0]].rdf_oxy_g_r[0][atom_index]
                                    r   = self.rdfs[pure_water_index[0]].rdf_oxy_r[0][atom_index]
                                    ## PLOTTING G_R VS R
                                    figs_axs[atom_index][solvent_index][1].plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac+1],
                                                    label= "Cosolvent mass frac: %.2f"%(0),
                                                    **LINE_STYLE)
                                ## CREATING LEGEND
                                figs_axs[atom_index][solvent_index][1].legend()
                                ## LABELING FIGURE
                                figs_axs[atom_index][solvent_index][1].label_ = "RDF_mass_frac_%s_%s_%s_%s"%(each_solute, each_solvent,each_solvent_name,atomname)
                                ## SAVING FIGURE
                                # save_fig_png(fig, label, save_fig, dpi=DPI_LEVEL)
                                self.figs_axs = figs_axs[:]
                        ## SAVING FIGURE IF NECESSARY
                        [ [save_fig_png(fig = figs_axs[atom_index][solvent_index][0],
                                         label=figs_axs[atom_index][solvent_index][1].label_, 
                                         save_fig=save_fig)] 
                                    for solvent_index in range(len(rdfs[mass_frac_indices[0]].solvent_name)) # Vary by solvent name
                                    for atom_index in range(len(rdfs[mass_frac_indices[0]].rdf_oxy_names)) ] # Vary by atom solute name
            return
    ## CLOSING ALL FIGURES
    plt.close('all')    
            
    multi_rdf = multi_plot_rdf(rdfs = rdfs,
                               names = list_of_pickles,
                               decode_type = 'solvent_effects',
                               )


    #%%
    ## PLOTTING
    multi_rdf.plot_rdf_solute_oxy_to_solvent_multiple_mass_frac(True)
    
    
    