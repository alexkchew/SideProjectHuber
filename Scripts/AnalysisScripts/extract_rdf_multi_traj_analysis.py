# -*- coding: utf-8 -*-
"""
extract_rdf_multi_traj_analysis.py
The purpose of this script is to analyze the data from the multi_traj_analysis_tool.py for RDFs

Author(s):
    Alex K. Chew (alexkchew@gmail.com)
"""

### IMPORTING FUNCTION TO GET TRAJ
from MDDescriptors.traj_tools.multi_traj import load_multi_traj_pickle
### IMPORTING FUNCTION TO FIND ALL PICKLES
from MDDescriptors.traj_tools.multi_traj import find_multi_traj_pickle
### IMPORTING PLOTTING FUNCTIONS
from MDDescriptors.geometry.plot_rdf import plot_rdf
import sys
## IMPORTING DECODER FUNCTIONS
from MDDescriptors.core.decoder import decode_name




#%% MAIN SCRIPT
if __name__ == "__main__":
    

    from MDDescriptors.geometry.rdf import calc_rdf
    ## DEFINING CLASS
    Descriptor_class = calc_rdf
    
    ## DEFINING DATE
    Date='180430'
    
    
    #%%
    ## DEFINING DESIRED DIRECTORY
    # Pickle_loading_file=r"mdRun_433.15_6_nm_ACE_50_WtPercWater_spce_dmso"
    Pickle_loading_file=r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_sulfolone"
    
    '''
    mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_NN-dimethylacetamide
    mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_NN-dimethylformamide
    mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_urea
    '''
    
    #### SINGLE TRAJ ANALYSIS
    multi_traj_results = load_multi_traj_pickle(Date, Descriptor_class, Pickle_loading_file )
    ### EXTRACTING THE RDF
    rdf = multi_traj_results
    plot_rdf_solute_solvent = plot_rdf(rdf)
    plot_rdf_solute_solvent.plot_rdf_solute_oxygen_to_solvent(True)
    # plot_rdf_solute_solvent.plot_rdf_solute_solvent(True)
    # plot_rdf_solute_solvent.plot_rdf_solute_solvent(True)
    
    
    #%%
    ##### MULTI TRAJ ANALYSIS
    list_of_pickles = find_multi_traj_pickle(Date, Descriptor_class)

    rdfs=[]
    
    for Pickle_loading_file in list_of_pickles:
        ### EXTRACTING THE DATA
        multi_traj_results = load_multi_traj_pickle(Date, Descriptor_class, Pickle_loading_file )
        ### EXTRACTING THE RDF
        rdf = multi_traj_results
        ### TURNING OFF SOLUTE AND SOLVENT COM
        rdf.solute_COM = []; rdf.solvent_COM = []
        ### STORING THE RDFS
        rdfs.append(rdf)
    
    
    ### PLOTTING
    [plot_rdf(rdf).plot_rdf_solute_oxygen_to_solvent(True) for rdf in rdfs]
    
    
    #%%
    ### DEFINING GLOBAL PLOTTING VARIABLES
    FONT_SIZE=16
    FONT_NAME="Arial"    
    
    ### DEFINING COLOR LIST
    COLOR_LIST=['k','b','r','g','m','y','k','w']
    
    ### DEFINING LINE STYLE
    LINE_STYLE={
                "linewidth": 1.4, # width of lines
                }
    ### DEFINING SAVING STYLE
    DPI_LEVEL=600
    
    ### IMPORTING MODULES
    import matplotlib.pyplot as plt
    from MDDescriptors.core.plot_tools import save_fig_png # code to save figures
    
    #########################################################################
    ### CLASS FUNCTION TO PLOTTING MULTIPLE RADIAL DISTRIBUTION FUNCTIONS ###
    #########################################################################
    class multi_plot_rdf:
        '''
        The purpose of this class is to take multiple class_rdf classes and plot it accordingly
        INPUTS:
            rdfs: list of calc_rdf classes
            names: list of names associated with each rdf
            decode_type: string denoting way to decode the names
        OUTPUTS:
            ## INPUTS
                self.rdfs: rdfs
                self.names: Names of the directories
                self.decode_type: decoding type for the directories
            ## DECODING
                self.names_decoded: decoded names
                self.unique_solute_names: unique solute names
            
            
        FUNCTIONS:
            find_unique: finds unique decoders
            convert_water_to_cosolvent_mass_frac: converts water mass fraction to cosolvent
            
        ACTIVE FUNCTIONS:
            plot_rdf_solute_solvent_multiple_mass_frac: Plots rdf solute to solvent for multiple mass fractions
            plot_rdf_solute_oxy_to_solvent_multiple_mass_frac: plots rdf of solute's oxygen to solvent for multiple mass fractions
        '''
        ### INITIALIZATION
        def __init__(self, rdfs, names, decode_type='solvent_effects'):
            ## DEFINING ORGANIZATION LEVELS
            self.organization_levels = [ 'solute_residue_name', 'cosolvent_name', 'mass_frac_water' ]
            
            ## STORING INPUTS
            self.rdfs = rdfs
            self.names = names
            self.decode_type = decode_type
            
            ## DECODING NAMES
            self.names_decoded = [decode_name(name=name,decode_type=decode_type) for name in self.names]
            
            ## FINDING UNIQUE SOLUTE NAMES
            self.unique_solute_names = self.find_unique('solute_residue_name')
            
            ## PLOTTING RDF FOR DIFFERENT MASS FRACTIONS
            # self.plot_rdf_solute_solvent_multiple_mass_frac()

        ### FUNCTION TO FIND ALL UNIQUE RESIDUES
        def find_unique(self,decoding_name):
            '''
            The purpose of this function is to find all unique solutes
            INPUTS:
                self: class property
                decoding_name: decoding name
                    e.g. 'solute_residue_name', etc.
            OUTPUTS:
                unique_names: unique names
            '''
            unique_names = list(set([each_decoded_name[decoding_name] for each_decoded_name in self.names_decoded]))
            return unique_names
            
        ### FUNCTION TO CREATE RDF PLOT
        def create_rdf_plot(self):
            '''
            The purpose of this function is to generate a figure for you to add your RDFs.
            Inputs:
                fontSize: Size of font for x and y labels
                fontName: Name of the font
            Output:
                fig: Figure to print
                ax: Axes to plot on
            '''
            ## CREATING PLOT
            fig = plt.figure() 
            ax = fig.add_subplot(111)
        
            ## DRAWING LABELS
            ax.set_xlabel('r (nm)',fontname=FONT_NAME,fontsize = FONT_SIZE)
            ax.set_ylabel('Radial Distribution Function',fontname=FONT_NAME,fontsize = FONT_SIZE)
            
            # Drawing ideal gas line
            ax.axhline(y=1, linewidth=1, color='black', linestyle='--')
            
            return fig, ax
        
        ### FUNCTION TO CONVERT MASS FRACTION FROM WATER TO COSOLVENT
        @staticmethod
        def convert_water_to_cosolvent_mass_frac(mass_frac_water_perc):
            '''
            The purpose of this script is to convert mass fraction from water to cosolvent
            INPUTS:
                mass_frac_water_perc: mass fraction of water (as a percent, e.g. 10)
            OUTPUTS:
                mass_frac_cosolvent: mass fraction of cosolvent (e.g. 0.90)
            '''
            return (100 - mass_frac_water_perc)/float(100)
        
        ### FUNCTION TO PLOT FOR DIFFERENT MASS FRACTIONS
        def plot_rdf_solute_solvent_multiple_mass_frac(self, save_fig=False):
            '''
            The purpose of this function is to plot the solute to solvent for multiple mass fractions
            INPUTS:
                self: class object
                save_fig: True if you want to save all the figures
            OUTPUTS:
                plot of RDF vs distance for different mass fractions of solvents
            '''
            ## LOOPING THROUGH EACH SOLUTE
            for each_solute in self.unique_solute_names:
                ## LOOPING THROUGH EACH COSOLVENT
                for each_solvent in self.find_unique('cosolvent_name'):
                    ## EXCLUDING IF PURE CASE
                    if each_solvent != 'Pure':
                        ## FINDING ALL INDICES THAT HAVE THIS SOLUTE AND SOLVENT
                        mass_frac_indices = [index for index, name_decoded in enumerate(self.names_decoded) \
                                             if name_decoded['solute_residue_name']==each_solute and name_decoded['cosolvent_name'] ==each_solvent]
                        ## FINDING ALL MASS FRACTIONS
                        water_mass_frac_values = [ self.names_decoded[index]['mass_frac_water'] for index in mass_frac_indices]
                        ## SORT BY THE SMALLEST MASS FRACTION OF WATER
                        water_mass_frac_values, mass_frac_indices = (list(t) for t in zip(*sorted(zip(water_mass_frac_values, mass_frac_indices))))
                        ## GETTING MASS FRACTION OF COSOLVENT
                        cosolvent_mass_frac_values = [ self.convert_water_to_cosolvent_mass_frac(each_mass_perc) for each_mass_perc in water_mass_frac_values ]                                                
                        
                        ## RDF -- SOLUTE - SOLVENT
                        for solvent_index,each_solvent_name in enumerate(rdfs[mass_frac_indices[0]].solvent_name):
                            ## CREATING RDF PLOT
                            fig, ax = self.create_rdf_plot()
                            ## SETTING THE TITLE
                            ax.set_title("%s --- %s"%(each_solute, each_solvent_name))
                            ## LOOPING THROUGH EACH MASS FRACTION AND PLOTTING
                            for each_mass_frac in range(len(mass_frac_indices)):
                                ## GETTING DATA INDEX
                                data_index = mass_frac_indices[each_mass_frac]
                                ## GETTING G_R AND R
                                g_r = self.rdfs[data_index].rdf_g_r[solvent_index]
                                r   = self.rdfs[data_index].rdf_r[solvent_index]
                                ## PLOTTING G_R VS R
                                ax.plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac],
                                                label= "Cosolvent mass frac: %.2f"%(cosolvent_mass_frac_values[each_mass_frac]),
                                                **LINE_STYLE)
                            ## ADDING PLOT IF 100% WATER EXISTS
                            pure_water_index = [index for index, name_decoded in enumerate(self.names_decoded) \
                                                 if name_decoded['solute_residue_name']==each_solute and \
                                                 name_decoded['cosolvent_name'] == 'Pure' and \
                                                 name_decoded['mass_frac_water'] == 100
                                                 ]
                            if len(pure_water_index) !=0 and each_solvent_name == 'HOH':
                                ## GETTING G_R AND R
                                g_r = self.rdfs[pure_water_index[0]].rdf_g_r[0]
                                r   = self.rdfs[pure_water_index[0]].rdf_r[0]
                                ## PLOTTING G_R VS R
                                ax.plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac+1],
                                                label= "Cosolvent mass frac: %.2f"%(0),
                                                **LINE_STYLE)
                            ## CREATING LEGEND
                            ax.legend()
                            ## LABELING FIGURE
                            label = "RDF_mass_frac_%s_%s_%s"%(each_solute, each_solvent,each_solvent_name)
                            ## SAVING FIGURE
                            save_fig_png(fig, label, save_fig, dpi=DPI_LEVEL)
                return
            
        ### FUNCTION TO PLOT OXYGENS
        def plot_rdf_solute_oxy_to_solvent_multiple_mass_frac(self, save_fig=False):
            '''
            The purpose of this function is to plot the solute to solvent for multiple mass fractions
            INPUTS:
                self: class object
                save_fig: True if you want to save all the figures
            OUTPUTS:
                plot of RDF vs distance for different mass fractions of solvents
            '''
            ## LOOPING THROUGH EACH SOLUTE
            for each_solute in self.unique_solute_names:
                ## LOOPING THROUGH EACH COSOLVENT
                for each_solvent in self.find_unique('cosolvent_name'):
                    ## EXCLUDING IF PURE CASE
                    if each_solvent != 'Pure':
                        ## FINDING ALL INDICES THAT HAVE THIS SOLUTE AND SOLVENT
                        mass_frac_indices = [index for index, name_decoded in enumerate(self.names_decoded) \
                                             if name_decoded['solute_residue_name']==each_solute and name_decoded['cosolvent_name'] ==each_solvent]
                        ## FINDING ALL MASS FRACTIONS
                        water_mass_frac_values = [ self.names_decoded[index]['mass_frac_water'] for index in mass_frac_indices]
                        ## SORT BY THE SMALLEST MASS FRACTION OF WATER
                        water_mass_frac_values, mass_frac_indices = (list(t) for t in zip(*sorted(zip(water_mass_frac_values, mass_frac_indices))))
                        ## GETTING MASS FRACTION OF COSOLVENT
                        cosolvent_mass_frac_values = [ self.convert_water_to_cosolvent_mass_frac(each_mass_perc) for each_mass_perc in water_mass_frac_values ]                                                
                        
                        ## CREATING FIGURE AND AXIS
                        figs_axs = [ [[self.create_rdf_plot()][0] for index in range(len(rdfs[mass_frac_indices[0]].solvent_name))]  # Vary by solvent name
                                        for atomname in range(len(rdfs[mass_frac_indices[0]].rdf_oxy_names)) ] # Vary by atom solute name
                        ### LOOPING OVER EACH ATOM NAME
                        for atom_index, atomname in enumerate(rdfs[mass_frac_indices[0]].rdf_oxy_names):
                            
                            ## LOOPING OVER EACH SOLVENT
                            for solvent_index,each_solvent_name in enumerate(rdfs[mass_frac_indices[0]].solvent_name):
                                ## SETTING THE TITLE
                                figs_axs[atom_index][solvent_index][1].set_title("%s-%s --- %s"%(each_solute,atomname, each_solvent_name))
                                ## LOOPING THROUGH EACH MASS FRACTION AND PLOTTING
                                for each_mass_frac in range(len(mass_frac_indices)):
                                    ## GETTING DATA INDEX
                                    data_index = mass_frac_indices[each_mass_frac]
                                    ## GETTING G_R AND R
                                    g_r = self.rdfs[data_index].rdf_oxy_g_r[solvent_index][atom_index]
                                    r   = self.rdfs[data_index].rdf_oxy_r[solvent_index][atom_index]
                                
                                    ## PLOTTING G_R VS R
                                    figs_axs[atom_index][solvent_index][1].plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac],
                                                    label= "Cosolvent mass frac: %.2f"%(cosolvent_mass_frac_values[each_mass_frac]),
                                                    **LINE_STYLE)
                                ## ADDING PLOT IF 100% WATER EXISTS
                                pure_water_index = [index for index, name_decoded in enumerate(self.names_decoded) \
                                                     if name_decoded['solute_residue_name']==each_solute and \
                                                     name_decoded['cosolvent_name'] == 'Pure' and \
                                                     name_decoded['mass_frac_water'] == 100
                                                     ]
                                if len(pure_water_index) !=0 and each_solvent_name == 'HOH':
                                    ## GETTING G_R AND R
                                    g_r = self.rdfs[pure_water_index[0]].rdf_oxy_g_r[0][atom_index]
                                    r   = self.rdfs[pure_water_index[0]].rdf_oxy_r[0][atom_index]
                                    ## PLOTTING G_R VS R
                                    figs_axs[atom_index][solvent_index][1].plot(r, g_r, '-', color = COLOR_LIST[each_mass_frac+1],
                                                    label= "Cosolvent mass frac: %.2f"%(0),
                                                    **LINE_STYLE)
                                ## CREATING LEGEND
                                figs_axs[atom_index][solvent_index][1].legend()
                                ## LABELING FIGURE
                                figs_axs[atom_index][solvent_index][1].label_ = "RDF_mass_frac_%s_%s_%s_%s"%(each_solute, each_solvent,each_solvent_name,atomname)
                                ## SAVING FIGURE
                                # save_fig_png(fig, label, save_fig, dpi=DPI_LEVEL)
                                self.figs_axs = figs_axs[:]
                        ## SAVING FIGURE IF NECESSARY
                        [ [save_fig_png(fig = figs_axs[atom_index][solvent_index][0],
                                         label=figs_axs[atom_index][solvent_index][1].label_, 
                                         save_fig=save_fig)] 
                                    for solvent_index in range(len(rdfs[mass_frac_indices[0]].solvent_name)) # Vary by solvent name
                                    for atom_index in range(len(rdfs[mass_frac_indices[0]].rdf_oxy_names)) ] # Vary by atom solute name
            return
    ## CLOSING ALL FIGURES
    plt.close('all')    
            
    multi_rdf = multi_plot_rdf(rdfs = rdfs,
                               names = list_of_pickles,
                               decode_type = 'solvent_effects',
                               )


    #%%
    ## PLOTTING
    multi_rdf.plot_rdf_solute_oxy_to_solvent_multiple_mass_frac(True)
    
    
    