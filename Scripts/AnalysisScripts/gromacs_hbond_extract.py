# -*- coding: utf-8 -*-
"""
gromacs_hbond_extract.py
The purpose of this script is to coincide with the gromacs_hbond_extract.sh script. This will be called to extract xvg files and find the appropiate numbers. Then, this will output the average hydrogen bonding, std, etc.

CLASSES:
    extract_xvg_hbond: extracts xvg hydrogen bonding

FUNCTIONS:
    decode_hbond_xvg_name: decodes hydrogen bond xvg name
    export_dict_to_csv: exports dictionary to csv

ALGORITHM:
    - read in xvg file
    - run class to extract the hydrogen bonding information
    - run a decoder for the directory name
    - run a decoder for the hydrogen bonding directory name
    - output all the information into a csv (read in csv if the file already exists)

CREATED ON: 04/17/2018
AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
"""

### IMPORTING MODULES
import numpy as np
from MDDescriptors.core.decoder import decode_name # Decoding the name of the directory
from MDDescriptors.core.read_write_tools import read_xvg # Reading xvg function
from MDDescriptors.core.calc_tools import merge_two_dicts # Merging two dictionaries

#############################################
### CLASS FUNCTION TO READ XVG HBOND DATA ###
#############################################
class extract_xvg_hbond:
    '''
    The purpose of this function is to extract num hbond from GROMACS xvg output
    INPUTS:
        xvg: class from read_xvg
    OUTPUTS:
        self.xvg: input data
        ## HYDROGEN BONDING RESULTS
        self.hbond_data: [dict] contains dictionary of the hydrogen bonding data:
            'Time_ps': [list] time in picoseconds
            'hbonds': [list] number of hydrogen bonds at a specific time
            'pairs_within_nm': [list] number of pairs within a threshold nm (typically 0.35 nm)
        ## AVERAGE AND STD OF HYDROGEN BONDS
        self.hbond_avg: [float] average number of hydrogen bonds for the trajectory
        self.hbond_std: [float] standard deviation of the number of hydrogen bonds
    FUNCTIONS:
        extract_xvg_hbond: Extract hydrogen bonding information from xvg file
        calc_avg_std_hbond: Calculates average and standard deviation of hydrogen bonds
    ALGORITHM:
        - Extract the hydrogen bonds from xvg file
        - Calculate mean and standard deviation of the number of hydrogen bonds
        
    '''
    ### INITIALIZING
    def __init__(self, xvg):
        ## STORING INPUTS
        self.xvg = xvg
        
        ## EXTRACTING NUMBER OF HYDROGEN BONDS
        self.extract_xvg_hbond()

        ## FINDING AVERAGE NUMBER OF HYDROGEN BONDS, ETC.
        self.calc_avg_std_hbond()
        
    ### FUNCTION TO EXTRACT THE NUMBER OF HYDROGEN BONDS PER FRAME, ETC.
    def extract_xvg_hbond(self):
        '''
        The purpose of this function is to extract the xvg hydrogen bonds as a form of a dictionary
        INPUTS:
            self: class object
        OUTPUTS:
            self.hbond_data: [dict] contains dictionary of the hydrogen bonding data:
                'Time_ps': [list] time in picoseconds
                'hbonds': [list] number of hydrogen bonds at a specific time
                'pairs_within_nm': [list] number of pairs within a threshold nm (typically 0.35 nm)
        '''
        ## CREATING DICTIONARY
        self.hbond_data={
                         'Time_ps':         [],     # Time step in picoseconds
                         'hbonds' :         [],     # Number of hydrogen bonds within the time step
                         'pairs_within_nm': [],     # Number of pairs within some threshold nm
                            }
        for each_data in self.xvg.xvg_data:
            self.hbond_data['Time_ps'].append(int(each_data[0]))
            self.hbond_data['hbonds'].append(int(each_data[1]))
            self.hbond_data['pairs_within_nm'].append(int(each_data[2]))
        return
    ### FUNCTION TO FIND AVERAGE HYDROGEN BONDS, ETC.
    def calc_avg_std_hbond(self):
        '''
        The purpose of this function is to find the average and standard deviation of hydrogen bonds
        INPUTS:
            self: class object
        OUTPUTS:
            self.hbond_avg: [float] average number of hydrogen bonds for the trajectory
            self.hbond_std: [float] standard deviation of the number of hydrogen bonds
        '''
        ## FINDING AVERAGE
        self.hbond_avg = np.mean(self.hbond_data['hbonds'])
        self.hbond_std = np.std(self.hbond_data['hbonds'])
    
### FUNCTION TO DECODE XVG NAME
def decode_hbond_xvg_name(xvg_file_name):
    '''
    This function simply decodes the xvg file name. Assumption that it looks something like this: hbnum_total_ACE_SOL.xvg
    Therefore, we can split the name based on residue 1 and residue 2 as Group_1 and Group_2
    INPUTS:
        xvg_file_name:[str] name of the xvg file
    OUTPUTS:
        decoded_name: [dict] dictionary of the decoded name
    '''
    ## STRIPPING XVG FROM FILE NAME
    xvg_file_name = xvg_file_name.strip('.xvg')
    ## SPLITTING TO LIST
    xvg_file_name = xvg_file_name.split('_')
    ## DEFINING GROUPS 1 AND GROUPS 2
    decoded_name = {
                    'Group_1': xvg_file_name[-2], # e.g. ACE
                    'Group_2': xvg_file_name[-1], # e.g. SOL
                    }
    return decoded_name
    
### FUNCTION TO EXPORT TO FILE
def export_dict_to_csv(extract_dict, file_name='export.csv'):
    '''
    The purpose of this function is to export a dictionary to a csv file. We will try to find similar names to output as a full list
    INPUTS:
        extract_dict: [dict] Extracted dictionary with a single key and single variable
    OUTPUTS:
        dataframe: [pd.Dataframe] pandas database of the information that you have given it
    '''
    ## IMPORTING DATABASE MODULE
    import pandas as pd
    import os.path
    
    ## CREATING A DATAFRAME
    new_dataframe = pd.DataFrame([extract_dict])
    
    ## CHECKING IF CSV FILE EXISTS
    if os.path.isfile(file_name) is True:
        ## IMPORTING THE CSV FILE
        print("IMPORTING %s"%(file_name))
        dataframe_import = pd.read_csv(file_name,index_col=0) # index column is noted
        ## CONCATENATE, DROP DUPLICATES, AND RESET THE INDEX
        dataframe = pd.concat( (dataframe_import, new_dataframe), axis = 0 , ignore_index  = True).drop_duplicates().reset_index(drop=True)
    else:
        ## Simply copy the dataframe
        dataframe = new_dataframe
    ## OUTPUTTING DATABASE INTO CSV
    print('WRITING OUTPUT TO %s'%(file_name))
    dataframe.to_csv(file_name)
    return dataframe

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### SEEING IF TEST IS TRUE
    testing = False # False if you are on command line
    
    ################################
    ### DEFINING INPUT VARIABLES ###
    ################################
    if testing is True:
        ## DEFINING DIRECTORY STRUCTURE
        directory_name = r"mdRun_433.15_6_nm_ACE_10_WtPercWater_spce_dioxane"
        xvg_file_name = r"hbnum_total_ACE_SOL.xvg"
        path = r"R:\scratch\SideProjectHuber\Analysis\180413-PDO_PRO_COMBINED\ACE"

        ## DEFINING OUTPUT FILE
        output_csv = 'hbond_export.csv'
    else:
        ## ON COMMAND LINE
        from optparse import OptionParser # Used to allow commands within command line
        ### ADDING OPTIONS TO COMMAND LINE
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ### ADDING OPTIONS
        parser.add_option("--path", dest="path", action="store", type="string", help="Path to input directory", default="path")
        parser.add_option("--dir", dest="input_dir", action="store", type="string", help="Input directory", default="test_dir")
        parser.add_option("--xvg", dest="xvg_file_name", action="store", type="string", help="XVG file name", default="test_dir")
        parser.add_option("--out", dest="output_csv", action="store", type="string", help="Output csv name", default="output.csv")
        
        ### TAKING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## EXTRACTING ARGUMENTS
        path            = options.path                  # Path to input directory
        directory_name  = options.input_dir             # Directory input
        xvg_file_name   = options.xvg_file_name         # XVG FILE NAME
        output_csv      = options.output_csv            # Output CSV file name
        
    ###################
    ### MAIN SCRIPT ###
    ###################
    
    ## DEFINING FULL PATH TO XVG FILE
    xvg_file= path + '/' + directory_name + "/" + "output_hbond" + "/" + xvg_file_name
        
    ### RUNNING CLASS FUNCTION TO READ THE XVG FILE
    xvg = read_xvg(xvg_file)
    
    ### RUNNING CLASS TO EXTRACT HYDROGEN BONDS
    hbonds = extract_xvg_hbond(xvg)
    
    ### DECODING THE NAME
    decoded_name = decode_name(directory_name, 'solvent_effects')
    
    ### DECODING THE HYDROGEN BOND XVG NAME
    decoded_xvg_name = decode_hbond_xvg_name(xvg_file_name)
    
    ### MERGING DICTIONARIES AND INCLUDING THE RESULTS
    extract = merge_two_dicts(decoded_name, decoded_xvg_name)
    
    ### STORING HYDROGEN BONDING INFORMATION
    extract['num_hbond_avg'] = hbonds.hbond_avg
    extract['num_hbond_std'] = hbonds.hbond_std
    
    ### EXTRACTING INFORMATION
    dataframe = export_dict_to_csv(extract, output_csv)
    