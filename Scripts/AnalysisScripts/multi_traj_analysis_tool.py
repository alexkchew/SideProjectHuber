# -*- coding: utf-8 -*-
"""
multi_traj_analysis_tool.py
The purpose of this script is to load all trajectories, use a corresponding function to analyze the trajectories, then save the information for subsequent plotting/analysis

INPUTS:
    directories
    files within directories that you are interested in

Written by: Alex K. Chew (alexkchew@gmail.com, 03/02/2018)
"""
### IMPORTING MODULES
from MDDescriptors.traj_tools.multi_traj import multi_traj_analysis, add_to_all_dicts_of_dicts

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### DEFINING LOGICALS
    want_RDF                      = False                    # For radial distribution functions -- contains cumulative distribution functions
    want_pref_int                 = False                   # For preferential interaction coefficients
    want_accessible_hydroxyl_frac = False                   # For accessible hydroxyl fraction
    want_prob_density_map         = False # True                   # For probability density maps
    want_h_bonds                  = True                   # For hydrogen bonding details
    want_general_traj             = False                   # For general trajectory information  
    
    ## GROMACS LOGICALS
    want_extract_gmx_dipoles      = False                    # True if you want gmx dipoles extraction
    want_extract_gmx_potential_density  = False                    # True if you want gmx dipoles extraction
    
    ### FOLDER DEFINITIONS
    ### PARENT PATH TO THE MAIN DIRECTORY
    Main_Directory_Parent_Path = r"R:\scratch\SideProjectHuber\Analysis\\"
    ### DIRECTORY TO WORK ON
    # Main_Directory = r"180312-PDO_PRO_ACE_Spatial_Mapping"
    # Main_Directory = r"180319-PDO_PRO_ACE_DIO_DMSO_SIMS"
    # Main_Directory = r"FINAL_PDO_PRO_ACE_DIO_DMSO" # r"180531-HMF_DMSO_sims"
    Main_Directory = r"190114-Expanded_NoSolute_Pure_Sims" # r"180531-HMF_DMSO_sims" 180928-HYD_Solute_Solvent_LINCS3_normal_MD_runs 190107-HYD_Normal_MD_Runs
    Main_Directory = r"190118-FINAL_HYD_NORMAL_MD_90WT_DIO_DMSO"
    Main_Directory = r"190118-NoSolute_DIO_DMSO"
    Main_Directory = r"190621-THD_CHD_Most_likely"
    Main_Directory = r"190207-PDO_Mostlikely_Configs_10000"
    Main_Directory = r"190713-GLY_BTL_sims_most_likely"
    Main_Directory = r"190922-PDO_most_likely_gauche"
    
    ## DEFINING DIRECTORIES
    directory_dict = {
#            'ACE': r"190612-FRU_HMF_Run",
#            'MECN': r'190614-Run_MeCN',
#            'DMSO': r'190305-TBA_FRU_MIXED_SOLV_DMSO',
            # 'PDO_trans': r"190207-PDO_Mostlikely_Configs_10000", # Default
            'THD_CHD':"190621-THD_CHD_Most_likely"
            # 'PDO_gauche': r"190922-PDO_most_likely_gauche",
            }
    
    ## LOOPING
    for each_key in directory_dict:
        Main_Directory = directory_dict[each_key]
    
        ## TURNING ON WANT ONLY DIRECTORIES (Does not load trajectories!)
        if want_extract_gmx_dipoles is True or want_extract_gmx_potential_density is True:
            want_only_directories = True
        else:
            want_only_directories = False
        
        ### FILES WITHIN MAIN DIRECTORIES (LIST)
        Categories = [  
                        #   'XYL',  
                        #  'HYDTEST2',
                        # 'NoSolute',
                        # 'HYDTEST'
                        # 'HYD',
                        'CHD',
                        'THD',
                         #  'PDO',
#                          'FRU',
#                          'tBuOH',
#                          'PDO',
#                          'GLU',
                          # 'GLY',
                          # 'BTL',
                          # 'PRO',
                         # 'ACE',
                        # 'TET',
    
                        # 'FRI',
                        # 'tBuOH',
                        # 'GLY',
                        #  'HMF',
                        #  'GLU',
                        # 'FRU',
                        # 'CLL',
                        # 'HBU',
                        # 'DIM',
                        # 'HYA',
                        # 'HDH',
                        # 'LEV',
                     ]
    
        ## DEFINING FILES WITHIN MAIN DIRECTORY
        Specific_dir_within_category = { 
                                            # 'PDO': [ # 'mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane',
                                                     # 'mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dmso',
                                                     # 'mdRun_433.15_6_nm_PDO_100_WtPercWater_spce_Pure',
                                                       # ],     
    #                                        'PRO': [ 'mdRun_433.15_6_nm_PRO_10_WtPercWater_spce_GVL_L',
    #                                                   ],     
    #                                        'ACE': [ 'mdRun_433.15_6_nm_ACE_10_WtPercWater_spce_GVL_L',
    #                                                   ],
                                            'HYD': [
                                                    'mdRun_300.00_6_nm_HYD_100_WtPercWater_spce_Pure',
                                                    ]
        }
        Specific_dir_within_category = None
        
        ### DEFINING FILES WITHIN CATEGORIES THAT ARE OF INTEREST
        Files = { 
                 # 'structure_file': 'mixed_solv_prod.gro', # For spatial dist map, etc.
                 'structure_file': 'mixed_solv_prod_structure.pdb', # <-- turn on for H-bonding
                 # 'xtc_file': 'mixed_solv_prod_10_ns_whole.xtc',
                 # 'xtc_file': 'mixed_solv_prod_whole.xtc',
                 # 'xtc_file': 'mixed_solv_prod_first_10_ns.xtc', #  
                 # 'xtc_file': r"mixed_solv_prod_first_20_ns_centered_with_10ns.xtc",
                 'xtc_file': 'mixed_solv_prod_10ns_to_200ns_pbcmol.xtc', # SPATIAL DIST MAPS <-- for PDO dehydration -- DIO and DMSO -- spatial dist. maps
                 # 'xtc_file': 'mixed_solv_prod_10ns_to_100ns_pbcmol_rot_trans_center.xtc',
                 # 'xtc_file': 'mixed_solv_prod_5_ns_center.xtc',
                 # 'xtc_file': 'mixed_solv_prod_10_ns_whole.xtc',
                 # 'xtc_file': 'mixed_solv_prod_10_ns_center_rot_trans_center.xtc',# <-- SPATIAL DISTRIBUTION MAPS
                }
        ### DEFINING EMPTY LISTS
        Descriptor_Class_List, Descriptor_Inputs_List= [], []
            
        ### CUSTOM LISTS FOR THIS PROJECT
        solvent_lists = ['HOH', 'GVLL', 'dmso', 'DIO','THF','URE','DMF','DMA','NMP','BUT','SUL','TET', 'ACE'] # 'ACE'
        if 'ACE' in Categories:
            solvent_lists.remove('ACE')
        
        ####################################
        ### RADIAL DISTRIBUTION FUNCTION ###
        ####################################
        if want_RDF is True:
            from MDDescriptors.application.solvent_effects.rdf import calc_rdf
            ## APPENDING THE CLASS
            Descriptor_Class_List.append(calc_rdf)
            ## DESCRIPTOR INPUTS
            Descriptor_Inputs = {
                                    'ACE':
                                        {
                                            'solute_name'        : ['ACE'],           # Solute of interest
                                        },
                                    'HYD':
                                        {
                                            'solute_name'        : ['HYD'],           # Solute of interest
                                        },
                                    'CLL':
                                        {
                                            'solute_name'        : ['CLL'],           # Solute of interest
                                        },
                                    'PRO':
                                        {
                                            'solute_name'        : ['PRO'],           # Solute of interest
                                        },
                                    'PDO':
                                        {
                                            'solute_name'        : ['PDO'],           # Solute of interest
                                        },
                                    'GLY':
                                        {
                                            'solute_name'        : ['GLY'],           # Solute of interest
                                        },
                                    'DIM':
                                        {
                                            'solute_name'        : ['DIM'],           # Solute of interest
                                        },
                                    'HBU':
                                        {
                                            'solute_name'        : ['HBU'],           # Solute of interest
                                        },
                                    'FRU':
                                        {
                                            'solute_name'        : ['FRU'],           # Solute of interest
                                        },
                                    'HMF':
                                        {
                                            'solute_name'        : ['HMF'],           # Solute of interest
                                        },
                                    'GLU':
                                        {
                                            'solute_name'        : ['GLU'],           # Solute of interest
                                        },
                                    'HYA':
                                        {
                                            'solute_name'        : ['HYA'],           # Solute of interest
                                        },
                                    'HDH':
                                        {
                                            'solute_name'        : ['HDH'],           # Solute of interest
                                        },
                                    'LEV':
                                        {
                                            'solute_name'        : ['LEV'],           # Solute of interest
                                        },
                                    'HYDTEST':
                                        {
                                            'solute_name'        : ['XYL','HYD'],           # Solute of interest
                                        },
                                    'HYDTEST2':
                                        {
                                            'solute_name'        : ['XYL','HYD'],           # Solute of interest
                                        },
                                    'XYL':
                                        {
                                            'solute_name'        : ['XYL'],           # Solute of interest
                                        },
                                    }
            ## INCLUDING TO ALL INPUTS
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'solvent_name', solvent_lists[:])  # Solvents you want radial distribution functions for
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'bin_width', 0.02)         # 0.02 Bin width of the radial distribution function
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'cutoff_radius', 2.00)     # Radius of cutoff for the RDF (OPTIONAL)
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_oxy_rdf', True)     # True if you want oxygen rdfs
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_oxy_rdf_all_solvent', True)     # True if you want mole fractions
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_solute_all_solvent_rdf', False)     # True if you want the solvents to be considered as a single solvent, then calcualte an RDF for it
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_composition_with_r', False)     # True if you want mole fractions
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'split_rdf', 1)     # 2  >1 if you want rdf splits to occur
            # Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'frames', [250, 500, 750, 1000, 2000, 5000, 7000, 9000, -1] )     # True if you want oxygen rdfs
            # Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_reverse_frames', True )     # True if you want oxygen rdfs
            ## FOR REVERSE FRAMES
            # Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'frames', [0, 50, 100,200, 500, 1000, 2000, 5000] )     # True if you want oxygen rdfs 
            # gmx trjconv -s mixed_solv_prod.tpr -f mixed_solv_prod.xtc -e 10000 -o mixed_solv_prod_first_10_ns.xtc -pbc whole
            # gmx trjconv -s mixed_solv_prod.tpr -f mixed_solv_prod.xtc -o mixed_solv_prod_whole.xtc -pbc whole
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
                                        
        ############################################
        ### PREFERENTIAL INTERACTION COEFFICIENT ###
        ############################################
        if want_pref_int is True:
            from MDDescriptors.application.solvent_effects.preferential_interaction_coefficients import calc_pref_interaction_coefficients
            ## APPENDING THE CLASS
            Descriptor_Class_List.append(calc_pref_interaction_coefficients)
            ## DESCRIPTOR INPUTS
            Descriptor_Inputs = {
                                    'ACE':
                                        {
                                            'solute_name'        : ['ACE'],           # Solute of interest
                                        },
                                    'PRO':
                                        {
                                            'solute_name'        : ['PRO'],           # Solute of interest
                                        },
                                    'PDO':
                                        {
                                            'solute_name'        : ['PDO'],           # Solute of interest
                                        },
                                    'GLY':
                                        {
                                            'solute_name'        : ['GLY'],           # Solute of interest
                                        },
                                    'FRU':
                                        {
                                            'solute_name'        : ['FRU'],           # Solute of interest
                                        },
                                    'GLU':
                                        {
                                            'solute_name'        : ['GLU'],           # Solute of interest
                                        },
                                    'FRI':
                                        {
                                            'solute_name'        : ['FRI'],           # Solute of interest
                                        },
                                    'HMF':
                                        {
                                            'solute_name'        : ['HMF'],           # Solute of interest
                                        },
                                    'tBuOH':
                                        {
                                            'solute_name'        : ['tBuOH'],           # Solute of interest
                                        },
                                    }
            ## INCLUDING TO ALL INPUTS
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'solvent_name', solvent_lists[:])  # Solvents you want radial distribution functions for
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'bin_width', 0.02)                 # Bin width of the radial distribution function
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'cutoff_radius', 2.00)             # Radius of cutoff for the RDF (OPTIONAL)
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_oxy_pref_int_coeff', True)   # True if you want oxygen rdfs
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'traj_num_split', 2)               # Number of times to split the trajectory
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'job_type', ['pref_int','excess_coord'])               # Job types that you want
                
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
        
        ####################################
        ### ACCESSIBLE HYDROXYL FRACTION ###
        ####################################
        if want_accessible_hydroxyl_frac is True:
            from MDDescriptors.geometry.accessible_hydroxyl_fraction import calc_accessible_hydroxyl_fraction
            ## APPENDING THE CLASS
            Descriptor_Class_List.append(calc_accessible_hydroxyl_fraction)
            ## DESCRIPTOR INPUTS
            Descriptor_Inputs = {
                                    'ACE':
                                        {
                                            'Solute'        : 'ACE',           # Solute of interest
                                            'solute_itp_file': 'acetone.itp',   # ITP File for solute
                                        },
                                    'PRO':
                                        {
                                            'Solute'        : 'PRO',           # Solute of interest
                                            'solute_itp_file': 'propanal.itp',   # ITP File for solute
                                        },
                                    'PDO':
                                        {
                                            'Solute'        : 'PDO',           # Solute of interest
                                            'solute_itp_file': '12-propanediol.itp',   # ITP File for solute
                                        },
                                    'GLY':
                                        {
                                            'Solute'        : 'GLY',           # Solute of interest
                                            'solute_itp_file': 'glycerol.itp',   # ITP File for solute
                                        },
                                    'HBU':
                                        {
                                            'Solute'        : 'HBU',           # Solute of interest
                                            'solute_itp_file': '1-hydroxy-2-butanone.itp',   # ITP File for solute
                                        },
                                    'FRU':
                                        {
                                            'Solute'        : 'FRU',           # Solute of interest
                                            'solute_itp_file': 'fructose.itp',   # ITP File for solute
                                        },
                                    'FRI':
                                        {
                                            'Solute'        : 'FRI',           # Solute of interest
                                            'solute_itp_file': 'fru-int-2.itp',   # ITP File for solute
                                        },
                                    'HMF':
                                        {
                                            'Solute'        : 'HMF',           # Solute of interest
                                            'solute_itp_file': '5-hydroxymethylfurfural.itp',   # ITP File for solute
                                        },
                                                
                                    }
            ## INCLUDING TO ALL INPUTS
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'SASA_type', 'alcohol2allSASA') # Type of SASA you want
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'probe_radius', 0.14)          # Probe radius in nm (size of the probe, by default 0.14 nm for the VDW of water)
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'num_sphere_pts', 960)         # Number of sphere points, the higher, the more accurate
            
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
                
            
        ################################
        ### PROBABILITY DENSITY MAPS ###
        ################################
        if want_prob_density_map is True:
            # FOR PROBABILITY DISTRIBUTION FUNCTIONS
            ### IMPORTING CLASS TO ANALYZE TRAJECTORIES
            from MDDescriptors.visualization.probability_density_map import calc_prob_density_map
        
            ### DEFINING THE FUNCTION AND INPUT PARAMETERS
            Descriptor_Class_List.append(calc_prob_density_map)
            
            ### DEFINING DESCRIPTOR INPUTS -- NOTE THAT THEY SHOULD CORRESPOND WITH CATEGORY NAMES
            Descriptor_Inputs = {
                                        'ACE':
                                            {
                                                'Solute'        : 'ACE',           # Solute of interest
                                                'solute_itp_file': 'acetone.itp',   # ITP File for solute
                                            },
                                        'CHD':
                                            {
                                                'Solute'        : 'CHD',           # Solute of interest
                                                'solute_itp_file': 'cis-12-cyclohexanediol.itp',   # ITP File for solute
                                            },
                                        'THD':
                                            {
                                                'Solute'        : 'THD',           # Solute of interest
                                                'solute_itp_file': 'trans-12-cyclohexanediol.itp',   # ITP File for solute
                                            },
                                        'PRO':
                                            {
                                                'Solute'        : 'PRO',           # Solute of interest
                                                'solute_itp_file': 'propanal.itp',   # ITP File for solute
                                            },
                                        'PDO':
                                            {
                                                'Solute'        : 'PDO',           # Solute of interest
                                                'solute_itp_file': '12-propanediol.itp',   # ITP File for solute
                                            },
                                        'GLY':
                                            {
                                                'Solute'        : 'GLY',           # Solute of interest
                                                'solute_itp_file': 'glycerol.itp',   # ITP File for solute
                                            },
                                        'BTL':
                                            {
                                                'Solute'        : 'BTL',           # Solute of interest
                                                'solute_itp_file': '1butanol.itp',   # ITP File for solute
                                            },                                            
                                        'DIM':
                                            {
                                                'Solute'        : 'DIM',           # Solute of interest
                                                'solute_itp_file': '22-dimethyl-13-dioxan-5-ol.itp',   # ITP File for solute
                                            },
                                        'HBU':
                                            {
                                                'Solute'        : 'HBU',           # Solute of interest
                                                'solute_itp_file': '1-hydroxy-2-butanone.itp',   # ITP File for solute
                                            },
                                        'FRU':
                                            {
                                                'Solute'        : 'FRU',           # Solute of interest
                                                'solute_itp_file': 'fructose.itp',   # ITP File for solute
                                            },
                                        'HMF':
                                            {
                                                'Solute'        : 'HMF',           # Solute of interest
                                                'solute_itp_file': '5-hydroxymethylfurfural.itp',   # ITP File for solute
                                            },
                                    }
            ### INCLUDING OTHER INPUTS
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'Solvent', solvent_lists[:])  # Solvents you want radial distribution functions for
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'map_box_size', 3.0)  # nm box length in all three dimensions
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'map_box_increment', 0.1)  # box cell increments
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'map_type', 'allatom')  # box cell increments
            
            ## INCLUDING INPUTS THAT ARE CUSTOM TO MULTITRAJ
            # Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'xtc_file', 'mixed_solv_prod_10_ns_center_rot_trans_center.xtc')  # Solvents you want radial distribution functions for
            
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
            
        ######################################
        ### GENERAL TRAJECTORY INFORMATION ###
        ######################################
        if want_general_traj is True:
            # FOR PROBABILITY DISTRIBUTION FUNCTIONS
            ### IMPORTING CLASS TO ANALYZE TRAJECTORIES
            from MDDescriptors.core.general_traj_info import general_traj_info
        
            ### DEFINING THE FUNCTION AND INPUT PARAMETERS
            Descriptor_Class_List.append(general_traj_info)
            
            ### DEFINING DESCRIPTOR INPUTS -- NOTE THAT THEY SHOULD CORRESPOND WITH CATEGORY NAMES
            ## EMPTY CATGORIES
            Descriptor_Inputs = {}
            for each_category in Categories:
                Descriptor_Inputs[each_category] = {}
            
            ### INCLUDING OTHER INPUTS
            # Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'Solvent', solvent_lists[:])  # Solvents you want radial distribution functions for
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
        
        ########################
        ### HYDROGEN BONDING ###
        ########################
        if want_h_bonds is True:
            ### IMPORTING CLASS TO ANALYZE TRAJECTORIES
            from MDDescriptors.application.solvent_effects.h_bond import calc_hbond
            
            ### DEFINING THE FUNCTION AND INPUT PARAMETERS
            Descriptor_Class_List.append(calc_hbond)
            
            ### DEFINING DESCRIPTOR INPUTS -- NOTE THAT THEY SHOULD CORRESPOND WITH CATEGORY NAMES
            # Descriptor_Inputs = { each_cat: {'solute_name' : each_cat} for each_cat in Categories}

            Descriptor_Inputs = {
                            'ACE':
                                {
                                    'solute_name'        : 'ACE',           # Solute of interest
                                },
                            'PRO':
                                {
                                    'solute_name'        : 'PRO',           # Solute of interest
                                },
                            'PDO':
                                {
                                    'solute_name'        : 'PDO',           # Solute of interest
                                },
                            'CHD':
                                {
                                    'solute_name'        : 'CHD',           # Solute of interest
                                },
                            'THD':
                                {
                                    'solute_name'        : 'THD',           # Solute of interest
                                },
                        }
            ### INCLUDING OTHER INPUTS
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'solvent_name', solvent_lists[:])  # Solvents
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_serial', True) # Desired solute donor breakdown                
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'want_solute_acceptor_donor_info', True) # Desired solute donor breakdown                
    
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
            
            
        ############################
        ### DIELECTRIC CONSTANTS ###
        ############################
        if want_extract_gmx_dipoles is True:
            ### IMPORTING CLASS TO ANALYZE TRAJECTORIES
            from MDDescriptors.application.solvent_effects.extract_gmx_dipoles import analyze_gmx_dipoles
            
            ### DEFINING THE FUNCTION AND INPUT PARAMETERS
            Descriptor_Class_List.append(analyze_gmx_dipoles)
            
            ### DEFINING DESCRIPTOR INPUTS -- NOTE THAT THEY SHOULD CORRESPOND WITH CATEGORY NAMES
            Descriptor_Inputs = {
                                        'NoSolute':
                                            {                                        },
                                    }
            ### INCLUDING OTHER INPUTS
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'summary_file', "gmx_dipole_summary.txt")  # Solvents
    
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
            
        #############################
        ### POTENTIAL AND DENSITY ###
        #############################
        if want_extract_gmx_potential_density is True:
            ### IMPORTING CLASS TO ANALYZE TRAJECTORIES
            from MDDescriptors.application.solvent_effects.extract_gmx_potential_density import analyze_gmx_potential_density
            
            ### DEFINING THE FUNCTION AND INPUT PARAMETERS
            Descriptor_Class_List.append(analyze_gmx_potential_density)
            
            ### DEFINING DESCRIPTOR INPUTS -- NOTE THAT THEY SHOULD CORRESPOND WITH CATEGORY NAMES
            Descriptor_Inputs = {
                                        'NoSolute':
                                            {                                        },
                                    }
            ### INCLUDING OTHER INPUTS
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'xvg_files', ['density.xvg', 'potential.xvg'])  # Solvents
            Descriptor_Inputs = add_to_all_dicts_of_dicts(Descriptor_Inputs, 'xvg_definitions', ["density.xvg", "potential.xvg"])  # Solvents
    
            ## APPENDING THE INPUTS
            Descriptor_Inputs_List.append(Descriptor_Inputs)
            
        #%%
        ### RUNNING MULTI TRAJ FUNCTION
        multi_traj = multi_traj_analysis(Main_Directory_Parent_Path = Main_Directory_Parent_Path, 
                                         Main_Directory = Main_Directory, 
                                         Files = Files,
                                         Categories = Categories, 
                                         Descriptor_Class = Descriptor_Class_List, 
                                         Descriptor_Inputs = Descriptor_Inputs_List, 
                                         Specific_dir_within_category = Specific_dir_within_category, 
                                         want_only_directories = want_only_directories,
                                         date_suffix ='_' + each_key)
    

        