#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J COMBINED
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
gro_file=water_box_min
input_top=combined.top
# output_prefix=mixed_solv

output_prefix=water_box_min

# minimize
# gmx grompp -f minim_LIQ.mdp -c ${gro_file} -p ${input_top} -o ${output_prefix}_em -maxwarn 1
# gmx mdrun -v -nt 28 -deffnm ${output_prefix}_em

# run now
gmx grompp -f equil_LIQ.mdp -c ${output_prefix}.gro -p ${input_top} -o ${output_prefix}_equil
gmx mdrun -v -nt 28 -deffnm ${output_prefix}_equil

# md production
# gmx grompp -f production_LIQ.mdp -c ${output_prefix}_equil.gro -p ${input_top} -o ${output_prefix}_prod -maxwarn 1
# gmx mdrun -v -nt 28 -deffnm ${output_prefix}_prod