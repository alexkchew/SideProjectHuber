
; RUN CONTROL PARAMETERS
integrator               = md
dt                       = 0.002
nsteps                   = 250000 ; 500 ps; seems to be plenty
; mode for center of mass motion removal
comm-mode                = Linear
; number of steps for center of mass motion removal
nstcomm                  = 1
refcoord_scaling	 = com

; OUTPUT CONTROL OPTIONS
; Output frequency for coords (x), velocities (v) and forces (f)
nstxout                  = 0
nstvout                  = 0
nstfout                  = 0
; Checkpointing helps you continue after crashes
nstcheckpoint            = 1000
; Output frequency for energies to log file and energy file
; Output frequency and precision for xtc file
xtc-precision            = 1000

; NEIGHBORSEARCHING PARAMETERS
; nblist update frequency
; ns algorithm (simple or grid)
ns-type                  = grid
; Periodic boundary conditions: xyz (default), no (vacuum)
; or full (infinite systems only)
pbc                      = xyz
; nblist cut-off
rlist                    = 1.2

; OPTIONS FOR ELECTROSTATICS AND VDW
; Method for doing electrostatics
coulombtype              = PME
rcoulomb-switch          = 0
rcoulomb                 = 1.2
; Relative dielectric constant for the medium and the reaction field
epsilon_r                = 1
epsilon_rf               = 1
; Method for doing Van der Waals
vdw-type                 = Cut-off
vdw-modifier		 = Force-switch
; cut-off lengths
rvdw-switch              = 1.0 ; according to H3O+
rvdw                     = 1.2 ; 

; Turning on soft corep otentials
sc-sigma=0.3
sc-power=1
sc-alpha=0.5

; Apply long range dispersion corrections for Energy and Pressure
DispCorr                 = EnerPres
; Spacing for the PME/PPPM FFT grid
fourierspacing           = 0.12
; EWALD/PME/PPPM parameters
pme_order                = 4

; OPTIONS FOR WEAK COUPLING ALGORITHMS
; Temperature coupling
; Groups to couple separately
tc-grps                  = system
; Time constant (ps) and reference temperature (K)
; Pressure coupling
; Time constant (ps), compressibility (1/bar) and reference P (bar)
compressibility          = 5e-5
ref-p                    = 1.0 ; regular pressure for initial equil
tau-p			 = 5.0 ; 5 ps to avoid pressure scaling from oscillating during equil

; GENERATE VELOCITIES FOR STARTUP RUN
gen-vel                  = yes
gen-temp                 = 363.15
gen-seed                 = -1

constraint-algorithm = SHAKE
constraints          = all-bonds
lincs-order          = 4
shake-tol            = 1e-5
lincs-iter           = 2
lincs-warnangle      = 30
cutoff-scheme        = Verlet
verlet-buffer-tolerance = 0.0001
tcoupl               = Berendsen
ref-t                = 363.15
tau-t                = 1
nsttcouple           = 1
nstpcouple           = 1
pcoupl               = Berendsen
nstlist              = 20
nstxtcout            = 5000
nstcalcenergy        = 1
