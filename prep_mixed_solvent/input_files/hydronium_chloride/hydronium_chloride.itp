; itp file for hydronium chloride ions
; Parameters given by Netz and others in reference below:
; Bonthuis, D. J., Mamatkulov, S. I. & Netz, R. R. Optimization of classical nonpolarizable force fields for OH- and H3O+. J. Chem. Phys. 144, (2016).

; Defining atom types -- must be before any moleculetype
[ atomtypes ]
; name  at.num    mass    charge   ptype          sigma(nm)      epsilon (kJ/mol)
CL_L   17          35.450000    -1   A   0.452    0.4186; Chlorine ion
HYDO   8          9.951400    -1.4   A   0.31     0.8; Hydronium Oxygen
HYDH   1          4.032000    0.8   A   0.0         0.0; Hydronium Hydrogen

; ----- Chloride ion -----
[ moleculetype ]
; molname   nrexcl <-- Exclude n bonds away (should be 2)
CLL     1

[ atoms ]
;   nr   type  resnr residue  atom   cgnr     charge       mass
     1     CL_L   1    CLL    CL      1      -1

; ----- Hydronium ion -----
[ moleculetype ]
; molname   nrexcl <-- Exclude n bonds away (should be 2)
HYD     2

[ atoms ]
;   nr   type  resnr residue  atom   cgnr     charge       mass
     1     HYDO   1    HYD    O1      1      -1.4
     2     HYDH   1    HYD    H1      1       0.8
     3     HYDH   1    HYD    H2      1       0.8
     4     HYDH   1    HYD    H3      1       0.8
 
#ifndef WANT_HYDRONIUM_CONSTRAINTS
; Setting constraints according to reference -> Needs SHAKE later, which resplaces the bond constraints
[ bonds ] 
; i j   funct   length  force.c.
 1   2   1   0.098 345000  0.1     345000
 1   3   1   0.098 345000  0.1     345000
 1   4   1   0.098 345000  0.1     345000
 2   3   1   0.1619 345000  0.1     345000
 3   4   1   0.1619 345000  0.1     345000
 2   4   1   0.1619 345000  0.1     345000
 

#else
; Alternative constraints, activate by adding #define WANT_HYDRONIUM_CONSTRAINTS in mdp file
[ constraints ] 
; i j   type (1 or 2)   length  force.c.
1   2   1   0.098
1   3   1   0.098
1   4   1   0.098
2   3   1   0.1619
3   4   1   0.1619
2   4   1   0.1619
 
#endif

; Adding exclusions, probably unnecessary. Adding them just incase 
[ exclusions ]
 1   2   3   4
 2   1   3   4
 3   1   2   4
 4   1   2   3
