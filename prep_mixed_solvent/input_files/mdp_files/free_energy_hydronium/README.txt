README.txt

Thie folder contains files used for hydronium chloride ion -- which requires the SHAKE algorithm to be implemented correctly. 


Hydronium Ion Constraints

From Paper:

Bonthuis, D. J., Mamatkulov, S. I. & Netz, R. R. Optimization of classical nonpolarizable force fields for OH- and H3O+. J. Chem. Phys. 144, (2016).


MDP Options: http://manual.gromacs.org/online/mdp_opt.html#vdw

Simple rigid water model SPC/E
Simple LJ Cutoff: 10 Angstroms (1.0 nm) with shifted cutoff scheme [ vdw-modifier = Force-switch ]
	Update: [ vdw-modifier = Potential-shift ] -- which shifts the VDW potential by a constant so that it is zero at the cutoff
	rvdw = 1.0 <-- cutoff at 1 nm
	////
	
	Let's stick with force-switch at rvdw = 1.0 and rvdw-switch = 1.2??? no
PME Coulomb Cutoff 1.2 nm, cubic interpolations, and grid spacing of 0.12 nm 
	[ coulombtype=PME ]
	[ fourierspacing =0.12 nm ]
Temperature: 300 K [ Berendsen Thermostat ] --- For RDFs [ Nose-Hoover Coupling ]
Pressure: 1 Bar [ Berendsen Barostat ] --- For RDFs [ Parrinello-Rahman ]

All distances within ions are constrained using SHAKE algorithm  (Not used with energy minimization)
	[ constraint-algorithm=SHAKE ]
	[ shake-tol=0.0001 ]
	
All distances within water molecules are constrained using SETTLE algorithm

Used a soft-core interaction potentials for LJ integration
	Soft core sigma for particles: 0.3 nm [ sc-sigma=0.3 ]
	Soft core power of lambda = 1 [ sc-power=1 ]
	Soft core alpha value: 0.5 [ sc-alpha=0.5 ]


Simulation boxes are equilibrated for at least 2 ns, data collected every 0.2 ns for 5 ns at every value alpha

ITP File:

H-H bond constrainted to 0.1619 nm
O-H bond length set to 0.98 angstroms (0.098 nm)
Equivalent to angle of 111.4 degrees

NOTES:
- These MDP files ran successfully for DMSO, DIO, ACE, and other cosolvents
- Energy minimization is lowered to prevent LINCS error from blowing up the hydronium ion
- Hydronium ion transfers are ~3 times slower than normal transfers, since hydronium ion requires the SHAKE constraint algorithm


