; md.mdp
; MD Simulation Portion (Berendsen barostat)
; Uses SHAKE algorithm for the ions
; Parameters based on Bonthuis, D. J., Mamatkulov, S. I. & Netz, R. R. Optimization of classical nonpolarizable force fields for OH- and H3O+. J. Chem. Phys. 144, (2016)


; RUN CONTROL PARAMETERS
integrator               = sd       ; Langevin dynamics
tinit                    = 0
dt                       = 0.002
nsteps                   = 2500000   ;  5 ns

; CENTER OF MASS REMOVAL
; mode for center of mass motion removal
comm-mode                = Linear    ; center of mass removal mode (removal of translation)
nstcomm                  = 100       ; frequency for center of mass removal
refcoord_scaling	 = com           ; scaling of reference foordinate using a pressure coupling matrix

; OUTPUT CONTROL OPTIONS
; Output frequency for coords (x), velocities (v) and forces (f)
nstxout                  = 0
nstvout                  = 0
nstfout                  = 0
nstxout-compressed       = 10000 ; output every 20 ps

; NEIGHBORSEARCHING PARAMETERS
cutoff-scheme            = verlet
nstlist                  = 20
ns_type                  = grid
pbc                      = xyz
rlist                    = 1.2

; OPTIONS FOR ELECTROSTATICS AND VDW
; ELECTROSTATICS
coulombtype              = PME
rcoulomb                 = 1.2
; Spacing for the PME/PPPM FFT grid
fourierspacing           = 0.12
; PME parameters
pme_order                = 4

; VAN DER WAALS
vdwtype             = Cut-off
vdw-modifier        = Force-switch
rvdw-switch         = 1.0 ; Adding nonlinear function to switch force to zero
rvdw                = 1.2 ; Cutoff at 1.0 according to article
DispCorr            = EnerPres ; Apply long range dispersion corrections for Energy and Pressure

; SOFT CORE POTENTIALS (For stable LJ interactions in FE calculations)
sc-alpha                 = 0.5
sc-coul                  = no       ; linear interpolation of Coulomb (none in this case)
sc-power                 = 1
sc-sigma                 = 0.3

; PRESSURE AND TEMPERATURE COUPLING
; Temperature coupling
; tcoupl is implicitly handled by the sd integrator
tc_grps                  = system
tau_t                    = 1.0
ref_t                    = TEMPERATURE
; Pressure coupling is on for NPT
Pcoupl                   = Parrinello-Rahman 
tau-p			         = 5 ; still weakly coupled to avoid blowing up system
compressibility          = 5e-5
ref_p                    = 1.0 

; FREE ENERGY CALCULATION PARAMETERS
free_energy              = yes
init_lambda_state        = INITIAL_LAMBDA_STATE
delta_lambda             = 0
calc_lambda_neighbors    = -1        ; -1 used for MBAR

; -- LAMBDA SPECIFICATIONS -- 
; Vectors of lambda specified here
; Each combination is an index that is retrieved from init_lambda_state for each simulation
; lambda state:            0    1       2       3       4        5       6       7       8       9       10      11      12      13   14   15   16   17
vdw_lambdas              = 1.00 1.00 1.00 1.00 1.00 0.99078 0.95206 0.88495 0.79366 0.68392 0.56262 0.43738 0.31608 0.260634 0.11505 0.04794 0.00922 0.00
; lambda state:            0    1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17
coul_lambdas             = 1.00 0.75 0.50 0.25 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
; We are not transforming any bonded or restrained interactions
; lambda state:            0    1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   
bonded_lambdas           = 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
restraint_lambdas        = 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
; Masses are not changing (particle identities are the same at lambda = 0 and lambda = 1)
mass_lambdas             = 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
; Not doing simulated temperature here
temperature_lambdas      = 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00

; -- FREE ENERGY OPTIONS --
couple-moltype           = MOLECULETYPE  ; name of moleculetype to decouple
couple-lambda0           = vdw-q     ; van der Waals and coulombic charge
couple-lambda1           = none      ; No interactions
couple-intramol          = no
nstdhdl                  = 100

; Do not generate velocities
gen_vel                  = no 
; since we are continuing from NPT
continuation             = yes 

; CONTRAINT ALGORITHMS
; Bond options
constraints              = all-bonds

; LINCS
constraint-algorithm     = lincs ; SHAKE (previously)
lincs-order              =     4 ; default: order of expansion in the constraint coupling matrix
lincs-iter               =     1 ; default: number of iterations to correct for rotational lengthening in lincs
lincs-warnangle          =    30 ; default: maximum angles a bond can rotate before LINCS will complain

; Shake tolerance
shake-tol            = 1e-5