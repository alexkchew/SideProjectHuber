# DESCRIPTION
This folder contains all mdp files for simulating mixed-solvent environments.

# FILES
## EQUILIBRATION
- equil_cosolvent_mixing.mdp: MDP file for equilibration cosolvent mixing
- equil_LIQ: MDP file for NPT equilibration before production
## HYDRONIUM CHLORIDE
- hydronium_chloride_equil.mdp: MDP file for equilibration in hydronium chloride runs
- hydronium_chloride_prod.mdp: MDP file for production in hydronium chloride runs
## ENERGY MINIMIZATION
- minim_LIQ.mdp: MDP file for energy minimization
## PRODUCTION RUNS
- production_LIQ.mdp: MDP file for production runs in mixed-solvent environments
- production_LIQ_extended.mdp: MDP file for extension of production runs -- useful for calculations of hydrogen bonding lifetimes
- production_LIQ_NVT.mdp: MDP file for NVT production runs in mixed-solvent environments

# FOLDERS
- free_energy: mdp files for general free energy perturbation simulations
- free_energy_hydronium: mdp files for hydronium ion free energy perturbation simulations
- free_energy_hydronium_LINCS: mdp files for hydronium ion with LINCS
- umbrella_sampling_hyd: mdp files for umbrella sampling simulations with hydronium ion transfer
