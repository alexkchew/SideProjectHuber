#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J DIRECTORY
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# submit_prod_extend.sh
# This script simply extends the production run

# LOADING GROMACS #
load_swarm_gromacs_2016

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
output_prefix="mixed_solv_prod"

# NPT Equilibration
gmx mdrun -v -nt 28 -deffnm ${output_prefix}
