#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J sideProject
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
gro_file=mixed_solv.gro
input_top=mixed_solv.top
output_prefix=mixed_solv

# Defining mdp scripts
em_mdp=EM_MDP
equil_mdp=EQUIL_MDP
prod_mdp=PROD_MDP

## DEFINING NUMBER OF NODES
num_cores=28

#########################
#### ENERGY MINIMIZE ####
#########################
if [ ! -e "${output_prefix}_em.gro" ]; then
	gmx grompp -f ${em_mdp} -c ${gro_file} -p ${input_top} -o ${output_prefix}_em -maxwarn 1
	gmx mdrun -v -nt "${num_cores}" -deffnm ${output_prefix}_em
else
	echo "Energy minimization is complete!"
fi


###########################
#### EQUILIBRATION RUN ####
###########################

if [ ! -e "${output_prefix}_equil.gro" ]; then
	gmx grompp -f ${equil_mdp} -c ${output_prefix}_em -p ${input_top} -o ${output_prefix}_equil -maxwarn 1
	gmx mdrun -v -nt "${num_cores}" -deffnm ${output_prefix}_equil
else
	echo "Initial equilibration is complete!"

fi

########################
#### PRODUCTION RUN ####
########################

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}_prod"

## CHECKING TPR FILE
if [ ! -e "${sim_prefix}.tpr" ]; then
	## GETTING MD PRODUCTION TPR
	gmx grompp -f ${prod_mdp} -c ${output_prefix}_equil.gro -p ${input_top} -o ${sim_prefix} -maxwarn 1
fi

## DEFINING TOTAL PRODUCTION TIME
total_prod_time=""

## CONVERTING TPR
if [ ! -z "${total_prod_time}" ]; then

	## CONVERTING TPR
	gmx convert-tpr -s "${sim_prefix}.tpr" \
	                -until "${total_prod_time}" \
	                -o "${sim_prefix}.tpr"

fi

## RUNNING PRODUCTION FILE
if [ ! -e "${sim_prefix}.gro" ]; then
	gmx mdrun -v -nt "${num_cores}" -deffnm "${output_prefix}_prod"
else
	## RESTART
	gmx mdrun -v \
		-nt "${num_cores}" \
        -s "${sim_prefix}.tpr" \
        -cpi "${sim_prefix}.cpt" \
        -append \
        -deffnm "${sim_prefix}" \

fi

echo "Simulations are complete!"