#!/bin/bash
# submit_free_energy_per_lambda.sh
# The purpose of this script is to go through each lambda and do the following:
# 1. Create a directory
# 2. Run energy minimization (2 steps), NVT, NPT, then production

# USAGE: bash submit_free_energy_per_lambda.sh NUM_OF_CORES

### INPUTS:
# $1: Number of threads

### AUTHORS:
#   Alex K. Chew (alexkchew@gmail.com)

############################
### FUNCTION DEFINITIONS ###
############################

## FUNCTION TO CHECK IF THE FILE EXISTS
# This function simply checks if a file exists and outputs true/false
# $1: Full path to file
# USAGE: check_file_exist $FILE
function check_file_exist () {
    if [ -e "$1" ]
    then
        echo "True"
    else
        echo "False"
    fi
}

###################
### MAIN SCRIPT ###
###################

# INITIAL VARIABLES #
num_of_threads="$1" # <-- NUMBER OF THREADS FOR THE JOB
current_lambda="LAMBDA_VALUE" # <-- Variable

## DIRECTORY STRUCTURE ##
Free_Energy_Dir="$(pwd)" # point of workign directory
Molecule_Dir="${Free_Energy_Dir}/1_Molecule"
MDP_Dir="$Free_Energy_Dir/2_MDP"
Lambda_Dir="${Free_Energy_Dir}/3_lambda"
# Defining prefix for gro / top files
output_prefix="mixed_solv" # Prefix for gro and top files

# Defining MDP files
mdp_em_1="$MDP_Dir/1_EM/em_steep_${current_lambda}.mdp"
# mdp_em_2="$MDP_Dir/2_EM/em_l-bfgs_${current_lambda}.mdp"
mdp_nvt="$MDP_Dir/2_NVT/nvt_${current_lambda}.mdp"
mdp_npt="$MDP_Dir/3_NPT/npt_${current_lambda}.mdp"
mdp_md="$MDP_Dir/4_MD/md_${current_lambda}.mdp"

# Defining current lambda directory
currentLambdaDir=${Lambda_Dir}/Lambda_$current_lambda

## Defining directory tree ##
em_1_dir="${currentLambdaDir}/1_EM"
# em_2_dir="${currentLambdaDir}/2_EM"
nvt_dir="${currentLambdaDir}/2_NVT"
npt_dir="${currentLambdaDir}/3_NPT"
md_dir="${currentLambdaDir}/4_MD"

# Defining GRO file
if [ "$current_lambda" -eq "0" ]; then
    # Start from beginning!
    input_gro=${Molecule_Dir}/${output_prefix}.gro
    else # No longer lambda = 0
    prev_lambda=$(( $current_lambda-1 ))
    input_gro="${Lambda_Dir}/Lambda_${prev_lambda}/4_MD/md${prev_lambda}.gro"
    
    ## CHECKING IF INPUT GRO EXISTS, IF NOT, STOP HERE
    check_input_gro=$(check_file_exist "${input_gro}")
    if [ "${check_input_gro}" == "False" ]; then
        echo "ERROR, input gro is not available, check: ${input_gro}"
        echo "Exiting now..."
        exit
    fi
    
fi

input_top=${Molecule_Dir}/${output_prefix}.top

echo "Free energy home directory set to ${Free_Energy_Dir}"
echo ".mdp files are stored in ${MDP_Dir}"

# Making directory
mkdir -p ${currentLambdaDir}

# Going to directory
cd ${currentLambdaDir}

# Creating directory tree
mkdir -p ${em_1_dir} ${nvt_dir} ${npt_dir} ${md_dir} # ${em_2_dir}

#################################
# ENERGY MINIMIZATION 1: STEEP  #
#################################
echo "-------------------------------------------"
echo "Starting minimization for lambda = ${current_lambda}..." 

# Going into directory
cd ${em_1_dir}

## CHECKING IF WE FINISHED THIS
min_check=$(check_file_exist "min${current_lambda}.gro")

if [ "${min_check}" == "False" ]; then
    # Iterative calls to grompp and mdrun to run the simulations
    gmx grompp -f ${mdp_em_1} -c ${input_gro} -p ${input_top} -o min${current_lambda}.tpr
    gmx mdrun -nt 1 -deffnm min${current_lambda}
fi

#################################
# ENERGY MINIMIZATION 2: L-BFGS #
#################################
#echo "-------------------------------------------"
#echo "Starting L-BFGS energy minimization..."
#
#
## Going into directory
#cd ${em_2_dir}

# We use -maxwarn 1 here because grompp incorrectly complains about use of a plain cutoff; this is a minor issue
# that will be fixed in a future version of Gromacs
# gmx grompp -f $mdp_em_2 -c ${em_1_dir}/min${current_lambda}.gro -p $input_top -o min${current_lambda}.tpr -maxwarn 1

# Run L-BFGS in serial (cannot be run in parallel)
#
#gmx mdrun -nt 1 -deffnm min${current_lambda}
#
#echo "Minimization complete."

#####################
# NVT EQUILIBRATION #
#####################
echo "Starting constant volume equilibration..."

# Going to directory
cd ${nvt_dir}

## CHECKING IF WE FINISHED THIS
nvt_check=$(check_file_exist "nvt${current_lambda}.gro")

if [ "${nvt_check}" == "False" ]; then
    gmx grompp -f $mdp_nvt -c ${em_1_dir}/min${current_lambda}.gro -p $input_top -o nvt${current_lambda}.tpr
    gmx mdrun -nt "${num_of_threads}" -deffnm nvt${current_lambda}
fi


echo "NVT equilibration complete."

#####################
# NPT EQUILIBRATION #
#####################
echo "-------------------------------------------"
echo "Starting constant pressure equilibration..."

# Going to directory
cd ${npt_dir}

## CHECKING IF WE FINISHED THIS
npt_check=$(check_file_exist "npt${current_lambda}.gro")

if [ "${npt_check}" == "False" ]; then
    gmx grompp -f ${mdp_npt} -c ${nvt_dir}/nvt${current_lambda}.gro -p $input_top -t ${nvt_dir}/nvt${current_lambda}.cpt -o npt${current_lambda}.tpr
    gmx mdrun -nt "${num_of_threads}" -deffnm npt${current_lambda}
fi

echo "NPT equilibration complete."

#################
# PRODUCTION MD #
#################
echo "-------------------------------------------"
echo "Starting production MD simulation..."

# Going to directory
cd ${md_dir}

## CHECKING IF WE FINISHED THIS
md_check=$(check_file_exist "md${current_lambda}.gro")

if [ "${md_check}" == "False" ]; then
    gmx grompp -f ${mdp_md} -c ${npt_dir}/npt${current_lambda}.gro -p $input_top -t ${npt_dir}/npt${current_lambda}.cpt -o md${current_lambda}.tpr
    gmx mdrun -nt "${num_of_threads}" -deffnm md${current_lambda}
fi

echo "Production MD complete."

## PRINTING SUMMARY
echo "Ending. Job completed for lambda = ${current_lambda}"
