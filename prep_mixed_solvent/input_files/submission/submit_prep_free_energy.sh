#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J DIRECTORY
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# LOADING GROMACS #
load_swarm_gromacs_2016

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
gro_file=mixed_solv_em.gro
input_top=mixed_solv.top
output_prefix=mixed_solv
equil_mdp_file_name="equil_cosolvent_mixing.mdp"

# NPT Equilibration
gmx grompp -f ${equil_mdp_file_name} -c ${gro_file} -p ${input_top} -o ${output_prefix}_equil -maxwarn 1
gmx mdrun -v -nt 28 -deffnm ${output_prefix}_equil
