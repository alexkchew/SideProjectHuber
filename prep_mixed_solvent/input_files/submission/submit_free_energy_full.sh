#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 1000:00:00
#SBATCH -J DIRECTORYNAME
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will terminate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
num_cores="NUMBER_OF_CORES" # number of cores you want to run with
LastAlpha="LASTALPHA"
ScriptDir="4_Submission_Files"

for currentJobIndex in $(seq 0 $LastAlpha); do
    # Echoing
    echo "---------------------------BEGIN---------------------------"
    echo "Running Alpha: ${currentJobIndex}"
    
    # Running each index
    bash ${ScriptDir}/submit_lambda_${currentJobIndex}.sh "${num_cores}"
done

echo "---------------------------END---------------------------"