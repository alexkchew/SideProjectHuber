# DESCRIPTION
This folder contains submission scripts used on the servers.

# FILES
## GENERAL SUBMISSION
- submit.sh: general submission file for *NPT* equilibration and production.
- submit_cosolvent.sh: submission file for cosolvents
- submit_prod_extend.sh: submission file for production extension simulations
- submit_expand_box.sh: submission file for NVT expanded box simulations based on "One_System_Prep_expand_box.sh"

## ANALYSIS SUBMISSIONS
- submit_compute_solvent_energies.sh: submission file for computing solvent energies using gmx energy and the "compute_solvent_energies.sh" script.

## FREE ENERGY PERTURBATIONS
- submit_free_energy_full.sh: submission file for free energy perturbation simulations (full script)
- submit_free_energy_full_split_windows.sh: submission file for free energy perturbation with ability to split across multiple cores
- submit_free_energy_per_lambda.sh: submission file for free energy perturbation in each lambda
- submit_free_energy_per_lambda_single_reference.sh: submission file for each lambda, with reference to the first initial conditions
- submit_prep_free_energy.sh: (**DEPRECIATED**) submission file for preparation of free energy
- submit_free_energy_per_lambda_single_reference_extended.sh: This script runs FEP calculations and extends if necessary. Should be the new generalized lambda script
- submit_free_energy_full_split_windows_extended: submission script for extended reference
