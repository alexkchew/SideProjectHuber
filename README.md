# Solvent effects project
In this project, we investigate the effects of solvent environment on the reaction rates of biomass-relevant molecules. We run molecular simulations of a single reactant or product in mixtures of organic solvents with water, then investigate solvation features that may lead to improvement of reactivity for acid-catalyzed reactions. 

# Generating cosolvent and reactant forcefield parameters with CGenFF
Force field parameters were generated with CGenFF/CHARMM36 all-atom forcefield. The workflow is:
- Generate reactant or solvent structures as a `*.mol2` file with Avogadro or find them in the Zinc Database (https://zinc.docking.org/)
- Generate a `.str` file from the [https://cgenff.umaryland.edu/](https://cgenff.umaryland.edu/) website. You may need an account to use this, so make sure to create one. On the website, click 'Upload Molecule' and choose the `*.mol2` file. Save the `*.mol2` and `.str` file in the same folder. 

This ReadMe is still in development, please refer to the Zenodo repository for details of the scripts: https://zenodo.org/record/3401701#.YEAHf5NKh-V


# References

The paper below is the original work that generated molecular dynamics simulations:
T. W. Walker*, A. K. Chew*, H. Li, B. Demir, Z. C. Zhang, G. W. Huber, R. C. Van Lehn, and J. A. Dumesic. “Universal kinetic solvent effects in acid-catalyzed reactions of biomass-derived oxygenates.” Energy & Environmental Science 2018 [[Link]](https://doi.org/10.1039/C7EE03432F) [[Zenodo]](https://zenodo.org/record/3401701#.YEAHf5NKh-V)

