# SIMULATION FOLDER
The purpose of this folder is to contain all simulation parameters. Here, we have all possible simulations, which can be used for subsequent analysis. This folder is empty because we cannot possibly upload all the simulation data into Git! So, the folder itself is a placeholder to ensure that the scripts run correctly. 

# FOLDERS
- 190107-HYD_CL_NMP_FEP_mixed_runs_stampede: contains hydronium and chlorine FEP calculations in aqueous mixtures of NMP. This will be run on STAMPEDE (pending)
- 190107-HYD_Normal_MD_Runs: Here, we run normal NPT runs with pure solvent systems (e.g. DMSO, etc.)
- 190107-HYD_temperature_dependent_runs_stampede: Here, we do hydronium ion FEP temperature dependent runs
- 190107-HYD_size_dependent_runs_swarm: Here, we do hydronium ion FEP calculations with 9 nm, and 12 nm

# COMPLETED SIMULATIONS
- COMPLETE_HYD_FEP: contains all completed hydronium ion FEP calculations
- COMPLETE_HYD_NORMAL_RUNS: contains all hydronium ion normal runs
